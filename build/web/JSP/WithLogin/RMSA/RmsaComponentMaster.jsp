<%-- 
    Document   : RmsaComponentMaster
    Created on : Jan 9, 2018, 1:19:45 PM
    Author     : 1259084
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String photo = null;
    if (request.getAttribute("RMSAChequeDetails") != null) {
        ArrayList list = (ArrayList) request.getAttribute("RMSAChequeDetails");
        for (int i = 0; i < list.size(); i++) {
            photo = "kpiinsert(ucsFileUpload" + i + ")";
        }
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script>

            function roundTo(n, digits) {
                var negative = false;
                if (digits === undefined) {
                    digits = 0;
                }
                if (n < 0) {
                    negative = true;
                    n = n * -1;
                }
                var multiplicator = Math.pow(10, digits);
                n = parseFloat((n * multiplicator).toFixed(11));
                n = (Math.round(n) / multiplicator).toFixed(2);
                if (negative) {
                    n = (n * -1).toFixed(2);
                }
                return n;
            }


            window.onload = function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);

            };

            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                $(document).bind("contextmenu", function(e) {
                    return false;
                });
                $(function() {
                    $(document).keydown(function(e) {
                        return (e.which || e.keyCode) !== 116;
                    });
                });
                if ($("#civilWorksReleased") !== null) {

                    $("#civilWorksSpent").autocomplete = "off";
                    $("#majorRepairsSpent").autocomplete = "off";
                    $("#minorRepairsSpent").autocomplete = "off";
                    $("#annualAndOtherSpent").autocomplete = "off";
                    $("#toiletsSpent").autocomplete = "off";
                    $("#selfDefenseGrantsSpent").autocomplete = "off";
                    //  document.getElementById("administrativeSanction").placeholder = "In Lakhs.";

                }
                $("#Search").button().click(function() {
                    var district = $('#district').val();
                    var mandal = $('#mandal').val();
                    var year = $('#year').val();
                    var village = $('#village').val();
                    var schoolName = $('#schoolName').val();
                    if (year === "0") {
                        alert("Select Year ");
                        $("#year").focus().css({'border': '1px solid red'});
                    }
                    else if (district === "0") {
                        alert("Select District Name");
                        $("#district").focus().css({'border': '1px solid red'});
                    }
                    else if (mandal === "0") {
                        alert("Select Mandal Name");
                        $("#mandal").focus().css({'border': '1px solid red'});
                    }
                    else if (village === "0") {
                        alert("Select Village Name");
                        $("#village").focus().css({'border': '1px solid red'});
                    }
                    else if (schoolName === "0") {
                        alert("Select School Name");
                        $("#schoolName").focus().css({'border': '1px solid red'});
                    }
                    else {
                        //var schoolName = $('#schoolName').val();
                        //var paraData = "schoolName=" + $.trim(schoolName);
                        document.forms[0].mode.value = "searchSchoolFunding";
                        document.forms[0].submit();
                    }
                });
                var input11 = document.querySelector('#civilWorksReleased');
                input11.addEventListener('input', function()
                {
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    var civilWorksReleased = input11.value;
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var toiletsReleased = $("#toiletsReleased").val();
                    if (toiletsReleased === "")
                        toiletsReleased = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var civilWorksAvailable = parseFloat(civilWorksReleased) - parseFloat(civilWorksSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsReleased);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(civilWorksAvailable)) {
                        $("#civilWorksAvailable").val(roundTo(civilWorksAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(totalCivilReleased);
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable,2));
                    } else {

                    }
                });
                var input12 = document.querySelector('#majorRepairsReleased');
                input12.addEventListener('input', function()
                {

                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    var majorRepairsReleased = input12.value;

                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var toiletsReleased = $("#toiletsReleased").val();
                    if (toiletsReleased === "")
                        toiletsReleased = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var majorRepairsAvailable = parseFloat(majorRepairsReleased) - parseFloat(majorRepairsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsReleased);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(majorRepairsAvailable)) {
                        $("#majorRepairsAvailable").val(roundTo(majorRepairsAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(totalCivilReleased);
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable,2));
                    } else {

                    }

                });
                var input13 = document.querySelector('#minorRepairsReleased');
                input13.addEventListener('input', function()
                {
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    var minorRepairsReleased = input13.value;
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var toiletsReleased = $("#toiletsReleased").val();
                    if (toiletsReleased === "")
                        toiletsReleased = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var minorRepairsAvailable = parseFloat(minorRepairsReleased) - parseFloat(minorRepairsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsReleased);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(minorRepairsAvailable)) {
                        $("#minorRepairsAvailable").val(roundTo(minorRepairsAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(totalCivilReleased);
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable,2));
                    } else {

                    }

                });

                var input14 = document.querySelector('#toiletsReleased');
                input14.addEventListener('input', function()
                {
                    var toiletsSpent = $("#toiletsSpent").val();
                    var toiletsReleased = input14.value;
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    if (toiletsReleased === "")
                        toiletsReleased = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsAvailable = parseFloat(toiletsReleased) - parseFloat(toiletsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsReleased);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(toiletsAvailable)) {
                        $("#toiletsAvailable").val(roundTo(toiletsAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(totalCivilReleased);
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable,2));
                    } else {

                    }

                });

                var input21 = document.querySelector('#waterElectricityTelephoneChargesReleased');
                input21.addEventListener('input', function()
                {
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    var waterElectricityTelephoneChargesReleased = input21.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";

                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";

                    var waterElectricityTelephoneChargesAvailable = parseFloat(waterElectricityTelephoneChargesReleased) - parseFloat(waterElectricityTelephoneChargesSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(waterElectricityTelephoneChargesAvailable)) {
                        $("#waterElectricityTelephoneChargesAvailable").val(roundTo(waterElectricityTelephoneChargesAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input22 = document.querySelector('#purchaseofBooksAndPeriodicalsAndNewsPapersReleased');
                input22.addEventListener('input', function()
                {
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = input22.value;
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";

                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";

                    var purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) - parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable)) {
                        $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").val(roundTo(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable,2));
                    } else {

                    }
                });

                var input23 = document.querySelector('#minorRepairsAnnualReleased');
                input23.addEventListener('input', function()
                {
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    var minorRepairsAnnualReleased = input23.value;
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";

                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";

                    var minorRepairsAnnualAvailable = parseFloat(minorRepairsAnnualReleased) - parseFloat(minorRepairsAnnualSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(minorRepairsAnnualAvailable)) {
                        $("#minorRepairsAnnualAvailable").val(roundTo(minorRepairsAnnualAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable,2));
                    } else {

                    }
                });

                var input24 = document.querySelector('#sanitationAndICTReleased');
                input24.addEventListener('input', function()
                {
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    var sanitationAndICTReleased = input24.value;
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";

                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";

                    var sanitationAndICTAvailable = parseFloat(sanitationAndICTReleased) - parseFloat(sanitationAndICTSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(sanitationAndICTAvailable)) {
                        $("#sanitationAndICTAvailable").val(roundTo(sanitationAndICTAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable,2));
                    } else {

                    }
                });

                var input25 = document.querySelector('#needBasedWorksReleased');
                input25.addEventListener('input', function()
                {
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    var needBasedWorksReleased = input25.value;
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";

                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";

                    var needBasedWorksAvailable = parseFloat(needBasedWorksReleased) - parseFloat(needBasedWorksSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(needBasedWorksAvailable)) {
                        $("#needBasedWorksAvailable").val(roundTo(needBasedWorksAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable,2));
                    } else {

                    }
                });

                var input26 = document.querySelector('#provisionalLaboratoryReleased');
                input26.addEventListener('input', function()
                {
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    var provisionalLaboratoryReleased = input26.value;
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";

                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";

                    var provisionalLaboratoryAvailable = parseFloat(provisionalLaboratoryReleased) - parseFloat(provisionalLaboratorySpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(provisionalLaboratoryAvailable)) {
                        $("#provisionalLaboratoryAvailable").val(roundTo(provisionalLaboratoryAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(totalAnnualReleased);
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable,2));
                    } else {

                    }
                });

                var input31 = document.querySelector('#furnitureReleased');
                input31.addEventListener('input', function()
                {
                    var furnitureSpent = $("#furnitureSpent").val();
                    var furnitureReleased = input31.value;
                    if (furnitureReleased === "")
                        furnitureReleased = "0.00";
                    var labEquipmentReleased = $("#labEquipmentReleased").val();
                    if (labEquipmentReleased === "")
                        labEquipmentReleased = "0.00";
                    var labEquipmentSpent = $("#labEquipmentSpent").val();
                    if (labEquipmentSpent === "")
                        labEquipmentSpent = "0.00";
                    var furnitureAvailable = parseFloat(furnitureReleased) - parseFloat(furnitureSpent);
                    var totalfurnitureAndLabReleased = parseFloat(furnitureReleased) + parseFloat(labEquipmentReleased);
                    var totalfurnitureAndLabSpent = parseFloat(furnitureSpent) + parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(furnitureAvailable)) {
                        $("#furnitureAvailable").val(roundTo(furnitureAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabReleased)) {
                        $("#totalfurnitureAndLabReleased").val(totalfurnitureAndLabReleased);
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable,2));
                    } else {

                    }
                });

                var input32 = document.querySelector('#labEquipmentReleased');
                input32.addEventListener('input', function()
                {
                    var labEquipmentSpent = $("#labEquipmentSpent").val();
                    var labEquipmentReleased = input32.value;
                    if (labEquipmentReleased === "")
                        labEquipmentReleased = "0.00";
                    var furnitureReleased = $("#furnitureReleased").val();
                    if (furnitureReleased === "")
                        furnitureReleased = "0.00";
                    var furnitureSpent = $("#furnitureSpent").val();
                    if (furnitureSpent === "")
                        furnitureSpent = "0.00";
                    var labEquipmentAvailable = parseFloat(labEquipmentReleased) - parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabReleased = parseFloat(furnitureReleased) + parseFloat(labEquipmentReleased);
                    var totalfurnitureAndLabSpent = parseFloat(furnitureSpent) + parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(labEquipmentAvailable)) {
                        $("#labEquipmentAvailable").val(roundTo(labEquipmentAvailable,2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabReleased)) {
                        $("#totalfurnitureAndLabReleased").val(totalfurnitureAndLabReleased);
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable,2));
                    } else {

                    }
                });

                var input41 = document.querySelector('#recurringExcursionRelease');
                input41.addEventListener('input', function()
                {
                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    var recurringExcursionRelease = input41.value;
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";
                    var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";
                    var recurringOtherRelease = $("#recurringOtherRelease").val();
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringExcursionAvailable = parseFloat(recurringExcursionRelease) - parseFloat(recurringExcursionSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringExcursionAvailable)) {
                        $("#recurringExcursionAvailable").val(roundTo(recurringExcursionAvailable,2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(recurringTotalRelease);
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable,2));
                    } else {

                    }
                });

                var input42 = document.querySelector('#recurringSelfDefenseRelease');
                input42.addEventListener('input', function()
                {
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    var recurringSelfDefenseRelease = input42.value;
                    var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";
                    var recurringOtherRelease = $("#recurringOtherRelease").val();
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";

                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";

                    var recurringSelfDefenseAvailable = parseFloat(recurringSelfDefenseRelease) - parseFloat(recurringSelfDefenseSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringSelfDefenseAvailable)) {
                        $("#recurringSelfDefenseAvailable").val(roundTo(recurringSelfDefenseAvailable,2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(recurringTotalRelease);
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable,2));
                    } else {

                    }
                });

                var input43 = document.querySelector('#recurringOtherRelease');
                input43.addEventListener('input', function()
                {
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    var recurringOtherRelease = input43.value;
                    var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";
                    var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";

                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";

                    var recurringOtherAvailable = parseFloat(recurringOtherRelease) - parseFloat(recurringOtherSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringOtherAvailable)) {
                        $("#recurringOtherAvailable").val(roundTo(recurringOtherAvailable,2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(recurringTotalRelease);
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable,2));
                    } else {

                    }
                });




            });
            function getDistricts() {
                document.forms[0].mode.value = "getDistrictList";
                document.forms[0].submit();
            }
            function getMandals() {
                document.forms[0].mode.value = "getMandalList";
                document.forms[0].submit();
            }
            function getVillages() {
                document.forms[0].mode.value = "getVillageList";
                document.forms[0].submit();
            }
            function getSchools() {
                $("#schoolName").val("0");
                document.forms[0].mode.value = "getSchoolList";
                document.forms[0].submit();
            }
            function submitForm(type) {

                var roleId = $("#roleId").val();

                ///Civil Works
                var civilWorksReleased = $("#civilWorksReleased").val();
                var majorRepairsReleased = $("#majorRepairsReleased").val();
                var minorRepairsReleased = $("#minorRepairsReleased").val();
                var toiletsReleased = $("#toiletsReleased").val();

                var civilWorksSpent = $("#civilWorksSpent").val();
                var majorRepairsSpent = $("#majorRepairsSpent").val();
                var minorRepairsSpent = $("#minorRepairsSpent").val();
                var toiletsSpent = $("#toiletsSpent").val();

                var civilWorksAvailable = $("#civilWorksAvailable").val();
                var majorRepairsAvailable = $("#majorRepairsAvailable").val();
                var minorRepairsAvailable = $("#minorRepairsAvailable").val();
                var toiletsAvailable = $("#toiletsAvailable").val();

                var totalCivilReleased = $("#totalCivilReleased").val();
                var totalCivilSpent = $("#totalCivilSpent").val();
                var totalCivilAvailable = $("#totalCivilAvailable").val();

                ///Annual Grants
                var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                var waterElectricityTelephoneChargesAvailable = $("#waterElectricityTelephoneChargesAvailable").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").val();
                var minorRepairsAnnualAvailable = $("#minorRepairsAnnualAvailable").val();
                var sanitationAndICTAvailable = $("#sanitationAndICTAvailable").val();
                var needBasedWorksAvailable = $("#needBasedWorksAvailable").val();
                var totalAnnualReleased = $("#totalAnnualReleased").val();
                var totalAnnualSpent = $("#totalAnnualSpent").val();
                var totalAnnualAvailable = $("#totalAnnualAvailable").val();

                ///Recurring
                var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                var recurringExcursionAvailable = $("#recurringExcursionAvailable").val();
                var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                var recurringSelfDefenseAvailable = $("#recurringSelfDefenseAvailable").val();
                var recurringOtherRelease = $("#recurringOtherRelease").val();
                var recurringOtherSpent = $("#recurringOtherSpent").val();
                var recurringOtherAvailable = $("#recurringOtherAvailable").val();
                var recurringTotalRelease = $("#recurringTotalRelease").val();
                var recurringTotalSpent = $("#recurringTotalSpent").val();
                var recurringTotalAvailable = $("#recurringTotalAvailable").val();
                ///Furniture & Lab Equipment
                var furnitureReleased = $("#furnitureReleased").val();
                var labEquipmentReleased = $("#labEquipmentReleased").val();
                var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                var furnitureSpent = $("#furnitureSpent").val();
                var totalfurnitureAndLabSpent = $("#totalfurnitureAndLabSpent").val();
                var labEquipmentSpent = $("#labEquipmentSpent").val();
                var furnitureAvailable = $("#furnitureAvailable").val();
                var labEquipmentAvailable = $("#labEquipmentAvailable").val();
                var totalfurnitureAndLabAvailable = $("#totalfurnitureAndLabAvailable").val();


                if (civilWorksReleased === "" || civilWorksReleased === "NaN" || civilWorksReleased === "") {
                    alert("Enter civilWorks Released value");
                    $("#civilWorksReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (majorRepairsReleased === "" || majorRepairsReleased === "NaN" || majorRepairsReleased === "") {
                    alert("Enter majorRepairs Released value");
                    $("#majorRepairsReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsReleased === "" || minorRepairsReleased === "NaN" || minorRepairsReleased === "") {
                    alert("Enter minorRepairs Released value");
                    $("#minorRepairsReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (toiletsReleased === "" || toiletsReleased === "NaN" || toiletsReleased === "") {
                    alert("Enter toilets Released value");
                    $("#toiletsReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (civilWorksSpent === "" || civilWorksSpent === "NaN" || civilWorksSpent === "") {
                    alert("Enter civil WorksSpent value");
                    $("#civilWorksSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (majorRepairsSpent === "" || majorRepairsSpent === "NaN" || majorRepairsSpent === "") {
                    alert("Enter majorRepairs Spent value");
                    $("#majorRepairsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsSpent === "" || minorRepairsSpent === "NaN" || minorRepairsSpent === "") {
                    alert("Enter minorRepairs Spent value");
                    $("#minorRepairsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (toiletsSpent === "" || toiletsSpent === "NaN" || toiletsSpent === "") {
                    alert("Enter toilets Spent value");
                    $("#toiletsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (civilWorksAvailable === "" || civilWorksAvailable === "NaN" || civilWorksAvailable === "") {
                    alert("Enter civilWorks Available value");
                    $("#civilWorksAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (majorRepairsAvailable === "" || majorRepairsAvailable === "NaN" || majorRepairsAvailable === "") {
                    alert("Enter majorRepairs Available value");
                    $("#majorRepairsAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsAvailable === "" || minorRepairsAvailable === "NaN" || minorRepairsAvailable === "") {
                    alert("Enter minorRepairs Available value");
                    $("#minorRepairsAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (toiletsAvailable === "" || toiletsAvailable === "NaN" || toiletsAvailable === "") {
                    alert("Enter toilets Available value");
                    $("#toiletsAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalCivilReleased === "" || totalCivilReleased === "NaN" || totalCivilReleased === "") {
                    alert("Enter totalCivil Released value");
                    $("#totalCivilReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalCivilSpent === "" || totalCivilSpent === "NaN" || totalCivilSpent === "") {
                    alert("Enter totalCivil Spent value");
                    $("#totalCivilSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalCivilAvailable === "" || totalCivilAvailable === "NaN" || totalCivilAvailable === "") {
                    alert("Enter totalCivil Available value");
                    $("#totalCivilAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringExcursionRelease === "" || recurringExcursionRelease === "NaN" || recurringExcursionRelease === "") {
                    alert("Enter recurringExcursion Release value");
                    $("#recurringExcursionRelease").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringSelfDefenseRelease === "" || recurringSelfDefenseRelease === "NaN" || recurringSelfDefenseRelease === "") {
                    alert("Enter recurringSelfDefense Release value");
                    $("#recurringSelfDefenseRelease").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringOtherRelease === "" || recurringOtherRelease === "NaN" || recurringOtherRelease === "") {
                    alert("Enter recurringOther Release value");
                    $("#recurringOtherRelease").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringExcursionSpent === "" || recurringExcursionSpent === "NaN" || recurringExcursionSpent === "") {
                    alert("Enter recurringExcursion Spent value");
                    $("#recurringExcursionSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringSelfDefenseSpent === "" || recurringSelfDefenseSpent === "NaN" || recurringSelfDefenseSpent === "") {
                    alert("Enter recurringSelfDefense Spent value");
                    $("#recurringSelfDefenseSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringOtherSpent === "" || recurringOtherSpent === "NaN" || recurringOtherSpent === "") {
                    alert("Enter recurringOther Spent value");
                    $("#recurringOtherSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringExcursionAvailable === "" || recurringExcursionAvailable === "NaN" || recurringExcursionAvailable === "") {
                    alert("Enter recurringExcursion Available value");
                    $("#recurringExcursionAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringSelfDefenseAvailable === "" || recurringSelfDefenseAvailable === "NaN" || recurringSelfDefenseAvailable === "") {
                    alert("Enter recurringSelfDefense Available value");
                    $("#recurringSelfDefenseAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringOtherAvailable === "" || recurringOtherAvailable === "NaN" || recurringOtherAvailable === "") {
                    alert("Enter recurringOther Available value");
                    $("#recurringOtherAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringTotalRelease === "" || recurringTotalRelease === "NaN" || recurringTotalRelease === "") {
                    alert("Enter recurringTotal Release value");
                    $("#recurringTotalRelease").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringTotalSpent === "" || recurringTotalSpent === "NaN" || recurringTotalSpent === "") {
                    alert("Enter recurringTotal Spent value");
                    $("#recurringTotalSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (recurringTotalAvailable === "" || recurringTotalAvailable === "NaN" || recurringTotalAvailable === "") {
                    alert("Enter recurring Total Available value");
                    $("#recurringTotalAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (waterElectricityTelephoneChargesReleased === "" || waterElectricityTelephoneChargesReleased === "NaN" || waterElectricityTelephoneChargesReleased === "") {
                    alert("Enter waterElectricityTelephoneCharges Released value");
                    $("#waterElectricityTelephoneChargesReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "" || purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "NaN" || purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "") {
                    alert("Enter purchaseofBooksAndPeriodicalsAndNewsPapers Released value");
                    $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsAnnualReleased === "" || minorRepairsAnnualReleased === "NaN" || minorRepairsAnnualReleased === "") {
                    alert("Enter minorRepairsAnnual Released value");
                    $("#minorRepairsAnnualReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (sanitationAndICTReleased === "" || sanitationAndICTReleased === "NaN" || sanitationAndICTReleased === "") {
                    alert("Enter sanitationAndICT Released value");
                    $("#sanitationAndICTReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (needBasedWorksReleased === "" || needBasedWorksReleased === "NaN" || needBasedWorksReleased === "") {
                    alert("Enter needBasedWorks Released value");
                    $("#needBasedWorksReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (waterElectricityTelephoneChargesSpent === "" || waterElectricityTelephoneChargesSpent === "NaN" || waterElectricityTelephoneChargesSpent === "") {
                    alert("Enter waterElectricityTelephoneCharges Spent value");
                    $("#waterElectricityTelephoneChargesSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "" || purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "NaN" || purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "") {
                    alert("Enter purchaseofBooksAndPeriodicalsAndNewsPapers Spent value");
                    $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsAnnualSpent === "" || minorRepairsAnnualSpent === "NaN" || minorRepairsAnnualSpent === "") {
                    alert("Enter minorRepairsAnnual Spent value");
                    $("#minorRepairsAnnualSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (sanitationAndICTSpent === "" || sanitationAndICTSpent === "NaN" || sanitationAndICTSpent === "") {
                    alert("Enter sanitationAndICT Spent value");
                    $("#sanitationAndICTSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (needBasedWorksSpent === "" || needBasedWorksSpent === "NaN" || needBasedWorksSpent === "") {
                    alert("Enter needBasedWorks Spent value");
                    $("#needBasedWorksSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (waterElectricityTelephoneChargesAvailable === "" || waterElectricityTelephoneChargesAvailable === "NaN" || waterElectricityTelephoneChargesAvailable === "") {
                    alert("Enter waterElectricityTelephoneCharges Available value");
                    $("#waterElectricityTelephoneChargesAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (purchaseofBooksAndPeriodicalsAndNewsPapersAvailable === "" || purchaseofBooksAndPeriodicalsAndNewsPapersAvailable === "NaN" || purchaseofBooksAndPeriodicalsAndNewsPapersAvailable === "") {
                    alert("Enter purchaseofBooksAndPeriodicalsAndNewsPapers Available value");
                    $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (minorRepairsAnnualAvailable === "" || minorRepairsAnnualAvailable === "NaN" || minorRepairsAnnualAvailable === "") {
                    alert("Enter minorRepairsAnnual Available value");
                    $("#minorRepairsAnnualAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (sanitationAndICTAvailable === "" || sanitationAndICTAvailable === "NaN" || sanitationAndICTAvailable === "") {
                    alert("Enter sanitationAndICT Available value");
                    $("#sanitationAndICTAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (needBasedWorksAvailable === "" || needBasedWorksAvailable === "NaN" || needBasedWorksAvailable === "") {
                    alert("Enter needBasedWorks Available value");
                    $("#needBasedWorksAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalAnnualReleased === "" || totalAnnualReleased === "NaN" || totalAnnualReleased === "") {
                    alert("Enter totalAnnual Released value");
                    $("#totalAnnualReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalAnnualSpent === "" || totalAnnualSpent === "NaN" || totalAnnualSpent === "") {
                    alert("Enter totalAnnual Spent value");
                    $("#totalAnnualSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalAnnualAvailable === "" || totalAnnualAvailable === "NaN" || totalAnnualAvailable === "") {
                    alert("Enter totalAnnual Available value");
                    $("#totalAnnualAvailable").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (furnitureReleased === "" || furnitureReleased === "NaN" || furnitureReleased === "") {
                    alert("Enter furniture Released value");
                    $("#furnitureReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (labEquipmentReleased === "" || labEquipmentReleased === "NaN" || labEquipmentReleased === "") {
                    alert("Enter labEquipment Released value");
                    $("#labEquipmentReleased").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (totalfurnitureAndLabReleased === "" || totalfurnitureAndLabReleased === "NaN" || totalfurnitureAndLabReleased === "") {
                    alert("Enter totalfurnitureAndLab Released value");
                    $("#totalfurnitureAndLabReleased").focus().css({'border': '1px solid red'});
                    return false;
                } else if (furnitureSpent === "" || furnitureSpent === "NaN" || furnitureSpent === "") {
                    alert("Enter furniture Spent value");
                    $("#furnitureSpent").focus().css({'border': '1px solid red'});
                    return false;
                } else if (totalfurnitureAndLabSpent === "" || totalfurnitureAndLabSpent === "NaN" || totalfurnitureAndLabSpent === "") {
                    alert("Enter total furnitureAndLab Spent value");
                    $("#totalfurnitureAndLabSpent").focus().css({'border': '1px solid red'});
                    return false;
                } else if (labEquipmentSpent === "" || labEquipmentSpent === "NaN" || labEquipmentSpent === "") {
                    alert("Enter labEquipment Spent value");
                    $("#labEquipmentSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (furnitureAvailable === "" || furnitureAvailable === "NaN" || furnitureAvailable === "") {
                    alert("Enter furniture Available value");
                    $("#furnitureAvailable").focus().css({'border': '1px solid red'});
                    return false;
                } else if (labEquipmentAvailable === "" || labEquipmentAvailable === "NaN" || labEquipmentAvailable === "") {
                    alert("Enter labEquipment Available value");
                    $("#labEquipmentAvailable").focus().css({'border': '1px solid red'});
                    return false;
                } else if (totalfurnitureAndLabAvailable === "" || totalfurnitureAndLabAvailable === "NaN" || totalfurnitureAndLabAvailable === "") {
                    alert("Enter totalfurnitureAndLab Available value");
                    $("#totalfurnitureAndLabAvailable").focus().css({'border': '1px solid red'});
                    return false;
                } else {
                    document.forms[0].mode.value = "updateRmsaComponentMaster";
                    document.forms[0].submit();
                }
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow === 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow === 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow === 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow === 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow === 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow === 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }

                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k !== 13 && k !== 8 && k !== 0) {
                    if ((e.ctrlKey === false) && (e.altKey === false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) !== -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 46)
                    return true;
                else if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (number.length < 1) {
                    if (evt.keyCode === 32) {
                        return false;
                    }
                }
                return true;
            }
        </script>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            input { 
                padding: 2px 5px;
                margin: 5px 0px;
                border: 1px solid #ccc;
                text-align: center;

            }
            .table tbody td {
                background: transparent !important;
            }
        </style>
    </head>
    <body>
        <html:form action="/rmsaComponent" method="post" >
            <html:hidden property="mode"/>
             <html:hidden property="sno"/>
            <input type="hidden" name="roleId" id="roleId" value="<%=session.getAttribute("RoleId").toString()%>"/>
            <%  String msg = null;
                if (request.getAttribute("msg") != null) {
                    msg = request.getAttribute("msg").toString();

            %>
        <center><div id="msg"><span style="color:#0095ff !important"><b><%=msg%><b></span></div></center><%}%>

                            <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">

                                            <div class="innerbodyrow">
                                                <div class="col-xs-12">
                                                    <h3> RMSA Expenditure </h3>
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Year :</label>
                                                    <html:select property="year" styleId="year" onchange="getDistricts(this.value);" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>Select Year</html:option>
                                                        <html:optionsCollection property="yearList" label="yearCode" value="yearCode"/>
                                                    </html:select> 
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">District Name :</label>
                                                    <%
                                                        boolean a = false, b = true, sanctionedbyGOI = false, ExpIncurred = false;
                                                        if (session.getAttribute("RoleId").equals("3")) {
                                                            a = true;
                                                            b = false;
                                                        } else if (session.getAttribute("RoleId").equals("52")) {
                                                            a = false;
                                                            b = false;
                                                            sanctionedbyGOI = true;
                                                            ExpIncurred = false;
                                                        }

                                                    %>
                                                    <html:select property="districtId"  styleId="district" disabled="<%=a%>" onchange="getMandals()" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value="0"> Select District Name </html:option>
                                                        <html:optionsCollection property="distList" label="distname" value="distCode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Mandal Name :</label>
                                                    <html:select property="mandalId" styleId="mandal" onchange="getVillages(this.value);" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>Select Mandal Name</html:option>
                                                        <html:optionsCollection property="mandalList" label="mndname" value="mndCode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Village Name :</label>
                                                    <html:select property="villageId" styleId="village" onchange="getSchools(this.value);" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>Select Village</html:option>
                                                        <html:optionsCollection property="villageList" label="vname" value="vcode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">School Name :</label>
                                                    <html:select property="schoolCode" styleId="schoolName" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>Select School Name</html:option>
                                                        <html:optionsCollection property="schoolList" label="schoolName" value="schoolCode"/>
                                                    </html:select> 
                                                </div>

                                                <div class="col-xs-12" style="text-align:right">
                                                    <input class="btn btn-primary" type='button' value='Search' id="Search"/>
                                                </div>

                                                <logic:present  name="RMSAMaster"> 

                                                    <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Bank Details</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">School Code</label>
                                                        <html:text property="schoolCode" styleId="schoolCode"  onkeypress='return onlyNumbers(event);'  readonly="true"  style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">Bank Name</label>
                                                        <html:text property="bankName" styleId="bankName"  onkeypress='return onlyNumbers(event);'  readonly="true"  style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">Account Number</label>
                                                        <html:text property="bankAccountNumber" styleId="bankAccountNumber"  onkeypress='return onlyNumbers(event);' readonly="true" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">IFSC Code</label>
                                                        <html:text property="ifscCode" styleId="ifscCode"  onkeypress='return onlyNumbers(event);'  readonly="true" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Non Recurring</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Civil Works (Construction)</th>
                                                                    <th style="width: 80px">Major Repairs</th>
                                                                    <th style="width: 80px">Minor Repairs</th>
                                                                    <th style="width: 80px">Toilets</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Released</th>
                                                                    <td style="text-align: center;"><html:text property="civilWorksReleased" styleId="civilWorksReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsReleased" styleId="majorRepairsReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9"/></td>
                                                                    <td style="text-align: center;"><html:text property="minorRepairsReleased" styleId="minorRepairsReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9"/></td>
                                                                    <td style="text-align: center;"><html:text property="toiletsReleased" styleId="toiletsReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="5"/></td>
                                                                    <td style="text-align: center;"><html:text property="totalCivilReleased" styleId="totalCivilReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Expenditure</th>
                                                                    <td style="text-align: center;"><html:text property="civilWorksSpent" styleId="civilWorksSpent" onkeypress='return onlyNumbers(event);' maxlength="9" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsSpent" styleId="majorRepairsSpent" onkeypress='return onlyNumbers(event);' maxlength="9" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="minorRepairsSpent" styleId="minorRepairsSpent" onkeypress='return onlyNumbers(event);' maxlength="9" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="toiletsSpent" styleId="toiletsSpent" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="totalCivilSpent" styleId="totalCivilSpent" onkeypress='return onlyNumbers(event);'  readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="civilWorksAvailable" styleId="civilWorksAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsAvailable" styleId="majorRepairsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAvailable" styleId="minorRepairsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="toiletsAvailable" styleId="toiletsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalCivilAvailable" styleId="totalCivilAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Furniture & Lab Equipment</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Furniture</th>
                                                                    <th style="width: 80px">Lab Equipment</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Released</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureReleased" styleId="furnitureReleased" style="width:80px"    onkeypress='return onlyNumbers(event);' maxlength="9"  /></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentReleased" styleId="labEquipmentReleased" style="width:80px"  onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabReleased" styleId="totalfurnitureAndLabReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Expenditure</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureSpent" styleId="furnitureSpent" onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentSpent" styleId="labEquipmentSpent"  onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabSpent" styleId="totalfurnitureAndLabSpent" onkeypress='return onlyNumbers(event);'  readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureAvailable" styleId="furnitureAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentAvailable" styleId="labEquipmentAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabAvailable" styleId="totalfurnitureAndLabAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Annual Grants</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Water, Electricity, Telephone Charges</th>
                                                                    <th style="width: 80px">Purchase of Books, Periodicals, News Papers</th>
                                                                    <th style="width: 80px">Minor Repairs</th>
                                                                    <th style="width: 80px">Sanitation and ICT</th>
                                                                    <th style="width: 80px">Need based Works etc..if any</th>
                                                                    <th style="width: 80px">Provisional’s for Laboratory</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Released</th>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesReleased" styleId="waterElectricityTelephoneChargesReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualReleased" styleId="minorRepairsAnnualReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTReleased" styleId="sanitationAndICTReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksReleased" styleId="needBasedWorksReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratoryReleased" styleId="provisionalLaboratoryReleased" style="width:80px" onkeypress='return onlyNumbers(event);' maxlength="9" /></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualReleased" styleId="totalAnnualReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Expenditure</th>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesSpent" styleId="waterElectricityTelephoneChargesSpent"  onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersSpent" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersSpent"  onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualSpent" styleId="minorRepairsAnnualSpent"  onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTSpent" styleId="sanitationAndICTSpent"  onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksSpent" styleId="needBasedWorksSpent" onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratorySpent" styleId="provisionalLaboratorySpent" onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualSpent" styleId="totalAnnualSpent" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesAvailable" styleId="waterElectricityTelephoneChargesAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersAvailable" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersAvailable" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualAvailable" styleId="minorRepairsAnnualAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTAvailable" styleId="sanitationAndICTAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksAvailable" styleId="needBasedWorksAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratoryAvailable" styleId="provisionalLaboratoryAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualAvailable" styleId="totalAnnualAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Recurring</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Excursion Trip</th>
                                                                    <th style="width: 80px">Self Defense</th>
                                                                    <th style="width: 80px">Other Recurring Grants</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Released</th>
                                                                    <td style="text-align: center;"> <html:text property="recurringExcursionRelease" styleId="recurringExcursionRelease" style="width:80px"    onkeypress='return onlyNumbers(event);' maxlength="5"  ></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringSelfDefenseRelease" styleId="recurringSelfDefenseRelease" style="width:80px"  onkeypress='return onlyNumbers(event);' maxlength="5" ></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherRelease" styleId="recurringOtherRelease" style="width:80px"  onkeypress='return onlyNumbers(event);' maxlength="5" ></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalRelease" styleId="recurringTotalRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"  ></html:text></td>
                                                                    </tr>
                                                                    <tr colspan="4">
                                                                        <th style="width: 20px">Expenditure</th>
                                                                        <td style="text-align: center;"><html:text property="recurringExcursionSpent" styleId="recurringExcursionSpent"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"   style="text-align: center;width:80px;"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringSelfDefenseSpent" styleId="recurringSelfDefenseSpent"  onkeypress='return onlyNumbers(event);' maxlength="5"   style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherSpent" styleId="recurringOtherSpent" onkeypress='return onlyNumbers(event);' maxlength="5"   style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalSpent" styleId="recurringTotalSpent" onkeypress='return onlyNumbers(event);' maxlength="5"  readonly="true"   style="text-align: center;width:80px;"></html:text></td>
                                                                    </tr>
                                                                    <tr colspan="4">
                                                                        <th style="width: 20px">Balance</th>
                                                                        <td style="text-align: center;"><html:text property="recurringExcursionAvailable" styleId="recurringExcursionAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringSelfDefenseAvailable" styleId="recurringSelfDefenseAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherAvailable" styleId="recurringOtherAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5"   style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalAvailable" styleId="recurringTotalAvailable"  onkeypress='return onlyNumbers(event);' maxlength="5"  style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>    
                                                        </div>

                                                        <div class="col-xs-12 col-sm-12" style="padding:0px;">
                                                            <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Interest</p>
                                                        </div>   
                                                        <div class="col-xs-12 col-sm-12">
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                <thead>
                                                                    <tr colspan="3">
                                                                        <th style="width: 80px">Earned</th>
                                                                        <th style="width: 80px">Expenditure</th>
                                                                        <th style="width: 80px">Balance</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr colspan="3">
                                                                        <td style="text-align: center;"> <html:text property="interestEarned" styleId="interestEarned" onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="interestExpenditure" styleId="interestExpenditure" onkeypress='return onlyNumbers(event);' maxlength="9" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="interestBalance" styleId="interestBalance" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>       

                                                    </div>
                                                    <div class="col-xs-12" style="text-align:center">
                                                        <center> <input class="btn btn-primary"  type="button" value="Update" name="Update" onclick="submitForm();" /></center>
                                                    </div>
                                                </logic:present>
                                            </div>
                                        </div>    
                                    </div>
                                    <!-- End row --> 
                                </div>
                                <!-- End Container --> 
                            </section>



                        </html:form>
                        </body>
                        </html>