<%-- 
    Document   : RMSAComponentsType1
    Created on : 06 Apr, 2017, 4:32:50 PM
    Author     : 1250892
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String photo = null;
    if (request.getAttribute("RMSAChequeDetails") != null) {
        ArrayList list = (ArrayList) request.getAttribute("RMSAChequeDetails");
        for (int i = 0; i < list.size(); i++) {
            photo = "kpiinsert(ucsFileUpload" + i + ")";
        }
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title>
<!--        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet" type="text/css" />-->
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script>


            window.onload = function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);

            };

            $(document).ready(function() {
                $('input, :input').attr('autocomplete', 'off');
                $(document).bind("contextmenu", function(e) {
                    return false;
                });
                $(function() {
                    $(document).keydown(function(e) {
                        return (e.which || e.keyCode) !== 116;
                    });
                });

                if (document.getElementById("artCraftSanction") !== null) {
                    document.getElementById("artCraftSanction").autocomplete = "off";
                    document.getElementById("computerRoomSanction").autocomplete = "off";
                    document.getElementById("scienceLabSanction").autocomplete = "off";
                    document.getElementById("libraryRoomSanction").autocomplete = "off";
                    document.getElementById("additionalClassRoomSanction").autocomplete = "off";
                    document.getElementById("toiletBlockSanction").autocomplete = "off";
                    document.getElementById("drinkingWaterSanction").autocomplete = "off";
                    document.getElementById("administrativeSanction").autocomplete = "off";
                    document.getElementById("totalFurniture").autocomplete = "off";
                    document.getElementById("furnitureAndLabEquipment").autocomplete = "off";
                    document.getElementById("estimatedCost").autocomplete = "off";
                    document.getElementById("tenderCostValue").autocomplete = "off";
                    document.getElementById("vat").autocomplete = "off";
                    document.getElementById("departemntCharges").autocomplete = "off";
                    document.getElementById("totalEligibleCost").autocomplete = "off";
                    document.getElementById("releases").autocomplete = "off";
                    document.getElementById("expenditureIncurred").autocomplete = "off";

                    document.getElementById("administrativeSanction").placeholder = "In Lakhs.";
                    document.getElementById("totalFurniture").placeholder = "In Lakhs.";
                    document.getElementById("furnitureAndLabEquipment").placeholder = "In Lakhs.";
                    document.getElementById("estimatedCost").placeholder = "In Lakhs.";
                    document.getElementById("tenderCostValue").placeholder = "In Lakhs.";
                    document.getElementById("vat").placeholder = "In Percentage.";
                    document.getElementById("departemntCharges").placeholder = "In Lakhs.";
                    document.getElementById("totalEligibleCost").placeholder = "In Lakhs.";
                    document.getElementById("releases").placeholder = "In Lakhs.";
                    document.getElementById("expenditureIncurred").placeholder = "In Lakhs.";
                    document.getElementById("balanceToBeReleased").placeholder = "In Lakhs.";
                }
                $("#Search").button().click(function() {
                    var district = $('#district').val();
                    var mandal = $('#mandal').val();
                    var phaseNo = $('#phaseNo').val();
                    var village = $('#village').val();
                    var schoolName = $('#schoolName').val();
                    if (district == "0") {
                        alert("Select District Name");
                        $("#district").focus().css({'border': '1px solid red'});
                    }
                    else if (mandal == "0") {
                        alert("Select Mandal Name");
                        $("#mandal").focus().css({'border': '1px solid red'});
                    }
                    else if (village == "0") {
                        alert("Select Village Name");
                        $("#village").focus().css({'border': '1px solid red'});
                    }
                    else if (phaseNo == "0") {
                        alert("Select PhaseNo ");
                        $("#phaseNo").focus().css({'border': '1px solid red'});
                    }
                    else if (schoolName == "0") {
                        alert("Select School Name");
                        $("#schoolName").focus().css({'border': '1px solid red'});
                    }
                    else {
                        var schoolName = $('#schoolName').val();
                        var paraData = "schoolName=" + $.trim(schoolName);
                        document.forms[0].mode.value = "searchSchoolFunding";
                        document.forms[0].submit();


                    }
                });
                $("#releases").keyup(function() {
                    var releasesAmount = $("#releases").val();
                    var totalEligibleCost = $("#totalEligibleCost").val();
                    if (releasesAmount === '.')
                        releasesAmount = 0;
                    if (totalEligibleCost === '.')
                        totalEligibleCost = 0;
                    var total = totalEligibleCost - releasesAmount;
                    total = Math.round(total * 100) / 100;
                    $("#balanceToBeReleased").val(total);
                });
                $("#totalEligibleCost").keyup(function() {
                    var releasesAmount = $("#releases").val();
                    var totalEligibleCost = $("#totalEligibleCost").val();
                    if (releasesAmount === '.')
                        releasesAmount = 0;
                    if (totalEligibleCost === '.')
                        totalEligibleCost = 0;
                    var total = totalEligibleCost - releasesAmount;
                    total = Math.round(total * 100) / 100;
                    $("#balanceToBeReleased").val(total);
                });
            });
            function getMandals() {
                document.forms[0].mode.value = "getMandalList";
                document.forms[0].submit();

            }
            function getVillages() {
                document.forms[0].mode.value = "getVillageList";
                document.forms[0].submit();
            }
            function getSchools() {
                $("#schoolName").val("0");
                document.forms[0].mode.value = "getSchoolList";
                document.forms[0].submit();
            }
            function getPhases() {
                $("#phaseNo").val("0");
                document.forms[0].mode.value = "getPhaseList";
                document.forms[0].submit();

            }
//            function submitForm(type) {
//                var roleId = $("#roleId").val();
//                alert("hello-1"+roleId);
//                if ($("#artCraftSanction").val() === '' || $("#artCraftSanction").val() === '.') {
//                    alert("Please Enter Art/Craft Room Sanction");
//                    $("#artCraftSanction").focus().css({'border': '1px solid red'});
//                    return false;
//                } else if (roleId === '52') {
//                    if ($("#artCraftStageOfProgress").val() === 'Select' || $("#artCraftStageOfProgress").val() === '.') {
//                        alert("Please Select Art/Craft room Stage of Progress");
//                        $("#artCraftStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#computerRoomSanction").val() === '' || $("#computerRoomSanction").val() === '.') {
//                    alert("Please Enter Computer Room Sanction");
//                    $("#computerRoomSanction").focus().css({'border': '1px solid red'});
//                    return false;
//                } else if (roleId === '52') {
//                    if ($("#computerRoomStageOfProgress").val() === 'Select' || $("#computerRoomStageOfProgress").val() === '.') {
//                        alert("Please Select Computer Room Stage of Progress");
//                        $("#computerRoomStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#scienceLabSanction").val() === '' || $("#scienceLabSanction").val() === '.') {
//                    alert("Please Enter Science Lab Sanction");
//                    $("#scienceLabSanction").focus().css({'border': '1px solid red'});
//                    return false;
//                } else if (roleId === '52') {
//                    if ($("#scienceLabStageOfProgress").val() === 'Select' || $("#scienceLabStageOfProgress").val() === '.') {
//                        alert("Please Select Science Lab Stage of Progress");
//                        $("#scienceLabStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#libraryRoomSanction").val() === '' || $("#libraryRoomSanction").val() === '.') {
//                        alert("Please Enter Library Room Sanction");
//                        $("#libraryRoomSanction").focus().css({'border': '1px solid red'});
//                        return false;
//                } else if (roleId === '52') {
//                    if ($("#libraryRoomStageOfProgress").val() === 'Select' || $("#libraryRoomStageOfProgress").val() === '.') {
//                        alert("Please Select Library Room Stage of Progress");
//                        $("#libraryRoomStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#additionalClassRoomSanction").val() === '' || $("#additionalClassRoomSanction").val() === '.') {
//                        alert("Please Enter Additional Class Room Sanction");
//                        $("#additionalClassRoomSanction").focus().css({'border': '1px solid red'});
//                        return false;
//                } else if (roleId === '52') {
//                    if ($("#additionalClassRoomStageOfProgress").val() === 'Select' || $("#additionalClassRoomStageOfProgress").val() === '.') {
//                        alert("Please Select Additional Class Room Stage of Progress");
//                        $("#additionalClassRoomStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#toiletBlockSanction").val() === '' || $("#toiletBlockSanction").val() === '.') {
//                         alert("Please Enter Toilet Block Sanction");
//                         $("#toiletBlockSanction").focus().css({'border': '1px solid red'});
//                         return false;
//                } else if (roleId === '52') {
//                    if ($("#toiletBlockStageOfProgress").val() === 'Select' || $("#toiletBlockStageOfProgress").val() === '.') {
//                        alert("Please Select Toilet Block Stage of Progress");
//                        $("#toiletBlockStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if ($("#drinkingWaterSanction").val() === '' || $("#drinkingWaterSanction").val() === '.') {
//                         alert("Please Enter Drinking Water Sanction");
//                         $("#drinkingWaterSanction").focus().css({'border': '1px solid red'});
//                         return false;
//                } else if (roleId === '52') {
//                    if ($("#drinkingWaterStageOfProgress").val() === 'Select' || $("#drinkingWaterStageOfProgress").val() === '.') {
//                        alert("Please Select Drinking Water Stage of Progress");
//                        $("#drinkingWaterStageOfProgress").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                } else if(roleId!==52){
//                     if ($("#administrativeSanction").val() === '' || $("#administrativeSanction").val() === '.') {
//                         alert("Please Enter Sanctioned For Civil Cost");
//                         $("#administrativeSanction").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                     else if ($("#totalFurniture").val() === '' || $("#totalFurniture").val() === '.') {
//                         alert("Please Enter Sanctioned For Furniture");
//                         $("#totalFurniture").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                     else if ($("#furnitureAndLabEquipment").val() === '' || $("#furnitureAndLabEquipment").val() === '.') {
//                         alert("Please Enter Total Sanctioned");
//                         $("#furnitureAndLabEquipment").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                } else if ($("#estimatedCost").val() === '' || $("#estimatedCost").val() === '.') {
//                         alert("Please Enter Estimated Cost");
//                         $("#estimatedCost").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                     else if ($("#tenderCostValue").val() === '' || $("#tenderCostValue").val() === '.') {
//                         alert("Please Enter Tender");
//                         $("#tenderCostValue").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                     else if ($("#vat").val() === '' || $("#vat").val() === '.') {
//                         alert("Please Enter Vat");
//                         $("#vat").focus().css({'border': '1px solid red'});
//                         return false;
//                     }
//                     else if ($("#departemntCharges").val() === '' || $("#departemntCharges").val() === '.') {
//                         alert("Please Enter Dept.Charges");
//                         $("#departemntCharges").focus().css({'border': '1px solid red'});
//                         return false;
//                     } else if ($("#totalEligibleCost").val() === '' || $("#totalEligibleCost").val() === '.') {
//                         alert("Please Enter Total Eligible Cost");
//                         $("#totalEligibleCost").focus().css({'border': '1px solid red'});
//                         return false;
//                     } else if ($("#releases").val() === '' || $("#releases").val() === '.') {
//                         alert("Please Enter Total Released");
//                         $("#releases").focus().css({'border': '1px solid red'});
//                         return false;
//                     } else if ($("#expenditureIncurred").val() === '' || $("#expenditureIncurred").val() === '.') {
//                         alert("Please Enter Expenditure Incurred");
//                         $("#expenditureIncurred").focus().css({'border': '1px solid red'});
//                         return false;
//                     } else {
//                         alert("hello-2");
//                         var releasesAmount = document.getElementById("releases").value;
//                         var expenditureIncurred = document.getElementById("expenditureIncurred").value;
//                         //alert(releasesAmount);
//                         //alert(expenditureIncurred);
//                         if (type === "Update") {
//                             alert("hello-3");
//                             if (releasesAmount >= expenditureIncurred) {
//                                 document.forms[0].mode.value = "rmsaMasterUpdate";
//                                 document.forms[0].submit();
//                             } else {
//                                 $("#expenditureIncurred").focus().css({'border': '1px solid red'});
//                                 alert("Expenditure Incurred Should Be Less Than Or Equals To Total Released");
//                             }
//
//                         } else if (type === "Submit1") {
//                             if (releasesAmount >= expenditureIncurred) {
//                                 document.forms[0].mode.value = "rmsaMasterSubmit";
//                                 document.forms[0].submit();
//                             } else {
//                                 $("#expenditureIncurred").focus().css({'border': '1px solid red'});
//                                 alert("Expenditure Incurred Should Be Less Than Or Equals To Total Released");
//                             }
//                         } else if (type === "Submit2") {
//                             alert("Submit1");
//                         }
//                     }
//                 }
            function submitForm(type) {
                var roleId = $("#roleId").val();
                if ($("#artCraftSanction").val() === '' || $("#artCraftSanction").val() === '.') {
                    alert("Please Enter Art/Craft Room Sanction");
                    $("#artCraftSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#computerRoomSanction").val() === '' || $("#computerRoomSanction").val() === '.') {
                    alert("Please Enter Computer Room Sanction");
                    $("#computerRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#scienceLabSanction").val() === '' || $("#scienceLabSanction").val() === '.') {
                    alert("Please Enter Science Lab Sanction");
                    $("#scienceLabSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#libraryRoomSanction").val() === '' || $("#libraryRoomSanction").val() === '.') {
                    alert("Please Enter Library Room Sanction");
                    $("#libraryRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#additionalClassRoomSanction").val() === '' || $("#additionalClassRoomSanction").val() === '.') {
                    alert("Please Enter Additional Class Room Sanction");
                    $("#additionalClassRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#toiletBlockSanction").val() === '' || $("#toiletBlockSanction").val() === '.') {
                    alert("Please Enter Toilet Block Sanction");
                    $("#toiletBlockSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#drinkingWaterSanction").val() === '' || $("#drinkingWaterSanction").val() === '.') {
                    alert("Please Enter Drinking Water Sanction");
                    $("#drinkingWaterSanction").focus().css({'border': '1px solid red'});
                    return false;
                } else if (roleId === "51") {
                    if ($("#administrativeSanction").val() === '' || $("#administrativeSanction").val() === '.') {
                        alert("Please Enter Sanctioned For Civil Cost");
                        $("#administrativeSanction").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#totalFurniture").val() === '' || $("#totalFurniture").val() === '.') {
                        alert("Please Enter Sanctioned For Furniture");
                        $("#totalFurniture").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#labEquipment").val() === '' || $("#labEquipment").val() === '.') {
                        alert("Please Enter Sanction of Lab Equipment");
                        $("#labEquipment").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#furnitureAndLabEquipment").val() === '' || $("#furnitureAndLabEquipment").val() === '.') {
                        alert("Please Enter Total Sanctioned");
                        $("#furnitureAndLabEquipment").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#releases").val() === '' || $("#releases").val() === '.') {
                        alert("Please Enter Total Released");
                        $("#releases").focus().css({'border': '1px solid red'});
                        return false;
                    } else {
                        var releasesAmount = document.getElementById("releases").value;
                        var expenditureIncurred = document.getElementById("expenditureIncurred").value;
                        if (releasesAmount >= expenditureIncurred) {
                            document.forms[0].mode.value = "rmsaMasterUpdate";
                            document.forms[0].submit();
                            return true;
                        } else {
                            $("#releases").focus().css({'border': '1px solid red'});
                            alert("Total Released Should Be greater Than Or Equals To Expenditure Incurred");
                            return false;
                        }
                    }
                } else if (roleId === "52") {
                    if ($("#estimatedCost").val() === '' || $("#estimatedCost").val() === '.') {
                        alert("Please Enter Estimated Cost");
                        $("#estimatedCost").focus().css({'border': '1px solid red'});
                        return false;
                    } else if ($("#tenderCostValue").val() === '' || $("#tenderCostValue").val() === '.') {
                        alert("Please Enter Tender");
                        $("#tenderCostValue").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#vat").val() === '' || $("#vat").val() === '.') {
                        alert("Please Enter Vat");
                        $("#vat").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if ($("#departemntCharges").val() === '' || $("#departemntCharges").val() === '.') {
                        alert("Please Enter Dept.Charges");
                        $("#departemntCharges").focus().css({'border': '1px solid red'});
                        return false;
                    } else if ($("#totalEligibleCost").val() === '' || $("#totalEligibleCost").val() === '.') {
                        alert("Please Enter Total Eligible Cost");
                        $("#totalEligibleCost").focus().css({'border': '1px solid red'});
                        return false;
                    } else {
                        var releasesAmount = document.getElementById("releases").value;
                        var expenditureIncurred = document.getElementById("expenditureIncurred").value;
                        var totalEligibleCost = document.getElementById("totalEligibleCost").value;
                        if (totalEligibleCost < releasesAmount) {
                            $("#totalEligibleCost").focus().css({'border': '1px solid red'});
                            alert("Total Eligible Cost Should Be greater Than Or Equals To Total Released");
                            return false;
                        } else if (releasesAmount >= expenditureIncurred) {
                            document.forms[0].mode.value = "rmsaMasterUpdate";
                            document.forms[0].submit();
                            return true;
                        } else {
                            $("#expenditureIncurred").focus().css({'border': '1px solid red'});
                            alert("Total Released Should Be Less Than Or Equals To Expenditure Incurred");
                            return false;
                        }
                    }
                } else if (roleId === "3") {
                    if ($("#expenditureIncurred").val() === '' || $("#expenditureIncurred").val() === '.') {
                        alert("Please Enter Expenditure Incurred");
                        $("#expenditureIncurred").focus().css({'border': '1px solid red'});
                        return false;
                    } else {
                        var releasesAmount = document.getElementById("releases").value;
                        var expenditureIncurred = document.getElementById("expenditureIncurred").value;
                        if (releasesAmount >= expenditureIncurred) {
                            document.forms[0].mode.value = "rmsaMasterUpdate";
                            document.forms[0].submit();
                        } else {
                            $("#expenditureIncurred").focus().css({'border': '1px solid red'});
                            alert("Expenditure Incurred Should Be Less Than Or Equals To Total Released");
                            return false;
                        }
                    }
                }


            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }

                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 46)
                    return true;
                else if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }

            function addRow(tableID3) {
                document.getElementById("cheque").style.display = "";
                var table = document.getElementById(tableID3);
                var rowCount = table.rows.length;
                document.forms[0].noofRec.value = rowCount;
                var row = table.insertRow(rowCount);
                for (var i = 0; i < 4; i++) {
                    var newcell = row.insertCell(i);
                    newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                    if (i == 0) {
                        var na = 'releases' + rowCount;
                        newcell.innerHTML = "<input type='text' name='releases' id='" + na + "'  placeholder='Total Released' onkeydown='return space(event,this);' onkeypress='return onlyNumbers(event);' autocomplete='off' />";
                    }
                    else if (i == 1) {
                        //var t = 'testT(this);';
                        var na = 'expenditureIncurred' + rowCount;
                        newcell.innerHTML = "<input type='date' name='expenditureIncurred' id='" + na + "' onkeydown='return space(event,this);' onkeypress='return inputLimiter(event,'Numbers');'/>";
                    }
//                        else if (i == 2) {
//                            
//                            var na = 'balanceToBeReleased' + rowCount;
//                               newcell.innerHTML = "<input type='text'   name='balanceToBeReleased' id='" + na + "'  placeholder='Cheque Amount In Lakhs.' onkeydown='return space(event,this);'  onkeypress='return onlyNumbers(event);' autocomplete='off' />";
////                            newcell.innerHTML = "<input type='text'   name='balanceToBeReleased' id='" + na + "'  placeholder='Cheque Amount In Lakhs.' onkeydown='return space(event,this);'  onkeypress='return onlyNumbers(event);' autocomplete='off' />";
//                        }
                    else if (i == 2) {
                        var t = 'deleteRow("tableID3", this,id)';
                        newcell.innerHTML = "<input type='button' value='Delete' onclick='" + t + "'/>";
                    }

                }

                document.forms[0].noofRec.value = rowCount;
            }
            function deleteRow(tableID3, currentRow) {
                try {
                    var table = document.getElementById(tableID3);
                    var rowCount = table.rows.length;
                    for (var i = 0; i < rowCount; i++) {
                        var row = table.rows[i];
                        if (row == currentRow.parentNode.parentNode) {
                            //  if (rowCount <= 2) {
                            //    alert("Cannot delete all the rows.");
                            //      break;
                            //   }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                        }
                        document.forms[0].noofRec.value = rowCount;
                    }

                } catch (e) {
                    alert(e);
                }

            }
        </script>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
           input { 
                padding: 2px 5px;
                margin: 5px 0px;
                    border: 1px solid #ccc;


            }
            .table tbody td {
                background: transparent !important;
            }
        </style>
    </head>
    <body>
        <html:form action="/rmsaMaster" method="post" >
            <html:hidden property="mode"/>
            <html:hidden property="noofRec"/>
            <html:hidden property="userName"/>
            <html:hidden property="ucsSubmitted"/>
            <html:hidden property="ucsFilesCount" styleId="<%=photo%>"/>
            <input type="hidden" name="roleId" id="roleId" value="<%=session.getAttribute("RoleId").toString()%>"/>
            <%  String msg = null;
                if (request.getAttribute("msg") != null) {
                    msg = request.getAttribute("msg").toString();

            %>
        <center><div id="msg"><span style="color:#0095ff !important"><b><%=msg%><b></span></div></center><%}%>
                            <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">

                                            <div class="innerbodyrow">
                                                <div class="col-xs-12">
                                                    <h3>RMSA Budget Release</h3>
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">District Name :</label>
                                                    <%
                                                        String releaseStyle = "width:170;";
                                                        String eligibleCosttextStyle = "width:170;";
                                                        String sanctionedAmountStyle = "width:170;";
                                                        String expenditureIncurredStyle = "width:170;";
                                                        String totalEligibleCostStyle = "width:170:font-weight:bold;";
                                                        boolean a = false, b = true, sanctionedbyGOI = false, ExpIncurred = false, EligibleCost = false, releases = false;
                                                        if (session.getAttribute("RoleId").equals("3")) {
                                                            a = true;
                                                            b = true;
                                                            releases = true;
                                                            EligibleCost = true;
                                                            ExpIncurred = false;
                                                            sanctionedbyGOI = true;
                                                            expenditureIncurredStyle = "width:170;";
                                                            releaseStyle = "width:170;border:0px;text-align:center;";
                                                            eligibleCosttextStyle = "width:170;border:0px;text-align:center;";
                                                            sanctionedAmountStyle = "width:170;border:0px;text-align:center;";
                                                            totalEligibleCostStyle = "width:170;font-weight:bold;border:0px;text-align:center;";
                                                        } else if (session.getAttribute("RoleId").equals("52")) {
                                                            b = true;
                                                            a = false;
                                                            releases = true;
                                                            ExpIncurred = true;
                                                            EligibleCost = false;
                                                            sanctionedbyGOI = true;
                                                            eligibleCosttextStyle = "width:170;";
                                                            totalEligibleCostStyle = "width:170;";
                                                            releaseStyle = "width:170;border:0px;text-align:center;";
                                                            sanctionedAmountStyle = "width:170;border:0px;text-align:center;";
                                                            expenditureIncurredStyle = "width:170;border:0px;text-align:center;";
                                                        } else if (session.getAttribute("RoleId").equals("51")) {
                                                            b = true;
                                                            a = false;
                                                            releases = false;
                                                            ExpIncurred = true;
                                                            EligibleCost = true;
                                                            sanctionedbyGOI = false;
                                                            releaseStyle = "width:170;";
                                                            sanctionedAmountStyle = "width:170;";
                                                            eligibleCosttextStyle = "width:170;border:0px;text-align:center;";
                                                            expenditureIncurredStyle = "width:170;border:0px;text-align:center;";
                                                            totalEligibleCostStyle = "width:170;font-weight:bold;border:0px;text-align:center;";
                                                        }

                                                    %>
                                                    <html:select property="districtId"  styleId="district" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"   disabled="<%=a%>" onchange="getMandals()" >
                                                        <html:option value="0"> Select District Name </html:option>
                                                        <html:optionsCollection property="distList" label="distname" value="distCode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Mandal Name :</label>
                                                    <html:select property="mandalId" styleId="mandal" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;" onchange="getVillages(this.value);">
                                                        <html:option value='0'>Select Mandal Name</html:option>
                                                        <html:optionsCollection property="mandalList" label="mndname" value="mndCode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Village Name :</label>
                                                    <html:select property="villageId" styleId="village" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;" onchange="getPhases(this.value);">
                                                        <html:option value='0'>Select Village</html:option>
                                                        <html:optionsCollection property="villageList" label="vname" value="vcode"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Phase No :</label>
                                                    <html:select property="phaseNo" styleId="phaseNo" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;" onchange="getSchools(this.value);">
                                                        <html:option value='0'>Select Phase</html:option>
                                                        <html:optionsCollection property="phaseList" label="phaseName" value="PhaseNo"/>
                                                    </html:select>  
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">School Name :</label>
                                                    <html:select property="schoolId" styleId="schoolName" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;" >
                                                        <html:option value='0'>Select School Name</html:option>
                                                        <html:optionsCollection property="schoolList" label="schoolName" value="schoolCode"/>
                                                    </html:select> 
                                                </div>


                                                <div class="col-xs-12" style="text-align:right">
                                                    <input class="btn btn-primary" type='button' value='Search' id="Search"/>
                                                </div>




                                                <logic:present  name="RMSAMaster"> 
                                                    <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                        <p style="text-align: right; font-size: 15px; font-weight: bold;">School Code : <span style=" color: #ff4000; margin-right: 20px;"><%=request.getAttribute("schoolCode")%></span> School Name : <span style=" color: #ff4000; margin-right: 20px;"><%=request.getAttribute("schoolName")%></span></p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Physical</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th>Sanction</th>
                                                                    <th>Stage of Progress</th>
                                                                    <th></th>
                                                                    <th>Sanction</th>
                                                                    <th>Stage of Progress</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th>Art/Craft room</th>
                                                                    <td> <html:text property="artCraftSanction" styleId="artCraftSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="artCraftStageOfProgress"  styleId="artCraftStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>

                                                                    <th style="width: 10px;text-decoration: nowrap" >Computer Room</th>
                                                                    <td> <html:text property="computerRoomSanction" styleId="computerRoomSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="computerRoomStageOfProgress"  styleId="computerRoomStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th>Science Lab</th>
                                                                    <td> <html:text property="scienceLabSanction" styleId="scienceLabSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="scienceLabStageOfProgress"  styleId="scienceLabStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>

                                                                    <th>Library Room</th>
                                                                    <td> <html:text property="libraryRoomSanction" styleId="libraryRoomSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="libraryRoomStageOfProgress"  styleId="libraryRoomStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 10px;text-">Additional Class Room</th>
                                                                    <td> <html:text property="additionalClassRoomSanction" styleId="additionalClassRoomSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="additionalClassRoomStageOfProgress"  styleId="additionalClassRoomStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>

                                                                    <th>Toilet Block</th>
                                                                    <td> <html:text property="toiletBlockSanction" styleId="toiletBlockSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="toiletBlockStageOfProgress"  styleId="toiletBlockStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select>  </td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th>Drinking Water</th>
                                                                    <td> <html:text property="drinkingWaterSanction" styleId="drinkingWaterSanction" style="width:80px;text-align:center" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true"/></td>
                                                                    <td>  <html:select property="drinkingWaterStageOfProgress"  styleId="drinkingWaterStageOfProgress"  disabled="<%=b%>">
                                                                            <html:option value="Select">--Select--</html:option>
                                                                            <html:option value="Not Sanction">Not Sanction</html:option>
                                                                            <html:option value="Not updated progress">Not Updated Progress</html:option>
                                                                            <html:option value="Site Problem">Site Problem</html:option>
                                                                            <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                            <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                            <html:option value="Basement Level">Basement Level</html:option>
                                                                            <html:option value="Roof Level">Roof Level</html:option>
                                                                            <html:option value="Roof Laid">Roof Laid</html:option>
                                                                            <html:option value="Finishings">Finishings</html:option>
                                                                            <html:option value="Completed">Completed</html:option>
                                                                            <html:option value="HandOver">HandOver</html:option>
                                                                        </html:select></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </logic:present>      

                                                <logic:present  name="RMSAMaster">  
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" id="tableID1" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Sanctioned Amount</p>
                                                            </div>
                                                            <tr colspan="4">
                                                                <th style="text-align: center;">Sanctioned For Civil Cost</th>
                                                                <th style="text-align: center;">Sanctioned For Furniture</th>
                                                                <th style="text-align: center;">Sanction of Lab Equipment</th>
                                                                <th style="text-align: center;">Total Sanctioned</th>
                                                            </tr>
                                                            <tr colspan="3">
                                                                <td style="text-align: center;"><html:text property="administrativeSanction" styleId="administrativeSanction" style="<%=sanctionedAmountStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=sanctionedbyGOI%>"/></td>
                                                                <td style="text-align: center;"><html:text property="totalFurniture" styleId="totalFurniture" style="<%=sanctionedAmountStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=sanctionedbyGOI%>"/></td>
                                                                <td style="text-align: center;"><html:text property="labEquipment" styleId="labEquipment" style="<%=sanctionedAmountStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=sanctionedbyGOI%>"/></td>
                                                                <td style="text-align: center;"><html:text property="furnitureAndLabEquipment" styleId="furnitureAndLabEquipment" style="<%=sanctionedAmountStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=sanctionedbyGOI%>"/></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" id="tableID2" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Eligible Cost</p>
                                                            </div>
                                                            <tr colspan="5">
                                                                <th style="text-align: center;">Estimated Cost</th>
                                                                <th style="text-align: center;">Tender</th>
                                                                <th style="text-align: center;">Vat</th>
                                                                <th style="text-align: center;">Dept.Charges</th>
                                                                <th style="text-align: center;">Total Eligible Cost</th>
                                                            </tr>
                                                            <tr colspan="5">
                                                                <td style="text-align: center;"><html:text property="estimatedCost" styleId="estimatedCost" style="<%=eligibleCosttextStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=EligibleCost%>"/></td>
                                                                <td style="text-align: center;"><html:text property="tenderCostValue" styleId="tenderCostValue" style="<%=eligibleCosttextStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=EligibleCost%>"/></td>
                                                                <td style="text-align: center;"><html:text property="vat" styleId="vat" style="<%=eligibleCosttextStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=EligibleCost%>"/></td>
                                                                <td style="text-align: center;"><html:text property="departemntCharges" styleId="departemntCharges" style="<%=eligibleCosttextStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=EligibleCost%>"/></td>
                                                                <td style="text-align: center;"><html:text property="totalEligibleCost" styleId="totalEligibleCost" style="<%=totalEligibleCostStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=EligibleCost%>"/></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" id="tableID3" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Releases And Expenditure</p>
                                                            </div> 
                                                            <tr colspan="4" id="cheque">
                                                                <th style="text-align: center;">Total Released</th>
                                                                <th style="text-align: center;">Expenditure Incurred</th>
                                                                <th style="text-align: center;">Balance to be Released</th>
<!--                                                                <th>
                                                                    <input class="btn btn-primary" type="button" value="Total Released" name="Add Row" style="width:200px;" onClick="addRow('tableID3')" />
                                                                </th>-->

                                                            </tr>
                                                            <tr colspan="4">
                                                                <td style="text-align: center;"><html:text property="releases" styleId="releases" style="<%=releaseStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=releases%>"/></td>
                                                                <td style="text-align: center;"><html:text property="expenditureIncurred" styleId="expenditureIncurred" style="<%=expenditureIncurredStyle%>"  onkeypress='return onlyNumbers(event);' maxlength="5" readonly="<%=ExpIncurred%>"/></td>
                                                                <td style="text-align: center;" ><html:text property="balanceToBeReleased" styleId="balanceToBeReleased" style="width:170px;border:0px;text-align:center" readonly="true"  onkeypress='return onlyNumbers(event);' /></td>
                                                             
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <br><br>
                                                    <div class="col-xs-12" style="text-align:center">
                                                        <%if (request.getAttribute("RMSAMaster") != null) {
                                                                ArrayList list = (ArrayList) request.getAttribute("RMSAMaster");
                                                                if (list.size() > 0) {%>
                                                        <center> <input class="btn btn-primary" type="button" value="Update" name="Update" onclick="submitForm('Update');" /></center>
                                                            <%} else {%>
                                                        <center> <input class="btn btn-primary" type="button" value="Submit" name="Submit1" onclick="submitForm('Submit1');" /></center>
                                                        <%}
                                                    } else {%><center><input class="btn btn-primary" type="button" value="Submit" name="Submit2" onclick="submitForm('Submit2');" /></center><%}%></logic:present>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                        <!-- End row --> 
                                    </div>
                                    <!-- End Container --> 
                                </section>



                        </html:form>
                        </body>
                        </html>