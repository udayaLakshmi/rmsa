<%-- 
    Document   : rmsaComponentReport
    Created on : Jan 23, 2018, 5:20:00 PM
    Author     : 1259084
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    int i = 1, checkId = 0;
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

    String district = null;
    if (request.getAttribute("district") != null) {
        district = (String) request.getAttribute("district");
    }
    String mandal = null;
    if (request.getAttribute("mandal") != null) {
        mandal = (String) request.getAttribute("mandal");
    }
%>
<html>
    <head>
        <title>:RMSA:</title>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/>
    </script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/>
</script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/>
</script>

<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }
    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }
    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {
        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }
</style>
<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;
        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        font-size: 16px;
        text-align: right;
        font-weight: bold;
    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;
    }
</style>
<script type="text/javascript" class="init">
    $(document).ready(function() {
        $('input, :input').attr('autocomplete', 'off');
        $('#submit').hide();
        $(".checkbox").on("click", function() {
            var anyChecked = false;
            var chkbox = document.getElementsByName('checkid1');
            for (var b = 0, mn = chkbox.length; b < mn; b++) {
                if (chkbox[b].checked) {
                    anyChecked = true;
                }
            }
            if (anyChecked === true) {
                $('#submit').show();
            } else {
                $('#submit').hide();
            }

        });
    });

    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
    });
    function mysearch() {
        var district = $('#district').val();
        var mandal = $('#mandal').val();
        var phaseNo = $('#phaseNo').val();

        if (phaseNo === "0") {
            alert("Select PhaseNo ");
            $("#phaseNo").focus().css({'border': '1px solid red'});
        } else if (district === "0") {
            alert("Select District Name");
            $("#district").focus().css({'border': '1px solid red'});
        }
        else if (mandal === "0") {
            alert("Select Mandal Name");
            $("#mandal").focus().css({'border': '1px solid red'});
        }
        else {
            document.forms[0].mode.value = "searchSchoolFunding";
            document.forms[0].submit();
        }
    }
    function mySubmit() {
        var SanctionedForCivilCost = "";
        var SanctionedForFurniture = "";
        var SanctionofLabEquipment = "";
        var TotalSanctioned = "";
        var TotalRelease = "";
        var checkedBoxes = [];
        var anyChecked = false;
        var chkbox = document.getElementsByName('checkid1');
        for (var j = 0, n = chkbox.length; j < n; j++) {
            if (chkbox[j].checked) {
                checkedBoxes.push(chkbox[j].value);
                if ($('#sc' + j).val() === null || $('#sc' + j).val() === "") {
                    alert("Please Enter Sanctioned For Civil Cost ");
                    $('#sc' + j).focus();
                    return false;
                } else {
                    SanctionedForCivilCost = SanctionedForCivilCost + $('#sc' + j).val() + "~";
                }
                if ($('#sf' + j).val() === null || $('#sf' + j).val() === "") {
                    alert("Please Enter Sanctioned For Furniture ");
                    $('#sf' + j).focus();
                    return false;
                } else {
                    SanctionedForFurniture = SanctionedForFurniture + $('#sf' + j).val() + "~";
                }
                if ($('#sl' + j).val() === null || $('#sl' + j).val() === "") {
                    alert("Please Enter Sanction of Lab Equipment ");
                    $('#sl' + j).focus();
                    return false;
                } else {
                    SanctionofLabEquipment = SanctionofLabEquipment + $('#sl' + j).val() + "~";
                }
                if ($('#ts' + j).val() === null || $('#ts' + j).val() === "") {
                    alert("Please Enter Total Sanctioned ");
                    $('#ts' + j).focus();
                    return false;
                } else {
                    TotalSanctioned = TotalSanctioned + $('#ts' + j).val() + "~";
                }
                if ($('#tr' + j).val() === null || $('#tr' + j).val() === "") {
                    alert("Please Enter Total Release ");
                    $('#tr' + j).focus();
                    return false;
                } else {
                    TotalRelease = TotalRelease + $('#tr' + j).val() + "~";
                }
                anyChecked = true;
            }
        }
        if (anyChecked === false) {
            alert("Please select at least once cehck box");
            return false;
        } else {
            document.forms[0].checkedBoxesdp.value = checkedBoxes;
            document.forms[0].sanctionedForCivilCost.value = SanctionedForCivilCost;
            document.forms[0].sanctionedForFurniture.value = SanctionedForFurniture;
            document.forms[0].sanctionofLabEquipment.value = SanctionofLabEquipment;
            document.forms[0].totalSanctioned1.value = TotalSanctioned;
            document.forms[0].totalRelease.value = TotalRelease;
            document.forms[0].checkedIndex.value = j;
            document.forms[0].mode.value = "rmsaMasterUpdate";
            document.forms[0].submit();
            return true;
        }
    }
    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode === 46)
            return true;
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function space(evt, thisvalue) {
        var number = thisvalue.value;
        if (number.length < 1) {
            if (evt.keyCode === 32) {
                return false;
            }
        }
        return true;
    }

    function getFile(filename) {
        document.forms[0].filename = filename;
        document.forms[0].mode.value = "viewStatisticstransfer";
        document.forms[0].submit();
    }


</script>
</head>
<body>
    <html:form action="/rtsView" method="post" >
        <html:hidden property="mode"/>

        <%--<html:form action="/rmsaCivils" method="post" enctype="multipart/form-data">
            <html:hidden property="mode"/>
            <html:hidden property="checkedBoxesdp"/>
            <html:hidden property="sanctionedForCivilCost"/>
            <html:hidden property="sanctionedForFurniture"/>
            <html:hidden property="sanctionofLabEquipment"/>
            <html:hidden property="totalSanctioned1"/>
            <html:hidden property="totalRelease"/>
            <html:hidden property="checkedIndex"/>--%>


        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="innerbodyrow">
                            <div class="col-xs-12">
                                <h3>RTI ACTS</h3>
                            </div>
                            </br>
                            <logic:present name="myList">

                                <div style="overflow-x: scroll; width: 1070px; margin: 0 20px">
                                    <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr class="darkgrey" >
                                                <th  style="text-align: center">Sl.No</th>
                                                <th  style="text-align: center">Act Type</th>
                                                <th  style="text-align: center">Provision Act</th>
                                                <th  style="text-align: center">View</th>
                                                <th  style="text-align: center">Download</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <logic:iterate name="myList" id="list">
                                                <tr>
                                                    <td style="text-align: center"  class=" normalRow"><%=i++%></td>
                                                    <td style="text-align: center"  class=" normalRow">${list.typeOfAct}</td>
                                                    <td style="text-align: center"  class=" normalRow">${list.ProvisionsOfAct}</td>
                                                    <td style="text-align: center"  class=" normalRow">
                                                        <a href="./rtsView.do?mode=viewFile&filename=${list.UploadAct}" target="_blank"/><img src="./images/View.png" alt="" /></a>
                                                    </td>
                                                    <td style="text-align: center"  class=" normalRow">
                                                        <a href="./rtsView.pdf?mode=downloadStatisticstransfers&filename=${list.UploadAct}"/><img src="./images/pdf.jpg" alt="" style="width: 20px;"/></a>
                                                    </td>
                                                </tr>
                                            </logic:iterate>
                                        </tbody>                                    
                                    </table>

                                </div>
                            </logic:present>


                        </div>
                    </div>
                </div>
            </div>
        </section>   
    </html:form>

</body>
</html>