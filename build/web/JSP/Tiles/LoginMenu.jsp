<%-- 
    Document   : LoginMenu
    Created on : Mar 7, 2015, 5:56:22 PM
    Author     : 1250892
--%>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@page import="java.util.Vector;"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<% Vector serviceList = (Vector) session.getAttribute("services");%>

<html>

    <head>
        <script type="text/javascript" src="js/jquery-2.1.4.js"></script> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: RMSA ::</title>
        <script type="text/javascript" src="<%=basePath%>js/BuildMenu.js"></script>
        <script type="text/javascript" src="<%=basePath%>js/MenuLayout.js"></script>

        <script type = "text/javascript">
            function preventBack() {
                window.history.forward();
            }
            setTimeout("preventBack()", 0);

            window.onunload = function() {
                null
            };

            document.onkeydown = showDown;

            function showDown(evt) {
                evt = (evt) ? evt : ((event) ? event : null);
                if (evt) {

                    if (event.keyCode == 116) {
                        // When F5 is pressed
                        cancelKey(evt);
                    }
                    else if (event.keyCode == 122) {
                        // When F11 is pressed
                        cancelKey(evt);
                    } else if (event.ctrlKey && (event.keyCode == 78 || event.keyCode == 82)) {
                        // When ctrl is pressed with R or N
                        cancelKey(evt);
                    }
                    else if (event.altKey && event.keyCode == 37) {
                        // stop Alt left cursor
                        return false;
                    }
                }
            }

            function cancelKey(evt) {
                if (evt.preventDefault) {
                    evt.preventDefault();
                    return false;
                }
                else {
                    evt.keyCode = 0;
                    evt.returnValue = false;
                }
            }


        </script>
        <style>
            .jqueryslidemenu{
                padding-top: 6px;
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right" >
            <tr>
                <td style="background:#000;text-indent:5px;"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 

                        <tr> 
                            <td class="yborder"></td> 
                        </tr> 
                        <tr> 
                            <td height="28" align="left" valign="top" style=" height:33px;"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center"> 
                                    <tr> 
                                        <td  align="left" valign="top">       
                                            <div  id="myslidemenu" class="jqueryslidemenu"> 

                                                <%
                                                    if (serviceList != null) {
                                                        out.println("<script LANGUAGE=\"JavaScript\">");
                                                        for (int i = 0; i < serviceList.size(); i++) {
                                                            String servicedesc[] = (String[]) serviceList.elementAt(i);
                                                            String serviceId = servicedesc[0];
                                                            String parentId = servicedesc[1];
                                                            String target_url = servicedesc[2];
                                                            String service_name = servicedesc[3];
                                                            out.println("AddMenuItem (" + serviceId + ", " + parentId + ", \"" + target_url + "\", \"" + service_name + "\", \"\");");
                                                        }
                                                    } else {
                                                        //out.println("could not get services");
                                                        response.sendRedirect("./unAuthorised.do");
                                                    }
                                                    out.println("</script>");
                                                %>
                                                <script LANGUAGE="JavaScript" type="text/javascript">
                                                    DrawMenu();
                                                </script>
                                        </td>
                                    </tr> 
                                </table> 
                            </td> 
                        </tr> 
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
