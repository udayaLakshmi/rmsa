/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithOutLogin.Action;

import com.aponline.rmsa.WithOutLogin.Dao.RtaActDao;
import com.aponline.rmsa.WithOutLogin.Form.RtiForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.commons.CommonDetails;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.servlet.ServletOutputStream;

/**
 *
 * @author 1250892
 */
public class RtaAction extends DispatchAction {

    String target = "success";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RtaActDao rtadao = new RtaActDao();
        HttpSession session = request.getSession();
        try {
            ArrayList list = (ArrayList) rtadao.getRtaDetails();
            System.out.println("list " + list.size());
            request.setAttribute("myList", list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward viewFile(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String rowId = null;
        try {

            if (request.getParameter("filename") != null && request.getParameter("filename").toString().length() > 0) {
                String filename = request.getParameter("filename");
                BufferedInputStream in = null;
                File dir = new File("D:\\Department\\SChoolEducation\\RMSA\\RTA\\" + filename + ".pdf");
                FileInputStream fin = new FileInputStream(dir);
                in = new BufferedInputStream(fin);
                response.setContentType("application/pdf");
                ServletOutputStream out = response.getOutputStream();
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();

            } else {
                request.setAttribute("msg", "Selected Go Not Available");
            }
        } catch (FileNotFoundException e) {
            request.setAttribute("errorMsg", " >>>>>>>>>>>>> Requested Document Was Not Available. <<<<<<<<<<<<<");
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward downloadStatisticstransfers(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (request.getParameter("filename") != null && request.getParameter("filename").toString().length() > 0) {
            String filename = (String) request.getParameter("filename");
            request.setAttribute("filename", filename);
            unspecified(mapping, form, request, response);
        }
        return mapping.findForward("transfers");
    }
}
