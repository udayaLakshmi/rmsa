/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.loginaction;

//import apol.NewsEvents.service.NewsEventsService;
//import apol.NewsEvents.serviceFactory.NewsEventsFactory;
//import apol.NewsEvents.dto.NewsEventsDTO;
//import apol.login.dao.LoginDAO;
//import apol.login.form.LoginForm;
//import apol.form.MastersForm;
import com.aponline.rmsa.WithLogin.Dao.ChangePasswordDao;
import com.aponline.rmsa.WithLogin.Form.ChangePasswordForm;
import com.aponline.rmsa.logindao.LoginDAO;
import com.aponline.rmsa.loginforms.LoginForm;
import com.aponline.rmsa.login.service.LoginService;
import com.aponline.rmsa.login.servicefactory.LoginServiceFactory;
import java.io.PrintWriter;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.w3c.dom.Element;
import java.util.Random;
import org.w3c.dom.Node;
import org.w3c.dom.CharacterData;
import java.io.*;
import java.util.ArrayList;

/**
 * This class is for checking the login details and getting the services to user
 * based on logins and roles and session management.
 *
 * @author 1250892
 */
public class LoginAction extends DispatchAction {

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        try {
            String lastUpdatedDate = null;
            ArrayList list = new ArrayList();
            LoginService loginService = LoginServiceFactory.getLoginServiceImpl();
            LoginForm loginForm = (LoginForm) form;
            LoginDAO loginDAO = new LoginDAO();
            if (lastUpdatedDate != null && lastUpdatedDate.length() > 0) {
                request.setAttribute("lastUpdatedDate", lastUpdatedDate);
            }
            //END

            String path1 = null;
            String sourcepath = null;
            HttpSession session = request.getSession(true);
            // festival start
            // FestivalDAO festivalDAO = new FestivalDAO();
            // sourcepath = festivalDAO.getFestivalImage();
            if (sourcepath != null && sourcepath.length() > 0) {
                path1 = sourcepath.split("#")[0];
                String file = sourcepath.split("#")[1];
                request.setAttribute("file", file);
                File targetDir = null;
                File sourceDir = null;
                String uploadPath = getServlet().getServletContext().getRealPath("/") + "Festival";
                targetDir = new File(uploadPath);
                if (targetDir.exists()) {
                    String[] entries = targetDir.list();
                    for (String s : entries) {
                        File currentFile = new File(targetDir.getPath(), s);
                        currentFile.delete();
                    }
                    targetDir.delete();
                }
                if (!targetDir.exists()) {
                    targetDir.mkdir();
                }
                sourceDir = new File(path1);
                // festivalDAO.copyDirectory(sourceDir, targetDir);
                request.setAttribute("popup", "popup");
                // GETTING THE NEW AND EVENTS DETAILS FOR DISPLAYING ON HOME PAGE
            }
            String flag = loginService.getPasswordReset(loginForm);
            if (flag.equalsIgnoreCase("N")) {
                target = "changePasswordRMSA";
                request.setAttribute("uname", loginForm.getUserName());
            } else {
                target = "success";
            }
            //festival end
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward checkDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LoginService loginService = LoginServiceFactory.getLoginServiceImpl();
        LoginForm loginForm = (LoginForm) form;
        Vector resultList = new Vector();
        int loginDetails = 0;
        String target = "success";
        HttpSession session = null;
        String color = null;
        String flag = null;
        LoginDAO dao = new LoginDAO();
        String password = null;
        try {
            session = request.getSession();
            request.setAttribute("login", "login");
            if (loginForm.getUserName() != null && loginForm.getPassword() != null
                    && loginForm.getUserName().length() > 0 && loginForm.getPassword().length() > 0) {  // If the username and password fields are not empty
//                session.removeAttribute("uname");
//                session.removeAttribute("pwd");
//                session.setAttribute("uname", loginForm.getUserName());
//                session.setAttribute("pwd", loginForm.getPassword());
                loginDetails = loginService.checkLoginDetails(loginForm); // Check login details
                flag = loginService.getPasswordReset(loginForm);
                if (loginDetails != 0 && flag.equalsIgnoreCase("N")) {
                    target = "changePasswordRMSA";
                    request.setAttribute("Username", loginForm.getUserName());
                } else if (loginDetails != 0 && flag.equalsIgnoreCase("Y")) {        // If login success
                    //sessionChecking(mapping, form, request, response); // check session
                    session.invalidate();
                    session = request.getSession(true); // Create The new Session
                    loginForm.setLoginStatus("Success");
                    synchronized (request) {
                        //loginService.insertAccessInformation(loginForm, request);
                    }
                    loginForm.setColor(color);  // Set color theme to session in DAO
                    loginService.getUserInfo(loginForm, request); // Getting user info

                    /*  GETTING SERVICES FOR LOGGED USER */

                    resultList = loginService.getUserServices(loginForm, request); // Getting Services based on role
                    if (resultList.size() > 0) {                // If services found
                        session.setAttribute("services", resultList);
                        // loginService.storeLoginStatus(session.getAttribute("RoleId").toString(), session.getAttribute("userName").toString(), "Login");
                        target = "welcomePage";
                    } else {
                        target = "noServices";     // If the services not found page will redirected to "NOServices" error page
                    }

                } else {
                    request.setAttribute("msg", "Invalid UserName / Password");
                    loginForm.setLoginStatus("Fail");
                    //loginService.insertAccessInformation(loginForm, request);
                    loginForm.setUserName("");
                    loginForm.setPassword("");
                    target = "failure";
                }
//                    }


            } else {
                request.setAttribute("msg", "Invalid UserName / Password");
                target = "failure";
            }
            if (session.getAttribute("RoleId") != null) {
                if (session.getAttribute("RoleId").toString().equals("4") || session.getAttribute("RoleId").toString().equals("3")) {
                    if (session.getAttribute("districtid").toString() != null) {
                        loginForm.setDistCode(session.getAttribute("districtid").toString());
                    }
                    session.setAttribute("pdwelcomeRole", "pdwelcomeRole");
                } else if (session.getAttribute("RoleId").toString().equals("5")) {
                    ArrayList list = loginService.getDirectorDetails(loginForm);
                    session.setAttribute("dirfetchDetails", list);
                    session.setAttribute("dirwelcomeRole", "dirwelcomeRole");
                } else if (session.getAttribute("RoleId").toString().equals("2")) {
                    if (session.getAttribute("districtid").toString() != null) {
                        loginForm.setDistCode(session.getAttribute("districtid").toString());
                    }
                    if (session.getAttribute("mandalid").toString() != null) {
                        loginForm.setMandalCode(session.getAttribute("mandalid").toString());
                    }
                    ArrayList list = loginService.getMpdodetails(loginForm);
                    session.setAttribute("mpdofetchDetails", list);
                    session.setAttribute("mpdoRole", "mpdoRole");
                    request.setAttribute("districtId", session.getAttribute("districtid").toString());
                    request.setAttribute("mandalId", session.getAttribute("mandalid").toString());
                    if (session.getAttribute("userName") != null) {
                        request.setAttribute("loginId", session.getAttribute("userName").toString());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward sessionChecking(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String SE = null;
        SE = request.getParameter("loginStatus");
        if (SE != null && !SE.equalsIgnoreCase("")) {
            SE = SE.substring(SE.indexOf(".(") + 2, SE.lastIndexOf(")."));
            try {
                if (!SE.equals(request.getSession(false).getId())) {
                    request.getSession(false).invalidate();
                    request.getSession(true);
                    request.getRequestDispatcher("/403.jsp").forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapping.findForward("success");
    }

    public ActionForward changePassword(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        LoginService loginService = LoginServiceFactory.getLoginServiceImpl();
        LoginForm loginForm = (LoginForm) form;
        String changedStatus = null;
        HttpSession session = null;
        session = request.getSession(true);

        if (session.getAttribute("captchaCodeForCPW").equals(request.getParameter("captchaCode"))) {
            loginForm.setUserName(session.getAttribute("userName").toString());
            changedStatus = loginService.changePassword(loginForm);
            if (changedStatus != null) {
                request.setAttribute("msg", changedStatus);
                loginForm.setOldPassword("");
                loginForm.setNewPassword("");
                loginForm.setConfirmPassword("");
            }
        } else {
            request.getRequestDispatcher("./403.jsp").forward(request, response);
        }
        return mapping.findForward("success");
    }

    public ActionForward getEncryptPwd(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String encPwd = null;
        PrintWriter out = null;
        try {
            response.setHeader("Cache-Control", "private");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setContentType("text/xml");
            out = response.getWriter();
            if (request.getParameter("encString") != null && request.getParameter("encString").length() > 0) {
                if (encPwd != null && encPwd.length() > 0) {
                    out.println(encPwd);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCharacterDataFromElement(Element e) {
        if (e != null && e.toString().length() > 0) {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        }
        return "";
    }

    public ActionForward disFilesList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ArrayList list = new ArrayList();
        try {

            LoginService loginService = LoginServiceFactory.getLoginServiceImpl();
            LoginForm loginForm = (LoginForm) form;
            if (request.getParameter("type") != null && request.getParameter("type").equalsIgnoreCase("disb")) {
                request.setAttribute("dispdfList", "disbursePdf");
            } else if (request.getParameter("type") != null && request.getParameter("type").equalsIgnoreCase("prnt")) {
                request.setAttribute("prntpdfList", "printPdf");
            } else if (request.getParameter("type") != null && request.getParameter("type").equalsIgnoreCase("coldis")) {
                if (request.getParameter("distcode") != null) {
                    loginForm.setDistCode(request.getParameter("distcode"));
                    loginForm.setFile_type(Integer.parseInt(request.getParameter("file_type")));
                    list = loginService.getCollectorApprovalDetails(loginForm);
                    request.setAttribute("discolapr", "discolappr");
                    request.setAttribute("clist", list);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapping.findForward("docList");
    }

    public ActionForward changePasswordReset(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LoginForm loginForm = (LoginForm) form;
        String chnageDetails = null;
        LoginDAO loginDAO = new LoginDAO();
        String target = "success";
        HttpSession session = null;
        try {
            String path1 = null;
            String code = "";
            String codeGen = "";
            String randomInt = null;
            Random randomGenerator = new Random();
            session = request.getSession(true);
            for (int idx = 1; idx <= 5; ++idx) {
                randomInt = Integer.toHexString(randomGenerator.nextInt(6));
                code = code + "" + randomInt;
                codeGen = codeGen + " " + randomInt;
            }
            if (code != null) {
                request.setAttribute("captchaCode", codeGen);
                session.setAttribute("captcha", code);
            }
            chnageDetails = loginDAO.changePasswordForReset(loginForm);
            if (chnageDetails != null) {
                request.setAttribute("msg", chnageDetails);
                loginForm.setConfirmPassword("");
                loginForm.setNewPassword("");
                loginForm.setOldPassword("");
            } else {
                request.setAttribute("msg", "Error in Change Password");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getUsernamePassword(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList list = new ArrayList();
        LoginForm loginForm = (LoginForm) form;
        LoginDAO loginDAO = new LoginDAO();
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("districtid") != null) {
                loginForm.setDistCode(session.getAttribute("districtid").toString());
            }
            list = loginDAO.getUserNamePasswordDeailsDetails(loginForm);
            if (list != null) {
                request.setAttribute("upList", list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("userNamePwd");
    }

    public ActionForward gotoHome(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = null;
        LoginForm loginForm = (LoginForm) form;
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("userName") != null) {
                target = "welcomePage";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward logout(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        try {
            session.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("logout");
    }

    public ActionForward getDashBoardData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = null;
        LoginForm loginForm = (LoginForm) form;
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("userName") != null) {
                target = "welcomePage";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward updatePassword(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String password = null;
        String target = null;
        String password1 = null;
        HttpSession session = request.getSession();
        ChangePasswordDao mydao = new ChangePasswordDao();
        LoginForm formBean = (LoginForm) form;
        try {
            if (formBean.getUserName() != null) {

                String userName = formBean.getUserName().toString();
                formBean.setUserName(userName);

                //Changing updated pasword as encreption
                if (formBean.getUpdatePassword() != null && formBean.getUpdatePassword().length() > 0) {
                    password = formBean.getUpdatePassword().substring(5, formBean.getUpdatePassword().length() - 5);
                }
                String updateEncrepted = mydao.passwordEncrypt(password);
                formBean.setUpdatePassword(updateEncrepted);

                //Changing current pasword as encreption
                if (formBean.getCurrentPasswordEncreption() != null && formBean.getCurrentPasswordEncreption().length() > 0) {
                    password1 = formBean.getCurrentPasswordEncreption().substring(5, formBean.getCurrentPasswordEncreption().length() - 5);
                }
                String currentEncrepted = mydao.passwordEncrypt(password1);
                formBean.setCurrentPasswordEncreption(currentEncrepted);

                //Updating password
                int myreturn = mydao.passwordReset(formBean);

                if (myreturn == 0) {
                    target = "changePasswordRMSA";
                    request.setAttribute("SuccessMsg", "Current Password didn't match please try again");
                    request.setAttribute("colour", "red");
                } else if (myreturn == 1) {
                    target = "success";
                    request.setAttribute("SuccessMsg", "Password updated successfully");
                    request.setAttribute("colour", "#0E94C5");
                } else {
                    target = "changePasswordRMSA";
                    request.setAttribute("SuccessMsg", "Password updated failed please try again");
                    request.setAttribute("colour", "red");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}