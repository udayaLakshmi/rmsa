/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.login.service;

import com.aponline.rmsa.loginforms.LoginForm;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 1250892
 */
public interface LoginService {

    public Vector getUserServices(LoginForm loginForm, HttpServletRequest request) throws Exception;

    public ArrayList getUserInfo(LoginForm loginForm, HttpServletRequest request) throws Exception;

    public int checkUserAttempts(LoginForm loginForm) throws Exception;

    public int blockUserAccount(LoginForm loginForm) throws Exception;

    public int checkLoginDetails(LoginForm loginForm) throws Exception;

    public void insertAccessInformation(LoginForm loginForm, HttpServletRequest request) throws Exception;

    public String changePassword(LoginForm loginForm) throws Exception;

    // public void storeLoginStatus(String roleId, String userName, String logStatus) throws Exception;
    public ArrayList getDirectorDetails(LoginForm loginForm) throws Exception, SQLException;

    public ArrayList getCollectorApprovalDetails(LoginForm loginForm) throws Exception, SQLException;

//    public ArrayList getFTODetails(LoginForm loginForm) throws Exception, SQLException;
//    
//    public ArrayList storeFtrdetails(LoginForm loginForm) throws Exception, SQLException;
    public ArrayList getPDdetails(LoginForm loginForm) throws Exception, SQLException;

    public ArrayList getMpdodetails(LoginForm loginForm) throws Exception, SQLException;

    public String getPasswordReset(LoginForm LoginForm) throws Exception, SQLException;
}
