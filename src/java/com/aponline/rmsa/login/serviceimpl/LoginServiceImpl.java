/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.login.serviceimpl;

//import apol.dao.FtoDetailsDAO;
import com.aponline.rmsa.logindao.LoginDAO;
import com.aponline.rmsa.loginforms.LoginForm;
import com.aponline.rmsa.login.service.LoginService;
//import apol.utils.Exception;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 747577
 */
public class LoginServiceImpl implements LoginService {

    /**
     * This method is for getting the userservices based on login
     * @param loginForm
     * @param request
     * @return
     * @throws Exception
     */
    public Vector getUserServices(LoginForm loginForm, HttpServletRequest request) throws Exception {
        Vector userServices = new Vector();
        try {
        
            LoginDAO loginDAO = new LoginDAO();
        userServices = loginDAO.getUserServices(loginForm, request);
        } catch (Exception e) {
        }
        return userServices;
    }

    /**
     * This method is for getting the user information and placed into session Object which is required for entire application
     * @param loginForm
     * @param request
     * @return ArrayList
     * @throws Exception
     */
    public ArrayList getUserInfo(LoginForm loginForm, HttpServletRequest request) throws Exception {
        ArrayList userInfo = new ArrayList();
        LoginDAO loginDAO = new LoginDAO();
        userInfo = loginDAO.getUserInfo(loginForm, request);
        return userInfo;
    }

    /**
     * This method is capture the number of login attempts to ccla application
     * @param loginForm
     * @return int
     * @throws Exception
     */
    public int checkUserAttempts(LoginForm loginForm) throws Exception {
        int userAttempts = 0;
        LoginDAO loginDAO = new LoginDAO();
        userAttempts = loginDAO.checkUserAttempts(loginForm);
        return userAttempts;
    }

    /**
     * This method is user for blocking the user account if the user attempts more then 3 times.
     * @param loginForm
     * @return blockUserStatus
     * @throws Exception
     */
    public int blockUserAccount(LoginForm loginForm) throws Exception {
        int blockUserAccount = 0;
        LoginDAO loginDAO = new LoginDAO();
        blockUserAccount = loginDAO.blockUserAccount(loginForm);
        return blockUserAccount;
    }

    /**
     * This method is validate weather the login name and passwords are same or not.
     * @param loginForm
     * @return int
     * @throws Exception
     */
    public int checkLoginDetails(LoginForm loginForm) throws Exception {
        int checkLogin = 0;
        LoginDAO loginDAO = new LoginDAO();
        checkLogin = loginDAO.checkLoginDetails(loginForm);
        return checkLogin;
    }

    /**
     * This method is for track the website access information
     * @param loginForm
     * @param request
     * @throws Exception
     */
    public void insertAccessInformation(LoginForm loginForm, HttpServletRequest request) throws Exception {
        try {
            LoginDAO loginDAO = new LoginDAO();
            loginDAO.insertAccessInformation(loginForm, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is for changing the password
     * @param loginForm
     * @return
     * @throws Exception
     */
    public String changePassword(LoginForm loginForm) throws Exception {
        LoginDAO loginDAO = new LoginDAO();
        String changedStatus = null;
        changedStatus = loginDAO.changePassword(loginForm);
        return changedStatus;
    }
    
//     public void storeLoginStatus(String roleId, String userId, String logStatus) throws Exception {
//        LoginDAO loginDAO = new LoginDAO();
//        loginDAO.storeLoginStatus(roleId, userId, logStatus);
//    }

    public ArrayList getDirectorDetails(LoginForm loginForm) throws Exception, SQLException{
        ArrayList directorDetails = new ArrayList();
       try{
           LoginDAO loginDAO = new LoginDAO();       
           directorDetails =loginDAO.getDirectorDetails(loginForm);
        }catch(Exception e){
            e.printStackTrace();
        }
        return directorDetails ;
    }
    public ArrayList getCollectorApprovalDetails(LoginForm loginForm) throws Exception, SQLException{
        ArrayList list = new ArrayList();
       try{
           LoginDAO loginDAO = new LoginDAO();       
           list =loginDAO.getCollectorApprovalDetails(loginForm);
        }catch(Exception e){
            e.printStackTrace();
        }
        return list ;
    }
//    public ArrayList getFTODetails(LoginForm loginForm) throws Exception, SQLException{
//        ArrayList list = new ArrayList();
//        try{
//            FtoDetailsDAO ftoDetailsDAO = new FtoDetailsDAO();
//            list = ftoDetailsDAO.getFTODetails(loginForm);
//           }catch(Exception e){
//               e.printStackTrace();
//           }
//        return list;
//    }
//     public ArrayList storeFtrdetails(LoginForm loginForm) throws Exception, SQLException{
//        ArrayList list = new ArrayList();
//        try{
//            FtoDetailsDAO ftoDetailsDAO = new FtoDetailsDAO();
//            list = ftoDetailsDAO.storeFtrdetails(loginForm);
//           }catch(Exception e){
//               e.printStackTrace();
//           }
//        return list;
//    }
    public ArrayList getPDdetails(LoginForm loginForm) throws Exception, SQLException{
        ArrayList pdDetails = new ArrayList();
       try{
           LoginDAO loginDAO = new LoginDAO();       
           pdDetails =loginDAO.getPDdetails(loginForm);
        }catch(Exception e){
            e.printStackTrace();
        }
        return pdDetails ;
    }
    
    public ArrayList getMpdodetails(LoginForm loginForm) throws Exception, SQLException {
        ArrayList list = new ArrayList();
        try {
            LoginDAO loginDAO = new LoginDAO();
            list = loginDAO.getMpdodetails(loginForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public String getPasswordReset(LoginForm LoginForm) throws Exception, SQLException {
       
        LoginDAO loginDAO = new LoginDAO();
        String changedStatus = null;
        changedStatus = loginDAO.getPasswordReset(LoginForm);
        return changedStatus;
    }
}
