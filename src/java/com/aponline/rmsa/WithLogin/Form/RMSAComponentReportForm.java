/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author 1250881
 */
public class RMSAComponentReportForm extends ActionForm {

    private String mode;
    private String year;
    private String year1;
    private ArrayList yearList = new ArrayList();
    private ArrayList mgmtList = new ArrayList();
    private String management;
    private String management1;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public ArrayList getYearList() {
        return yearList;
    }

    public void setYearList(ArrayList yearList) {
        this.yearList = yearList;
    }

    public ArrayList getMgmtList() {
        return mgmtList;
    }

    public void setMgmtList(ArrayList mgmtList) {
        this.mgmtList = mgmtList;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getYear1() {
        return year1;
    }

    public void setYear1(String year1) {
        this.year1 = year1;
    }

    public String getManagement1() {
        return management1;
    }

    public void setManagement1(String management1) {
        this.management1 = management1;
    }
    
    

    
    
}
