package com.aponline.rmsa.WithLogin.Form;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 1250892
 */
public class ChangePasswordForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String userName;
    private String currentPassword;
    private String newPassword;
    private String confirmPassword;
    private String updatePassword;
    private String currentPasswordEncreption;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(String updatePassword) {
        this.updatePassword = updatePassword;
    }

    public String getCurrentPasswordEncreption() {
        return currentPasswordEncreption;
    }

    public void setCurrentPasswordEncreption(String currentPasswordEncreption) {
        this.currentPasswordEncreption = currentPasswordEncreption;
    }
    
    
}
