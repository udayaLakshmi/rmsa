package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.commons.CommonDetails;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;




public class RMSAAction extends DispatchAction {

    public RMSAAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        String target = "success";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                //Get distict year list
                String userName = session.getAttribute("userName").toString();
                 ArrayList phaseList = rmsaDAO.getPhaseList(userName);
                 myform.setPhaseList(phaseList);
                 target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
    
    
    public ActionForward getData(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        HashMap map = null;
        ArrayList list = null;
        ArrayList list1 = null;
        String target = "success";
        String schoolcode = null;
        String employeeName = null;
        String distname = null;
        String mandalname = null;
        String mgtname = null;
        RMSAForm myform = (RMSAForm) form;
        CommonDAO commonDAO = new CommonDAO();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                schoolcode = session.getAttribute("userName").toString();
                distname = commonDAO.getDistrictname(schoolcode.substring(0, 4));
                mandalname = commonDAO.getMandalName(schoolcode.substring(0, 6));
                mgtname = commonDAO.getSchoolMgmtName(schoolcode);
                myform.setSchoolId(schoolcode);
                //Get RMSA Master data
                list = (ArrayList) rmsaDAO.getRMSAMasterBySchoolCode(myform, schoolcode);
                if (list.size() > 0) {
                    map = (HashMap) list.get(0);
                    myform.setUserName(schoolcode);
                    request.setAttribute("schoolName", map.get("schoolName").toString());
                    myform.setSchoolId(map.get("schoolName").toString());
                    myform.setArtCraftSanction(map.get("phyartCraftRoom").toString());
                    myform.setComputerRoomSanction(map.get("phycompRoom").toString());
                    myform.setScienceLabSanction(map.get("phyScienceLap").toString());
                    myform.setLibraryRoomSanction(map.get("phyLibray").toString());
                    myform.setAdditionalClassRoomSanction(map.get("phyACR").toString());
                    myform.setToiletBlockSanction(map.get("phyToilet").toString());
                    myform.setDrinkingWaterSanction(map.get("phyDrinkingWater").toString());
                    if (map.get("artCraftStageOfProgress") != null) {
                        myform.setArtCraftStageOfProgress(map.get("artCraftStageOfProgress").toString());
                    }
                    if (map.get("computerRoomStageOfProgress") != null) {
                        myform.setComputerRoomStageOfProgress(map.get("computerRoomStageOfProgress").toString());
                    }
                    if (map.get("scienceLabStageOfProgress") != null) {
                        myform.setScienceLabStageOfProgress(map.get("scienceLabStageOfProgress").toString());
                    }
                    if (map.get("libraryRoomStageOfProgress") != null) {
                        myform.setLibraryRoomStageOfProgress(map.get("libraryRoomStageOfProgress").toString());
                    }
                    if (map.get("additionalClassRoomStageOfProgress") != null) {
                        myform.setAdditionalClassRoomStageOfProgress(map.get("additionalClassRoomStageOfProgress").toString());
                    }
                    if (map.get("toiletBlockStageOfProgress") != null) {
                        myform.setToiletBlockStageOfProgress(map.get("toiletBlockStageOfProgress").toString());
                    }
                    if (map.get("drinkingWaterStageOfProgress") != null) {
                        myform.setDrinkingWaterStageOfProgress(map.get("drinkingWaterStageOfProgress").toString());
                    }
                    if (map.get("labEquipmentStageOfProgress") != null) {
                        myform.setLabEquipmentStageOfProgress(map.get("labEquipmentStageOfProgress").toString());
                    }
                    if (map.get("furnitureStageOfProgress") != null) {
                        myform.setFurnitureStageOfProgress(map.get("furnitureStageOfProgress").toString());
                    }
                    if (map.get("totalFurnitureInLaks") != null) {
                        myform.setTotalFurniture(map.get("totalFurnitureInLaks").toString());
                    }
                    if (map.get("totalCivilSanction") != null) {
                        myform.setAdministrativeSanction(map.get("totalCivilSanction").toString());
                    }
                    if (map.get("furnitureAndLabEquipment") != null) {
                        myform.setFurnitureAndLabEquipment(map.get("furnitureAndLabEquipment").toString());
                    }
                    if (map.get("totalSanctioned") != null) {
                        myform.setTotalSanctioned(map.get("totalSanctioned").toString());
                    }
                    if (map.get("tenderCostValue") != null) {
                        myform.setTenderCostValue(map.get("tenderCostValue").toString());
                    }
                    if (map.get("vat") != null) {
                        myform.setVat(map.get("vat").toString());
                    }
                    if (map.get("departemntCharges") != null) {
                        myform.setDepartemntCharges(map.get("departemntCharges").toString());
                    }
                     if (map.get("releases") != null) {
//                        myform.setReleases(myform.getRelease().toString());//reshma
                        myform.setReleases(map.get("releases").toString());//reshma
//                        myform.setReleases(map.get("releases").toString());//reshma
                    }
//                    if (map.get("releases") != null) {
//                        myform.setReleases(map.get("releases").toString());
//                    }
                    if (map.get("estimatedCost") != null) {
                        myform.setEstimatedCost(map.get("estimatedCost").toString());
                    }
                    if (map.get("totalEligibleCost") != null) {
                        myform.setTotalEligibleCost(map.get("totalEligibleCost").toString());
                    }
                    if (map.get("balanceToBeReleased") != null) {
                        myform.setBalanceToBeReleased(map.get("balanceToBeReleased").toString());
                    }
                    String totalEC = map.get("totalEligibleCost").toString();
                    String rel = map.get("releases").toString();
                    BigDecimal d1 = BigDecimal.valueOf(Double.parseDouble(totalEC));
                    BigDecimal d2 = BigDecimal.valueOf(Double.parseDouble(rel));
                    BigDecimal d3 = d1.subtract(d2);
                    myform.setBalanceToBeReleased(d3.toString());
                    if (map.get("expenditure") != null) {
                        myform.setExpenditureIncurred(map.get("expenditure").toString());
                    }
                    String expenditure = map.get("expenditure").toString();
                    BigDecimal d4 = BigDecimal.valueOf(Double.parseDouble(expenditure));
                    BigDecimal d5 = d2.subtract(d4);
                    myform.setBalanceAvailable(d5.toString());
                    request.setAttribute("RMSAMaster", list);

                    //reshma 
                    myform.setPhaseNo(map.get("phaseNo").toString());
                    myform.setYear(map.get("year").toString());
                    session.setAttribute("year", myform.getYear());

                    //omkar
                    ArrayList releaseMasterList = rmsaDAO.getReleaseMaster(schoolcode, map.get("phaseNo").toString());
                    if (releaseMasterList.size() > 0) {
                        request.setAttribute("releaseMasterList", releaseMasterList);
                    }

                    releaseMasterList = null;
                } else {
                    Map map1 = rmsaDAO.getSchoolDataBySchoolCode(schoolcode);
                    request.setAttribute("schoolName", map1.get("schoolName").toString());
                }
                //Get RMSA Cheque details
                list1 = (ArrayList) rmsaDAO.getRMSAChequeDetailsBySchoolCode(myform);
                ArrayList filesList = (ArrayList) rmsaDAO.getRMSAUcsFiledBySchoolCode(schoolcode, null, myform.getYear());
                request.setAttribute("RMSAChequeDetails", list1);
                request.setAttribute("filesList", filesList);
                request.setAttribute("schoolCode", schoolcode);
                request.setAttribute("distname", distname);
                request.setAttribute("mandalname", mandalname);
                request.setAttribute("mgtname", mgtname);
                request.setAttribute("phaseNo", myform.getPhaseNumber());
                request.setAttribute("year", myform.getYear());

                if (session.getAttribute("msg") != null) {
                    request.setAttribute("msg", session.getAttribute("msg"));
                    session.removeAttribute("msg");
                }
                if (session.getAttribute("result") != null) {
                    request.setAttribute("result", session.getAttribute("result"));
//                 session.setAttribute("result", result);
//                    request.setAttribute("msg",session.getAttribute("msg"));
                    session.removeAttribute("result");

                }
                
                ArrayList phaseList = rmsaDAO.getPhaseList(schoolcode);
                myform.setPhaseList(phaseList);
                myform.setPhaseNumber(myform.getPhaseNumber());

            } else {
                response.sendRedirect("officialLogin.do");
                target = "failure";
            }
            // target = "failure";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward RmsaSubmit(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String schoolCode = null;
        String target = "success";
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        String url = getServlet().getServletContext().getRealPath("/");
        boolean flag1 = false;
        String folderName = null;
        String fileName = null;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                String year = (String) session.getAttribute("year");
                myform.setYear(year);
                schoolCode = session.getAttribute("userName").toString();
                String[] chequeNumber = request.getParameterValues("ChequeNumber");
                String[] chequeDate = request.getParameterValues("ChequeDate");
                String[] chequeAmount = request.getParameterValues("ChequeAmount");
                String[] ucsAmount = request.getParameterValues("UCSAmount");
                myform.setUCSAmount(ucsAmount);
                myform.setChequeNumber(chequeNumber);
                myform.setChequeDate(chequeDate);
                myform.setChequeAmount(chequeAmount);
                myform.setUserName(schoolCode);
                String phno = myform.getPhaseNo();
                myform.setArtCraftStageOfProgress(myform.getArtCraftStageOfProgress1());
                myform.setComputerRoomStageOfProgress(myform.getComputerRoomStageOfProgress1());
                myform.setScienceLabStageOfProgress(myform.getScienceLabStageOfProgress1());
                myform.setLibraryRoomStageOfProgress(myform.getLibraryRoomStageOfProgress1());
                myform.setAdditionalClassRoomStageOfProgress(myform.getAdditionalClassRoomStageOfProgress1());
                myform.setToiletBlockStageOfProgress(myform.getToiletBlockStageOfProgress1());
                myform.setDrinkingWaterStageOfProgress(myform.getDrinkingWaterStageOfProgress1());
                myform.setPhaseNo("Phase" + myform.getPhaseNo());
                int noofRecords = 0;
                if (!myform.getNoofRec().toString().equals("")) {
                    noofRecords = Integer.parseInt(myform.getNoofRec());
                }
                for (int i = 0; i < noofRecords; i++) {
                    if (noofRecords == 1) {
                        if (i == 0) {
                            i = 1;
                        }
                    }
                    if (myform.getKpiinsert("ucsFileUpload" + i) != null && myform.getKpiinsert("ucsFileUpload" + i) != "") {
                        fileName = myform.getKpiinsert("ucsFileUpload" + i).toString();
                        if (!fileName.equalsIgnoreCase("null") && !fileName.equals("")) {
                            int dotPos = fileName.lastIndexOf(".");
                            if (!fileName.startsWith("[")) {
                                FormFile formfile1 = (FormFile) myform.getKpiinsert("ucsFileUpload" + i);
                                flag1 = rmsaDAO.uploadGosPhoto(formfile1, schoolCode, myform.getUrl(), myform.getPhaseNo());
                                if (flag1) {
                                    rmsaDAO.uploadUcsDetails(schoolCode, formfile1.getFileName().toString(), myform.getUCSAmount()[noofRecords - 1].toString(), myform);
                                }
                            } else {
                                ArrayList mylist = (ArrayList) myform.getKpiinsert("ucsFileUpload" + i);
                                int count = noofRecords - mylist.size();
                                for (int j = 0; j <= mylist.size() - 1; j++) {
                                    FormFile formfile = (FormFile) mylist.get(j);
                                    long filesize = formfile.getFileSize();
                                    long filesizeInKB = filesize / 1024;
                                    if ((filesizeInKB > 3 && filesizeInKB < 10240)) {
                                        flag1 = rmsaDAO.uploadGosPhoto(formfile, schoolCode, myform.getUrl(), myform.getPhaseNo());
                                        if (flag1) {
                                            rmsaDAO.uploadUcsDetails(schoolCode, formfile.getFileName().toString(), myform.getUCSAmount()[count++].toString(), myform);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                myform.setUrl(getServlet().getServletContext().getRealPath("/"));
                myform.setPhaseNo(phno);
                phno = null;
                rmsaDAO.updateRMSAMasterBySchoolCode(myform, request);
//                rmsaDAO.updateRMSAChequeDetailsBySchoolCode(myform, request);
                String result = rmsaDAO.updateRMSAChequeDetailsBySchoolCode(myform, request);

                if (result != null) {
                    request.setAttribute("result", result);
                    session.setAttribute("result", result);
//                  session.removeAttribute("result");
//                   request.removeAttribute("result");
                } else {
                    request.setAttribute("result", "Insertion Failed");
                }

//                session.removeAttribute("msg");
                request.setAttribute("msg", "<font color=\"green\">Details updated Successfully</font>");
                session.setAttribute("msg", "<font color=\"green\">Details updated Successfully</font>");
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("rmsaSchool.do");
        //unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward deleteUcsFile(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                if (request.getParameter("schoolName") != null && request.getParameter("sno") != null) {
                    String schoolName = request.getParameter("schoolName").toString();
                    String sNo = request.getParameter("sno").toString();
                    RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                    rmsaDAO.deleteUcsFile(schoolName, sNo);
                }
            } else {
                response.sendRedirect("officialLogin.do");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ActionForward deleteCheque(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                if (request.getParameter("schoolName") != null && request.getParameter("sno") != null) {
                    String schoolName = request.getParameter("schoolName").toString();
                    String sNo = request.getParameter("sno").toString();
                    RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                    rmsaDAO.deleteCheque(schoolName, sNo);
                }
            } else {
                response.sendRedirect("officialLogin.do");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ActionForward RmsaDelete(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String schoolcode = null;
        String target = "success";
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        String url = getServlet().getServletContext().getRealPath("/");
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                schoolcode = session.getAttribute("userName").toString();
                String[] ChequeNumber = request.getParameterValues("ChequeNumber");
                String[] ChequeDate = request.getParameterValues("ChequeDate");
                String[] ChequeAmount = request.getParameterValues("ChequeAmount");
                myform.setChequeNumber(ChequeNumber);
                myform.setChequeDate(ChequeDate);
                myform.setChequeAmount(ChequeAmount);
                myform.setUserName(schoolcode);
                myform.setUrl(getServlet().getServletContext().getRealPath("/"));
                rmsaDAO.deleteRMSAChequeDetailsBySchoolCode(myform, request);
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return (mapping.findForward(target));
    }

    public ActionForward downloadFile(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RMSAForm myform = (RMSAForm) form;
        String folderName = null;
        String fileName = null;
        String target = "success";
        fileName = myform.getUserName().toString();
        folderName = myform.getUcsSubmitted().toString();
        String Phase = "Phase" + myform.getPhaseNo();
           CommonConstants conistants = new CommonConstants();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=" + folderName);
                ServletOutputStream stream = response.getOutputStream();
                BufferedInputStream fif =
                        new BufferedInputStream(new FileInputStream(CommonDetails.getDrivePath() + conistants.RMSA_Path + "\\" + fileName.substring(0, 11) + "//\\//" + Phase + "//\\//" + folderName), 165535);
                int data;
                while ((data = fif.read()) != -1) {
                    stream.write(data);
                }
                fif.close();
                stream.flush();
                stream.close();
                target = "success";
            } else {
                target = "sessionExp";
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }
    static final int BUFFER = 2048;

    public ActionForward downloadDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        int insertDetails = 0;
        String[] details = null;
        byte[] buffer = new byte[1024];
           CommonConstants conistants = new CommonConstants();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=" + "kpipics.zip");
                ServletOutputStream stream = response.getOutputStream();
                BufferedInputStream fif =
                        new BufferedInputStream(new FileInputStream(CommonDetails.getDrivePath() + conistants.RMSA_Path + request.getParameter("schcode") + "\\" + request.getParameter("photopath")), 165535);
                int data;
                while ((data = fif.read()) != -1) {
                    stream.write(data);
                }
                fif.close();
                stream.flush();
                stream.close();
                target = "success";
            } else {
                target = "sessionExp";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}