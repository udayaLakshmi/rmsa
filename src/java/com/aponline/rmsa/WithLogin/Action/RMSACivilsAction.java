package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSACivilsDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class RMSACivilsAction extends DispatchAction {

    public RMSACivilsAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
//                CommonDAO commonDAO = new CommonDAO();
//                ArrayList distList = commonDAO.getDistrictDetails();
//                myform.setDistList(distList);
                RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
                ArrayList list = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list);
                if (session.getAttribute("RoleId").equals("3")) {
//                    myform.setDistrictId(session.getAttribute("districtId").toString());
//                    ArrayList mandalList = commonDAO.getMandalDetails(myform.getDistrictId());
//                    myform.setMandalList(mandalList);
                }
                //distList = null;
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

//    public ActionForward getDistrictList(ActionMapping mapping, ActionForm form,
//            HttpServletRequest request, HttpServletResponse response)
//            throws Exception {
//        String target = "failure";
//        RMSAForm myform = (RMSAForm) form;
//        try {
//            HttpSession session = request.getSession();
//            //session checking
//            if (session.getAttribute("userName") != null) {
//                RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
//                ArrayList districtList = (ArrayList) rmsaDAO.getDistrictList(myform.getPhaseNo().toString());
//                myform.setDistList(districtList);
//                districtList = null;
//                target = "success";
//            } else {
//                target = "sessionExp";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        unspecified(mapping, form, request, response);
//        return mapping.findForward(target);
//    }
//
//    public ActionForward getMandalList(ActionMapping mapping, ActionForm form,
//            HttpServletRequest request, HttpServletResponse response)
//            throws Exception {
//        String target = "failure";
//        RMSAForm myform = (RMSAForm) form;
//        try {
//            HttpSession session = request.getSession();
//            //session checking
//            if (session.getAttribute("userName") != null) {
//                RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
//                ArrayList mandalList = rmsaDAO.getMandalDetails(myform.getDistrictId(), myform.getPhaseNo());
//                myform.setMandalList(mandalList);
//                mandalList = null;
//                target = "success";
//            } else {
//                target = "sessionExp";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        getDistrictList(mapping, form, request, response);
//        unspecified(mapping, form, request, response);
//        return mapping.findForward(target);
//    }
    public ActionForward searchSchoolFunding(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        ArrayList list = null;
        HashMap map = null;
        RMSAForm myform = (RMSAForm) form;

        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
                list = (ArrayList) rmsaDAO.getRmsaMasterData(myform.getMandalId(), myform.getPhaseNo());
                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                ArrayList mandalList = rmsaDAO.getMandalDetails(myform.getDistrictId(), myform.getPhaseNo());
                myform.setMandalList(mandalList);
                ArrayList districtList = (ArrayList) rmsaDAO.getDistrictList(myform.getPhaseNo().toString());
                myform.setDistList(districtList);
                myform.setPhaseList(list1);
                if (list.size() > 0) {
                    request.setAttribute("RMSAMaster", list);
                } else {
                }
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward rmsaMasterUpdate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        String user = null;
        RMSAForm myform = (RMSAForm) form;
        RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
        ArrayList<RMSAForm> myList = new ArrayList<RMSAForm>();
        ArrayList<String> myreturnList = new ArrayList<String>();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                user = (String) session.getAttribute("userName");
                myform.setUserName(user);
                String checkbox = myform.getCheckedBoxesdp();
                String SanctionedForCivilCost1 = myform.getSanctionedForCivilCost();
                String sanctionedForFurniture1 = myform.getSanctionedForFurniture();
                String sanctionofLabEquipment1 = myform.getSanctionofLabEquipment();
                String totalSanctioned1 = myform.getTotalSanctioned1();
                String totalRelease1 = myform.getTotalRelease();

                String checkbox1[] = checkbox.split(",");
                String SanctionedForCivilCost[] = SanctionedForCivilCost1.split("~");
                String sanctionedForFurniture[] = sanctionedForFurniture1.split("~");
                String sanctionofLabEquipment[] = sanctionofLabEquipment1.split("~");
                String totalSanctioned[] = totalSanctioned1.split("~");
                String totalRelease[] = totalRelease1.split("~");
                
                for (int i = 0; i <= SanctionedForCivilCost.length - 1; i++) {
                    String myreturn = rmsaDAO.updateBudgetRelease(checkbox1[i].toString(), user, SanctionedForCivilCost[i].toString(), sanctionedForFurniture[i].toString(), sanctionofLabEquipment[i].toString(), totalSanctioned[i].toString(), totalRelease[i].toString());
                    myreturnList.add(myreturn);
                }
                if(myreturnList.contains("1") && myreturnList.contains("0")){
                    request.setAttribute("SuccessMsg", "Some of them failed to update");
                    request.setAttribute("colour", "red");
                }else if(myreturnList.contains("0")){
                    request.setAttribute("SuccessMsg", "Failed to update try again");
                    request.setAttribute("colour", "red");
                }else if(myreturnList.contains("1")){
                    request.setAttribute("SuccessMsg", "Successfully updated");
                    request.setAttribute("colour", "#0E94C5");
                }
                
                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                ArrayList mandalList = rmsaDAO.getMandalDetails(myform.getDistrictId(), myform.getPhaseNo());
                myform.setMandalList(mandalList);
                ArrayList districtList = (ArrayList) rmsaDAO.getDistrictList(myform.getPhaseNo().toString());
                myform.setDistList(districtList);
                myform.setPhaseList(list1);
                ArrayList list = (ArrayList) rmsaDAO.getRmsaMasterData(myform.getMandalId(), myform.getPhaseNo());
                if (list.size() > 0) {
                    request.setAttribute("RMSAMaster", list);
                } else {
                }
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getDistrictList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        DataSource ds = null;
        PrintWriter out = response.getWriter();
        response.setHeader("Cache-Control", "private");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setContentType("text/xml");

        ArrayList district_info = new ArrayList();
        ArrayList district_id_list = new ArrayList();
        ArrayList district_name_list = new ArrayList();
        String phaseNo = request.getParameter("phaseNo");
        RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
        district_info = (ArrayList) rmsaDAO.getDistrictList1(phaseNo);
        district_name_list = (ArrayList) district_info.get(0);
        district_id_list = (ArrayList) district_info.get(1);
        request.setAttribute("districtList", district_info);
        int id_size = district_name_list.size();
        int count = 0;
        out.println("<root>");
        if (id_size != 0) {
            out.println("<name>" + id_size + "</name>");
            out.println("<id>" + id_size + "</id>");
            for (count = 0; count < district_id_list.size(); count++) {
                out.println("<name>" + (String) district_name_list.get(count) + "</name>");
                out.println("<id>" + district_id_list.get(count) + "</id>");
            }
        } else {
            out.println("<name>" + 0 + "</name>");
            out.println("<id>" + 0 + "</id>");

        }
        out.println("</root>");
        return null;

    }

    public ActionForward getMandalList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        DataSource ds = null;
        PrintWriter out = response.getWriter();
        response.setHeader("Cache-Control", "private");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setContentType("text/xml");

        ArrayList mandal_info = new ArrayList();
        ArrayList mandal_id_list = new ArrayList();
        ArrayList mandal_name_list = new ArrayList();
        String phaseNo = request.getParameter("phaseNo");
        String districtId = request.getParameter("districtId");
        RMSACivilsDAO rmsaDAO = new RMSACivilsDAO();
        mandal_info = (ArrayList) rmsaDAO.getMandalList1(districtId, phaseNo);
        mandal_name_list = (ArrayList) mandal_info.get(0);
        mandal_id_list = (ArrayList) mandal_info.get(1);
        request.setAttribute("mandalList", mandal_info);
        int id_size = mandal_name_list.size();
        int count = 0;
        out.println("<root>");
        if (id_size != 0) {
            out.println("<name>" + id_size + "</name>");
            out.println("<id>" + id_size + "</id>");
            for (count = 0; count < mandal_id_list.size(); count++) {
                out.println("<name>" + (String) mandal_name_list.get(count) + "</name>");
                out.println("<id>" + mandal_id_list.get(count) + "</id>");
            }
        } else {
            out.println("<name>" + 0 + "</name>");
            out.println("<id>" + 0 + "</id>");
        }
        out.println("</root>");
        return null;
    }
}