/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Action.TpSMSServiceStub;
import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;


public class TpSendSMS {
    
    public static String sendSMS(String phoneNo,String message) throws AxisFault,
                                                                   RemoteException {
        
        TpSMSServiceStub stub =  new TpSMSServiceStub();
        TpSMSServiceStub.SendSMSForSIMS smsList =  new TpSMSServiceStub.SendSMSForSIMS();
        if(message.length()>200){
            message=message.substring(0,200);                   
        }        
        smsList.setMessage(message);
        smsList.setPhoneNo(phoneNo);
        TpSMSServiceStub.AuthHeaderE authHeaderE = new TpSMSServiceStub.AuthHeaderE();
        TpSMSServiceStub.AuthHeader authHeader =  new TpSMSServiceStub.AuthHeader();
        authHeader.setUsername("SIMS");
        authHeader.setPassword("SIMS@321");
        authHeaderE.setAuthHeader(authHeader);
        TpSMSServiceStub.SendSMSForSIMSResponse response = stub.SendSMSForSIMS(smsList,authHeaderE);
        return response.getSendSMSForSIMSResult();
    }

}
