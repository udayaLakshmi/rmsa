/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.VCOrganizationDAO;
import com.aponline.rmsa.WithLogin.Form.VCOrganizationForm;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1250881
 */
public class VCOrganizationAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        VCOrganizationForm myform = (VCOrganizationForm) form;
        String schoolId = "";
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userName") != null && session.getAttribute("userName") != "" && session.getAttribute("RoleId") != null && session.getAttribute("RoleId") != "") {
                if (session.getAttribute("RoleId").equals("59")) {
                    myform.setVcOrganizationId("1");
                } else if (session.getAttribute("RoleId").equals("64")) {
                    myform.setVcOrganizationId("2");
                } else {
                    request.setAttribute("msg", "This Service is not available for your Management");
                }
                VCOrganizationDAO dao = new VCOrganizationDAO();
                ArrayList vclist = dao.getVCHMConfirmationList(myform);
                  if (vclist.size() > 0) {
//                   HashMap map = (HashMap) vclist.get(0);
//               String status = (String) map.get("statusFlag");
                for (int i = 0; i <= vclist.size() - 1; i++) {
                    Map map = (Map) vclist.get(i);
                    String status = (String) map.get("statusFlag");
                    if (status.equalsIgnoreCase("N")) {
                        request.setAttribute("checkList", vclist);
                        request.setAttribute("vclist", vclist);
                    } else if (status.equalsIgnoreCase("Y")) {
                        request.setAttribute("vlist", vclist);
                        request.setAttribute("vclist", vclist);
                    } else {
                        request.setAttribute("msg", "No Records are available");
                    }
                }
                  }else{
                   request.setAttribute("msg", "No Records are available");
                  }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapping.findForward(SUCCESS);
    }

    public ActionForward updateRecords(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCOrganizationForm myform = (VCOrganizationForm) form;
        VCOrganizationDAO dao = new VCOrganizationDAO();
        try {
            if (myform.getVcTrainerIdValue() != null || myform.getVcTrainerIdValue() != "" && myform.getStatusFlag() != null || myform.getStatusFlag() != "") {
                String vcTrainerIdValue = myform.getVcTrainerIdValue();
                String statusFlag = myform.getStatusFlag();
                vcTrainerIdValue = vcTrainerIdValue.substring(1);
                statusFlag = statusFlag.substring(1);
                String[] vcTrainerIdValue1 = vcTrainerIdValue.split("~");
                String[] statusFlag1 = statusFlag.split("~");
                ArrayList vcIdAL = new ArrayList();
                ArrayList statusFlagAL = new ArrayList();
                for (int i = 0; i <= statusFlag1.length - 1; i++) {
                    vcIdAL.add(vcTrainerIdValue1[i]);
                    statusFlagAL.add(statusFlag1[i]);
                    if (statusFlag1[i].equalsIgnoreCase("N") && vcTrainerIdValue1[i] != null && vcTrainerIdValue1[i] != "") {
                        myform.setVcTrainerIdValue(vcIdAL.get(i).toString());
                        myform.setStatusFlag("Y");
                        String status = dao.updateRecords(myform);
                        if (status != null) {
                            request.setAttribute("status", status);
                        } else {
                            request.setAttribute("msg", "Updation Failed");
                        }
                    }
                }
            } else {
                request.setAttribute("msg", "Updation Failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }
}
