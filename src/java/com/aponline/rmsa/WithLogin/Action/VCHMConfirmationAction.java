/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.VCHMConfirmationDAO;
import com.aponline.rmsa.WithLogin.Form.VCHMConfirmationForm;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1250881
 */
public class VCHMConfirmationAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        String schoolId = "";
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userName") != null && session.getAttribute("userName") != "") {
                VCHMConfirmationDAO dao = new VCHMConfirmationDAO();
                schoolId = (String) session.getAttribute("userName");
                myform.setSchoolId(schoolId);
                ArrayList vclist = dao.getVCHMConfirmationList(myform);
                if (vclist.size() > 0) {
                    myform.setVcList(vclist);
                    request.setAttribute("vclist", vclist);
                } else {
                    request.setAttribute("msg", "No Records are available");
                }
            } else {
                request.setAttribute("msg", "This Service is not available for your Management");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);
    }

    public ActionForward vcHMStatusUpdate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCHMConfirmationDAO dao = new VCHMConfirmationDAO();
          VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        try {
        
//            myform.getvTrainerId()
//            myform.setSchoolId(request.getParameter("schcd"));
//            myform.setVcTrainerId(request.getParameter("vctrainerId"));
//            myform.setVcHMremarks(request.getParameter("vcHMremarksId"));
//            myform.setVcHMRemarksText(request.getParameter("vcHMRemarksText"));
//            System.out.println("==="+myform.getVcHMremarks()+"======="+myform.getVcHMRemarksText());
//             System.out.println("sch=="+myform.getSchoolId()+"==vct==-"+myform.getvTrainerId()+"--==hmfeed=="+myform.getVcHMremarks()+"==rema=="+myform.getVcHMRemarksText());
           
            String status = dao.vcHMStatusUpdate(myform);
            if (status != null) {
                request.setAttribute("status", status);
            } else {
                request.setAttribute("msg", "No Records are available");
            }
            myform.setVcHMremarks("");
            myform.setVcHMRemarksText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }

    public ActionForward vcHMStatusPDF(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        VCHMConfirmationDAO dao = new VCHMConfirmationDAO();
        try {
            String schoolId = request.getParameter("schcd");
            String vctraineId = request.getParameter("vctrainerId");
            myform.setSchoolId(schoolId);
            myform.setVcTrainerId(vctraineId);
            ArrayList vclist = dao.getVCHMConfirmationList(myform);
            myform.setVcList(vclist);
            request.setAttribute("vclist", vclist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("vchmpdf");
    }

    public ActionForward vcHMStatusView(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        VCHMConfirmationDAO dao = new VCHMConfirmationDAO();
        try {
            String schoolId = request.getParameter("schcd");
            String vctraineId = request.getParameter("vctrainerId");
            myform.setSchoolId(schoolId);
            myform.setVcTrainerId(vctraineId);
            ArrayList vclist = dao.getVCHMConfirmationList(myform);
            myform.setVcList(vclist);
            request.setAttribute("vclist", vclist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("vchmview");
    }
}
