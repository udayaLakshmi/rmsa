package com.aponline.rmsa.WithLogin.Action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendFileEmail {

    public static boolean sendEmailWithAttachments(ArrayList<InternetAddress> ToMailsList, ArrayList<InternetAddress> CCMailsList, ArrayList<InternetAddress> BCCMailsList,
            String subject, String message, String[] attachFiles)
            throws AddressException, MessagingException {
        boolean result = false;
        // sets SMTP server properties
        final String email = "vijaykumardevu@gmail.com";
        final String password = "munnasrinu";
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "10.100.102.91");
        properties.put("mail.smtp.port", "25");
        //properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        // properties.put("mail.user",email);
        // properties.put("mail.password", password);
        try {
//            Authenticator auth = new Authenticator() {
//                public PasswordAuthentication getPasswordAuthentication() {
//                    return new PasswordAuthentication(email, password);
//                }
//            };
            Authenticator auth = null;
            Session session = Session.getInstance(properties, auth);
            // creates a new e-mail message
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("RMSA@ap.gov.in"));
            InternetAddress[] iaarrayTo = new InternetAddress[ToMailsList.size()];
            InternetAddress[] iaarrayCC = new InternetAddress[CCMailsList.size()];
            InternetAddress[] iaarrayBCC = new InternetAddress[BCCMailsList.size()];
            //Multipart multipart = new MimeMultipart();
            int count = 0;
            for (InternetAddress ia : ToMailsList) {
                iaarrayTo[count] = ia;
                count++;
            }
            count = 0;
            for (InternetAddress ia : CCMailsList) {
                iaarrayCC[count] = ia;
                count++;
            }
            count = 0;
            for (InternetAddress ia : BCCMailsList) {
                iaarrayBCC[count] = ia;
                count++;
            }
            msg.setRecipients(Message.RecipientType.TO, iaarrayTo);
            msg.setRecipients(Message.RecipientType.CC, iaarrayCC);
            msg.setRecipients(Message.RecipientType.BCC, iaarrayBCC);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(message, "text/html");
            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            // adds attachments
            if (attachFiles != null && attachFiles.length > 0) {
                for (String filePath : attachFiles) {
                    MimeBodyPart attachPart = new MimeBodyPart();
                    try {
                        if (filePath != null) {
                            attachPart.attachFile(filePath);
                            multipart.addBodyPart(attachPart);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            // sets the multi-part as e-mail's content
            msg.setContent(multipart);
            // sends the e-mail
            Transport.send(msg);
            result = true;
        } catch (Exception e) {

            e.printStackTrace();
            result = false;
        }
        return result;
    }

    public static String ReminderProgressMailFormat(String body) {
        String Msg = null;
        Msg = "<html><head><title>Task Tracker</title></head>"
                + "<body>"
                + "<table width='80%' align='center' border='0' cellpadding='0' cellspacing='0'>"
                + "   <tr> "
                + " 	<td>Dear Sir/Madam,</td>"
                + " </tr><tr><td>&nbsp;</td></tr>"
                + " <tr>   "
                + " 		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + body + ".</td>"
                + " </tr>"
                + " <tr>  <br/>"
                + "<table width='80%' align='center' border='0' cellpadding='0' cellspacing='0'>"
                + "<tr> <td><tr><td>&nbsp;</td></tr><br/><br/>"
                + " Thanks & Regards<br/>"
                + " SIMS Team .<br/>"
                + "</td> </tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr>"
                + "<td style='color:red; font-size:12px;'>Note : Its an Auto Generated email.Please do not reply to this eMail.</td></tr> </table>"
                + "</body></html>";
        return Msg;
    }
}
