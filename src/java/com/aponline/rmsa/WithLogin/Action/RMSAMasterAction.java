package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;




public class RMSAMasterAction extends DispatchAction {

    public RMSAMasterAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                CommonDAO commonDAO = new CommonDAO();
                ArrayList distList = commonDAO.getDistrictDetails();
                myform.setDistList(distList);

                if (session.getAttribute("RoleId").equals("3")) {

                    myform.setDistrictId(session.getAttribute("districtId").toString());
                    ArrayList mandalList = commonDAO.getMandalDetails(myform.getDistrictId());
                    myform.setMandalList(mandalList);

                }

                distList = null;
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getMandalList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                CommonDAO commonDAO = new CommonDAO();
                //ArrayList mandalList = commonDAO.getMandalDetails(myform.getDistrictId());
                ArrayList mandalList = commonDAO.getMandalDetails1(myform.getDistrictId());
                myform.setMandalList(mandalList);
                mandalList = null;
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward getVillageList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                CommonDAO commonDAO = new CommonDAO();
                //ArrayList getVillage = commonDAO.getVillageDetails(myform.getMandalId());
                ArrayList getVillage = commonDAO.getVillageDetails1(myform.getMandalId());
                myform.setVillageList(getVillage);
                getVillage = null;
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getMandalList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }

    public ActionForward getPhaseList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RMSAForm myform = (RMSAForm) form;
        try {
//            myform.setPhaseNo("0");
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                //ArrayList list = (ArrayList) rmsaDAO.getPhaseList();
                ArrayList list = (ArrayList) rmsaDAO.getPhaseList1(myform);
                myform.setPhaseList(list);
                list = null;
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getVillageList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }

    public ActionForward getSchoolList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        RMSAForm myform = (RMSAForm) form;
        ArrayList schoolList = new ArrayList();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                //CommonDAO commonDAO = new CommonDAO();
                schoolList = (ArrayList) rmsaDAO.getSchoolListByVillage(myform.getVillageId(), myform.getPhaseNo());
                //getSchools = commonDAO.SchoolCodeListNew(myform.getVillage());
                myform.setSchoolList(schoolList);
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getPhaseList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward searchSchoolFunding(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        ArrayList list = null;
        ArrayList list1 = null;
        HashMap map = null;
        String mgtname = null;
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                String schoolName = rmsaDAO.getSchoolName(myform.getSchoolId());
                request.setAttribute("schoolCode", myform.getSchoolId());
                request.setAttribute("schoolName", schoolName);
                CommonDAO commonDAO = new CommonDAO();
                mgtname = commonDAO.getSchoolMgmtName(myform.getSchoolId());
                list = (ArrayList) rmsaDAO.getRMSAMasterBySchoolCode(myform, session.getAttribute("RoleId").toString());
                request.setAttribute("RMSAMaster", list);
                if (list.size() > 0) {
                    map = (HashMap) list.get(0);
                    myform.setArtCraftSanction(map.get("phyartCraftRoom").toString());
                    myform.setComputerRoomSanction(map.get("phycompRoom").toString());
                    myform.setScienceLabSanction(map.get("phyScienceLap").toString());
                    myform.setLibraryRoomSanction(map.get("phyLibray").toString());
                    myform.setAdditionalClassRoomSanction(map.get("phyACR").toString());
                    myform.setToiletBlockSanction(map.get("phyToilet").toString());
                    myform.setDrinkingWaterSanction(map.get("phyDrinkingWater").toString());
                    myform.setFurnitureAndLabEquipment("");
                    if (map.get("artCraftStageOfProgress") != null) {
                        myform.setArtCraftStageOfProgress(map.get("artCraftStageOfProgress").toString());
                    }
                    if (map.get("computerRoomStageOfProgress") != null) {
                        myform.setComputerRoomStageOfProgress(map.get("computerRoomStageOfProgress").toString());
                    }
                    if (map.get("scienceLabStageOfProgress") != null) {
                        myform.setScienceLabStageOfProgress(map.get("scienceLabStageOfProgress").toString());
                    }
                    if (map.get("libraryRoomStageOfProgress") != null) {
                        myform.setLibraryRoomStageOfProgress(map.get("libraryRoomStageOfProgress").toString());
                    }
                    if (map.get("additionalClassRoomStageOfProgress") != null) {
                        myform.setAdditionalClassRoomStageOfProgress(map.get("additionalClassRoomStageOfProgress").toString());
                    }
                    if (map.get("toiletBlockStageOfProgress") != null) {
                        myform.setToiletBlockStageOfProgress(map.get("toiletBlockStageOfProgress").toString());
                    }
                    if (map.get("drinkingWaterStageOfProgress") != null) {
                        myform.setDrinkingWaterStageOfProgress(map.get("drinkingWaterStageOfProgress").toString());
                    }
                    if (map.get("labEquipmentStageOfProgress") != null) {
                        myform.setLabEquipmentStageOfProgress(map.get("labEquipmentStageOfProgress").toString());
                    }
                    if (map.get("furnitureStageOfProgress") != null) {
                        myform.setFurnitureStageOfProgress(map.get("furnitureStageOfProgress").toString());
                    }
                    if (map.get("totalFurnitureInLaks") != null) {
                        myform.setTotalFurniture(map.get("totalFurnitureInLaks").toString());
                    }
                    if (map.get("totalCivilSanction") != null) {
                        myform.setAdministrativeSanction(map.get("totalCivilSanction").toString());
                    }
                    if (map.get("furnitureAndLabEquipment") != null) {
                        myform.setLabEquipment(map.get("furnitureAndLabEquipment").toString());
                    }
                    if (map.get("totalSanctioned") != null) {
                        myform.setFurnitureAndLabEquipment(map.get("totalSanctioned").toString());
                    }
                    if (map.get("tenderCostValue") != null) {
                        myform.setTenderCostValue(map.get("tenderCostValue").toString());
                    }
                    if (map.get("vat") != null) {
                        myform.setVat(map.get("vat").toString());
                    }
                    if (map.get("departemntCharges") != null) {
                        myform.setDepartemntCharges(map.get("departemntCharges").toString());
                    }
                    if (map.get("releases") != null) {
                        myform.setReleases(map.get("releases").toString());//reshma
                       
                        
                    } else {
                        myform.setReleases("0");
                    }
                    if (map.get("estimatedCost") != null) {
                        myform.setEstimatedCost(map.get("estimatedCost").toString());
                    }
                    if (map.get("totalEligibleCost") != null) {
                        myform.setTotalEligibleCost(map.get("totalEligibleCost").toString());
                    }
                    if (map.get("balanceToBeReleased") != null) {
                        myform.setBalanceToBeReleased(map.get("balanceToBeReleased").toString());
                    }
                    String totalEC = map.get("totalEligibleCost").toString();
                    String rel = map.get("releases").toString();
                    BigDecimal d1 = BigDecimal.valueOf(Double.parseDouble(totalEC));
                    BigDecimal d2 = BigDecimal.valueOf(Double.parseDouble(rel));
                    BigDecimal d3 = d1.subtract(d2);
                    myform.setBalanceToBeReleased(d3.toString());
                    if (map.get("expenditure") != null) {
                        myform.setExpenditureIncurred(map.get("expenditure").toString());
                    }
                    String expenditure = map.get("expenditure").toString();
                    BigDecimal d4 = BigDecimal.valueOf(Double.parseDouble(expenditure));
                    BigDecimal d5 = d2.subtract(d4);
                    myform.setBalanceAvailable(d5.toString());
                    request.setAttribute("RMSAMaster", list);

                    //omkar
                    ArrayList releaseMasterList = rmsaDAO.getReleaseMaster(myform.getSchoolId(), map.get("phaseNo").toString());
                    if (releaseMasterList.size() > 0) {
                        request.setAttribute("releaseMasterList", releaseMasterList);
                    }

                    releaseMasterList = null;
                    Map map1 = rmsaDAO.getSchoolDataBySchoolCode(myform.getSchoolId());
                    request.setAttribute("schoolName", map1.get("schoolName").toString());
                    request.setAttribute("distname", map1.get("distName").toString());
                    request.setAttribute("mandalname", map1.get("mandalName").toString());
                    request.setAttribute("phaseNo", myform.getPhaseNo());
                    request.setAttribute("year", map.get("year").toString());
                    request.setAttribute("mgtname", mgtname);
                    map1 = null;
                    map = null;

                } else {
                    myform.setArtCraftSanction("");
                    myform.setComputerRoomSanction("");
                    myform.setScienceLabSanction("");
                    myform.setLibraryRoomSanction("");
                    myform.setAdditionalClassRoomSanction("");
                    myform.setToiletBlockSanction("");
                    myform.setDrinkingWaterSanction("");
                    myform.setArtCraftStageOfProgress("");
                    myform.setComputerRoomStageOfProgress("");
                    myform.setScienceLabStageOfProgress("");
                    myform.setLibraryRoomStageOfProgress("");
                    myform.setAdditionalClassRoomStageOfProgress("");
                    myform.setToiletBlockStageOfProgress("");
                    myform.setDrinkingWaterStageOfProgress("");
                    myform.setLabEquipmentStageOfProgress("");
                    myform.setFurnitureStageOfProgress("");
                    myform.setFurnitureAndLabEquipment("");
                    myform.setAdministrativeSanction("");
                    myform.setTotalEligibleCost("");
                    myform.setTenderCostValue("");
                    myform.setVat("");
                    myform.setDepartemntCharges("");
                    myform.setReleases("");
                    myform.setBalanceToBeReleased("");
                    myform.setExpenditureIncurred("");
                    myform.setBalanceAvailable("");
                    myform.setEstimatedCost("");
                    myform.setFurnitureSanction("");
                    myform.setFurnitureAndLabEquipment("");
                    myform.setTotalFurniture("");
                }
                //Get RMSA Cheque details
                list1 = (ArrayList) rmsaDAO.getRMSAChequeDetailsBySchoolCode(myform);
                request.setAttribute("RMSAChequeDetails", list1);
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getPhaseList(mapping, form, request, response);
        getSchoolList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward rmsaMasterUpdate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        String user = null;
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                user = (String) session.getAttribute("userName");
                myform.setUserName(user);
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                String roleId = session.getAttribute("RoleId").toString();
                String[] releasesRow = request.getParameterValues("releasesRow");
                String[] expenditureIncurredDate = request.getParameterValues("expenditureIncurredDate");
                myform.setReleasesRow(releasesRow);
                myform.setExpenditureIncurredDate(expenditureIncurredDate);
                rmsaDAO.updateReleaseMaster(myform);
                int count = rmsaDAO.updateRmsaMaster(myform, roleId);
                if (count == 1) {
                    request.setAttribute("msg", "Updated Successfully");
                } else {
                    request.setAttribute("msg", "Updation failed");
                }
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        searchSchoolFunding(mapping, form, request, response);
        getSchoolList(mapping, form, request, response);
        getPhaseList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward rmsaMasterSubmit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking

            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                int count = rmsaDAO.createRmsaMaster(myform);
                if (count == 1) {
                    request.setAttribute("msg", "Inserted Successfully");
                } else {
                    request.setAttribute("msg", "Insertion failed");
                }
                target = "success";
            } else {
                response.sendRedirect("officialLogin.do");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        searchSchoolFunding(mapping, form, request, response);
        getSchoolList(mapping, form, request, response);
        getPhaseList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }
 public ActionForward getRmsaDistWiseReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "distReport";
        RMSAForm myform = (RMSAForm) form;
         String id = null;
          String loginId = null;
        try {
            HttpSession session = request.getSession();
           //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                 if (session.getAttribute("districtId").toString().equals("00")) {
                        id = "ALL";
                    } else {
                        loginId = "DEO";
                        id = session.getAttribute("districtId").toString();
                    }
                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);
                list1 = null;
                if (myform.getPhaseNo() != null) {
                    request.setAttribute("phaseNo", myform.getPhaseNo().toString());
                    ArrayList list = (ArrayList) rmsaDAO.getRmsaDistWiseReportData(id, loginId, myform.getPhaseNo());
                    request.setAttribute("masterList", list);
                    Map map = (Map) list.get(list.size() - 1);
                    DecimalFormat df = new DecimalFormat("###.##");

                    request.setAttribute("totalDistSanction", df.format(Double.parseDouble(map.get("totalDistSanction").toString())));
                    request.setAttribute("totalDistamountEstimated", df.format(Double.parseDouble(map.get("totalDistamountEstimated").toString())));
                    request.setAttribute("totalDistamountReleased", df.format(Double.parseDouble(map.get("totalDistamountReleased").toString())));
                    request.setAttribute("totalDistamountYetToBeRelease", df.format(Double.parseDouble(map.get("totalDistamountYetToBeRelease").toString())));
                    request.setAttribute("totalDistexpenditureIncurred", df.format(Double.parseDouble(map.get("totalDistexpenditureIncurred").toString())));
                    request.setAttribute("totalDistucsSubmitted", df.format(Double.parseDouble(map.get("totalDistucsSubmitted").toString())));
                    request.setAttribute("totalOpeningBal", df.format(Double.parseDouble(map.get("totalOpeningBal").toString())));
                    request.setAttribute("totalClosingBal", df.format(Double.parseDouble(map.get("totalClosingBal").toString())));


                    list = null;
                    if (request.getParameter("type") != null) {
                        target = "excelDistReport";
                    } else {
                        target = "distReport";
                    }
                }

            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getRmsaDistSchoolReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "distReport";
        RMSAForm myform = (RMSAForm) form;
        String loginId= null;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();

                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);
                list1 = null;
                if (request.getParameter("phaseNo") != null && request.getParameter("distId") != null) {
                    String distId = request.getParameter("distId").toString();
                    String phaseNo = request.getParameter("phaseNo").toString();
                    loginId = "NIL";
                    request.setAttribute("phaseNo", phaseNo);
                    request.setAttribute("distId", distId);
                    ArrayList list = (ArrayList) rmsaDAO.getRmsaDistWiseReportData(distId, loginId, phaseNo);
                    request.setAttribute("masterList", list);
                    Map map = (Map) list.get(list.size() - 1);
                    DecimalFormat df = new DecimalFormat("###.##");

                    request.setAttribute("totalDistSanction", df.format(Double.parseDouble(map.get("totalDistSanction").toString())));
                    request.setAttribute("totalDistamountEstimated", df.format(Double.parseDouble(map.get("totalDistamountEstimated").toString())));
                    request.setAttribute("totalDistamountReleased", df.format(Double.parseDouble(map.get("totalDistamountReleased").toString())));
                    request.setAttribute("totalDistamountYetToBeRelease", df.format(Double.parseDouble(map.get("totalDistamountYetToBeRelease").toString())));
                    request.setAttribute("totalDistexpenditureIncurred", df.format(Double.parseDouble(map.get("totalDistexpenditureIncurred").toString())));
                    request.setAttribute("totalDistucsSubmitted", df.format(Double.parseDouble(map.get("totalDistucsSubmitted").toString())));
                    request.setAttribute("totalOpeningBal", df.format(Double.parseDouble(map.get("totalOpeningBal").toString())));
                    request.setAttribute("totalClosingBal", df.format(Double.parseDouble(map.get("totalClosingBal").toString())));

                    list = null;
                    if (request.getParameter("type") != null) {
                        target = "excelSchoolReport";
                    } else {
                        target = "excelWiseSchoolReport";
                    }

                }
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

}