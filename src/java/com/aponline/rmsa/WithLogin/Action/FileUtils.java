package com.aponline.rmsa.WithLogin.Action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author 1250892
 */
public class FileUtils {

    
    public static String filePath(String path) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("com.myapp.struts.ApplicationResource");
        String filePath = resourceBundle.getString("drive") + resourceBundle.getString(path);
        return filePath;
    }
}
