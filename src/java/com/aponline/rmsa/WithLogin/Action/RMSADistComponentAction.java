package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentForm;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class RMSADistComponentAction extends DispatchAction {

    public RMSADistComponentAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList distList = rmsaDAO.getComponentDistrict();
                myform.setDistList(distList);
                myform.setDistrictId(session.getAttribute("districtId").toString());
                ArrayList yesrList = rmsaDAO.getComponentYears();
                myform.setYearList(yesrList);
                //   ArrayList mandalList = rmsaDAO.getComponentMandals(myform.getDistrictId(),myform.getYear());
                // myform.setMandalList(mandalList);
                // yesrList = null;
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getMandalList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                myform.setDistrictId(session.getAttribute("districtId").toString());
                ArrayList mandalList = rmsaDAO.getComponentMandals(myform.getDistrictId(), myform.getYear());
                myform.setMandalList(mandalList);
                mandalList = null;
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward getVillageList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList getVillage = rmsaDAO.getComponentVillages(myform.getMandalId(), myform.getYear());
                myform.setVillageList(getVillage);
                getVillage = null;
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getMandalList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward getSchoolList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList schoolList = rmsaDAO.getComponentSchools(myform.getVillageId(), myform.getYear());
                myform.setSchoolList(schoolList);
                target = "success";

            } else {
                target = "sessionExp";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getVillageList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward searchSchoolFunding(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        ArrayList list = null;
        ArrayList list1 = null;
        HashMap map = null;
        RMSAComponentForm myform = (RMSAComponentForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                //Civil Works
                myform.setCivilWorksReleased("");
                myform.setCivilWorksSpent("");
                myform.setCivilWorksAvailable("");
                myform.setMajorRepairsReleased("");
                myform.setMajorRepairsSpent("");
                myform.setMajorRepairsAvailable("");
                myform.setMinorRepairsReleased("");
                myform.setMinorRepairsSpent("");
                myform.setMinorRepairsAvailable("");
                myform.setAnnualAndOtherReleased("");
                myform.setAnnualAndOtherSpent("");
                myform.setAnnualAndOtherAvailable("");
                myform.setToiletsReleased("");
                myform.setToiletsSpent("");
                myform.setToiletsAvailable("");
                myform.setSelfDefenseGrantsReleased("");
                myform.setSelfDefenseGrantsSpent("");
                myform.setSelfDefenseGrantsAvailable("");
                myform.setTotalCivilReleased("");
                myform.setTotalCivilSpent("");
                myform.setTotalCivilAvailable("");
                //Annual Grants 
                myform.setWaterElectricityTelephoneChargesReleased("");
                myform.setWaterElectricityTelephoneChargesSpent("");
                myform.setWaterElectricityTelephoneChargesAvailable("");
                myform.setProvisionalLaboratoryReleased("");
                myform.setProvisionalLaboratoryAvailable("");
                myform.setProvisionalLaboratorySpent("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersReleased("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersSpent("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersAvailable("");
                myform.setMinorRepairsAnnualReleased("");
                myform.setMinorRepairsAnnualSpent("");
                myform.setMinorRepairsAnnualAvailable("");
                myform.setSanitationAndICTReleased("");
                myform.setSanitationAndICTSpent("");
                myform.setSanitationAndICTAvailable("");
                myform.setNeedBasedWorksReleased("");
                myform.setNeedBasedWorksSpent("");
                myform.setNeedBasedWorksAvailable("");
                myform.setTotalAnnualReleased("");
                myform.setTotalAnnualSpent("");
                myform.setTotalAnnualAvailable("");
                //Furniture & Lab Equipment
                myform.setFurnitureReleased("");
                myform.setFurnitureSpent("");
                myform.setFurnitureAvailable("");
                myform.setLabEquipmentReleased("");
                myform.setLabEquipmentSpent("");
                myform.setLabEquipmentAvailable("");
                myform.setTotalfurnitureAndLabReleased("");
                myform.setTotalfurnitureAndLabSpent("");
                myform.setTotalfurnitureAndLabAvailable("");

                //Recurring
                myform.setRecurringExcursionRelease("");
                myform.setRecurringExcursionSpent("");
                myform.setRecurringExcursionAvailable("");
                myform.setRecurringSelfDefenseRelease("");
                myform.setRecurringSelfDefenseSpent("");
                myform.setRecurringSelfDefenseAvailable("");
                myform.setRecurringExcursionSpent("");
                myform.setRecurringOtherSpent("");
                myform.setRecurringOtherAvailable("");
                myform.setRecurringTotalRelease("");
                myform.setRecurringTotalSpent("");
                myform.setRecurringTotalAvailable("");

                rmsaDAO.getRmsaComponentMaster(myform);
                String str = rmsaDAO.checkDistrictExpSub(myform.getYear(), myform.getSchoolCode());
                if (str.equalsIgnoreCase("YES")) {
                    request.setAttribute("readStatus", "true");
                } else {
                    request.setAttribute("readStatus", "false");
                }
                request.setAttribute("RMSAMaster", "");
                target = "success";
            } else {
                target = "sessionExp";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getSchoolList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward updateRmsaComponentMaster(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int count = 0;
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                String roleId = session.getAttribute("RoleId").toString();
                myform.setUserName(session.getAttribute("userName").toString());
                if (myform.getSno() != null) {
                    rmsaDAO.updateComponentsTrack(myform.getSno());
                    count = rmsaDAO.updateRmsaComponentMaster(myform, roleId);
                    String str = rmsaDAO.checkDistrictExpSub(myform.getYear(), myform.getSchoolCode());
                    if (str.equalsIgnoreCase("YES")) {
                        request.setAttribute("readStatus", "true");
                    } else {
                        request.setAttribute("readStatus", "false");
                    }
                }
                if (count == 1) {
                    request.setAttribute("msg", "Updated Successfully");
                } else {
                    request.setAttribute("msg", "Updation failed");
                }
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        searchSchoolFunding(mapping, form, request, response);
        getSchoolList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward rmsaMasterSubmit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                int count = rmsaDAO.createRmsaMaster(myform);
                if (count == 1) {
                    request.setAttribute("msg", "Inserted Successfully");
                } else {
                    request.setAttribute("msg", "Insertion failed");
                }
                target = "success";
            } else {
                target = "failure";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        searchSchoolFunding(mapping, form, request, response);
        getSchoolList(mapping, form, request, response);
        getMandalList(mapping, form, request, response);
        getVillageList(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward getRmsaDistWiseReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "distReport";
        RMSAForm myform = (RMSAForm) form;
        String id = null;
        String loginId = null;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                if (session.getAttribute("districtId").toString().equals("00")) {
                    id = "ALL";
                } else {
                    loginId = "DEO";
                    id = session.getAttribute("districtId").toString();
                }
                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);
                list1 = null;
                if (myform.getPhaseNo() != null) {
                    request.setAttribute("phaseNo", myform.getPhaseNo().toString());
                    ArrayList list = (ArrayList) rmsaDAO.getRmsaDistWiseReportData(id, loginId, myform.getPhaseNo());
                    request.setAttribute("masterList", list);
                    Map map = (Map) list.get(list.size() - 1);
                    DecimalFormat df = new DecimalFormat("###.##");

                    request.setAttribute("totalDistSanction", df.format(Double.parseDouble(map.get("totalDistSanction").toString())));
                    request.setAttribute("totalDistamountEstimated", df.format(Double.parseDouble(map.get("totalDistamountEstimated").toString())));
                    request.setAttribute("totalDistamountReleased", df.format(Double.parseDouble(map.get("totalDistamountReleased").toString())));
                    request.setAttribute("totalDistamountYetToBeRelease", df.format(Double.parseDouble(map.get("totalDistamountYetToBeRelease").toString())));
                    request.setAttribute("totalDistexpenditureIncurred", df.format(Double.parseDouble(map.get("totalDistexpenditureIncurred").toString())));
                    request.setAttribute("totalDistucsSubmitted", df.format(Double.parseDouble(map.get("totalDistucsSubmitted").toString())));
                    request.setAttribute("totalOpeningBal", df.format(Double.parseDouble(map.get("totalOpeningBal").toString())));
                    request.setAttribute("totalClosingBal", df.format(Double.parseDouble(map.get("totalClosingBal").toString())));


                    list = null;
                    if (request.getParameter("type") != null) {
                        target = "excelDistReport";
                    } else {
                        target = "distReport";
                    }
                }

            } else {
                target = "failure";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getRmsaDistSchoolReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "distReport";
        RMSAForm myform = (RMSAForm) form;
        String loginId = null;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();

                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);
                list1 = null;
                if (request.getParameter("phaseNo") != null && request.getParameter("distId") != null) {
                    String distId = request.getParameter("distId").toString();
                    String phaseNo = request.getParameter("phaseNo").toString();
                    loginId = "NIL";
                    request.setAttribute("phaseNo", phaseNo);
                    request.setAttribute("distId", distId);
                    ArrayList list = (ArrayList) rmsaDAO.getRmsaDistWiseReportData(distId, loginId, phaseNo);
                    request.setAttribute("masterList", list);
                    Map map = (Map) list.get(list.size() - 1);
                    DecimalFormat df = new DecimalFormat("###.##");

                    request.setAttribute("totalDistSanction", df.format(Double.parseDouble(map.get("totalDistSanction").toString())));
                    request.setAttribute("totalDistamountEstimated", df.format(Double.parseDouble(map.get("totalDistamountEstimated").toString())));
                    request.setAttribute("totalDistamountReleased", df.format(Double.parseDouble(map.get("totalDistamountReleased").toString())));
                    request.setAttribute("totalDistamountYetToBeRelease", df.format(Double.parseDouble(map.get("totalDistamountYetToBeRelease").toString())));
                    request.setAttribute("totalDistexpenditureIncurred", df.format(Double.parseDouble(map.get("totalDistexpenditureIncurred").toString())));
                    request.setAttribute("totalDistucsSubmitted", df.format(Double.parseDouble(map.get("totalDistucsSubmitted").toString())));
                    request.setAttribute("totalOpeningBal", df.format(Double.parseDouble(map.get("totalOpeningBal").toString())));
                    request.setAttribute("totalClosingBal", df.format(Double.parseDouble(map.get("totalClosingBal").toString())));

                    list = null;
                    if (request.getParameter("type") != null) {
                        target = "excelSchoolReport";
                    } else {
                        target = "excelWiseSchoolReport";
                    }

                }
            } else {
                target = "failure";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}