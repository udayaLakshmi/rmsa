package com.aponline.rmsa.WithLogin.Action;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentForm;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;

public class RMSASchoolComponentAction extends DispatchAction {

    public RMSASchoolComponentAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList yesrList = rmsaDAO.getComponentYears();
                myform.setYearList(yesrList);
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward searchSchoolFunding(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "failure";
        ArrayList list = null;
        ArrayList list1 = null;
        HashMap map = null;
        RMSAComponentForm myform = (RMSAComponentForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {

                String schoolCode = session.getAttribute("userName").toString();
                myform.setSchoolCode(schoolCode);
                //Civil Works
                myform.setCivilWorksReleased("");
                myform.setCivilWorksSpent("");
                myform.setCivilWorksAvailable("");
                myform.setMajorRepairsReleased("");
                myform.setMajorRepairsSpent("");
                myform.setMajorRepairsAvailable("");
                myform.setMinorRepairsReleased("");
                myform.setMinorRepairsSpent("");
                myform.setMinorRepairsAvailable("");
                myform.setAnnualAndOtherReleased("");
                myform.setAnnualAndOtherSpent("");
                myform.setAnnualAndOtherAvailable("");
                myform.setToiletsReleased("");
                myform.setToiletsSpent("");
                myform.setToiletsAvailable("");
                myform.setSelfDefenseGrantsReleased("");
                myform.setSelfDefenseGrantsSpent("");
                myform.setSelfDefenseGrantsAvailable("");
                myform.setTotalCivilReleased("");
                myform.setTotalCivilSpent("");
                myform.setTotalCivilAvailable("");
                //Annual Grants 
                myform.setWaterElectricityTelephoneChargesReleased("");
                myform.setWaterElectricityTelephoneChargesSpent("");
                myform.setWaterElectricityTelephoneChargesAvailable("");
                myform.setProvisionalLaboratoryReleased("");
                myform.setProvisionalLaboratoryAvailable("");
                myform.setProvisionalLaboratorySpent("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersReleased("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersSpent("");
                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersAvailable("");
                myform.setMinorRepairsAnnualReleased("");
                myform.setMinorRepairsAnnualSpent("");
                myform.setMinorRepairsAnnualAvailable("");
                myform.setSanitationAndICTReleased("");
                myform.setSanitationAndICTSpent("");
                myform.setSanitationAndICTAvailable("");
                myform.setNeedBasedWorksReleased("");
                myform.setNeedBasedWorksSpent("");
                myform.setNeedBasedWorksAvailable("");
                myform.setTotalAnnualReleased("");
                myform.setTotalAnnualSpent("");
                myform.setTotalAnnualAvailable("");
                //Furniture & Lab Equipment
                myform.setFurnitureReleased("");
                myform.setFurnitureSpent("");
                myform.setFurnitureAvailable("");
                myform.setLabEquipmentReleased("");
                myform.setLabEquipmentSpent("");
                myform.setLabEquipmentAvailable("");
                myform.setTotalfurnitureAndLabReleased("");
                myform.setTotalfurnitureAndLabSpent("");
                myform.setTotalfurnitureAndLabAvailable("");

                //Recurring
                myform.setRecurringExcursionRelease("");
                myform.setRecurringExcursionSpent("");
                myform.setRecurringExcursionAvailable("");
                myform.setRecurringSelfDefenseRelease("");
                myform.setRecurringSelfDefenseSpent("");
                myform.setRecurringSelfDefenseAvailable("");
                myform.setRecurringExcursionSpent("");
                myform.setRecurringOtherSpent("");
                myform.setRecurringOtherAvailable("");
                myform.setRecurringTotalRelease("");
                myform.setRecurringTotalSpent("");
                myform.setRecurringTotalAvailable("");

                RMSAComponentForm newform = rmsaDAO.getRmsaComponentMaster(myform);
                if (newform.getSno() != null) {
                    request.setAttribute("RMSAMaster", "");
                     String year = "";
                    session.setAttribute(year, myform.getYear());
                }else{
                    request.setAttribute("msg", " No Data Avialable");
                }
                target = "success";
            } else {
                target = "sessionExp";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward updateRmsaComponentMaster(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int count = 0 ;
        String target = "failure";
        RMSAComponentForm myform = (RMSAComponentForm) form;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                String roleId = session.getAttribute("RoleId").toString();
                myform.setUserName(session.getAttribute("userName").toString());
                if (myform.getSno() != null) {
                    rmsaDAO.updateComponentsTrack(myform.getSno());
                    count = rmsaDAO.updateRmsaComponentMaster(myform, roleId);
                }
                if (count == 1) {
                    request.setAttribute("msg", "Updated Successfully");
                } else {
                    request.setAttribute("msg", "Updation failed");
                }
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        searchSchoolFunding(mapping, form, request, response);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }
}