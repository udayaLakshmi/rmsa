/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.RMSAComponentForm;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentsForm;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import com.aponline.rmsa.database.DataBasePlugin;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.struts.upload.FormFile;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.commons.CommonDetails;
import java.math.RoundingMode;

/**
 *
 * @author 1259084
 */
public class RMSAComponentsDAO {

    ///displaying schoolname///
    public String getSchoolname(String schoolcode) {
        String st1 = null;
        String query = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        Connection con = null;
        Map map = new HashMap();
        String schoolname = null;
        PreparedStatement st = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select SCHNAME from STEPS_SCHOOL where schcd=?";
            st = con.prepareStatement(query);
            st.setString(1, schoolcode);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    schoolname = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoolname;
    }

    ///displaying schemas////
    public ArrayList getSchemeList() {

        String query = null;
        ArrayList schemelist = new ArrayList();
        PreparedStatement st = null, st1 = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select SchemeId,SchemeName from [RMSA_SCHEMES]";

            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schemeid", rs.getString(1));
                    map.put("schemename", rs.getString(2));
                    schemelist.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schemelist;
    }

    ///displaying rmsa details///
    public ArrayList getRmdadtlslist(String schemeid, String componentId) {
        String query = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        ArrayList schemelist = new ArrayList();
        PreparedStatement st = null, st1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select ComponentId,ComponentValue,SchemaId,b.RowID from ShemaValues a left join [RMSA_Insert] b on(a.SchemaId=b.schem and a.ComponentId=b.recurringid and a.ComponentId=b.nonrecurringid) where [SchemaId]=? and ComponentId=? ";
            st = con.prepareStatement(query);
            st.setString(1, schemeid);
            st.setString(2, componentId);
            rs = st.executeQuery();
            //System.out.println("query" + query);
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("ComponentId", rs.getString(1));
                    map.put("ComponentValue", rs.getString(2));
                    map.put("schemaid", rs.getString(3));
                    map.put("update_id", rs.getString(4));
                    schemelist.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schemelist;
    }

    ///insert rmsa///
    public boolean insertRMSA(RMSAComponentsForm rmsaform, String schoolcode, String ipAdress) throws ParseException {
        Statement st = null;
        Statement stmt = null;
        Connection con = null;
        String query = null;
        ResultSet rs = null;
        boolean insertStatus = false;
        CommonConstants commonConistants = new CommonConstants();
        int noOfRecords = 0;
        int noOfRecords1 = 0;
        try {
            con = DataBasePlugin.getConnection(commonConistants.DBCONNECTION);
            con.setAutoCommit(false);
            stmt = con.createStatement();
            noOfRecords = Integer.parseInt(rmsaform.getNoOfRecords());

            noOfRecords1 = Integer.parseInt(rmsaform.getNoOfRecords1());
            for (int i = 0; i < noOfRecords; i++) {
                String updateId = (String) rmsaform.getRmsainsert("updateId" + i);
                if (updateId != null && !"null".equals(updateId) && !"".equals(updateId)) {
                    query = "update [RMSA_Recurring] set recurringvalue='" + rmsaform.getRmsainsert("recurring" + i) + "',"
                            + " IPAddress='" + ipAdress + "' "
                            + " where id='" + rmsaform.getRmsainsert("updateId" + i) + "' ";

                    stmt.addBatch(query);
                } else {
                    query = "insert into [RMSA_Recurring] (UserName,recurringid,recurringvalue,IPAddress,insertdate,schem)"
                            + "values('" + schoolcode + "','" + rmsaform.getRmsainsert("recucomponentId" + i) + "','" + rmsaform.getRmsainsert("recurring" + i) + "',"
                            + "'" + ipAdress + "',getDate(),'" + rmsaform.getRmsainsert("recuschemaid" + i) + "')";

                    stmt.addBatch(query);
                    //  System.out.println("query.sssssssssssss..........." + query);
                }
            }

            for (int j = 0; j < noOfRecords1; j++) {
                String updateId = (String) rmsaform.getRmsainsert("updateId" + j);
                if (updateId != null && !"null".equals(updateId) && !"".equals(updateId)) {
                    query = "update [RMSA_NonRecurring] set NonrecurringValue='" + rmsaform.getRmsainsert("nonrecurring" + j) + "',"
                            + " IpAdress='" + ipAdress + "' "
                            + " where id='" + rmsaform.getRmsainsert("updateId" + j) + "' ";

                    stmt.addBatch(query);
                } else {
                    query = "insert into [RMSA_NonRecurring] (UserName,NonrecurringId,NonrecurringValue,IpAdress,insertdate,schem)"
                            + "values('" + schoolcode + "','" + rmsaform.getRmsainsert("nonrecucomponentId" + j) + "','" + rmsaform.getRmsainsert("nonrecurring" + j) + "',"
                            + "'" + ipAdress + "',getDate(),'" + rmsaform.getRmsainsert("nonrecuschemaid" + j) + "')";

                    stmt.addBatch(query);
                    // System.out.println("query............" + query);
                }
            }

            int[] numUpdates = stmt.executeBatch();

            if (numUpdates.length > 0) {
                insertStatus = true;
            }

            con.commit();
            con.setAutoCommit(true);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return insertStatus;
    }

    /// Omkaram addded------relese amount modification
    public List getRMSAMasterBySchoolCode(RMSAForm myform, String roleId) {

        String query = null;
        String schoolname = null;
        PreparedStatement st = null, st1 = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = null;
        List list = new ArrayList();
        String phy_scienceLap = null;
        double releases = 0;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
             System.out.println("roleId "+roleId);
            if (roleId.equals("51") || roleId.equals("52") || roleId.equals("3")) {
//                query = "SELECT a.(SNO,YEAR,PHASE,UDISE_CODE,SchoolSanction,SchoolAsPerUdise,District,Block,Physicalsanc_ScienceLab,Physicalsanc_CompRoom,Physicasanc_artcraftroom,Physicalsanc_Library,physicalsanc_ACR,Physicalsance_DrinkingWater,Physicalsanc_Toilet,ArtCraftroom_progress,ComputerRoom_progress,ScienceLab_progress,LibraryRoom_progress,AdditionalClassRoom_progress,ToiletBlock_progress,DrinkingWater_progress,LabEquipment_progress,Furniture_progress,financialsanc_ScienceLab,financialsanc_CompRoom,financialsanc_artcraftroom,financialsanc_Library,financialsanc_ACR,financialsanc_DrinkingWater,financialsanc_Toilet,Totalcivilsanction,furnitureforclassroomlabs,labequipment,TOTALFURNITURE_IN_LAKHS,FINANCIAL_SANC_IN_LAKHS,Tendervalue,[VAT%],DEPT,Releases,Expenditure,UCSfilepath,EstimatedCost,TotalEligibleAmount,BalanceToBeReleased,Created_date,CreatedBy) FROM a.RMSAMASTER and  b.ReleaseAmount from b.RMSA_Release_Master where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "'";
//               query = "select ReleaseAmount from RMSA_Release_Master where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "' order by created_date";
//           
                query = "SELECT \n"
                        + "a.SNO,a.YEAR,a.PHASE,a.UDISE_CODE,a.SchoolSanction,a.SchoolAsPerUdise,a.District,Block,a.Physicalsanc_ScienceLab,a.Physicalsanc_CompRoom,\n"
                        + "a.Physicasanc_artcraftroom,a.Physicalsanc_Library,a.physicalsanc_ACR,a.Physicalsance_DrinkingWater,a.Physicalsanc_Toilet,a.ArtCraftroom_progress,\n"
                        + "a.ComputerRoom_progress,a.ScienceLab_progress,a.LibraryRoom_progress,a.AdditionalClassRoom_progress,a.ToiletBlock_progress,a.DrinkingWater_progress,\n"
                        + "a.LabEquipment_progress,a.Furniture_progress,a.financialsanc_ScienceLab,a.financialsanc_CompRoom,a.financialsanc_artcraftroom,a.financialsanc_Library,\n"
                        + "coalesce(a.financialsanc_ACR,'0')financialsanc_ACR,a.financialsanc_DrinkingWater,a.financialsanc_Toilet,a.Totalcivilsanction,a.furnitureforclassroomlabs,coalesce(a.labequipment,'0')labequipment,\n"
                        + "coalesce(a.TOTALFURNITURE_IN_LAKHS,'0')TOTALFURNITURE_IN_LAKHS,coalesce(a.FINANCIAL_SANC_IN_LAKHS,'0')FINANCIAL_SANC_IN_LAKHS,coalesce(a.Tendervalue,'0')Tendervalue,coalesce([VAT%],'0')VAT,coalesce(a.DEPT,'0')DEPT,coalesce(a.Releases,'0')Releases,coalesce(a.Expenditure,'0')Expenditure,a.UCSfilepath,coalesce(a.EstimatedCost,'0')EstimatedCost,\n"
                        + "coalesce(a.TotalEligibleAmount,'0')TotalEligibleAmount,coalesce(a.BalanceToBeReleased,'0')BalanceToBeReleased,a.Created_date,a.CreatedBy from \n"
                        + "\n"
                        + "(select *  FROM RMSAMASTER where UDISE_CODE=? and PHASE=? ) a \n"
                        + "left join \n"
                        + "(select school_code,sum(cast(ReleaseAmount as float)) releases from RMSA_Release_Master where school_code=? \n"
                        + "and PHASE=? group by school_code) b\n"
                        + "on(a.UDISE_CODE=b.school_code)";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
                st.setString(2, myform.getPhaseNo());
                st.setString(2, myform.getSchoolId());
               // System.out.println("query-1 "+query);
            } else {
                query = "SELECT \n"
                        + "a.SNO,a.YEAR,a.PHASE,a.UDISE_CODE,a.SchoolSanction,a.SchoolAsPerUdise,a.District,Block,a.Physicalsanc_ScienceLab,a.Physicalsanc_CompRoom,\n"
                        + "a.Physicasanc_artcraftroom,a.Physicalsanc_Library,a.physicalsanc_ACR,a.Physicalsance_DrinkingWater,a.Physicalsanc_Toilet,a.ArtCraftroom_progress,\n"
                        + "a.ComputerRoom_progress,a.ScienceLab_progress,a.LibraryRoom_progress,a.AdditionalClassRoom_progress,a.ToiletBlock_progress,a.DrinkingWater_progress,\n"
                        + "a.LabEquipment_progress,a.Furniture_progress,a.financialsanc_ScienceLab,a.financialsanc_CompRoom,a.financialsanc_artcraftroom,coalesce(a.financialsanc_Library,'0')financialsanc_Library,\n"
                        + "a.financialsanc_ACR,a.financialsanc_DrinkingWater,a.financialsanc_Toilet,a.Totalcivilsanction,a.furnitureforclassroomlabs,coalesce(a.labequipment,'0')labequipment,\n"
                        + "coalesce(a.TOTALFURNITURE_IN_LAKHS,'0') TOTALFURNITURE_IN_LAKHS,coalesce(a.FINANCIAL_SANC_IN_LAKHS,'0') FINANCIAL_SANC_IN_LAKHS,coalesce(a.Tendervalue,'0') Tendervalue,coalesce([VAT%],'0') VAT,coalesce(a.DEPT,'0') DEPT,coalesce(b.Releases,'0')Releases,coalesce(a.Expenditure,'0')Expenditure,a.UCSfilepath,coalesce(a.EstimatedCost,'0')EstimatedCost,\n"
                        + "coalesce(a.TotalEligibleAmount,'0'),coalesce(a.BalanceToBeReleased,'0') BalanceToBeReleased ,a.Created_date,a.CreatedBy from \n"
                        + "\n"
                        + "(select *  FROM RMSAMASTER where UDISE_CODE=? and PHASE=?) a \n"
                        + "left join \n"
                        + "(select school_code,sum(cast(ReleaseAmount as float)) releases from RMSA_Release_Master where school_code=? and Phase=? \n"
                        + " group by school_code) b\n"
                        + "on(a.UDISE_CODE=b.school_code)";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
                st.setString(2, myform.getPhaseNumber());
                st.setString(3, myform.getSchoolId());
                st.setString(4, myform.getPhaseNumber());
               // System.out.println("SchoolId "+myform.getSchoolId());
               // System.out.println("PhaseNo "+myform.getPhaseNumber());
                //System.out.println("query-2 "+query);
            }
           
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schoolName", rs.getString(6));
                    map.put("phyScienceLap", rs.getString(9));
                    map.put("phycompRoom", rs.getString(10));
                    map.put("phyartCraftRoom", rs.getString(11));
                    //  map.put("phyartCraftRoom", '0');
                    map.put("phyLibray", rs.getString(12));
                    map.put("phyACR", rs.getString(13));
                    map.put("phyDrinkingWater", rs.getString(14));
                    map.put("phyToilet", rs.getString(15));
                    if (rs.getString(16) == null) {
                    } else {
                        map.put("artCraftStageOfProgress", rs.getString(16));
                    }
                    if (rs.getString(17) == null) {
                    } else {
                        map.put("computerRoomStageOfProgress", rs.getString(17));
                    }
                    if (rs.getString(18) == null) {
                    } else {
                        map.put("scienceLabStageOfProgress", rs.getString(18));
                    }
                    if (rs.getString(19) == null) {
                    } else {
                        map.put("libraryRoomStageOfProgress", rs.getString(19));
                    }
                    if (rs.getString(20) == null) {
                    } else {
                        map.put("additionalClassRoomStageOfProgress", rs.getString(20));
                    }
                    if (rs.getString(21) == null) {
                    } else {
                        map.put("toiletBlockStageOfProgress", rs.getString(21));
                    }
                    if (rs.getString(22) == null) {
                    } else {
                        map.put("drinkingWaterStageOfProgress", rs.getString(22));
                    }
                    if (rs.getString(23) == null) {
                    } else {
                        map.put("labEquipmentStageOfProgress", rs.getString(23));
                    }
                    if (rs.getString(24) == null) {
                    } else {
                        map.put("furnitureStageOfProgress", rs.getString(24));
                    }
                    if (rs.getString(32) == null) {
                    } else {
                        map.put("totalCivilSanction", rs.getString(32));
                    }
                    if (rs.getString(35) == null) {
                    } else {
                        map.put("totalFurnitureInLaks", rs.getString(35));
                    }
                    if (rs.getString(34) == null) {//reshma
                    } else {
                        map.put("furnitureAndLabEquipment", rs.getString(34));
                    }
                    if (rs.getString(36) == null) {
                    } else {
                        map.put("totalSanctioned", rs.getString(36));
                    }
                    if (rs.getString(37) == null) {
                    } else {
                        map.put("tenderCostValue", rs.getString(37));
                    }
                    if (rs.getString(38) == null) {
                    } else {
                        map.put("vat", rs.getString(38));
                    }
                    if (rs.getString(39) == null) {
                    } else {
                        map.put("departemntCharges", rs.getString(39));
                    }
                    if (rs.getString(40) == null) {//reshma
                    } else {
                        map.put("releases", rs.getString(40));

                    }
                    if (rs.getString(41) == null) {
                    } else {
                        map.put("expenditure", rs.getString(41));
                    }
                    if (rs.getString(42) == null) {
                    } else {
                        map.put("ucsFileName", rs.getString(42));
                    }
                    if (rs.getString(43) == null) {
                    } else {
                        map.put("estimatedCost", rs.getString(43));
                    }
                    if (rs.getString(44) == null) {
                    } else {
                        map.put("totalEligibleCost", rs.getString(44));
                    }
                    if (rs.getString(45) == null) {
                    } else {
                        map.put("balanceToBeReleased", rs.getString(45));
                    }

                    map.put("year", rs.getString(2));
                    map.put("phaseNo", rs.getString(3));
                    if (rs.getString(2) == null) {
                        map.put("year", "-");
                    } else {
                        map.put("year", rs.getString(2));
                    }
                    if (rs.getString(3) == null) {
                        map.put("phaseNo", "-");
                    } else {
                        map.put("phaseNo", rs.getString(3));
                    }
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /// Omkaram addded
    public List getRMSAMasterBySchoolCode11(RMSAForm myform, String roleId) {
        Map map = null;
        String query = null;
        Connection con = null;
        String schoolname = null;
        List list = new ArrayList();
        String phy_scienceLap = null;
        ResultSet rs = null, rs1 = null;
        PreparedStatement st = null, st1 = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (roleId.equals("51") || roleId.equals("52") || roleId.equals("3")) {
                query = "select * from RMSAMASTER where UDISE_CODE=? and PHASE=? ";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
                st.setString(2, myform.getPhaseNo());
            } else {
                query = "select * from RMSAMASTER where UDISE_CODE=?";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
            }
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schoolName", rs.getString(6));
                    map.put("phyScienceLap", rs.getString(9));
                    map.put("phycompRoom", rs.getString(10));
                    map.put("phyartCraftRoom", rs.getString(11));
//                    map.put("phyartCraftRoom", '0');
                    map.put("phyLibray", rs.getString(12));
                    map.put("phyACR", rs.getString(13));
                    map.put("phyDrinkingWater", rs.getString(14));
                    map.put("phyToilet", rs.getString(15));
                    if (rs.getString(16) == null) {
                    } else {
                        map.put("artCraftStageOfProgress", rs.getString(16));
                    }
                    if (rs.getString(17) == null) {
                    } else {
                        map.put("computerRoomStageOfProgress", rs.getString(17));
                    }
                    if (rs.getString(18) == null) {
                    } else {
                        map.put("scienceLabStageOfProgress", rs.getString(18));
                    }
                    if (rs.getString(19) == null) {
                    } else {
                        map.put("libraryRoomStageOfProgress", rs.getString(19));
                    }
                    if (rs.getString(20) == null) {
                    } else {
                        map.put("additionalClassRoomStageOfProgress", rs.getString(20));
                    }
                    if (rs.getString(21) == null) {
                    } else {
                        map.put("toiletBlockStageOfProgress", rs.getString(21));
                    }
                    if (rs.getString(22) == null) {
                    } else {
                        map.put("drinkingWaterStageOfProgress", rs.getString(22));
                    }
                    if (rs.getString(23) == null) {
                    } else {
                        map.put("labEquipmentStageOfProgress", rs.getString(23));
                    }
                    if (rs.getString(24) == null) {
                    } else {
                        map.put("furnitureStageOfProgress", rs.getString(24));
                    }
                    if (rs.getString(32) == null) {
                    } else {
                        map.put("totalCivilSanction", rs.getString(32));
                    }
                    if (rs.getString(35) == null) {
                    } else {
                        map.put("totalFurnitureInLaks", rs.getString(35));
                    }
                    if (rs.getString(34) == null) {//reshma
                    } else {
                        map.put("furnitureAndLabEquipment", rs.getString(34));
                    }
                    if (rs.getString(36) == null) {
                    } else {
                        map.put("totalSanctioned", rs.getString(36));
                    }
                    if (rs.getString(37) == null) {
                    } else {
                        map.put("tenderCostValue", rs.getString(37));
                    }
                    if (rs.getString(38) == null) {
                    } else {
                        map.put("vat", rs.getString(38));
                    }
                    if (rs.getString(39) == null) {
                    } else {
                        map.put("departemntCharges", rs.getString(39));
                    }
                    if (rs.getString(40) == null) {
                    } else {
                        map.put("releases", rs.getString(40));
                    }
                    if (rs.getString(41) == null) {
                    } else {
                        map.put("expenditure", rs.getString(41));
                    }
                    if (rs.getString(42) == null) {
                    } else {
                        map.put("ucsFileName", rs.getString(42));
                    }
                    if (rs.getString(43) == null) {
                    } else {
                        map.put("estimatedCost", rs.getString(43));
                    }
                    if (rs.getString(44) == null) {
                    } else {
                        map.put("totalEligibleCost", rs.getString(44));
                    }
                    if (rs.getString(45) == null) {
                    } else {
                        map.put("balanceToBeReleased", rs.getString(45));
                    }
                    //reshma
                    map.put("year", rs.getString(2));
                    map.put("phaseNo", rs.getString(3));
                    if (rs.getString(2) == null) {
                        map.put("year", "-");
                    } else {
                        map.put("year", rs.getString(2));
                    }
                    if (rs.getString(3) == null) {
                        map.put("phaseNo", "-");
                    } else {
                        map.put("phaseNo", rs.getString(3));
                    }
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    /// Omkaram addded

    public int updateRMSAMasterBySchoolCode(RMSAForm form, HttpServletRequest request) {
        int myReturn = 0;
        Connection con = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        String query = null;
        try {
            BigDecimal d1 = BigDecimal.valueOf(Double.parseDouble(form.getExpenditureIncurred().toString()));
            BigDecimal d2 = null;
            BigDecimal d3 = BigDecimal.valueOf(Double.parseDouble("0"));
            if (form.getChequeNumber() != null) {
                for (int i = 0; i < form.getChequeNumber().length; i++) {
                    String checkamm = form.getChequeAmount()[i].toString();
                    d2 = BigDecimal.valueOf(Double.parseDouble(checkamm));
                    d3 = d3.add(d2);
                    d2 = null;
                }
            }
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (d1.compareTo(d3) < 0) {
                query = "update RMSAMASTER set Expenditure='" + form.getExpenditureIncurred() + "',ArtCraftroom_progress='" + form.getArtCraftStageOfProgress() + "',ComputerRoom_progress='" + form.getComputerRoomStageOfProgress() + "',ScienceLab_progress='" + form.getScienceLabStageOfProgress() + "',LibraryRoom_progress='" + form.getLibraryRoomStageOfProgress() + "',AdditionalClassRoom_progress='" + form.getAdditionalClassRoomStageOfProgress() + "',ToiletBlock_progress='" + form.getToiletBlockStageOfProgress() + "',DrinkingWater_progress='" + form.getDrinkingWaterStageOfProgress() + "',Releases='" + d3 + "' where UDISE_CODE='" + form.getUserName() + "' and PHASE='" + form.getPhaseNo() + "'";
            } else {
                query = "update RMSAMASTER set Expenditure='" + form.getExpenditureIncurred() + "',ArtCraftroom_progress='" + form.getArtCraftStageOfProgress() + "',ComputerRoom_progress='" + form.getComputerRoomStageOfProgress() + "',ScienceLab_progress='" + form.getScienceLabStageOfProgress() + "',LibraryRoom_progress='" + form.getLibraryRoomStageOfProgress() + "',AdditionalClassRoom_progress='" + form.getAdditionalClassRoomStageOfProgress() + "',ToiletBlock_progress='" + form.getToiletBlockStageOfProgress() + "',DrinkingWater_progress='" + form.getDrinkingWaterStageOfProgress() + "' where UDISE_CODE='" + form.getUserName() + "' and PHASE='" + form.getPhaseNo() + "'";
            }
            pstmt = con.prepareStatement(query);
            myReturn = pstmt.executeUpdate();
        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }

            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }
        }
        return myReturn;
    }
    /// Omkaram addded

    public String updateRMSAChequeDetailsBySchoolCode(RMSAForm form, HttpServletRequest request) {
        int myReturn = 0;
        int[] myReturn1;
        Connection con = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        String query = null;
        boolean insertStatus = false;
        ResultSet rs = null;
        String erromsg = null;
        String result = null;
        HttpSession session = request.getSession();
        ArrayList list = new ArrayList();
        HashMap<String, String> map = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            stmt = con.createStatement();
            int count = 0;

//            query = "delete from RMSA_cheque_details where school_code='" + form.getUserName() + "'";
//            stmt.addBatch(query);
            if (form.getChequeNumber() != null) {
                for (int i = 0, j = 0, k = 0; i <= form.getChequeNumber().length - 1 && j <= form.getChequeDate().length - 1 && k <= form.getChequeAmount().length - 1; i++, j++, k++) {
//                   query = "if ((select count(*) from RMSA_cheque_details where cheque_no='" + form.getChequeNumber()[i]  + "' )=0)\n" +
//                            "begin\n" +
//                            "insert into RMSA_cheque_details(school_code,FY,cheque_no\n" +
//                            ",cheque_date,cheque_amount,created_date)\n" +
//                            " values('" + form.getUserName() + "','2016-2017','" + form.getChequeNumber()[i] + "','" + form.getChequeDate()[j] + "','" + form.getChequeAmount()[k] + "',getDate())\n" +
//                            " end";
//                     stmt.addBatch(query);
//                    count=count+1;
                    query = "select count(*) from RMSA_cheque_details where cheque_no='" + form.getChequeNumber()[i] + "'";
                    System.out.println("query "+query);
                    pstmt = con.prepareStatement(query);
                    rs = pstmt.executeQuery();
                    if (rs != null) {
                        while (rs.next()) {
                            count = rs.getInt(1);
                        }
                    }
                    if (count == 0) {
                        query = "insert into RMSA_cheque_details"
                                + "(school_code,FY,Cheque_no,cheque_date,cheque_amount,created_date)"
                                + " values('" + form.getUserName() + "','" + form.getYear() + "','" + form.getChequeNumber()[i] + "','" + form.getChequeDate()[i] + "','" + form.getChequeAmount()[i] + "',getDate())";
                        stmt.addBatch(query);
                    }
                }
            }
            myReturn1 = stmt.executeBatch();
            if (myReturn1.length > 0) {
                result = "<font color=\"green\">Details updated Successfully</font>";
            }

        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }

            return result;
        }
    }

    /// Omkaram addded
    public int uploadUcsDetails(String schoolCode, String fileName, String ucsAmount, RMSAForm form) {
        int myReturn = 0;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            CommonDetails commonDetails = new CommonDetails();
            String strDirectoytemp = commonDetails.getDrivePath() + CommonConstants.RMSA_Path + schoolCode + "/" + form.getPhaseNo() + "/" + fileName;
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "insert into RMSA_UCS_FILES_Details"
                    + "(school_code,ucsAmount,ucs_file_name,FY,created_date,filePath)"
                    + " values('" + schoolCode + "','" + ucsAmount + "','" + fileName + "','" + form.getYear() + "',getDate(),'" + strDirectoytemp + "')";
            pstmt = con.prepareStatement(query);
            myReturn = pstmt.executeUpdate();
        } catch (Exception e) {
            myReturn = 100;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 200;
                e.printStackTrace();
            }
            return myReturn;
        }
    }
    /// Omkaram addded

    public List getRMSAChequeDetailsBySchoolCode(RMSAForm form) {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = null;
        List list = new ArrayList();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            //query = "select sno,school_code,FY,Cheque_no,convert(varchar(10),cheque_date,101),cheque_amount,created_date from RMSA_cheque_details where school_code='" + schoolcode + "'";
            query = "select * from RMSA_cheque_details where school_code=? and FY=? ";
            st = con.prepareStatement(query);
            st.setString(1, form.getUserName());
            st.setString(2, form.getYear());
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("schoolCode", rs.getString(2));
                    map.put("fYear", rs.getString(3));
                    map.put("chequeNo", rs.getString(4));
                    map.put("chequeDate", rs.getString(5));
                    map.put("chequeAmount", rs.getString(6));
                    map.put("createdDate", rs.getString(7));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /// Omkaram addded
    public List getRMSAUcsFiledBySchoolCode(String schoolcode, String sno, String fy) {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        List list = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (sno == null) {
                query = "select * from RMSA_UCS_FILES_Details where school_code=? and FY=?";
                st = con.prepareStatement(query);
                st.setString(1, schoolcode);
                st.setString(2, fy);
            } else {
                query = "select * from RMSA_UCS_FILES_Details where school_code=? and sno=?";
                st = con.prepareStatement(query);
                st.setString(1, schoolcode);
                st.setString(2, sno);
            }
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("schoolCode", rs.getString(2));
                    map.put("ucsAmount", rs.getString(3));
                    map.put("ucsFileName", rs.getString(4));
                    map.put("FY", rs.getString(5));
                    map.put("createdDate", rs.getString(6));
                    map.put("filePath", rs.getString(7));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    /// Omkaram addded

    public boolean uploadGosPhoto(FormFile uploadFileName, String schoolCode, String url, String phNo) {
        String extension = null;
        String strDirectoytemp = null;
        boolean flag = false;
        CommonDetails commonDetails = new CommonDetails();
        try {
            String fileName = uploadFileName.getFileName();
            strDirectoytemp = commonDetails.getDrivePath() + CommonConstants.RMSA_Path + schoolCode + "\\" + phNo;
            if (strDirectoytemp != null && !"".equals(strDirectoytemp) && strDirectoytemp.length() > 0) { // If directory is not exists it will create
                File directorytemp = new File(strDirectoytemp);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
                File fileToCreatetemp = new File(strDirectoytemp, fileName); // Copy the file into directory
                FileOutputStream fileOutStreamtemp = new FileOutputStream(fileToCreatetemp); // Write the file content into buffer
                if (uploadFileName.getFileSize() > 0) {
                    fileOutStreamtemp.write(uploadFileName.getFileData());
                    fileOutStreamtemp.flush();
                    fileOutStreamtemp.close();
                    flag = true;
                } else {
                    flag = false;
                }
            } else {
                flag = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    /// Omkaram addded

    public int deleteRMSAChequeDetailsBySchoolCode(RMSAForm form, HttpServletRequest request) {
        int myReturn = 0;
        int[] myReturn1;
        Connection con = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            for (int i = 0; i < form.getChequeNumber().length; i++) {
                String query = "delete from RMSA_cheque_details where school_code='" + form.getUserName() + "' and Cheque_no='" + form.getChequeNumber()[i] + "'";
                stmt = con.createStatement();
                stmt.addBatch(query);
            }
            myReturn1 = stmt.executeBatch();
            if (myReturn1.length > 0) {
                myReturn = 1;
            }
        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }
            return myReturn;
        }
    }
    /// Omkaram addded

    public int deleteUcsFile(String schoolName, String sNo) {
        int myReturn = 0;
        String filePath = null;
        Connection con = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        PreparedStatement pStmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            String query = "select filePath from RMSA_UCS_FILES_Details where  school_code='" + schoolName + "' and sno='" + sNo + "'";
            pStmt = con.prepareStatement(query);
            resultSet = pStmt.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    filePath = resultSet.getString(1).toString();
                }
                query = "delete from RMSA_UCS_FILES_Details where school_code='" + schoolName + "' and sno='" + sNo + "'";
                stmt = con.createStatement();
                myReturn = stmt.executeUpdate(query);
                if (myReturn > 0) {
                    File file = new File(filePath);
                    file.delete();
                }
            }
        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (pStmt != null) {
                    pStmt.close();
                }
            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }
            return myReturn;
        }
    }

    /// Omkaram addded
    public int deleteCheque(String schoolName, String sNo) {
        int myReturn = 0;
        String query = null;
        Connection con = null;
        Statement stmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "delete from RMSA_cheque_details where school_code='" + schoolName + "' and sno='" + sNo + "'";
            stmt = con.createStatement();
            myReturn = stmt.executeUpdate(query);
            query = null;
        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }
            return myReturn;
        }
    }

    /// Omkaram addded
    public List getMandalListByDist(String distCode) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList mandalList = new ArrayList();;
        String mandallist = "<option value='0'>Select Mandal</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select blkcd,blkname from dbo.STEPS_BLOCK where blkcd like ? order by blkname ";
            pst = con.prepareStatement(query);
            pst.setString(1, distCode+"%");
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map.put("mandalCode", rs.getString(1));
                    map.put("mandalName", rs.getString(2));
                    mandalList.add(map);
                    map = null;
                    mandallist = mandallist + "<option value='" + rs.getString(1) + "' name='" + rs.getString(2) + "'>" + rs.getString(2) + "</option>";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }
    /// Omkaram addded

    public List getVillageListByMandal(String mandalCode) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList villageList = new ArrayList();
        String villagelist = "<option value='0'>Select Village</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select vilcd,vilname from dbo.STEPS_VILLAGE where vilcd like ? order by vilname ";
            pst = con.prepareStatement(query);
            pst.setString(1, mandalCode+"%");
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("villageCode", rs.getString(1));
                    map.put("villageName", rs.getString(2));
                    villageList.add(map);
                    map = null;
                    villagelist = villagelist + "<option value='" + rs.getString(1) + "'>" + rs.getString(2) + "</option>";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return villageList;
    }

    /// Omkaram addded
    public List getPhaseList() {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList phaseList = new ArrayList();
        String phaselist = "<option value='0'>Select Phase</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select distinct(PHASE) from RMSAMASTER order by PHASE";
            pst = con.prepareStatement(query);
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("PhaseNo", rs.getString(1));
                    String phaseName = "Phase-" + rs.getString(1);
                    map.put("phaseName", phaseName);
                    phaseList.add(map);
                    phaselist = phaselist + "<option value='" + rs.getString(1) + "'>" + phaseName + "</option>";
                    map = null;
                    phaseName = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return phaseList;
    }

    /// Omkaram addded
    public List getSchoolListByVillage(String villageCode, String phaseNo) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList schoolList = new ArrayList();

        String schoollist = "<option value='0'>Select School Name</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            //query = "select * from steps_school where schcd like '" + villageCode + "%'";
            query = "select UDISE_CODE,SchoolAsPerUdise from RMSAMASTER where PHASE =? and UDISE_CODE like ?";
            pst = con.prepareStatement(query);
            pst.setString(1, phaseNo);
            pst.setString(2, villageCode);
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schoolCode", rs.getString(1));
                    map.put("schoolName", rs.getString(2));
                    schoolList.add(map);
                    map = null;
                    schoollist = schoollist + "<option value='" + rs.getString(1) + "'>" + rs.getString(2) + "</option>";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoolList;
    }

    /// Omkaram addded
    public Map getSchoolDataBySchoolCode(String schoolcode) {
        String query = null;
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select \n"
                    + " (select DISTNAME from STEPS_DISTRICT(nolock)  where DISTCD=SUBSTRING('" + schoolcode + "',1,4) ) distName,\n"
                    + " (select BLKNAME from STEPS_BLOCK(nolock) where BLKCD=SUBSTRING('" + schoolcode + "',1,6)) mandalName,\n"
                    + " (select VILNAME from STEPS_VILLAGE(nolock) where VILCD=SUBSTRING('" + schoolcode + "',1,9)) villageName,\n"
                    + " (select SCHNAME from STEPS_SCHOOL(nolock) where SCHCD='" + schoolcode + "') schoolName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    if (rs.getString(1) == null) {
                        map.put("distName", "unknown");
                    } else {
                        map.put("distName", rs.getString(1).toString());
                    }
                    if (rs.getString(2) == null) {
                        map.put("mandalName", "unknown");
                    } else {
                        map.put("mandalName", rs.getString(2).toString());
                    }
                    if (rs.getString(3) == null) {
                        map.put("villageName", "unknown");
                    } else {
                        map.put("villageName", rs.getString(3).toString());
                    }
                    if (rs.getString(4) == null) {
                        map.put("schoolName", "unknown");
                    } else {
                        map.put("schoolName", rs.getString(4).toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /// Omkaram addded
    public int createRmsaMaster(RMSAForm myform) {
        int myReturn = 0;
        int count = 0;
        String distName = null;
        String mandalName = null;
        String schoolName = null;
        String query = null;
        String villageName = null;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select COUNT(*) from RMSAMASTER where UDISE_CODE='" + myform.getSchoolId() + "'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = Integer.parseInt(rs.getString(1).toString());
                }
                if (count == 0) {

                    query = "select \n"
                            + " (select DISTNAME from STEPS_DISTRICT(nolock)  where DISTCD=SUBSTRING('" + myform.getSchoolId() + "',1,4) ) distName,\n"
                            + " (select BLKNAME from STEPS_BLOCK(nolock) where BLKCD=SUBSTRING('" + myform.getSchoolId() + "',1,6)) mandalName,\n"
                            + " (select VILNAME from STEPS_VILLAGE(nolock) where VILCD=SUBSTRING('" + myform.getSchoolId() + "',1,9)) villageName,\n"
                            + " (select SCHNAME from STEPS_SCHOOL(nolock) where SCHCD='" + myform.getSchoolId() + "') schoolName";
                    pstmt = con.prepareStatement(query);
                    rs = pstmt.executeQuery();
                    if (rs != null) {
                        while (rs.next()) {
                            distName = rs.getString(1).toString();
                            mandalName = rs.getString(2).toString();
                            villageName = rs.getString(3).toString();
                            schoolName = rs.getString(4).toString();
                        }
                    }
                    if (distName != null && mandalName != null && villageName != null && schoolName != null) {
                        query = "insert into RMSAMASTER"
                                + "(YEAR,PHASE,UDISE_CODE,SchoolSanction,SchoolAsPerUdise,District,Block,Physicalsanc_ScienceLab,Physicalsanc_CompRoom,Physicasanc_artcraftroom,Physicalsanc_Library"
                                + ",physicalsanc_ACR,Physicalsance_DrinkingWater,Physicalsanc_Toilet,ArtCraftroom_progress,ComputerRoom_progress,ScienceLab_progress,LibraryRoom_progress,AdditionalClassRoom_progress"
                                + ",ToiletBlock_progress,DrinkingWater_progress,LabEquipment_progress,Furniture_progress,financialsanc_ScienceLab,financialsanc_CompRoom,financialsanc_artcraftroom"
                                + ",financialsanc_Library,financialsanc_ACR,financialsanc_DrinkingWater,financialsanc_Toilet,Totalcivilsanction,furnitureforclassroomlabs,labequipment,TOTALFURNITURE_IN_LAKHS"
                                + ",FINANCIAL_SANC_IN_LAKHS,Tendervalue,[VAT%],DEPT,Releases,Expenditure,UCSfilepath,EstimatedCost,TotalEligibleAmount,BalanceToBeReleased)"
                                + " values('2010-2011','2','" + myform.getSchoolId() + "','UnKnown','" + schoolName + "','" + distName + "','" + mandalName + "','" + myform.getScienceLabSanction() + "','" + myform.getComputerRoomSanction() + "','" + myform.getArtCraftSanction() + "','" + myform.getLibraryRoomSanction()
                                + "','" + myform.getAdditionalClassRoomSanction() + "','" + myform.getDrinkingWaterSanction() + "','" + myform.getToiletBlockSanction() + "','" + myform.getArtCraftStageOfProgress() + "','" + myform.getComputerRoomStageOfProgress() + "','" + myform.getScienceLabStageOfProgress() + "','" + myform.getLibraryRoomStageOfProgress() + "','" + myform.getAdditionalClassRoomStageOfProgress()
                                + "','" + myform.getToiletBlockStageOfProgress() + "','" + myform.getDrinkingWaterStageOfProgress() + "',NULL,NULL,NULL,NULL,NULL"
                                + ",NULL,NULL,NULL,NULL,'" + myform.getAdministrativeSanction() + "',NULL,NULL,'" + myform.getTotalFurniture()
                                + "','" + myform.getTotalSanctioned() + "','" + myform.getTenderCostValue() + "','" + myform.getVat() + "','" + myform.getDepartemntCharges() + "','" + myform.getReleases() + "','" + myform.getExpenditureIncurred() + "',NULL,'" + myform.getEstimatedCost() + "','" + myform.getTotalEligibleCost() + "','" + myform.getBalanceToBeReleased() + "')";
                        pstmt = con.prepareStatement(query);
                        myReturn = pstmt.executeUpdate();
                    } else {
                        myReturn = 50;
                    }
                } else {
                    myReturn = 50;
                }
            }

        } catch (Exception e) {
            myReturn = 100;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 200;
                e.printStackTrace();
            }
            return myReturn;
        }
    }

    /// Omkaram addded
    public int updateRmsaMaster(RMSAForm myform, String roleId) {
        int myReturn = 0;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);

            if (roleId.equals("51")) {
                query = "update RMSAMASTER set Totalcivilsanction='" + myform.getAdministrativeSanction() + "',TOTALFURNITURE_IN_LAKHS='" + myform.getTotalFurniture() + "',labequipment='" + myform.getLabEquipment() + "',FINANCIAL_SANC_IN_LAKHS='" + myform.getFurnitureAndLabEquipment()
                        + "',Releases='" + myform.getReleases() + "',BalanceToBeReleased='" + myform.getBalanceToBeReleased()
                        + "' where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "'";

            } else if (roleId.equals("52")) {
                query = "update RMSAMASTER set EstimatedCost='" + myform.getEstimatedCost() + "',Tendervalue='" + myform.getTenderCostValue() + "',[VAT%]='" + myform.getVat() + "',DEPT='" + myform.getDepartemntCharges()
                        + "',TotalEligibleAmount='" + myform.getTotalEligibleCost() + "',BalanceToBeReleased='" + myform.getBalanceToBeReleased()
                        + "' where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "'";
            } else if (roleId.equals("3")) {
                query = "update RMSAMASTER set Expenditure='" + myform.getExpenditureIncurred() + "' where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "'";
            } else {
                query = "update RMSAMASTER set Physicalsanc_ScienceLab='" + myform.getScienceLabSanction() + "',Physicalsanc_CompRoom='" + myform.getComputerRoomSanction() + "',Physicasanc_artcraftroom='" + myform.getArtCraftSanction() + "',Physicalsanc_Library='" + myform.getLibraryRoomSanction()
                        + "',physicalsanc_ACR='" + myform.getAdditionalClassRoomSanction() + "',Physicalsance_DrinkingWater='" + myform.getDrinkingWaterSanction() + "',Physicalsanc_Toilet='" + myform.getToiletBlockSanction() + "',ArtCraftroom_progress='" + myform.getArtCraftStageOfProgress() + "',ComputerRoom_progress='" + myform.getComputerRoomStageOfProgress() + "',ScienceLab_progress='" + myform.getScienceLabStageOfProgress() + "',LibraryRoom_progress='" + myform.getLibraryRoomStageOfProgress() + "',AdditionalClassRoom_progress='" + myform.getAdditionalClassRoomStageOfProgress()
                        + "',ToiletBlock_progress='" + myform.getToiletBlockStageOfProgress() + "',DrinkingWater_progress='" + myform.getDrinkingWaterStageOfProgress() + "'"
                        + ",Totalcivilsanction='" + myform.getAdministrativeSanction() + "',furnitureforclassroomlabs=NULL,TOTALFURNITURE_IN_LAKHS='" + myform.getTotalFurniture()
                        + "',FINANCIAL_SANC_IN_LAKHS='" + myform.getTotalSanctioned() + "',Tendervalue='" + myform.getTenderCostValue() + "',[VAT%]='" + myform.getVat() + "',DEPT='" + myform.getDepartemntCharges() + "',Releases='" + myform.getReleases() + "',Expenditure='" + myform.getExpenditureIncurred() + "',UCSfilepath=NULL,EstimatedCost='" + myform.getEstimatedCost() + "',TotalEligibleAmount='" + myform.getTotalEligibleCost() + "',BalanceToBeReleased='" + myform.getBalanceToBeReleased() + "' where UDISE_CODE='" + myform.getSchoolId() + "'";
            }
//            System.out.println("query------"+query);
            pstmt = con.prepareStatement(query);
            myReturn = pstmt.executeUpdate();
        } catch (Exception e) {
            myReturn = 100;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 200;
                e.printStackTrace();
            }
            return myReturn;
        }
    }
    /// Omkaram addded

    public String getSchoolName(String schoolCode) {
        String schoolName = null;
        String query = null;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select SCHNAME from STEPS_SCHOOL where SCHCD=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, schoolCode);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    schoolName = rs.getString(1).toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoolName;
    }

    //omkaram
    public ArrayList getRmsaDistWiseReportData(String distId, String loginId, String phaseNo) throws Exception {
        Map map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList distlList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        float totalDistSanction = 0;
        float totalDistamountEstimated = 0;
        float totalDistamountReleased = 0;
        float totalDistamountYetToBeRelease = 0;
        float totalDistexpenditureIncurred = 0;
        float totalDistucsSubmitted = 0;
        float totalOpeningBal = 0;
        float totalClosingBal = 0;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);

            if (distId == "ALL") {
                cstmt = con.prepareCall("{call Usp_Get_RMSAAmounts(?,?)}");
                cstmt.setString(1, phaseNo);
                cstmt.setString(2, distId);

            } else {
                cstmt = con.prepareCall("{call Usp_Get_RMSAAmounts(?,?,?)}");
                cstmt.setString(1, phaseNo);
                cstmt.setString(2, loginId);
                cstmt.setString(3, distId);
            }
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();

                    float ttSanction = rs.getFloat(3);
                    float ttamountEstimated = rs.getFloat(4);
                    float ttamountReleased = rs.getFloat(5);
                    float ttamountYetToBeRelease = rs.getFloat(6);
                    float ttexpenditureIncurred = rs.getFloat(7);
                    float ttucsSubmitted = rs.getFloat(8);
                    //reshma
                    float ttopeningBal = rs.getFloat(9);
                    float ttclosingBal = rs.getFloat(10);
                    if (loginId == "NIL") {
                        map.put("schoolId", rs.getString(11));
                        map.put("schoolName", rs.getString(12));
                    }

                    map.put("distId", rs.getString(1));
                    map.put("distName", rs.getString(2));
                    map.put("totalSanction", ttSanction);
                    map.put("amountEstimated", ttamountEstimated);
                    map.put("amountReleased", ttamountReleased);
                    map.put("amountYetToBeRelease", ttamountYetToBeRelease);
                    map.put("expenditureIncurred", ttexpenditureIncurred);
                    map.put("ucsSubmitted", ttucsSubmitted);
                    //resh
                    map.put("openingBal", ttopeningBal);
                    map.put("closingBal", ttclosingBal);

                    totalDistSanction = totalDistSanction + ttSanction;
                    map.put("totalDistSanction", totalDistSanction);

                    totalDistamountEstimated = totalDistamountEstimated + ttamountEstimated;
                    map.put("totalDistamountEstimated", totalDistamountEstimated);

                    totalDistamountReleased = totalDistamountReleased + ttamountReleased;
                    map.put("totalDistamountReleased", totalDistamountReleased);

                    totalDistamountYetToBeRelease = totalDistamountYetToBeRelease + ttamountYetToBeRelease;
                    map.put("totalDistamountYetToBeRelease", totalDistamountYetToBeRelease);

                    totalDistexpenditureIncurred = totalDistexpenditureIncurred + ttexpenditureIncurred;
                    map.put("totalDistexpenditureIncurred", totalDistexpenditureIncurred);

                    totalDistucsSubmitted = totalDistucsSubmitted + ttucsSubmitted;
                    map.put("totalDistucsSubmitted", totalDistucsSubmitted);
                    //reshma
                    totalOpeningBal = totalOpeningBal + ttopeningBal;
                    map.put("totalOpeningBal", totalOpeningBal);
                    totalClosingBal = totalClosingBal + ttclosingBal;
                    map.put("totalClosingBal", totalClosingBal);

                    distlList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distlList;
    }

    /// Omkaram addded 30082017
    public ArrayList getComponentYears() throws Exception {
        Map map = null;
        String query = null;
        ArrayList yearList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct year from RMSAComponents where year is not NULL or year!=? order by year";
            st = con.prepareStatement(query);
            st.setString(1, "");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("yearCode", rs.getString(1));
                    yearList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return yearList;
    }

    /// Omkaram addded 30082017
    public ArrayList getComponentDistricts(String year) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList distList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct DistId,DistrictName from RMSAComponents where year=? order by DistrictName";
            st = con.prepareStatement(query);
            st.setString(1, year);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("distCode", rs.getString(1));
                    map.put("distname", rs.getString(2));
                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }

    /// Omkaram addded 30082017
    public ArrayList getComponentMandals(String distId, String year) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct MandalId,MandalName from RMSAComponents where YEAR=? and DistId=? order by MandalName";
            st = con.prepareStatement(query);
            st.setString(1, year);
            st.setString(2, distId);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("mndCode", rs.getString(1));
                    map.put("mndname", rs.getString(2));
                    mandalList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }

    /// Omkaram addded 30082017
    public ArrayList getComponentVillages(String mandalId, String year) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList villageList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct VillageId,VillageName from RMSAComponents where YEAR=? and MandalId=? order by VillageName";
            st = con.prepareStatement(query);
            st.setString(1, year);
            st.setString(2, mandalId);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("vcode", rs.getString(1));
                    map.put("vname", rs.getString(2));
                    villageList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return villageList;
    }

    /// Omkaram addded 30082017
    public ArrayList getComponentSchools(String villageId, String year) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList schoolList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct SchoolId,SchoolName from RMSAComponents where YEAR=? and VillageId=? order by SchoolName";
            st = con.prepareStatement(query);
            st.setString(1, year);
            st.setString(2, villageId);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schoolCode", rs.getString(1));
                    map.put("schoolName", rs.getString(2));
                    schoolList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoolList;
    }

    /// Omkaram addded 29082017
    public RMSAComponentForm getRmsaComponentMaster(RMSAComponentForm newForm) {
        //RMSAComponentForm myform = null;
        //myform = new RMSAComponentForm();
        String query = null;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select * from RMSAComponents where SchoolId=? and YEAR=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, newForm.getSchoolCode());
            pstmt.setString(2, newForm.getYear());
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    newForm.setSno(rs.getString(1).toString());
                    newForm.setDistrictId(rs.getString(2).toString());
                    newForm.setDistrictName(rs.getString(3).toString());
                    newForm.setMandalId(rs.getString(4).toString());
                    newForm.setMandalName(rs.getString(5).toString());
                    newForm.setVillageId(rs.getString(6).toString());
                    newForm.setVillageName(rs.getString(7).toString());
                    newForm.setSchoolCode(rs.getString(8).toString());
                    newForm.setSchoolName(rs.getString(9).toString());
                    newForm.setBankAccountNumber(rs.getString(10).toString());
                    newForm.setIfscCode(rs.getString(11).toString());
                    newForm.setBankName(rs.getString(12).toString());
                    newForm.setYear(rs.getString(13).toString());

                    //Civil Works
                    BigDecimal civilWorksReleased = new BigDecimal(rs.getDouble(14));
                    newForm.setCivilWorksReleased(civilWorksReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal civilWorksSpent = new BigDecimal(rs.getDouble(20));
                    newForm.setCivilWorksSpent(civilWorksSpent.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal civilWorksAvailable = civilWorksReleased.subtract(civilWorksSpent);
                    newForm.setCivilWorksAvailable(civilWorksAvailable.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal majorRepairsReleased = new BigDecimal(rs.getDouble(15));
                    newForm.setMajorRepairsReleased(majorRepairsReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal majorRepairsSpent = new BigDecimal(rs.getDouble(21));
                    newForm.setMajorRepairsSpent(majorRepairsSpent.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal majorRepairsAvailable = majorRepairsReleased.subtract(majorRepairsSpent);
                    newForm.setMajorRepairsAvailable(majorRepairsAvailable.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal minorRepairsReleased = new BigDecimal(rs.getDouble(16));
                    newForm.setMinorRepairsReleased(minorRepairsReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal minorRepairsSpent = new BigDecimal(rs.getDouble(22));
                    newForm.setMinorRepairsSpent(minorRepairsSpent.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal minorRepairsAvailable = minorRepairsReleased.subtract(minorRepairsSpent);
                    newForm.setMinorRepairsAvailable(minorRepairsAvailable.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal annualAndOtherReleased = new BigDecimal(rs.getDouble(17));
                    newForm.setAnnualAndOtherReleased(annualAndOtherReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal toiletsReleased = new BigDecimal(rs.getDouble(18));
                    newForm.setToiletsReleased(toiletsReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal toiletsSpent = new BigDecimal(rs.getDouble(24));
                    newForm.setToiletsSpent(toiletsSpent.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal toiletsAvailable = toiletsReleased.subtract(toiletsSpent);
                    newForm.setToiletsAvailable(toiletsAvailable.setScale(2, RoundingMode.HALF_UP).toString());

                    // int totalCivilReleased = Integer.parseInt(rs.getString(14).toString()) + Integer.parseInt(rs.getString(15).toString()) + Integer.parseInt(rs.getString(16).toString()) + Integer.parseInt(rs.getString(17).toString()) + Integer.parseInt(rs.getString(18).toString()) + Integer.parseInt(rs.getString(19).toString());
                    BigDecimal totalCivilReleased = civilWorksReleased.add(majorRepairsReleased).add(minorRepairsReleased).add(toiletsReleased);
                    newForm.setTotalCivilReleased(totalCivilReleased.setScale(2, RoundingMode.HALF_UP).toString());

                    // int totalCivilSpent = Integer.parseInt(rs.getString(20).toString()) + Integer.parseInt(rs.getString(21).toString()) + Integer.parseInt(rs.getString(22).toString()) + Integer.parseInt(rs.getString(23).toString()) + Integer.parseInt(rs.getString(24).toString()) + Integer.parseInt(rs.getString(25).toString());
                    BigDecimal totalCivilSpent = civilWorksSpent.add(majorRepairsSpent).add(minorRepairsSpent).add(toiletsSpent);
                    newForm.setTotalCivilSpent(totalCivilSpent.setScale(2, RoundingMode.HALF_UP).toString());

                    BigDecimal totalCivilAvailable = totalCivilReleased.subtract(totalCivilSpent);
                    newForm.setTotalCivilAvailable(totalCivilAvailable.setScale(2, RoundingMode.HALF_UP).toString());

                    //Annual Grants 
                    BigDecimal waterElectricityTelephoneChargesReleased1 = new BigDecimal(rs.getDouble(26));
                    BigDecimal waterElectricityTelephoneChargesReleased = waterElectricityTelephoneChargesReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setWaterElectricityTelephoneChargesReleased(waterElectricityTelephoneChargesReleased.toString());
                    BigDecimal waterElectricityTelephoneChargesSpent1 = new BigDecimal(rs.getDouble(31));
                    BigDecimal waterElectricityTelephoneChargesSpent = waterElectricityTelephoneChargesSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setWaterElectricityTelephoneChargesSpent(waterElectricityTelephoneChargesSpent.setScale(2, RoundingMode.HALF_UP).toString());
                    BigDecimal waterElectricityTelephoneChargesAvailable = waterElectricityTelephoneChargesReleased.subtract(waterElectricityTelephoneChargesSpent);
                    newForm.setWaterElectricityTelephoneChargesAvailable(waterElectricityTelephoneChargesAvailable.toString());

                    BigDecimal purchaseofBooksAndPeriodicalsAndNewsPapersReleased1 = new BigDecimal(rs.getDouble(27));
                    BigDecimal purchaseofBooksAndPeriodicalsAndNewsPapersReleased = purchaseofBooksAndPeriodicalsAndNewsPapersReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setPurchaseofBooksAndPeriodicalsAndNewsPapersReleased(purchaseofBooksAndPeriodicalsAndNewsPapersReleased.toString());

                    BigDecimal purchaseofBooksAndPeriodicalsAndNewsPapersSpent1 = new BigDecimal(rs.getDouble(32));
                    BigDecimal purchaseofBooksAndPeriodicalsAndNewsPapersSpent = purchaseofBooksAndPeriodicalsAndNewsPapersSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setPurchaseofBooksAndPeriodicalsAndNewsPapersSpent(purchaseofBooksAndPeriodicalsAndNewsPapersSpent.toString());

                    BigDecimal purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = purchaseofBooksAndPeriodicalsAndNewsPapersReleased.subtract(purchaseofBooksAndPeriodicalsAndNewsPapersSpent);
                    newForm.setPurchaseofBooksAndPeriodicalsAndNewsPapersAvailable(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable.toString());

                    BigDecimal minorRepairsAnnualReleased1 = new BigDecimal(rs.getDouble(28));
                    BigDecimal minorRepairsAnnualReleased = minorRepairsAnnualReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setMinorRepairsAnnualReleased(minorRepairsAnnualReleased.toString());
                    BigDecimal minorRepairsAnnualSpent1 = new BigDecimal(rs.getDouble(33));
                    BigDecimal minorRepairsAnnualSpent = minorRepairsAnnualSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setMinorRepairsAnnualSpent(minorRepairsAnnualSpent.toString());
                    BigDecimal minorRepairsAnnualAvailable = minorRepairsAnnualReleased.subtract(minorRepairsAnnualSpent);
                    newForm.setMinorRepairsAnnualAvailable(minorRepairsAnnualAvailable.toString());

                    BigDecimal sanitationAndICTReleased1 = new BigDecimal(rs.getDouble(29));
                    BigDecimal sanitationAndICTReleased = sanitationAndICTReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setSanitationAndICTReleased(sanitationAndICTReleased.toString());
                    BigDecimal sanitationAndICTSpent1 = new BigDecimal(rs.getDouble(34));
                    BigDecimal sanitationAndICTSpent = sanitationAndICTSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setSanitationAndICTSpent(sanitationAndICTSpent.toString());
                    BigDecimal sanitationAndICTAvailable = sanitationAndICTReleased.subtract(sanitationAndICTSpent);
                    newForm.setSanitationAndICTAvailable(sanitationAndICTAvailable.toString());

                    BigDecimal needBasedWorksReleased1 = new BigDecimal(rs.getDouble(30));
                    BigDecimal needBasedWorksReleased = needBasedWorksReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setNeedBasedWorksReleased(needBasedWorksReleased.toString());
                    BigDecimal needBasedWorksSpent1 = new BigDecimal(rs.getDouble(35));
                    BigDecimal needBasedWorksSpent = needBasedWorksSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setNeedBasedWorksSpent(needBasedWorksSpent.toString());
                    BigDecimal needBasedWorksAvailable = needBasedWorksReleased.subtract(needBasedWorksSpent);
                    newForm.setNeedBasedWorksAvailable(needBasedWorksAvailable.toString());

                    BigDecimal provisionalLaboratoryReleased1 = new BigDecimal(rs.getDouble(48));
                    BigDecimal provisionalLaboratoryReleased = provisionalLaboratoryReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setProvisionalLaboratoryReleased(provisionalLaboratoryReleased.toString());
                    BigDecimal provisionalLaboratorySpent1 = new BigDecimal(rs.getDouble(49));
                    BigDecimal provisionalLaboratorySpent = provisionalLaboratorySpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setProvisionalLaboratorySpent(provisionalLaboratorySpent.toString());
                    BigDecimal provisionalLaboratoryAvailable = provisionalLaboratoryReleased.subtract(provisionalLaboratorySpent);
                    newForm.setProvisionalLaboratoryAvailable(provisionalLaboratoryAvailable.toString());

                    //int totalAnnualReleased = Integer.parseInt(rs.getString(26).toString()) + Integer.parseInt(rs.getString(27).toString()) + Integer.parseInt(rs.getString(28).toString()) + Integer.parseInt(rs.getString(29).toString()) + Integer.parseInt(rs.getString(30).toString()) + Integer.parseInt(rs.getString(48).toString());
                    BigDecimal totalAnnualReleased = waterElectricityTelephoneChargesReleased.add(purchaseofBooksAndPeriodicalsAndNewsPapersReleased).add(minorRepairsAnnualReleased).add(sanitationAndICTReleased).add(needBasedWorksReleased).add(provisionalLaboratoryReleased);
                    newForm.setTotalAnnualReleased(totalAnnualReleased.toString());
                    //int totalAnnualSpent = Integer.parseInt(rs.getString(31).toString()) + Integer.parseInt(rs.getString(32).toString()) + Integer.parseInt(rs.getString(33).toString()) + Integer.parseInt(rs.getString(34).toString()) + Integer.parseInt(rs.getString(35).toString()) + Integer.parseInt(rs.getString(49).toString());
                    BigDecimal totalAnnualSpent = waterElectricityTelephoneChargesSpent.add(purchaseofBooksAndPeriodicalsAndNewsPapersSpent).add(minorRepairsAnnualSpent).add(sanitationAndICTSpent).add(needBasedWorksSpent).add(provisionalLaboratorySpent);
                    newForm.setTotalAnnualSpent(totalAnnualSpent.toString());
                    BigDecimal totalAnnualAvailable = totalAnnualReleased.subtract(totalAnnualSpent);
                    newForm.setTotalAnnualAvailable(totalAnnualAvailable.toString());

                    //Furniture & Lab Equipment
                    BigDecimal furnitureReleased1 = new BigDecimal(rs.getDouble(36));
                    BigDecimal furnitureReleased = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setFurnitureReleased(furnitureReleased.toString());
                    BigDecimal furnitureSpent1 = new BigDecimal(rs.getDouble(38));
                    BigDecimal furnitureSpent = furnitureSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setFurnitureSpent(furnitureSpent.toString());
                    BigDecimal furnitureAvailable = furnitureReleased.subtract(furnitureSpent);
                    newForm.setFurnitureAvailable(furnitureAvailable.toString());

                    BigDecimal labEquipmentReleased1 = new BigDecimal(rs.getDouble(37));
                    BigDecimal labEquipmentReleased = labEquipmentReleased1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setLabEquipmentReleased(labEquipmentReleased.toString());
                    BigDecimal labEquipmentSpent1 = new BigDecimal(rs.getDouble(39));
                    BigDecimal labEquipmentSpent = labEquipmentSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setLabEquipmentSpent(labEquipmentSpent.toString());
                    BigDecimal labEquipmentAvailable = labEquipmentReleased.subtract(labEquipmentSpent);
                    newForm.setLabEquipmentAvailable(labEquipmentAvailable.toString());


                    BigDecimal totalfurnitureAndLabReleased = furnitureReleased.add(labEquipmentReleased);
                    newForm.setTotalfurnitureAndLabReleased(totalfurnitureAndLabReleased.toString());
                    BigDecimal totalfurnitureAndLabSpent = furnitureSpent.add(labEquipmentSpent);
                    newForm.setTotalfurnitureAndLabSpent(totalfurnitureAndLabSpent.toString());
                    BigDecimal totalfurnitureAndLabAvailable = totalfurnitureAndLabReleased.subtract(totalfurnitureAndLabSpent);
                    newForm.setTotalfurnitureAndLabAvailable(totalfurnitureAndLabAvailable.toString());

                    //Recurring
                    BigDecimal recurringExcursionRelease1 = new BigDecimal(rs.getDouble(40));
                    BigDecimal recurringExcursionRelease = recurringExcursionRelease1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringExcursionRelease(recurringExcursionRelease.toString());
                    BigDecimal RecurringSelfDefenseRelease1 = new BigDecimal(rs.getDouble(41));
                    BigDecimal RecurringSelfDefenseRelease = RecurringSelfDefenseRelease1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringSelfDefenseRelease(RecurringSelfDefenseRelease.toString());
                    BigDecimal recurringOtherRelease1 = new BigDecimal(rs.getDouble(42));
                    BigDecimal recurringOtherRelease = recurringOtherRelease1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringOtherRelease(recurringOtherRelease.toString());

                    BigDecimal recurringExcursionSpent1 = new BigDecimal(rs.getDouble(43));
                    BigDecimal recurringExcursionSpent = recurringExcursionSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringExcursionSpent(recurringExcursionSpent.toString());
                    BigDecimal recurringSelfDefenseSpent1 = new BigDecimal(rs.getDouble(44));
                    BigDecimal recurringSelfDefenseSpent = recurringSelfDefenseSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringSelfDefenseSpent(recurringSelfDefenseSpent.toString());
                    BigDecimal recurringOtherSpent1 = new BigDecimal(rs.getDouble(45));
                    BigDecimal recurringOtherSpent = recurringOtherSpent1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setRecurringOtherSpent(recurringOtherSpent.toString());

                    BigDecimal recurringTotalRelease = recurringExcursionRelease.add(RecurringSelfDefenseRelease).add(recurringOtherRelease);
                    newForm.setRecurringTotalRelease(recurringTotalRelease.toString());

                    BigDecimal recurringTotalSpent = recurringExcursionSpent.add(recurringSelfDefenseSpent).add(recurringOtherSpent);
                    newForm.setRecurringTotalSpent(recurringTotalSpent.toString());

                    BigDecimal recurringExcursionAvailable = recurringExcursionRelease.subtract(recurringExcursionSpent);
                    newForm.setRecurringExcursionAvailable(recurringExcursionAvailable.toString());

                    BigDecimal recurringSelfDefenseAvailable = RecurringSelfDefenseRelease.subtract(recurringSelfDefenseSpent);
                    newForm.setRecurringSelfDefenseAvailable(recurringSelfDefenseAvailable.toString());

                    BigDecimal recurringOtherAvailable = recurringOtherRelease.subtract(recurringOtherSpent);
                    newForm.setRecurringOtherAvailable(recurringOtherAvailable.toString());

                    BigDecimal recurringTotalAvailable = recurringTotalRelease.subtract(recurringTotalSpent);
                    newForm.setRecurringTotalAvailable(recurringTotalAvailable.toString());

                    BigDecimal interestEarned1 = new BigDecimal(rs.getDouble(72));
                    BigDecimal interestEarned = interestEarned1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setInterestEarned(interestEarned.toString());

                    BigDecimal interestExpenditure1 = new BigDecimal(rs.getDouble(73));
                    BigDecimal interestExpenditure = interestExpenditure1.setScale(2, RoundingMode.HALF_UP);
                    newForm.setInterestExpenditure(interestExpenditure.toString());

                    BigDecimal interestBalance = interestEarned.subtract(interestExpenditure);
                    newForm.setInterestBalance(interestBalance.toString());

                }
            } else {
                newForm = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newForm;
    }

    /// Omkaram addded
    public int updateRmsaComponentMaster(RMSAComponentForm myform, String roleId) {
        int myReturn = 0;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        //CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            if (roleId.equals(CommonConstants.StateOrAdminRole) || roleId.equals(CommonConstants.ApwidcRole)) {
                query = "update RMSAComponents set CivilWork_CivilReleased='" + myform.getCivilWorksReleased() + "',MajorRepair_CivilReleased='" + myform.getMajorRepairsReleased() + "',MinorRepair_CivilReleased='" + myform.getMinorRepairsReleased() + "',Toilets_CivilReleased='" + myform.getToiletsReleased()
                        + "',Excursion_RecurringReleased='" + myform.getRecurringExcursionRelease() + "',SelfDefense_RecurringReleased='" + myform.getRecurringSelfDefenseRelease() + "',Other_RecurringReleased='" + myform.getRecurringOtherRelease()
                        + "',WaterElecticity_AnnualReleased='" + myform.getWaterElectricityTelephoneChargesReleased() + "',BooksPurchased_AnnualReleased='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersReleased() + "',MinorRepairs_AnnualReleased='" + myform.getMinorRepairsAnnualReleased() + "',SanitationICT_AnnualReleased='" + myform.getSanitationAndICTReleased() + "',NeedBasedWorks_AnnualReleased='" + myform.getNeedBasedWorksReleased()
                        + "',Furniture_Released='" + myform.getFurnitureReleased() + "',LabEquipment_Released='" + myform.getLabEquipmentReleased() + "',ProvisionalLaboratory_AnnualReleased='" + myform.getProvisionalLaboratoryReleased() + "' ,Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear() + "'";
            } else if (roleId.equals(CommonConstants.DistrictRole) && myform.getYear().equalsIgnoreCase("2016-17")) {
                query = "update RMSAComponents set CivilWork_CivilReleased='" + myform.getCivilWorksReleased() + "',MajorRepair_CivilReleased='" + myform.getMajorRepairsReleased() + "',MinorRepair_CivilReleased='" + myform.getMinorRepairsReleased() + "',Toilets_CivilReleased='" + myform.getToiletsReleased()
                        + "',Excursion_RecurringReleased='" + myform.getRecurringExcursionRelease() + "',SelfDefense_RecurringReleased='" + myform.getRecurringSelfDefenseRelease() + "',Other_RecurringReleased='" + myform.getRecurringOtherRelease()
                        + "',WaterElecticity_AnnualReleased='" + myform.getWaterElectricityTelephoneChargesReleased() + "',BooksPurchased_AnnualReleased='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersReleased() + "',MinorRepairs_AnnualReleased='" + myform.getMinorRepairsAnnualReleased() + "',SanitationICT_AnnualReleased='" + myform.getSanitationAndICTReleased() + "',NeedBasedWorks_AnnualReleased='" + myform.getNeedBasedWorksReleased()
                        + "',Furniture_Released='" + myform.getFurnitureReleased() + "',LabEquipment_Released='" + myform.getLabEquipmentReleased() + "',ProvisionalLaboratory_AnnualReleased='" + myform.getProvisionalLaboratoryReleased()
                        //+ "' ,Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear()
                        + "', CivilWork_CivilSpent='" + myform.getCivilWorksSpent() + "',MajorRepair_CivilSpent='" + myform.getMajorRepairsSpent() + "',MinorRepair_CivilSpent='" + myform.getMinorRepairsSpent() + "',Toilets_CivilSpent='" + myform.getToiletsSpent()
                        + "',WaterElecticity_AnnualSpent='" + myform.getWaterElectricityTelephoneChargesSpent() + "',BooksPurchased_AnnualSpent='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() + "',MinorRepairs_AnnualSpent='" + myform.getMinorRepairsAnnualSpent() + "',SanitationICT_AnnualSpent='" + myform.getSanitationAndICTSpent() + "',NeedBasedWorks_AnnualSpent='" + myform.getNeedBasedWorksSpent() + "',ProvisionalLaboratory_AnnualSpent='" + myform.getProvisionalLaboratorySpent()
                        + "',Excursion_RecurringSpent='" + myform.getRecurringExcursionSpent() + "',SelfDefense_RecurringSpent='" + myform.getRecurringSelfDefenseSpent() + "',Other_RecurringSpent='" + myform.getRecurringOtherSpent()
                        + "',Furniture_Spent='" + myform.getFurnitureSpent() + "',LabEquipment_Spent='" + myform.getLabEquipmentSpent() + "',Earned_Interest='" + myform.getInterestEarned() + "',Expenditure_Interest='" + myform.getInterestExpenditure() + "',Balance_Interest='" + myform.getInterestBalance()
                        + "',Deo_Submitted='YES',Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear() + "'";
            } else if (roleId.equals(CommonConstants.DistrictRole)) {
                query = "update RMSAComponents set CivilWork_CivilSpent='" + myform.getCivilWorksSpent() + "',MajorRepair_CivilSpent='" + myform.getMajorRepairsSpent() + "',MinorRepair_CivilSpent='" + myform.getMinorRepairsSpent() + "',Toilets_CivilSpent='" + myform.getToiletsSpent()
                        + "',WaterElecticity_AnnualSpent='" + myform.getWaterElectricityTelephoneChargesSpent() + "',BooksPurchased_AnnualSpent='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() + "',MinorRepairs_AnnualSpent='" + myform.getMinorRepairsAnnualSpent() + "',SanitationICT_AnnualSpent='" + myform.getSanitationAndICTSpent() + "',NeedBasedWorks_AnnualSpent='" + myform.getNeedBasedWorksSpent() + "',ProvisionalLaboratory_AnnualSpent='" + myform.getProvisionalLaboratorySpent()
                        + "',Excursion_RecurringSpent='" + myform.getRecurringExcursionSpent() + "',SelfDefense_RecurringSpent='" + myform.getRecurringSelfDefenseSpent() + "',Other_RecurringSpent='" + myform.getRecurringOtherSpent()
                        + "',Furniture_Spent='" + myform.getFurnitureSpent() + "',LabEquipment_Spent='" + myform.getLabEquipmentSpent() + "',Earned_Interest='" + myform.getInterestEarned() + "',Expenditure_Interest='" + myform.getInterestExpenditure() + "',Balance_Interest='" + myform.getInterestBalance()
                        + "',Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear() + "'";
            } else {
                if (myform.getYear().equalsIgnoreCase("2016-17") && roleId.equals(CommonConstants.SchoolRole)) {
                    query = "update RMSAComponents set CivilWork_CivilReleased='" + myform.getCivilWorksReleased() + "',MajorRepair_CivilReleased='" + myform.getMajorRepairsReleased() + "',MinorRepair_CivilReleased='" + myform.getMinorRepairsReleased() + "',Toilets_CivilReleased='" + myform.getToiletsReleased()
                            + "',Excursion_RecurringReleased='" + myform.getRecurringExcursionRelease() + "',SelfDefense_RecurringReleased='" + myform.getRecurringSelfDefenseRelease() + "',Other_RecurringReleased='" + myform.getRecurringOtherRelease()
                            + "',WaterElecticity_AnnualReleased='" + myform.getWaterElectricityTelephoneChargesReleased() + "',BooksPurchased_AnnualReleased='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersReleased() + "',MinorRepairs_AnnualReleased='" + myform.getMinorRepairsAnnualReleased() + "',SanitationICT_AnnualReleased='" + myform.getSanitationAndICTReleased() + "',NeedBasedWorks_AnnualReleased='" + myform.getNeedBasedWorksReleased()
                            + "',Furniture_Released='" + myform.getFurnitureReleased() + "',LabEquipment_Released='" + myform.getLabEquipmentReleased() + "',ProvisionalLaboratory_AnnualReleased='" + myform.getProvisionalLaboratoryReleased()
                            //+ "' ,Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear()
                            + "', CivilWork_CivilSpent='" + myform.getCivilWorksSpent() + "',MajorRepair_CivilSpent='" + myform.getMajorRepairsSpent() + "',MinorRepair_CivilSpent='" + myform.getMinorRepairsSpent() + "',Toilets_CivilSpent='" + myform.getToiletsSpent()
                            + "',WaterElecticity_AnnualSpent='" + myform.getWaterElectricityTelephoneChargesSpent() + "',BooksPurchased_AnnualSpent='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() + "',MinorRepairs_AnnualSpent='" + myform.getMinorRepairsAnnualSpent() + "',SanitationICT_AnnualSpent='" + myform.getSanitationAndICTSpent() + "',NeedBasedWorks_AnnualSpent='" + myform.getNeedBasedWorksSpent() + "',ProvisionalLaboratory_AnnualSpent='" + myform.getProvisionalLaboratorySpent()
                            + "',Excursion_RecurringSpent='" + myform.getRecurringExcursionSpent() + "',SelfDefense_RecurringSpent='" + myform.getRecurringSelfDefenseSpent() + "',Other_RecurringSpent='" + myform.getRecurringOtherSpent()
                            + "',Furniture_Spent='" + myform.getFurnitureSpent() + "',LabEquipment_Spent='" + myform.getLabEquipmentSpent() + "',Earned_Interest='" + myform.getInterestEarned() + "',Expenditure_Interest='" + myform.getInterestExpenditure() + "',Balance_Interest='" + myform.getInterestBalance()
                            + "',Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear() + "'";
                } else {
                    query = "update RMSAComponents set CivilWork_CivilSpent='" + myform.getCivilWorksSpent() + "',MajorRepair_CivilSpent='" + myform.getMajorRepairsSpent() + "',MinorRepair_CivilSpent='" + myform.getMinorRepairsSpent() + "',Toilets_CivilSpent='" + myform.getToiletsSpent()
                            + "',WaterElecticity_AnnualSpent='" + myform.getWaterElectricityTelephoneChargesSpent() + "',BooksPurchased_AnnualSpent='" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() + "',MinorRepairs_AnnualSpent='" + myform.getMinorRepairsAnnualSpent() + "',SanitationICT_AnnualSpent='" + myform.getSanitationAndICTSpent() + "',NeedBasedWorks_AnnualSpent='" + myform.getNeedBasedWorksSpent() + "',ProvisionalLaboratory_AnnualSpent='" + myform.getProvisionalLaboratorySpent()
                            + "',Excursion_RecurringSpent='" + myform.getRecurringExcursionSpent() + "',SelfDefense_RecurringSpent='" + myform.getRecurringSelfDefenseSpent() + "',Other_RecurringSpent='" + myform.getRecurringOtherSpent()
                            + "',Furniture_Spent='" + myform.getFurnitureSpent() + "',LabEquipment_Spent='" + myform.getLabEquipmentSpent()
                            + "',Updatedby='" + myform.getUserName() + "',UpdatedDate=getdate() where SchoolId='" + myform.getSchoolCode() + "' and YEAR='" + myform.getYear() + "'";
                }
            }
            pstmt = con.prepareStatement(query);
            myReturn = pstmt.executeUpdate();
        } catch (Exception e) {
            myReturn = 100;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                myReturn = 200;
                e.printStackTrace();
            }
            return myReturn;
        }
    }

    /// Omkaram addded 22092017
    public ArrayList getReleaseMaster(String schoolCode, String phNo) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        double totalRelease = 0;
        ArrayList releaseMasterList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select *from RMSA_Release_Master where school_code=? and Phase=? order by created_date";
            st = con.prepareStatement(query);
            st.setString(1, schoolCode);
            st.setString(2, phNo);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("phaseNo", rs.getString(2));
                    map.put("schoolCode", rs.getString(3));
                    map.put("releaseAmount", rs.getString(4));
                    double d = Double.parseDouble(rs.getString(4).toString());
                    totalRelease = totalRelease + d;
                    d = 0;
                    map.put("totalRelease", totalRelease);
                    map.put("createdDate", rs.getString(5));
                    map.put("createdBy", rs.getString(6));
                    releaseMasterList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return releaseMasterList;
    }

    /// Omkaram addded 25092017
    public int updateReleaseMaster(RMSAForm myform) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        String totalRelease = null;
        Statement stmt = null;
        int myReturn = 0;
        int[] myReturn1;
        ArrayList releaseMasterList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        int count = 0;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            stmt = con.createStatement();

            if (myform.getReleasesRow() != null) {
                for (int i = 0; i <= myform.getReleasesRow().length - 1; i++) {
                    query = "select count(*) from RMSA_Release_Master where school_code=? and Phase=? and ReleaseAmount=? and Release_date=? and CreatedBy=? ";
                    st = con.prepareStatement(query);
                    st.setString(1, myform.getSchoolId());
                    st.setString(2, myform.getPhaseNo());
                    st.setString(3, myform.getReleasesRow()[i]);
                    st.setString(4, myform.getExpenditureIncurredDate()[i]);
                    st.setString(5, myform.getUserName());
                    rs = st.executeQuery();
                    if (rs != null) {
                        while (rs.next()) {
                            count = rs.getInt(1);
                        }
                    }
                    if (count == 0) {
                        query = "insert into RMSA_Release_Master"
                                + "(Phase,school_code,ReleaseAmount,Release_date,CreatedBy,created_date)"
                                + " values('" + myform.getPhaseNo() + "','" + myform.getSchoolId() + "','" + myform.getReleasesRow()[i] + "','" + myform.getExpenditureIncurredDate()[i] + "','" + myform.getUserName() + "',getDate())";
                        stmt.addBatch(query);
//                        float a = Float.parseFloat(myform.getReleasesRow()[i]);
//                        float b = Float.parseFloat(myform.getReleases());
//                        float c = a + b;
//                        double d = (double) c;
//                        DecimalFormat df = new DecimalFormat("###.##");
//                        df.format(d);
//                        myform.setReleases(df.format(d).toString());
//                        df = null;
//                        query = "update RMSAMASTER set Releases='" + myform.getReleases() + "' where UDISE_CODE='" + myform.getSchoolId() + "' and PHASE='" + myform.getPhaseNo() + "'";
//                        stmt.addBatch(query);
                    }
                }

            }
            myReturn1 = stmt.executeBatch();
            if (myReturn1.length > 0) {
                myReturn = 1;
            }
        } catch (Exception e) {
            myReturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                myReturn = 0;
                e.printStackTrace();
            }
        }
        return myReturn;
    }

    /// Omkaram addded 25092017
    public ArrayList getComponentDistrict() throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList distList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distinct DistId,DistrictName from RMSAComponents order by DistrictName";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("distCode", rs.getString(1));
                    map.put("distname", rs.getString(2));
                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }

/// Omkaram addded 07102017
    public ArrayList<RMSAForm> readCivilsDataFromExcel(String fileName, String fileType, String distId) {
        ArrayList<RMSAForm> vectorArrayList = new ArrayList<RMSAForm>();
        try {
            FileInputStream myInput = new FileInputStream(fileName);
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
            //DataFormatter objDefaultFormat = new DataFormatter();
            //FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) myWorkBook);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator rowIter = mySheet.rowIterator();
            while (rowIter != null && rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                Vector cellStoreVector = null;
                cellStoreVector = new Vector();
                if (myRow != null) {
                    Iterator cellIter = myRow.cellIterator();
                    while (cellIter.hasNext()) {
                        HSSFCell myCell = (HSSFCell) cellIter.next();
                        cellStoreVector.addElement(myCell);
                    }
                }
                if (!cellStoreVector.get(0).toString().toUpperCase().equalsIgnoreCase("S.NO") && !cellStoreVector.get(0).toString().toUpperCase().equalsIgnoreCase("SNO")) {
                    if (cellStoreVector.get(3) != null) {
                        if (cellStoreVector.get(3).toString().length() > 10) {
                            if (distId.equalsIgnoreCase(cellStoreVector.get(3).toString().substring(0, 4))) {
                                RMSAForm myform = new RMSAForm();
                                String phaseNo = null;
                                if (cellStoreVector.get(1) == null) {
                                    phaseNo = "0";
                                } else if (cellStoreVector.get(1).toString().equalsIgnoreCase("")) {
                                    phaseNo = "0";
                                } else if (cellStoreVector.get(1).toString().startsWith("'")) {
                                    phaseNo = cellStoreVector.get(1).toString().startsWith("'") ? cellStoreVector.get(1).toString().substring(1) : cellStoreVector.get(1).toString();
                                    phaseNo = NumberToTextConverter.toText(Double.parseDouble(phaseNo));
                                } else {
                                    phaseNo = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(1).toString()));
                                }
                                myform.setPhaseNo(phaseNo);
                                phaseNo = null;
                                String year = null;
                                if (cellStoreVector.get(2) == null) {
                                    year = "0";
                                } else if (cellStoreVector.get(2).toString().equalsIgnoreCase("")) {
                                    year = "0";
                                } else if (cellStoreVector.get(2).toString().startsWith("'")) {
                                    year = cellStoreVector.get(2).toString().startsWith("'") ? cellStoreVector.get(2).toString().substring(1) : cellStoreVector.get(2).toString();
                                } else {
                                    year = cellStoreVector.get(2).toString();
                                }
                                myform.setYear(year);
                                year = null;
                                String schoolId = null;
                                if (cellStoreVector.get(3) == null) {
                                    schoolId = "0";
                                } else if (cellStoreVector.get(3).toString().equalsIgnoreCase("")) {
                                    schoolId = "0";
                                } else if (cellStoreVector.get(3).toString().startsWith("'")) {
                                    schoolId = cellStoreVector.get(3).toString().startsWith("'") ? cellStoreVector.get(3).toString().substring(1) : cellStoreVector.get(3).toString();
                                    schoolId = NumberToTextConverter.toText(Double.parseDouble(schoolId));
                                } else {
                                    schoolId = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(3).toString()));
                                }
                                myform.setSchoolId(schoolId);
                                schoolId = null;
                                String schoolName = null;
                                if (cellStoreVector.get(4) == null) {
                                    schoolName = "0";
                                } else if (cellStoreVector.get(4).toString().equalsIgnoreCase("")) {
                                    schoolName = "0";
                                } else if (cellStoreVector.get(4).toString().startsWith("'")) {
                                    schoolName = cellStoreVector.get(4).toString().startsWith("'") ? cellStoreVector.get(4).toString().substring(1) : cellStoreVector.get(4).toString();
                                } else {
                                    schoolName = cellStoreVector.get(4).toString();
                                }
                                myform.setSchoolName(schoolName);
                                schoolName = null;
                                year = null;
                                String artCraftSanction = null;
                                if (cellStoreVector.get(5) == null) {
                                    artCraftSanction = "0";
                                } else if (cellStoreVector.get(5).toString().equalsIgnoreCase("")) {
                                    artCraftSanction = "0";
                                } else if (cellStoreVector.get(5).toString().startsWith("'")) {
                                    artCraftSanction = cellStoreVector.get(5).toString().startsWith("'") ? cellStoreVector.get(5).toString().substring(1) : cellStoreVector.get(5).toString();
                                    artCraftSanction = NumberToTextConverter.toText(Double.parseDouble(artCraftSanction));
                                } else {
                                    artCraftSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(5).toString()));
                                }
                                myform.setArtCraftSanction(artCraftSanction);
                                artCraftSanction = null;

                                String scienceLabSanction = null;
                                if (cellStoreVector.get(6) == null) {
                                    scienceLabSanction = "0";
                                } else if (cellStoreVector.get(6).toString().equalsIgnoreCase("")) {
                                    scienceLabSanction = "0";
                                } else if (cellStoreVector.get(6).toString().startsWith("'")) {
                                    scienceLabSanction = cellStoreVector.get(6).toString().startsWith("'") ? cellStoreVector.get(6).toString().substring(1) : cellStoreVector.get(6).toString();
                                    scienceLabSanction = NumberToTextConverter.toText(Double.parseDouble(scienceLabSanction));
                                } else {
                                    scienceLabSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(6).toString()));
                                }
                                myform.setScienceLabSanction(scienceLabSanction);
                                scienceLabSanction = null;

                                String additionalClassRoomSanction = null;
                                if (cellStoreVector.get(7) == null) {
                                    additionalClassRoomSanction = "0";
                                } else if (cellStoreVector.get(7).toString().equalsIgnoreCase("")) {
                                    additionalClassRoomSanction = "0";
                                } else if (cellStoreVector.get(7).toString().startsWith("'")) {
                                    additionalClassRoomSanction = cellStoreVector.get(7).toString().startsWith("'") ? cellStoreVector.get(7).toString().substring(1) : cellStoreVector.get(7).toString();
                                    additionalClassRoomSanction = NumberToTextConverter.toText(Double.parseDouble(additionalClassRoomSanction));
                                } else {
                                    additionalClassRoomSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(7).toString()));
                                }
                                myform.setAdditionalClassRoomSanction(additionalClassRoomSanction);
                                additionalClassRoomSanction = null;

                                String drinkingWaterSanction = null;
                                if (cellStoreVector.get(8) == null) {
                                    drinkingWaterSanction = "0";
                                } else if (cellStoreVector.get(8).toString().equalsIgnoreCase("")) {
                                    drinkingWaterSanction = "0";
                                } else if (cellStoreVector.get(8).toString().startsWith("'")) {
                                    drinkingWaterSanction = cellStoreVector.get(8).toString().startsWith("'") ? cellStoreVector.get(8).toString().substring(1) : cellStoreVector.get(8).toString();
                                    drinkingWaterSanction = NumberToTextConverter.toText(Double.parseDouble(drinkingWaterSanction));
                                } else {
                                    drinkingWaterSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(8).toString()));
                                }
                                myform.setDrinkingWaterSanction(drinkingWaterSanction);
                                drinkingWaterSanction = null;

                                String computerRoomSanction = null;
                                if (cellStoreVector.get(9) == null) {
                                    computerRoomSanction = "0";
                                } else if (cellStoreVector.get(9).toString().equalsIgnoreCase("")) {
                                    computerRoomSanction = "0";
                                } else if (cellStoreVector.get(9).toString().startsWith("'")) {
                                    computerRoomSanction = cellStoreVector.get(9).toString().startsWith("'") ? cellStoreVector.get(9).toString().substring(1) : cellStoreVector.get(9).toString();
                                    computerRoomSanction = NumberToTextConverter.toText(Double.parseDouble(computerRoomSanction));
                                } else {
                                    computerRoomSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(9).toString()));
                                }
                                myform.setComputerRoomSanction(computerRoomSanction);
                                computerRoomSanction = null;

                                String libraryRoomSanction = null;
                                if (cellStoreVector.get(10) == null) {
                                    libraryRoomSanction = "0";
                                } else if (cellStoreVector.get(10).toString().equalsIgnoreCase("")) {
                                    libraryRoomSanction = "0";
                                } else if (cellStoreVector.get(10).toString().startsWith("'")) {
                                    libraryRoomSanction = cellStoreVector.get(10).toString().startsWith("'") ? cellStoreVector.get(10).toString().substring(1) : cellStoreVector.get(10).toString();
                                    libraryRoomSanction = NumberToTextConverter.toText(Double.parseDouble(libraryRoomSanction));
                                } else {
                                    libraryRoomSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(10).toString()));
                                }
                                myform.setLibraryRoomSanction(libraryRoomSanction);
                                libraryRoomSanction = null;

                                String toiletBlockSanction = null;
                                if (cellStoreVector.get(11) == null) {
                                    toiletBlockSanction = "0";
                                } else if (cellStoreVector.get(11).toString().equalsIgnoreCase("")) {
                                    toiletBlockSanction = "0";
                                } else if (cellStoreVector.get(11).toString().startsWith("'")) {
                                    toiletBlockSanction = cellStoreVector.get(11).toString().startsWith("'") ? cellStoreVector.get(11).toString().substring(1) : cellStoreVector.get(11).toString();
                                    toiletBlockSanction = NumberToTextConverter.toText(Double.parseDouble(toiletBlockSanction));
                                } else {
                                    toiletBlockSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(11).toString()));
                                }
                                myform.setToiletBlockSanction(toiletBlockSanction);
                                toiletBlockSanction = null;

                                String administrativeSanction = null;
                                if (cellStoreVector.get(12) == null) {
                                    administrativeSanction = "0";
                                } else if (cellStoreVector.get(12).toString().equalsIgnoreCase("")) {
                                    administrativeSanction = "0";
                                } else if (cellStoreVector.get(12).toString().startsWith("'")) {
                                    administrativeSanction = cellStoreVector.get(12).toString().startsWith("'") ? cellStoreVector.get(12).toString().substring(1) : cellStoreVector.get(12).toString();
                                    administrativeSanction = NumberToTextConverter.toText(Double.parseDouble(administrativeSanction));
                                } else {
                                    administrativeSanction = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(12).toString()));
                                }
                                myform.setAdministrativeSanction(administrativeSanction);
                                administrativeSanction = null;

                                String totalFurniture = null;
                                if (cellStoreVector.get(13) == null) {
                                    totalFurniture = "0";
                                } else if (cellStoreVector.get(13).toString().equalsIgnoreCase("")) {
                                    totalFurniture = "0";
                                } else if (cellStoreVector.get(13).toString().startsWith("'")) {
                                    totalFurniture = cellStoreVector.get(13).toString().startsWith("'") ? cellStoreVector.get(13).toString().substring(1) : cellStoreVector.get(13).toString();
                                    totalFurniture = NumberToTextConverter.toText(Double.parseDouble(totalFurniture));
                                } else {
                                    totalFurniture = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(13).toString()));
                                }
                                myform.setTotalFurniture(totalFurniture);
                                totalFurniture = null;

                                String labEquipment = null;
                                if (cellStoreVector.get(14) == null) {
                                    labEquipment = "0";
                                } else if (cellStoreVector.get(14).toString().equalsIgnoreCase("")) {
                                    labEquipment = "0";
                                } else if (cellStoreVector.get(14).toString().startsWith("'")) {
                                    labEquipment = cellStoreVector.get(14).toString().startsWith("'") ? cellStoreVector.get(14).toString().substring(1) : cellStoreVector.get(14).toString();
                                    labEquipment = NumberToTextConverter.toText(Double.parseDouble(labEquipment));
                                } else {
                                    labEquipment = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(14).toString()));
                                }
                                myform.setLabEquipment(labEquipment);
                                labEquipment = null;

                                String furnitureAndLabEquipment = null;
                                if (cellStoreVector.get(15) == null) {
                                    furnitureAndLabEquipment = "0";
                                } else if (cellStoreVector.get(15).toString().equalsIgnoreCase("")) {
                                    furnitureAndLabEquipment = "0";
                                } else if (cellStoreVector.get(15).toString().startsWith("'")) {
                                    furnitureAndLabEquipment = cellStoreVector.get(15).toString().startsWith("'") ? cellStoreVector.get(15).toString().substring(1) : cellStoreVector.get(15).toString();
                                    furnitureAndLabEquipment = NumberToTextConverter.toText(Double.parseDouble(furnitureAndLabEquipment));
                                } else {
                                    furnitureAndLabEquipment = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(15).toString()));
                                }
                                myform.setFurnitureAndLabEquipment(furnitureAndLabEquipment);
                                furnitureAndLabEquipment = null;

                                String estimatedCost = null;
                                if (cellStoreVector.get(16) == null) {
                                    estimatedCost = "0";
                                } else if (cellStoreVector.get(16).toString().equalsIgnoreCase("")) {
                                    estimatedCost = "0";
                                } else if (cellStoreVector.get(16).toString().startsWith("'")) {
                                    estimatedCost = cellStoreVector.get(16).toString().startsWith("'") ? cellStoreVector.get(16).toString().substring(1) : cellStoreVector.get(16).toString();
                                    estimatedCost = NumberToTextConverter.toText(Double.parseDouble(estimatedCost));
                                } else {
                                    estimatedCost = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(16).toString()));
                                }
                                myform.setEstimatedCost(estimatedCost);
                                estimatedCost = null;

                                String tenderCostValue = null;
                                if (cellStoreVector.get(17) == null) {
                                    tenderCostValue = "0";
                                } else if (cellStoreVector.get(17).toString().equalsIgnoreCase("")) {
                                    tenderCostValue = "0";
                                } else if (cellStoreVector.get(17).toString().startsWith("'")) {
                                    tenderCostValue = cellStoreVector.get(17).toString().startsWith("'") ? cellStoreVector.get(17).toString().substring(1) : cellStoreVector.get(17).toString();
                                    tenderCostValue = NumberToTextConverter.toText(Double.parseDouble(tenderCostValue));
                                } else {
                                    tenderCostValue = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(17).toString()));
                                }
                                myform.setTenderCostValue(tenderCostValue);
                                tenderCostValue = null;

                                String vat = null;
                                if (cellStoreVector.get(18) == null) {
                                    vat = "0";
                                } else if (cellStoreVector.get(18).toString().equalsIgnoreCase("")) {
                                    vat = "0";
                                } else if (cellStoreVector.get(18).toString().startsWith("'")) {
                                    vat = cellStoreVector.get(18).toString().startsWith("'") ? cellStoreVector.get(18).toString().substring(1) : cellStoreVector.get(18).toString();
                                    vat = NumberToTextConverter.toText(Double.parseDouble(vat));
                                } else {
                                    vat = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(18).toString()));
                                }
                                myform.setVat(vat);
                                vat = null;

                                String departemntCharges = null;
                                if (cellStoreVector.get(19) == null) {
                                    departemntCharges = "0";
                                } else if (cellStoreVector.get(19).toString().equalsIgnoreCase("")) {
                                    departemntCharges = "0";
                                } else if (cellStoreVector.get(19).toString().startsWith("'")) {
                                    departemntCharges = cellStoreVector.get(19).toString().startsWith("'") ? cellStoreVector.get(19).toString().substring(1) : cellStoreVector.get(19).toString();
                                    departemntCharges = NumberToTextConverter.toText(Double.parseDouble(departemntCharges));
                                } else {
                                    departemntCharges = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(19).toString()));
                                }
                                myform.setDepartemntCharges(departemntCharges);
                                departemntCharges = null;

                                String totalEligibleCost = null;
                                if (cellStoreVector.get(20) == null) {
                                    totalEligibleCost = "0";
                                } else if (cellStoreVector.get(20).toString().equalsIgnoreCase("")) {
                                    totalEligibleCost = "0";
                                } else if (cellStoreVector.get(20).toString().startsWith("'")) {
                                    totalEligibleCost = cellStoreVector.get(20).toString().startsWith("'") ? cellStoreVector.get(20).toString().substring(1) : cellStoreVector.get(20).toString();
                                    totalEligibleCost = NumberToTextConverter.toText(Double.parseDouble(totalEligibleCost));
                                } else {
                                    totalEligibleCost = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(20).toString()));
                                }
                                myform.setTotalEligibleCost(totalEligibleCost);
                                totalEligibleCost = null;

                                String releases = null;
                                if (cellStoreVector.get(21) == null) {
                                    releases = "0";
                                } else if (cellStoreVector.get(21).toString().equalsIgnoreCase("")) {
                                    releases = "0";
                                } else if (cellStoreVector.get(21).toString().startsWith("'")) {
                                    releases = cellStoreVector.get(21).toString().startsWith("'") ? cellStoreVector.get(21).toString().substring(1) : cellStoreVector.get(21).toString();
                                    releases = NumberToTextConverter.toText(Double.parseDouble(releases));
                                } else {
                                    releases = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(21).toString()));
                                }
                                myform.setReleases(releases);
                                releases = null;

                                String expenditureIncurred = null;
                                if (cellStoreVector.get(22) == null) {
                                    expenditureIncurred = "0";
                                } else if (cellStoreVector.get(22).toString().equalsIgnoreCase("")) {
                                    expenditureIncurred = "0";
                                } else if (cellStoreVector.get(22).toString().startsWith("'")) {
                                    expenditureIncurred = cellStoreVector.get(22).toString().startsWith("'") ? cellStoreVector.get(22).toString().substring(1) : cellStoreVector.get(22).toString();
                                    expenditureIncurred = NumberToTextConverter.toText(Double.parseDouble(expenditureIncurred));
                                } else {
                                    expenditureIncurred = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(22).toString()));
                                }
                                myform.setExpenditureIncurred(expenditureIncurred);
                                expenditureIncurred = null;

                                String balanceToBeReleased = null;
                                if (cellStoreVector.get(23) == null) {
                                    balanceToBeReleased = "0";
                                } else if (cellStoreVector.get(23).toString().equalsIgnoreCase("")) {
                                    balanceToBeReleased = "0";
                                } else if (cellStoreVector.get(23).toString().startsWith("'")) {
                                    balanceToBeReleased = cellStoreVector.get(23).toString().startsWith("'") ? cellStoreVector.get(23).toString().substring(1) : cellStoreVector.get(23).toString();
                                    balanceToBeReleased = NumberToTextConverter.toText(Double.parseDouble(balanceToBeReleased));
                                } else {
                                    balanceToBeReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(23).toString()));
                                }
                                myform.setBalanceToBeReleased(balanceToBeReleased);
                                balanceToBeReleased = null;
                                vectorArrayList.add(myform);
                                myform = null;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vectorArrayList;
    }

    /// Omkaram addded 07102017
    public int saveRmsaMaserExcelUpload(RMSAForm myform, String distId, String userName) {
        int[] myReturn1;
        int myreturn = 0;
        String query = null;
        Connection con = null;
        Statement stmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            stmt = con.createStatement();
            query = "insert into RMSAMASTER"
                    + "(YEAR,PHASE,UDISE_CODE,SchoolSanction,SchoolAsPerUdise,District,Block,Physicalsanc_ScienceLab,Physicalsanc_CompRoom,Physicasanc_artcraftroom,Physicalsanc_Library"
                    + ",physicalsanc_ACR,Physicalsance_DrinkingWater,Physicalsanc_Toilet,ArtCraftroom_progress,ComputerRoom_progress,ScienceLab_progress,LibraryRoom_progress,AdditionalClassRoom_progress"
                    + ",ToiletBlock_progress,DrinkingWater_progress,LabEquipment_progress,Furniture_progress,financialsanc_ScienceLab,financialsanc_CompRoom,financialsanc_artcraftroom"
                    + ",financialsanc_Library,financialsanc_ACR,financialsanc_DrinkingWater,financialsanc_Toilet,Totalcivilsanction,furnitureforclassroomlabs,labequipment,TOTALFURNITURE_IN_LAKHS"
                    + ",FINANCIAL_SANC_IN_LAKHS,Tendervalue,[VAT%],DEPT,Releases,Expenditure,UCSfilepath,EstimatedCost,TotalEligibleAmount,BalanceToBeReleased,Created_date,CreatedBy)"
                    + " values('" + myform.getYear() + "'," + myform.getPhaseNo() + ",'" + myform.getSchoolId() + "','" + myform.getSchoolName() + "','" + myform.getSchoolName() + "','" + distId + "',NULL,'" + myform.getScienceLabSanction() + "','" + myform.getComputerRoomSanction() + "','" + myform.getArtCraftSanction() + "','" + myform.getLibraryRoomSanction()
                    + "','" + myform.getAdditionalClassRoomSanction() + "','" + myform.getDrinkingWaterSanction() + "','" + myform.getToiletBlockSanction() + "',0,0,0,0,0"
                    + ",0,0,0,0, NULL,NULL,NULL"
                    + ",NULL,NULL,NULL,NULL,'" + myform.getAdministrativeSanction() + "',NULL," + myform.getLabEquipment() + ",'" + myform.getTotalFurniture()
                    + "','" + myform.getFurnitureAndLabEquipment() + "','" + myform.getTenderCostValue() + "','" + myform.getVat() + "','" + myform.getDepartemntCharges() + "','" + myform.getReleases() + "','" + myform.getExpenditureIncurred() + "',NULL,'" + myform.getEstimatedCost() + "','" + myform.getTotalEligibleCost() + "','" + myform.getBalanceToBeReleased() + "',getDate(),'" + userName + "')";
            stmt.addBatch(query);
            query = "insert into RMSA_Release_Master"
                    + "(Phase,school_code,ReleaseAmount,Release_date,CreatedBy,Created_date)"
                    + " values ('" + myform.getPhaseNo() + "','" + myform.getSchoolId() + "','" + myform.getReleases()
                    + "',getDate(),'" + userName + "',getdate())";
            stmt.addBatch(query);
            myReturn1 = stmt.executeBatch();
            if (myReturn1.length > 0) {
                myreturn = 1;
            }
        } catch (Exception e) {
            myreturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }

    /// Omkaram addded 09102017
    public ArrayList<RMSAComponentForm> readComponentDataFromExcel(String fileName, String fileType, String distId) {
        ArrayList<RMSAComponentForm> vectorArrayList = new ArrayList<RMSAComponentForm>();
        try {
            FileInputStream myInput = new FileInputStream(fileName);
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator rowIter = mySheet.rowIterator();
            while (rowIter != null && rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                Vector cellStoreVector = null;
                cellStoreVector = new Vector();
                if (myRow != null) {
                    Iterator cellIter = myRow.cellIterator();
                    while (cellIter.hasNext()) {
                        HSSFCell myCell = (HSSFCell) cellIter.next();
                        cellStoreVector.addElement(myCell);
                    }
                }
                if (!cellStoreVector.get(0).toString().toUpperCase().equalsIgnoreCase("S.NO") && !cellStoreVector.get(0).toString().toUpperCase().equalsIgnoreCase("SNO")) {
                    if (cellStoreVector.get(1) != null) {
                        if (cellStoreVector.get(1).toString().length() > 10) {
                            if (distId.equalsIgnoreCase(cellStoreVector.get(1).toString().substring(0, 4))) {
                                RMSAComponentForm myform = new RMSAComponentForm();
                                String schoolId = null;
                                if (cellStoreVector.get(1) == null) {
                                    schoolId = "0";
                                } else if (cellStoreVector.get(1).toString().equalsIgnoreCase("")) {
                                    schoolId = "0";
                                } else if (cellStoreVector.get(1).toString().startsWith("'")) {
                                    schoolId = cellStoreVector.get(1).toString().startsWith("'") ? cellStoreVector.get(1).toString().substring(1) : cellStoreVector.get(1).toString();
                                    schoolId = NumberToTextConverter.toText(Double.parseDouble(schoolId));
                                } else {
                                    schoolId = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(1).toString()));
                                }
                                myform.setSchoolCode(schoolId);
                                Map map = getSchoolDataBySchoolCode(schoolId);
                                myform.setSchoolName(map.get("schoolName").toString());
                                myform.setDistrictName(map.get("distName").toString());
                                myform.setMandalName(map.get("mandalName").toString());
                                myform.setVillageName(map.get("villageName").toString());
                                myform.setDistrictId(schoolId.substring(0, 4));
                                myform.setMandalId(schoolId.substring(0, 6));
                                myform.setVillageId(schoolId.substring(0, 9));
                                schoolId = null;

                                String year = null;
                                if (cellStoreVector.get(2) == null) {
                                    year = "0";
                                } else if (cellStoreVector.get(2).toString().equalsIgnoreCase("")) {
                                    year = "0";
                                } else if (cellStoreVector.get(2).toString().startsWith("'")) {
                                    year = cellStoreVector.get(2).toString().startsWith("'") ? cellStoreVector.get(2).toString().substring(1) : cellStoreVector.get(2).toString();
                                } else {
                                    year = cellStoreVector.get(2).toString();
                                }
                                myform.setYear(year);
                                year = null;

                                String bankName = null;
                                if (cellStoreVector.get(3) == null) {
                                    bankName = "0";
                                } else if (cellStoreVector.get(3).toString().equalsIgnoreCase("")) {
                                    bankName = "0";
                                } else if (cellStoreVector.get(3).toString().startsWith("'")) {
                                    bankName = cellStoreVector.get(3).toString().startsWith("'") ? cellStoreVector.get(3).toString().substring(1) : cellStoreVector.get(3).toString();
                                } else {
                                    bankName = cellStoreVector.get(3).toString();
                                }
                                myform.setBankName(bankName);
                                bankName = null;

                                String bankAccountNumber = null;
                                if (cellStoreVector.get(4) == null) {
                                    bankAccountNumber = "0";
                                } else if (cellStoreVector.get(4).toString().equalsIgnoreCase("")) {
                                    bankAccountNumber = "0";
                                } else if (cellStoreVector.get(4).toString().startsWith("'")) {
                                    bankAccountNumber = cellStoreVector.get(4).toString().startsWith("'") ? cellStoreVector.get(4).toString().substring(1) : cellStoreVector.get(4).toString();
                                } else {
                                    bankAccountNumber = cellStoreVector.get(4).toString();
                                }
                                myform.setBankAccountNumber(bankAccountNumber);
                                bankAccountNumber = null;

                                String ifscCode = null;
                                if (cellStoreVector.get(5) == null) {
                                    ifscCode = "0";
                                } else if (cellStoreVector.get(5).toString().equalsIgnoreCase("")) {
                                    ifscCode = "0";
                                } else if (cellStoreVector.get(5).toString().startsWith("'")) {
                                    ifscCode = cellStoreVector.get(5).toString().startsWith("'") ? cellStoreVector.get(5).toString().substring(1) : cellStoreVector.get(5).toString();
                                } else {
                                    ifscCode = cellStoreVector.get(5).toString();
                                }
                                myform.setIfscCode(ifscCode);
                                ifscCode = null;

                                String civilWorksReleased = null;
                                if (cellStoreVector.get(6) == null) {
                                    civilWorksReleased = "0";
                                } else if (cellStoreVector.get(6).toString().equalsIgnoreCase("")) {
                                    civilWorksReleased = "0";
                                } else if (cellStoreVector.get(6).toString().startsWith("'")) {
                                    civilWorksReleased = cellStoreVector.get(6).toString().startsWith("'") ? cellStoreVector.get(6).toString().substring(1) : cellStoreVector.get(6).toString();
                                    civilWorksReleased = NumberToTextConverter.toText(Double.parseDouble(civilWorksReleased));
                                } else {
                                    civilWorksReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(6).toString()));
                                }
                                myform.setCivilWorksReleased(civilWorksReleased);
                                civilWorksReleased = null;

                                String majorRepairsReleased = null;
                                if (cellStoreVector.get(7) == null) {
                                    majorRepairsReleased = "0";
                                } else if (cellStoreVector.get(7).toString().equalsIgnoreCase("")) {
                                    majorRepairsReleased = "0";
                                } else if (cellStoreVector.get(7).toString().startsWith("'")) {
                                    majorRepairsReleased = cellStoreVector.get(7).toString().startsWith("'") ? cellStoreVector.get(7).toString().substring(1) : cellStoreVector.get(7).toString();
                                    majorRepairsReleased = NumberToTextConverter.toText(Double.parseDouble(majorRepairsReleased));
                                } else {
                                    majorRepairsReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(7).toString()));
                                }
                                myform.setMajorRepairsReleased(majorRepairsReleased);
                                majorRepairsReleased = null;

                                String minorRepairsReleased = null;
                                if (cellStoreVector.get(8) == null) {
                                    minorRepairsReleased = "0";
                                } else if (cellStoreVector.get(8).toString().equalsIgnoreCase("")) {
                                    minorRepairsReleased = "0";
                                } else if (cellStoreVector.get(8).toString().startsWith("'")) {
                                    minorRepairsReleased = cellStoreVector.get(8).toString().startsWith("'") ? cellStoreVector.get(8).toString().substring(1) : cellStoreVector.get(8).toString();
                                    minorRepairsReleased = NumberToTextConverter.toText(Double.parseDouble(minorRepairsReleased));
                                } else {
                                    minorRepairsReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(8).toString()));
                                }
                                myform.setMinorRepairsReleased(minorRepairsReleased);
                                minorRepairsReleased = null;

                                String toiletsReleased = null;
                                if (cellStoreVector.get(9) == null) {
                                    toiletsReleased = "0";
                                } else if (cellStoreVector.get(9).toString().equalsIgnoreCase("")) {
                                    toiletsReleased = "0";
                                } else if (cellStoreVector.get(9).toString().startsWith("'")) {
                                    toiletsReleased = cellStoreVector.get(9).toString().startsWith("'") ? cellStoreVector.get(9).toString().substring(1) : cellStoreVector.get(9).toString();
                                    toiletsReleased = NumberToTextConverter.toText(Double.parseDouble(toiletsReleased));
                                } else {
                                    toiletsReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(9).toString()));
                                }
                                myform.setToiletsReleased(toiletsReleased);
                                toiletsReleased = null;

                                String civilWorksSpent = null;
                                if (cellStoreVector.get(10) == null) {
                                    civilWorksSpent = "0";
                                } else if (cellStoreVector.get(10).toString().equalsIgnoreCase("")) {
                                    civilWorksSpent = "0";
                                } else if (cellStoreVector.get(10).toString().startsWith("'")) {
                                    civilWorksSpent = cellStoreVector.get(10).toString().startsWith("'") ? cellStoreVector.get(10).toString().substring(1) : cellStoreVector.get(10).toString();
                                    civilWorksSpent = NumberToTextConverter.toText(Double.parseDouble(civilWorksSpent));
                                } else {
                                    civilWorksSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(10).toString()));
                                }
                                myform.setCivilWorksSpent(civilWorksSpent);
                                civilWorksSpent = null;

                                String majorRepairsSpent = null;
                                if (cellStoreVector.get(11) == null) {
                                    majorRepairsSpent = "0";
                                } else if (cellStoreVector.get(11).toString().equalsIgnoreCase("")) {
                                    majorRepairsSpent = "0";
                                } else if (cellStoreVector.get(11).toString().startsWith("'")) {
                                    majorRepairsSpent = cellStoreVector.get(11).toString().startsWith("'") ? cellStoreVector.get(11).toString().substring(1) : cellStoreVector.get(11).toString();
                                    majorRepairsSpent = NumberToTextConverter.toText(Double.parseDouble(majorRepairsSpent));
                                } else {
                                    majorRepairsSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(11).toString()));
                                }
                                myform.setMajorRepairsSpent(majorRepairsSpent);
                                majorRepairsSpent = null;

                                String minorRepairsSpent = null;
                                if (cellStoreVector.get(12) == null) {
                                    minorRepairsSpent = "0";
                                } else if (cellStoreVector.get(12).toString().equalsIgnoreCase("")) {
                                    minorRepairsSpent = "0";
                                } else if (cellStoreVector.get(12).toString().startsWith("'")) {
                                    minorRepairsSpent = cellStoreVector.get(12).toString().startsWith("'") ? cellStoreVector.get(12).toString().substring(1) : cellStoreVector.get(12).toString();
                                    minorRepairsSpent = NumberToTextConverter.toText(Double.parseDouble(minorRepairsSpent));
                                } else {
                                    minorRepairsSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(12).toString()));
                                }
                                myform.setMinorRepairsSpent(minorRepairsSpent);
                                minorRepairsSpent = null;

                                String toiletsSpent = null;
                                if (cellStoreVector.get(13) == null) {
                                    toiletsSpent = "0";
                                } else if (cellStoreVector.get(13).toString().equalsIgnoreCase("")) {
                                    toiletsSpent = "0";
                                } else if (cellStoreVector.get(13).toString().startsWith("'")) {
                                    toiletsSpent = cellStoreVector.get(13).toString().startsWith("'") ? cellStoreVector.get(13).toString().substring(1) : cellStoreVector.get(13).toString();
                                    toiletsSpent = NumberToTextConverter.toText(Double.parseDouble(toiletsSpent));
                                } else {
                                    toiletsSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(13).toString()));
                                }
                                myform.setToiletsSpent(toiletsSpent);
                                toiletsSpent = null;

                                String waterElectricityTelephoneChargesReleased = null;
                                if (cellStoreVector.get(14) == null) {
                                    waterElectricityTelephoneChargesReleased = "0";
                                } else if (cellStoreVector.get(14).toString().equalsIgnoreCase("")) {
                                    waterElectricityTelephoneChargesReleased = "0";
                                } else if (cellStoreVector.get(14).toString().startsWith("'")) {
                                    waterElectricityTelephoneChargesReleased = cellStoreVector.get(14).toString().startsWith("'") ? cellStoreVector.get(14).toString().substring(1) : cellStoreVector.get(14).toString();
                                    waterElectricityTelephoneChargesReleased = NumberToTextConverter.toText(Double.parseDouble(waterElectricityTelephoneChargesReleased));
                                } else {
                                    waterElectricityTelephoneChargesReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(14).toString()));
                                }
                                myform.setWaterElectricityTelephoneChargesReleased(waterElectricityTelephoneChargesReleased);
                                waterElectricityTelephoneChargesReleased = null;

                                String purchaseofBooksAndPeriodicalsAndNewsPapersReleased = null;
                                if (cellStoreVector.get(15) == null) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0";
                                } else if (cellStoreVector.get(15).toString().equalsIgnoreCase("")) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0";
                                } else if (cellStoreVector.get(15).toString().startsWith("'")) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersReleased = cellStoreVector.get(15).toString().startsWith("'") ? cellStoreVector.get(15).toString().substring(1) : cellStoreVector.get(15).toString();
                                    purchaseofBooksAndPeriodicalsAndNewsPapersReleased = NumberToTextConverter.toText(Double.parseDouble(purchaseofBooksAndPeriodicalsAndNewsPapersReleased));
                                } else {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(15).toString()));
                                }
                                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersReleased(purchaseofBooksAndPeriodicalsAndNewsPapersReleased);
                                purchaseofBooksAndPeriodicalsAndNewsPapersReleased = null;

                                String minorRepairsAnnualReleased = null;
                                if (cellStoreVector.get(16) == null) {
                                    minorRepairsAnnualReleased = "0";
                                } else if (cellStoreVector.get(16).toString().equalsIgnoreCase("")) {
                                    minorRepairsAnnualReleased = "0";
                                } else if (cellStoreVector.get(16).toString().startsWith("'")) {
                                    minorRepairsAnnualReleased = cellStoreVector.get(16).toString().startsWith("'") ? cellStoreVector.get(16).toString().substring(1) : cellStoreVector.get(16).toString();
                                    minorRepairsAnnualReleased = NumberToTextConverter.toText(Double.parseDouble(minorRepairsAnnualReleased));
                                } else {
                                    minorRepairsAnnualReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(16).toString()));
                                }
                                myform.setMinorRepairsAnnualReleased(minorRepairsAnnualReleased);
                                minorRepairsAnnualReleased = null;

                                String sanitationAndICTReleased = null;
                                if (cellStoreVector.get(17) == null) {
                                    sanitationAndICTReleased = "0";
                                } else if (cellStoreVector.get(17).toString().equalsIgnoreCase("")) {
                                    sanitationAndICTReleased = "0";
                                } else if (cellStoreVector.get(17).toString().startsWith("'")) {
                                    sanitationAndICTReleased = cellStoreVector.get(17).toString().startsWith("'") ? cellStoreVector.get(17).toString().substring(1) : cellStoreVector.get(17).toString();
                                    sanitationAndICTReleased = NumberToTextConverter.toText(Double.parseDouble(sanitationAndICTReleased));
                                } else {
                                    sanitationAndICTReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(17).toString()));
                                }
                                myform.setSanitationAndICTReleased(sanitationAndICTReleased);
                                sanitationAndICTReleased = null;

                                String needBasedWorksReleased = null;
                                if (cellStoreVector.get(18) == null) {
                                    needBasedWorksReleased = "0";
                                } else if (cellStoreVector.get(18).toString().equalsIgnoreCase("")) {
                                    needBasedWorksReleased = "0";
                                } else if (cellStoreVector.get(18).toString().startsWith("'")) {
                                    needBasedWorksReleased = cellStoreVector.get(18).toString().startsWith("'") ? cellStoreVector.get(18).toString().substring(1) : cellStoreVector.get(18).toString();
                                    needBasedWorksReleased = NumberToTextConverter.toText(Double.parseDouble(needBasedWorksReleased));
                                } else {
                                    needBasedWorksReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(18).toString()));
                                }
                                myform.setNeedBasedWorksReleased(needBasedWorksReleased);
                                needBasedWorksReleased = null;

                                String provisionalLaboratoryReleased = null;
                                if (cellStoreVector.get(19) == null) {
                                    provisionalLaboratoryReleased = "0";
                                } else if (cellStoreVector.get(19).toString().equalsIgnoreCase("")) {
                                    provisionalLaboratoryReleased = "0";
                                } else if (cellStoreVector.get(19).toString().startsWith("'")) {
                                    provisionalLaboratoryReleased = cellStoreVector.get(19).toString().startsWith("'") ? cellStoreVector.get(19).toString().substring(1) : cellStoreVector.get(19).toString();
                                    provisionalLaboratoryReleased = NumberToTextConverter.toText(Double.parseDouble(provisionalLaboratoryReleased));
                                } else {
                                    provisionalLaboratoryReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(19).toString()));
                                }
                                myform.setProvisionalLaboratoryReleased(provisionalLaboratoryReleased);
                                provisionalLaboratoryReleased = null;

                                String waterElectricityTelephoneChargesSpent = null;
                                if (cellStoreVector.get(20) == null) {
                                    waterElectricityTelephoneChargesSpent = "0";
                                } else if (cellStoreVector.get(20).toString().equalsIgnoreCase("")) {
                                    waterElectricityTelephoneChargesSpent = "0";
                                } else if (cellStoreVector.get(20).toString().startsWith("'")) {
                                    waterElectricityTelephoneChargesSpent = cellStoreVector.get(20).toString().startsWith("'") ? cellStoreVector.get(20).toString().substring(1) : cellStoreVector.get(20).toString();
                                    waterElectricityTelephoneChargesSpent = NumberToTextConverter.toText(Double.parseDouble(waterElectricityTelephoneChargesSpent));
                                } else {
                                    waterElectricityTelephoneChargesSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(20).toString()));
                                }
                                myform.setWaterElectricityTelephoneChargesSpent(waterElectricityTelephoneChargesSpent);
                                waterElectricityTelephoneChargesSpent = null;

                                String purchaseofBooksAndPeriodicalsAndNewsPapersSpent = null;
                                if (cellStoreVector.get(21) == null) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0";
                                } else if (cellStoreVector.get(21).toString().equalsIgnoreCase("")) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0";
                                } else if (cellStoreVector.get(21).toString().startsWith("'")) {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersSpent = cellStoreVector.get(21).toString().startsWith("'") ? cellStoreVector.get(21).toString().substring(1) : cellStoreVector.get(21).toString();
                                    purchaseofBooksAndPeriodicalsAndNewsPapersSpent = NumberToTextConverter.toText(Double.parseDouble(purchaseofBooksAndPeriodicalsAndNewsPapersSpent));
                                } else {
                                    purchaseofBooksAndPeriodicalsAndNewsPapersSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(21).toString()));
                                }
                                myform.setPurchaseofBooksAndPeriodicalsAndNewsPapersSpent(purchaseofBooksAndPeriodicalsAndNewsPapersSpent);
                                purchaseofBooksAndPeriodicalsAndNewsPapersSpent = null;

                                String minorRepairsAnnualSpent = null;
                                if (cellStoreVector.get(22) == null) {
                                    minorRepairsAnnualSpent = "0";
                                } else if (cellStoreVector.get(22).toString().equalsIgnoreCase("")) {
                                    minorRepairsAnnualSpent = "0";
                                } else if (cellStoreVector.get(22).toString().startsWith("'")) {
                                    minorRepairsAnnualSpent = cellStoreVector.get(22).toString().startsWith("'") ? cellStoreVector.get(22).toString().substring(1) : cellStoreVector.get(22).toString();
                                    minorRepairsAnnualSpent = NumberToTextConverter.toText(Double.parseDouble(minorRepairsAnnualSpent));
                                } else {
                                    minorRepairsAnnualSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(22).toString()));
                                }
                                myform.setMinorRepairsAnnualSpent(minorRepairsAnnualSpent);
                                minorRepairsAnnualSpent = null;

                                String sanitationAndICTSpent = null;
                                if (cellStoreVector.get(23) == null) {
                                    sanitationAndICTSpent = "0";
                                } else if (cellStoreVector.get(23).toString().equalsIgnoreCase("")) {
                                    sanitationAndICTSpent = "0";
                                } else if (cellStoreVector.get(23).toString().startsWith("'")) {
                                    sanitationAndICTSpent = cellStoreVector.get(23).toString().startsWith("'") ? cellStoreVector.get(23).toString().substring(1) : cellStoreVector.get(23).toString();
                                    sanitationAndICTSpent = NumberToTextConverter.toText(Double.parseDouble(sanitationAndICTSpent));
                                } else {
                                    sanitationAndICTSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(23).toString()));
                                }
                                myform.setSanitationAndICTSpent(sanitationAndICTSpent);
                                sanitationAndICTSpent = null;

                                String needBasedWorksSpent = null;
                                if (cellStoreVector.get(24) == null) {
                                    needBasedWorksSpent = "0";
                                } else if (cellStoreVector.get(24).toString().equalsIgnoreCase("")) {
                                    needBasedWorksSpent = "0";
                                } else if (cellStoreVector.get(24).toString().startsWith("'")) {
                                    needBasedWorksSpent = cellStoreVector.get(24).toString().startsWith("'") ? cellStoreVector.get(24).toString().substring(1) : cellStoreVector.get(24).toString();
                                    needBasedWorksSpent = NumberToTextConverter.toText(Double.parseDouble(needBasedWorksSpent));
                                } else {
                                    needBasedWorksSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(24).toString()));
                                }
                                myform.setNeedBasedWorksSpent(needBasedWorksSpent);
                                needBasedWorksSpent = null;

                                String provisionalLaboratorySpent = null;
                                if (cellStoreVector.get(25) == null) {
                                    provisionalLaboratorySpent = "0";
                                } else if (cellStoreVector.get(25).toString().equalsIgnoreCase("")) {
                                    provisionalLaboratorySpent = "0";
                                } else if (cellStoreVector.get(25).toString().startsWith("'")) {
                                    provisionalLaboratorySpent = cellStoreVector.get(25).toString().startsWith("'") ? cellStoreVector.get(25).toString().substring(1) : cellStoreVector.get(25).toString();
                                    provisionalLaboratorySpent = NumberToTextConverter.toText(Double.parseDouble(provisionalLaboratorySpent));
                                } else {
                                    provisionalLaboratorySpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(25).toString()));
                                }
                                myform.setProvisionalLaboratorySpent(provisionalLaboratorySpent);
                                provisionalLaboratorySpent = null;

                                String recurringExcursionRelease = null;
                                if (cellStoreVector.get(26) == null) {
                                    recurringExcursionRelease = "0";
                                } else if (cellStoreVector.get(26).toString().equalsIgnoreCase("")) {
                                    recurringExcursionRelease = "0";
                                } else if (cellStoreVector.get(26).toString().startsWith("'")) {
                                    recurringExcursionRelease = cellStoreVector.get(26).toString().startsWith("'") ? cellStoreVector.get(26).toString().substring(1) : cellStoreVector.get(26).toString();
                                    recurringExcursionRelease = NumberToTextConverter.toText(Double.parseDouble(recurringExcursionRelease));
                                } else {
                                    recurringExcursionRelease = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(26).toString()));
                                }
                                myform.setRecurringExcursionRelease(recurringExcursionRelease);
                                recurringExcursionRelease = null;

                                String recurringSelfDefenseRelease = null;
                                if (cellStoreVector.get(27) == null) {
                                    recurringSelfDefenseRelease = "0";
                                } else if (cellStoreVector.get(27).toString().equalsIgnoreCase("")) {
                                    recurringSelfDefenseRelease = "0";
                                } else if (cellStoreVector.get(27).toString().startsWith("'")) {
                                    recurringSelfDefenseRelease = cellStoreVector.get(27).toString().startsWith("'") ? cellStoreVector.get(27).toString().substring(1) : cellStoreVector.get(27).toString();
                                    recurringSelfDefenseRelease = NumberToTextConverter.toText(Double.parseDouble(recurringSelfDefenseRelease));
                                } else {
                                    recurringSelfDefenseRelease = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(27).toString()));
                                }
                                myform.setRecurringSelfDefenseRelease(recurringSelfDefenseRelease);
                                recurringSelfDefenseRelease = null;

                                String recurringOtherRelease = null;
                                if (cellStoreVector.get(28) == null) {
                                    recurringOtherRelease = "0";
                                } else if (cellStoreVector.get(28).toString().equalsIgnoreCase("")) {
                                    recurringOtherRelease = "0";
                                } else if (cellStoreVector.get(28).toString().startsWith("'")) {
                                    recurringOtherRelease = cellStoreVector.get(28).toString().startsWith("'") ? cellStoreVector.get(28).toString().substring(1) : cellStoreVector.get(28).toString();
                                    recurringOtherRelease = NumberToTextConverter.toText(Double.parseDouble(recurringOtherRelease));
                                } else {
                                    recurringOtherRelease = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(28).toString()));
                                }
                                myform.setRecurringOtherRelease(recurringOtherRelease);
                                recurringOtherRelease = null;

                                String recurringExcursionSpent = null;
                                if (cellStoreVector.get(29) == null) {
                                    recurringExcursionSpent = "0";
                                } else if (cellStoreVector.get(29).toString().equalsIgnoreCase("")) {
                                    recurringExcursionSpent = "0";
                                } else if (cellStoreVector.get(29).toString().startsWith("'")) {
                                    recurringExcursionSpent = cellStoreVector.get(29).toString().startsWith("'") ? cellStoreVector.get(29).toString().substring(1) : cellStoreVector.get(29).toString();
                                    recurringExcursionSpent = NumberToTextConverter.toText(Double.parseDouble(recurringExcursionSpent));
                                } else {
                                    recurringExcursionSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(29).toString()));
                                }
                                myform.setRecurringExcursionSpent(recurringExcursionSpent);
                                recurringExcursionSpent = null;

                                String recurringSelfDefenseSpent = null;
                                if (cellStoreVector.get(30) == null) {
                                    recurringSelfDefenseSpent = "0";
                                } else if (cellStoreVector.get(30).toString().equalsIgnoreCase("")) {
                                    recurringSelfDefenseSpent = "0";
                                } else if (cellStoreVector.get(30).toString().startsWith("'")) {
                                    recurringSelfDefenseSpent = cellStoreVector.get(30).toString().startsWith("'") ? cellStoreVector.get(30).toString().substring(1) : cellStoreVector.get(30).toString();
                                    recurringSelfDefenseSpent = NumberToTextConverter.toText(Double.parseDouble(recurringSelfDefenseSpent));
                                } else {
                                    recurringSelfDefenseSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(30).toString()));
                                }
                                myform.setRecurringSelfDefenseSpent(recurringSelfDefenseSpent);
                                recurringSelfDefenseSpent = null;

                                String recurringOtherSpent = null;
                                if (cellStoreVector.get(31) == null) {
                                    recurringOtherSpent = "0";
                                } else if (cellStoreVector.get(31).toString().equalsIgnoreCase("")) {
                                    recurringOtherSpent = "0";
                                } else if (cellStoreVector.get(31).toString().startsWith("'")) {
                                    recurringOtherSpent = cellStoreVector.get(31).toString().startsWith("'") ? cellStoreVector.get(31).toString().substring(1) : cellStoreVector.get(31).toString();
                                    recurringOtherSpent = NumberToTextConverter.toText(Double.parseDouble(recurringOtherSpent));
                                } else {
                                    recurringOtherSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(31).toString()));
                                }
                                myform.setRecurringOtherSpent(recurringOtherSpent);
                                recurringOtherSpent = null;

                                String furnitureReleased = null;
                                if (cellStoreVector.get(32) == null) {
                                    furnitureReleased = "0";
                                } else if (cellStoreVector.get(32).toString().equalsIgnoreCase("")) {
                                    furnitureReleased = "0";
                                } else if (cellStoreVector.get(32).toString().startsWith("'")) {
                                    furnitureReleased = cellStoreVector.get(32).toString().startsWith("'") ? cellStoreVector.get(32).toString().substring(1) : cellStoreVector.get(32).toString();
                                    furnitureReleased = NumberToTextConverter.toText(Double.parseDouble(furnitureReleased));
                                } else {
                                    furnitureReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(32).toString()));
                                }
                                myform.setFurnitureReleased(furnitureReleased);
                                furnitureReleased = null;

                                String labEquipmentReleased = null;
                                if (cellStoreVector.get(33) == null) {
                                    labEquipmentReleased = "0";
                                } else if (cellStoreVector.get(33).toString().equalsIgnoreCase("")) {
                                    labEquipmentReleased = "0";
                                } else if (cellStoreVector.get(33).toString().startsWith("'")) {
                                    labEquipmentReleased = cellStoreVector.get(33).toString().startsWith("'") ? cellStoreVector.get(33).toString().substring(1) : cellStoreVector.get(33).toString();
                                    labEquipmentReleased = NumberToTextConverter.toText(Double.parseDouble(labEquipmentReleased));
                                } else {
                                    labEquipmentReleased = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(33).toString()));
                                }
                                myform.setLabEquipmentReleased(labEquipmentReleased);
                                labEquipmentReleased = null;

                                String furnitureSpent = null;
                                if (cellStoreVector.get(34) == null) {
                                    furnitureSpent = "0";
                                } else if (cellStoreVector.get(34).toString().equalsIgnoreCase("")) {
                                    furnitureSpent = "0";
                                } else if (cellStoreVector.get(34).toString().startsWith("'")) {
                                    furnitureSpent = cellStoreVector.get(34).toString().startsWith("'") ? cellStoreVector.get(34).toString().substring(1) : cellStoreVector.get(34).toString();
                                    furnitureSpent = NumberToTextConverter.toText(Double.parseDouble(furnitureSpent));
                                } else {
                                    furnitureSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(34).toString()));
                                }
                                myform.setFurnitureSpent(furnitureSpent);
                                furnitureSpent = null;

                                String labEquipmentSpent = null;
                                if (cellStoreVector.get(35) == null) {
                                    labEquipmentSpent = "0";
                                } else if (cellStoreVector.get(35).toString().equalsIgnoreCase("")) {
                                    labEquipmentSpent = "0";
                                } else if (cellStoreVector.get(35).toString().startsWith("'")) {
                                    labEquipmentSpent = cellStoreVector.get(35).toString().startsWith("'") ? cellStoreVector.get(35).toString().substring(1) : cellStoreVector.get(35).toString();
                                    labEquipmentSpent = NumberToTextConverter.toText(Double.parseDouble(labEquipmentSpent));
                                } else {
                                    labEquipmentSpent = NumberToTextConverter.toText(Double.parseDouble(cellStoreVector.get(35).toString()));
                                }
                                myform.setLabEquipmentSpent(labEquipmentSpent);
                                labEquipmentSpent = null;

                                vectorArrayList.add(myform);
                                myform = null;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vectorArrayList;
    }

    /// Omkaram addded 09102017
    public int saveRmsaComponentMaserExcelUpload(RMSAComponentForm myform, String distId, String userName) {
        int myreturn = 0;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "insert into RMSAComponents"
                    + "(DistId,DistrictName,MandalId,MandalName,VillageId,VillageName,SchoolId,SchoolName"
                    + ",BankAccountNumber,IFSCCode,BankName,YEAR"
                    + ",CivilWork_CivilReleased,MajorRepair_CivilReleased,MinorRepair_CivilReleased,Toilets_CivilReleased"
                    + ",CivilWork_CivilSpent,MajorRepair_CivilSpent,MinorRepair_CivilSpent,Toilets_CivilSpent"
                    + ",WaterElecticity_AnnualReleased,BooksPurchased_AnnualReleased,MinorRepairs_AnnualReleased,SanitationICT_AnnualReleased,NeedBasedWorks_AnnualReleased,ProvisionalLaboratory_AnnualReleased"
                    + ",WaterElecticity_AnnualSpent,BooksPurchased_AnnualSpent,MinorRepairs_AnnualSpent,SanitationICT_AnnualSpent,NeedBasedWorks_AnnualSpent,ProvisionalLaboratory_AnnualSpent"
                    + ",Excursion_RecurringReleased,SelfDefense_RecurringReleased,Other_RecurringReleased"
                    + ",Excursion_RecurringSpent,SelfDefense_RecurringSpent,Other_RecurringSpent"
                    + ",Furniture_Released,LabEquipment_Released"
                    + ",Furniture_Spent,LabEquipment_Spent,Updatedby,UpdatedDate)"
                    + " values('" + myform.getDistrictId() + "','" + myform.getDistrictName() + "','" + myform.getMandalId() + "','" + myform.getMandalName() + "','" + myform.getVillageId() + "','" + myform.getVillageName() + "','" + myform.getSchoolCode() + "','" + myform.getSchoolName()
                    + "','" + myform.getBankAccountNumber() + "','" + myform.getIfscCode() + "','" + myform.getBankName() + "','" + myform.getYear()
                    + "','" + myform.getCivilWorksReleased() + "','" + myform.getMajorRepairsReleased() + "','" + myform.getMinorRepairsReleased() + "','" + myform.getToiletsReleased()
                    + "','" + myform.getCivilWorksSpent() + "','" + myform.getMajorRepairsSpent() + "','" + myform.getMinorRepairsSpent() + "','" + myform.getToiletsSpent()
                    + "','" + myform.getWaterElectricityTelephoneChargesReleased() + "','" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersReleased() + "','" + myform.getMinorRepairsAnnualReleased() + "','" + myform.getSanitationAndICTReleased() + "','" + myform.getNeedBasedWorksReleased() + "','" + myform.getProvisionalLaboratoryReleased()
                    + "','" + myform.getWaterElectricityTelephoneChargesSpent() + "','" + myform.getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() + "','" + myform.getMinorRepairsAnnualSpent() + "','" + myform.getSanitationAndICTSpent() + "','" + myform.getNeedBasedWorksSpent() + "','" + myform.getProvisionalLaboratorySpent()
                    + "','" + myform.getRecurringExcursionRelease() + "','" + myform.getRecurringSelfDefenseRelease() + "','" + myform.getRecurringOtherRelease()
                    + "','" + myform.getRecurringExcursionSpent() + "','" + myform.getRecurringSelfDefenseSpent() + "','" + myform.getRecurringOtherSpent()
                    + "','" + myform.getFurnitureReleased() + "','" + myform.getLabEquipmentReleased()
                    + "','" + myform.getFurnitureSpent() + "','" + myform.getLabEquipmentSpent()
                    + "','" + userName + "',getDate())";
            pstmt = con.prepareStatement(query);
            myreturn = pstmt.executeUpdate();
        } catch (Exception e) {
            myreturn = 0;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }

    /// Omkaram addded
    public List getPhaseList1(RMSAForm myform) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList phaseList = new ArrayList();
        String phaselist = "<option value='0'>Select Phase</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select distinct (Phase) from RMSAMaster where left(UDISE_CODE,9)=? order by Phase";
            pst = con.prepareStatement(query);
            pst.setString(1, myform.getVillageId());
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("PhaseNo", rs.getString(1));
                    String phaseName = "Phase-" + rs.getString(1);
                    map.put("phaseName", phaseName);
                    phaseList.add(map);
                    phaselist = phaselist + "<option value='" + rs.getString(1) + "'>" + phaseName + "</option>";
                    map = null;
                    phaseName = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return phaseList;
    }

    /// Omkaram addded
    public List getSchoolManagementList() {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList mgntList = new ArrayList();
        String mgtlist = "<option value='00'>Select School Management</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select SCHMGT_ID,SCHMGT_DESC  from STEPS_SCHMGT where (SCHMGT_ID between 10 and 34 or SCHMGT_ID in (0,62,63,64,65)) order by LTRIM(RTRIM(SCHMGT_DESC))";
            pst = con.prepareStatement(query);
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    if (rs.getString(2).toString().trim().equalsIgnoreCase("All Management")) {
                        map.put("mgtId", "All");
                    } else {
                        map.put("mgtId", rs.getString(1));
                    }
                    map.put("mgtName", rs.getString(2));
                    mgntList.add(map);
                    mgtlist = mgtlist + "<option value='" + rs.getString(1) + "'>" + rs.getString(2) + "</option>";
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mgntList;
    }
//omkaram

    public ArrayList getDistWiseCivilsReport(String phase, String mangId, String reptType, String user) {
        Map map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList distList = new ArrayList();

        int total_noOfSchools = 0;
        float total_physicalsanc_ACR = 0;
        float total_financialsanc_ACR = 0;
        float total_Physicalsanc_ScienceLab = 0;
        float total_financialsanc_ScienceLab = 0;
        float total_Physicalsanc_CompRoom = 0;
        float total_financialsanc_CompRoom = 0;
        float total_Physicasanc_artcraftroom = 0;
        float total_financialsanc_artcraftroom = 0;
        float total_Physicalsanc_Library = 0;
        float total_financialsanc_Library = 0;
        float total_Physicalsance_DrinkingWater = 0;
        float total_financialsanc_DrinkingWater = 0;
        float total_Physicalsanc_Toilet = 0;
        float total_financialsanc_Toilet = 0;

        float total_FINANCIAL_SANC_IN_LAKHS = 0;
        float total_EstimatedCost = 0;
        float total_Releases = 0;
        float total_Expenditure = 0;
        float total_amountAtToBeReleased = 0;
        float total_ucsSubmit = 0;
        float total_balanceAtUCS = 0;
        float total_balanceAtApwidc = 0;


        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[Usp_Get_RMSAAmounts_final](?,?,?,?)}");
            cstmt.setString(1, phase);
            cstmt.setString(2, mangId);
            cstmt.setString(3, reptType);
            cstmt.setString(4, user);
//            System.out.println("phase "+phase);
//            System.out.println("mangId "+mangId);
//            System.out.println("reptType "+reptType);
//            System.out.println("user "+user);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap();

                    map.put("distId", rs.getString(1));
                    map.put("distName", rs.getString(2));
                    map.put("noOfSchools", rs.getString(3));

                    map.put("physicalsanc_ACR", rs.getString(4));
                    map.put("financialsanc_ACR", rs.getString(5));
                    map.put("Physicalsanc_ScienceLab", rs.getString(6));
                    map.put("financialsanc_ScienceLab", rs.getString(7));
                    map.put("Physicalsanc_CompRoom", rs.getString(8));
                    map.put("financialsanc_CompRoom", rs.getString(9));
                    map.put("Physicasanc_artcraftroom", rs.getString(10));
                    map.put("financialsanc_artcraftroom", rs.getString(11));
                    map.put("Physicalsanc_Library", rs.getString(12));
                    map.put("financialsanc_Library", rs.getString(13));
                    map.put("Physicalsance_DrinkingWater", rs.getString(14));
                    map.put("financialsanc_DrinkingWater", rs.getString(15));
                    map.put("Physicalsanc_Toilet", rs.getString(16));
                    map.put("financialsanc_Toilet", rs.getString(17));

                    map.put("FINANCIAL_SANC_IN_LAKHS", rs.getString(18));
                    map.put("EstimatedCost", rs.getString(19));
                    map.put("Releases", rs.getString(20));
                    map.put("Expenditure", rs.getString(21));
                    map.put("amountAtToBeReleased", rs.getString(22));
                    map.put("ucsSubmit", rs.getString(23));
                    map.put("balanceAtUCS", rs.getString(24));
                    map.put("balanceAtApwidc", rs.getString(25));

                    // Totals

                    total_noOfSchools = total_noOfSchools + rs.getInt(3);
                    map.put("total_noOfSchools", total_noOfSchools);

                    float physicalsanc_ACR = rs.getFloat(4);
                    total_physicalsanc_ACR = total_physicalsanc_ACR + physicalsanc_ACR;
                    double total_physicalsanc_ACR1 = (double) Math.round(total_physicalsanc_ACR * 100) / 100;
                    map.put("total_physicalsanc_ACR", total_physicalsanc_ACR1);

                    float financialsanc_ACR = rs.getFloat(5);
                    total_financialsanc_ACR = total_financialsanc_ACR + financialsanc_ACR;
                    double total_financialsanc_ACR1 = (double) Math.round(total_financialsanc_ACR * 100) / 100;
                    map.put("total_financialsanc_ACR", total_financialsanc_ACR1);

                    float physicalsanc_ScienceLab = rs.getFloat(6);
                    total_Physicalsanc_ScienceLab = total_Physicalsanc_ScienceLab + physicalsanc_ScienceLab;
                    double total_Physicalsanc_ScienceLab1 = (double) Math.round(total_Physicalsanc_ScienceLab * 100) / 100;
                    map.put("total_Physicalsanc_ScienceLab", total_Physicalsanc_ScienceLab1);

                    float financialsanc_ScienceLab = rs.getFloat(7);
                    total_financialsanc_ScienceLab = total_financialsanc_ScienceLab + financialsanc_ScienceLab;
                    double total_financialsanc_ScienceLab1 = (double) Math.round(total_financialsanc_ScienceLab * 100) / 100;
                    map.put("total_financialsanc_ScienceLab", total_financialsanc_ScienceLab1);

                    float physicalsanc_CompRoom = rs.getFloat(8);
                    total_Physicalsanc_CompRoom = total_Physicalsanc_CompRoom + physicalsanc_CompRoom;
                    double total_Physicalsanc_CompRoom1 = (double) Math.round(total_Physicalsanc_CompRoom * 100) / 100;
                    map.put("total_Physicalsanc_CompRoom", total_Physicalsanc_CompRoom1);

                    float financialsanc_CompRoom = rs.getFloat(9);
                    total_financialsanc_CompRoom = total_financialsanc_CompRoom + financialsanc_CompRoom;
                    double total_financialsanc_CompRoom1 = (double) Math.round(total_financialsanc_CompRoom * 100) / 100;
                    map.put("total_financialsanc_CompRoom", total_financialsanc_CompRoom1);
                    total_financialsanc_CompRoom1 = 0;

                    float physicasanc_artcraftroom = rs.getFloat(10);
                    total_Physicasanc_artcraftroom = total_Physicasanc_artcraftroom + physicasanc_artcraftroom;
                    double total_Physicasanc_artcraftroom1 = (double) Math.round(total_Physicasanc_artcraftroom * 100) / 100;
                    map.put("total_Physicasanc_artcraftroom", total_Physicasanc_artcraftroom1);

                    float financialsanc_artcraftroom = rs.getFloat(11);
                    total_financialsanc_artcraftroom = total_financialsanc_artcraftroom + financialsanc_artcraftroom;
                    double total_financialsanc_artcraftroom1 = (double) Math.round(total_financialsanc_artcraftroom * 100) / 100;
                    map.put("total_financialsanc_artcraftroom", total_financialsanc_artcraftroom1);

                    float physicalsanc_Library = rs.getFloat(12);
                    total_Physicalsanc_Library = total_Physicalsanc_Library + physicalsanc_Library;
                    double total_Physicalsanc_Library1 = (double) Math.round(total_Physicalsanc_Library * 100) / 100;
                    map.put("total_Physicalsanc_Library", total_Physicalsanc_Library1);

                    float financialsanc_Library = rs.getFloat(13);
                    total_financialsanc_Library = total_financialsanc_Library + financialsanc_Library;
                    double total_financialsanc_Library1 = (double) Math.round(total_financialsanc_Library * 100) / 100;
                    map.put("total_financialsanc_Library", total_financialsanc_Library1);

                    float physicalsance_DrinkingWater = rs.getFloat(14);
                    total_Physicalsance_DrinkingWater = total_Physicalsance_DrinkingWater + physicalsance_DrinkingWater;
                    double total_Physicalsance_DrinkingWater1 = (double) Math.round(total_Physicalsance_DrinkingWater * 100) / 100;
                    map.put("total_Physicalsance_DrinkingWater", total_Physicalsance_DrinkingWater1);

                    float financialsanc_DrinkingWater = rs.getFloat(15);
                    total_financialsanc_DrinkingWater = total_financialsanc_DrinkingWater + financialsanc_DrinkingWater;
                    double total_financialsanc_DrinkingWater1 = (double) Math.round(total_financialsanc_DrinkingWater * 100) / 100;
                    map.put("total_financialsanc_DrinkingWater", total_financialsanc_DrinkingWater1);

                    float physicalsanc_Toilet = rs.getFloat(16);
                    total_Physicalsanc_Toilet = total_Physicalsanc_Toilet + physicalsanc_Toilet;
                    double total_Physicalsanc_Toilet1 = (double) Math.round(total_Physicalsanc_Toilet * 100) / 100;
                    map.put("total_Physicalsanc_Toilet", total_Physicalsanc_Toilet1);

                    float financialsanc_Toilet = rs.getFloat(17);
                    total_financialsanc_Toilet = total_financialsanc_Toilet + financialsanc_Toilet;
                    double total_financialsanc_Toilet1 = (double) Math.round(total_financialsanc_Toilet * 100) / 100;
                    map.put("total_financialsanc_Toilet", total_financialsanc_Toilet1);


                    float financial_san_in_lakshs = rs.getFloat(18);
                    total_FINANCIAL_SANC_IN_LAKHS = total_FINANCIAL_SANC_IN_LAKHS + financial_san_in_lakshs;
                    double total_FINANCIAL_SANC_IN_LAKHS1 = (double) Math.round(total_FINANCIAL_SANC_IN_LAKHS * 100) / 100;
                    map.put("total_FINANCIAL_SANC_IN_LAKHS", total_FINANCIAL_SANC_IN_LAKHS1);

                    float estimatedCost = rs.getFloat(19);
                    total_EstimatedCost = total_EstimatedCost + estimatedCost;
                    double total_EstimatedCost1 = (double) Math.round(total_EstimatedCost * 100) / 100;
                    map.put("total_EstimatedCost", total_EstimatedCost1);

                    float releases = rs.getFloat(20);
                    total_Releases = total_Releases + releases;
                    double total_Releases1 = (double) Math.round(total_Releases * 100) / 100;
                    map.put("total_Releases", total_Releases1);

                    float expenditure = rs.getFloat(21);
                    total_Expenditure = total_Expenditure + expenditure;
                    double total_Expenditure1 = (double) Math.round(total_Expenditure * 100) / 100;
                    map.put("total_Expenditure", total_Expenditure1);

                    float amountAtToBeReleased = rs.getFloat(22);
                    total_amountAtToBeReleased = total_amountAtToBeReleased + amountAtToBeReleased;
                    double total_amountAtToBeReleased1 = (double) Math.round(total_amountAtToBeReleased * 100) / 100;
                    map.put("total_amountAtToBeReleased", total_amountAtToBeReleased1);

                    float ucsSubmit = rs.getFloat(23);
                    total_ucsSubmit = total_ucsSubmit + ucsSubmit;
                    double total_ucsSubmit1 = (double) Math.round(total_ucsSubmit * 100) / 100;
                    map.put("total_ucsSubmit", total_ucsSubmit1);

                    float balanceAtUCS = rs.getFloat(24);
                    total_balanceAtUCS = total_balanceAtUCS + balanceAtUCS;
                    double total_balanceAtUCS1 = (double) Math.round(total_balanceAtUCS * 100) / 100;
                    map.put("total_balanceAtUCS", total_balanceAtUCS1);

                    float balanceAtApwidc = rs.getFloat(25);
                    total_balanceAtApwidc = total_balanceAtApwidc + balanceAtApwidc;
                    double total_balanceAtApwidc1 = (double) Math.round(total_balanceAtApwidc * 100) / 100;
                    map.put("total_balanceAtApwidc", total_balanceAtApwidc1);

                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }
//omkaram   

    public ArrayList getPhysicalCivilsReport(String phase, String mangId, String reptType, String user) {
        Map map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList distList = new ArrayList();

        int total_noOfSchools = 0;
        float total_physicalsanc_ACR = 0;
        float total_Physicalsanc_ScienceLab = 0;
        float total_Physicalsanc_CompRoom = 0;
        float total_Physicasanc_artcraftroom = 0;
        float total_Physicalsanc_Library = 0;
        float total_Physicalsance_DrinkingWater = 0;
        float total_Physicalsanc_Toilet = 0;

        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[Usp_Get_RMSAAmounts_final](?,?,?,?)}");
            cstmt.setString(1, phase);
            cstmt.setString(2, mangId);
            cstmt.setString(3, reptType);
            cstmt.setString(4, user);
//            System.out.println("phase "+phase);
//            System.out.println("mangId "+mangId);
//            System.out.println("reptType "+reptType);
//            System.out.println("user "+user);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap();

                    map.put("distId", rs.getString(1));
                    map.put("distName", rs.getString(2));
                    map.put("noOfSchools", rs.getString(3));

                    map.put("physicalsanc_ACR", rs.getString(4));
                    map.put("Physicalsanc_ScienceLab", rs.getString(5));
                    map.put("Physicalsanc_CompRoom", rs.getString(6));
                    map.put("Physicasanc_artcraftroom", rs.getString(7));
                    map.put("Physicalsanc_Library", rs.getString(8));
                    map.put("Physicalsance_DrinkingWater", rs.getString(9));
                    map.put("Physicalsanc_Toilet", rs.getString(10));

                    // Totals

                    total_noOfSchools = total_noOfSchools + rs.getInt(3);
                    map.put("total_noOfSchools", total_noOfSchools);

                    float physicalsanc_ACR = rs.getFloat(4);
                    total_physicalsanc_ACR = total_physicalsanc_ACR + physicalsanc_ACR;
                    double total_physicalsanc_ACR1 = (double) Math.round(total_physicalsanc_ACR * 100) / 100;
                    map.put("total_physicalsanc_ACR", total_physicalsanc_ACR1);

                    float physicalsanc_ScienceLab = rs.getFloat(5);
                    total_Physicalsanc_ScienceLab = total_Physicalsanc_ScienceLab + physicalsanc_ScienceLab;
                    double total_Physicalsanc_ScienceLab1 = (double) Math.round(total_Physicalsanc_ScienceLab * 100) / 100;
                    map.put("total_Physicalsanc_ScienceLab", total_Physicalsanc_ScienceLab1);

                    float physicalsanc_CompRoom = rs.getFloat(6);
                    total_Physicalsanc_CompRoom = total_Physicalsanc_CompRoom + physicalsanc_CompRoom;
                    double total_Physicalsanc_CompRoom1 = (double) Math.round(total_Physicalsanc_CompRoom * 100) / 100;
                    map.put("total_Physicalsanc_CompRoom", total_Physicalsanc_CompRoom1);

                    float physicasanc_artcraftroom = rs.getFloat(7);
                    total_Physicasanc_artcraftroom = total_Physicasanc_artcraftroom + physicasanc_artcraftroom;
                    double total_Physicasanc_artcraftroom1 = (double) Math.round(total_Physicasanc_artcraftroom * 100) / 100;
                    map.put("total_Physicasanc_artcraftroom", total_Physicasanc_artcraftroom1);

                    float physicalsanc_Library = rs.getFloat(8);
                    total_Physicalsanc_Library = total_Physicalsanc_Library + physicalsanc_Library;
                    double total_Physicalsanc_Library1 = (double) Math.round(total_Physicalsanc_Library * 100) / 100;
                    map.put("total_Physicalsanc_Library", total_Physicalsanc_Library1);

                    float physicalsance_DrinkingWater = rs.getFloat(9);
                    total_Physicalsance_DrinkingWater = total_Physicalsance_DrinkingWater + physicalsance_DrinkingWater;
                    double total_Physicalsance_DrinkingWater1 = (double) Math.round(total_Physicalsance_DrinkingWater * 100) / 100;
                    map.put("total_Physicalsance_DrinkingWater", total_Physicalsance_DrinkingWater1);

                    float physicalsanc_Toilet = rs.getFloat(10);
                    total_Physicalsanc_Toilet = total_Physicalsanc_Toilet + physicalsanc_Toilet;
                    double total_Physicalsanc_Toilet1 = (double) Math.round(total_Physicalsanc_Toilet * 100) / 100;
                    map.put("total_Physicalsanc_Toilet", total_Physicalsanc_Toilet1);

                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }
//omkaram

    public ArrayList getFinancialCivilsReport(String phase, String mangId, String reptType, String user) {
        Map map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList distList = new ArrayList();

        int total_noOfSchools = 0;
        float total_financialsanc_ACR = 0;
        float total_financialsanc_ScienceLab = 0;
        float total_financialsanc_CompRoom = 0;
        float total_financialsanc_artcraftroom = 0;
        float total_financialsanc_Library = 0;
        float total_financialsanc_DrinkingWater = 0;
        float total_financialsanc_Toilet = 0;

        float total_FINANCIAL_SANC_IN_LAKHS = 0;
        float total_EstimatedCost = 0;
        float total_Releases = 0;
        float total_Expenditure = 0;
        float total_amountAtToBeReleased = 0;
        float total_ucsSubmit = 0;
        float total_balanceAtUCS = 0;
        float total_balanceAtApwidc = 0;


        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[Usp_Get_RMSAAmounts_final](?,?,?,?)}");
            cstmt.setString(1, phase);
            cstmt.setString(2, mangId);
            cstmt.setString(3, reptType);
            cstmt.setString(4, user);
//            System.out.println("phase "+phase);
//            System.out.println("mangId "+mangId);
//            System.out.println("reptType "+reptType);
//            System.out.println("user "+user);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap();

                    map.put("distId", rs.getString(1));
                    map.put("distName", rs.getString(2));
                    map.put("noOfSchools", rs.getString(3));

                    map.put("financialsanc_ACR", rs.getString(4));
                    map.put("financialsanc_ScienceLab", rs.getString(5));
                    map.put("financialsanc_CompRoom", rs.getString(6));
                    map.put("financialsanc_artcraftroom", rs.getString(7));
                    map.put("financialsanc_Library", rs.getString(8));
                    map.put("financialsanc_DrinkingWater", rs.getString(9));
                    map.put("financialsanc_Toilet", rs.getString(10));

                    map.put("FINANCIAL_SANC_IN_LAKHS", rs.getString(11));
                    map.put("EstimatedCost", rs.getString(12));
                    map.put("Releases", rs.getString(13));
                    map.put("Expenditure", rs.getString(14));
                    map.put("amountAtToBeReleased", rs.getString(15));
                    map.put("ucsSubmit", rs.getString(16));
                    map.put("balanceAtUCS", rs.getString(17));
                    map.put("balanceAtApwidc", rs.getString(18));

                    // Totals

                    total_noOfSchools = total_noOfSchools + rs.getInt(3);
                    map.put("total_noOfSchools", total_noOfSchools);

                    float financialsanc_ACR = rs.getFloat(4);
                    total_financialsanc_ACR = total_financialsanc_ACR + financialsanc_ACR;
                    double total_financialsanc_ACR1 = (double) Math.round(total_financialsanc_ACR * 100) / 100;
                    map.put("total_financialsanc_ACR", total_financialsanc_ACR1);

                    float financialsanc_ScienceLab = rs.getFloat(5);
                    total_financialsanc_ScienceLab = total_financialsanc_ScienceLab + financialsanc_ScienceLab;
                    double total_financialsanc_ScienceLab1 = (double) Math.round(total_financialsanc_ScienceLab * 100) / 100;
                    map.put("total_financialsanc_ScienceLab", total_financialsanc_ScienceLab1);

                    float financialsanc_CompRoom = rs.getFloat(6);
                    total_financialsanc_CompRoom = total_financialsanc_CompRoom + financialsanc_CompRoom;
                    double total_financialsanc_CompRoom1 = (double) Math.round(total_financialsanc_CompRoom * 100) / 100;
                    map.put("total_financialsanc_CompRoom", total_financialsanc_CompRoom1);

                    float financialsanc_artcraftroom = rs.getFloat(7);
                    total_financialsanc_artcraftroom = total_financialsanc_artcraftroom + financialsanc_artcraftroom;
                    double total_financialsanc_artcraftroom1 = (double) Math.round(total_financialsanc_artcraftroom * 100) / 100;
                    map.put("total_financialsanc_artcraftroom", total_financialsanc_artcraftroom1);

                    float financialsanc_Library = rs.getFloat(8);
                    total_financialsanc_Library = total_financialsanc_Library + financialsanc_Library;
                    double total_financialsanc_Library1 = (double) Math.round(total_financialsanc_Library * 100) / 100;
                    map.put("total_financialsanc_Library", total_financialsanc_Library1);

                    float financialsanc_DrinkingWater = rs.getFloat(9);
                    total_financialsanc_DrinkingWater = total_financialsanc_DrinkingWater + financialsanc_DrinkingWater;
                    double total_financialsanc_DrinkingWater1 = (double) Math.round(total_financialsanc_DrinkingWater * 100) / 100;
                    map.put("total_financialsanc_DrinkingWater", total_financialsanc_DrinkingWater1);

                    float financialsanc_Toilet = rs.getFloat(10);
                    total_financialsanc_Toilet = total_financialsanc_Toilet + financialsanc_Toilet;
                    double total_financialsanc_Toilet1 = (double) Math.round(total_financialsanc_Toilet * 100) / 100;
                    map.put("total_financialsanc_Toilet", total_financialsanc_Toilet1);


                    float financial_san_in_lakshs = rs.getFloat(11);
                    total_FINANCIAL_SANC_IN_LAKHS = total_FINANCIAL_SANC_IN_LAKHS + financial_san_in_lakshs;
                    double total_FINANCIAL_SANC_IN_LAKHS1 = (double) Math.round(total_FINANCIAL_SANC_IN_LAKHS * 100) / 100;
                    map.put("total_FINANCIAL_SANC_IN_LAKHS", total_FINANCIAL_SANC_IN_LAKHS1);

                    float estimatedCost = rs.getFloat(12);
                    total_EstimatedCost = total_EstimatedCost + estimatedCost;
                    double total_EstimatedCost1 = (double) Math.round(total_EstimatedCost * 100) / 100;
                    map.put("total_EstimatedCost", total_EstimatedCost1);

                    float releases = rs.getFloat(13);
                    total_Releases = total_Releases + releases;
                    double total_Releases1 = (double) Math.round(total_Releases * 100) / 100;
                    map.put("total_Releases", total_Releases1);

                    float expenditure = rs.getFloat(14);
                    total_Expenditure = total_Expenditure + expenditure;
                    double total_Expenditure1 = (double) Math.round(total_Expenditure * 100) / 100;
                    map.put("total_Expenditure", total_Expenditure1);

                    float amountAtToBeReleased = rs.getFloat(15);
                    total_amountAtToBeReleased = total_amountAtToBeReleased + amountAtToBeReleased;
                    double total_amountAtToBeReleased1 = (double) Math.round(total_amountAtToBeReleased * 100) / 100;
                    map.put("total_amountAtToBeReleased", total_amountAtToBeReleased1);

                    float ucsSubmit = rs.getFloat(16);
                    total_ucsSubmit = total_ucsSubmit + ucsSubmit;
                    double total_ucsSubmit1 = (double) Math.round(total_ucsSubmit * 100) / 100;
                    map.put("total_ucsSubmit", total_ucsSubmit1);

                    float balanceAtUCS = rs.getFloat(17);
                    total_balanceAtUCS = total_balanceAtUCS + balanceAtUCS;
                    double total_balanceAtUCS1 = (double) Math.round(total_balanceAtUCS * 100) / 100;
                    map.put("total_balanceAtUCS", total_balanceAtUCS1);

                    float balanceAtApwidc = rs.getFloat(18);
                    total_balanceAtApwidc = total_balanceAtApwidc + balanceAtApwidc;
                    double total_balanceAtApwidc1 = (double) Math.round(total_balanceAtApwidc * 100) / 100;
                    map.put("total_balanceAtApwidc", total_balanceAtApwidc1);

                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }

    //omkaram
    public int updateComponentsTrack(String sno) throws Exception {
        int myreturn = 0;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            cstmt = con.prepareCall("{call RMSAComponents_Log_procedure(?)}");
            cstmt.setString(1, sno);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    myreturn = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }

    /// Omkaram addded 04062018
    public String checkDistrictExpSub(String year, String schoolId) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        String status = "NO";
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select Deo_Submitted from RMSAComponents where YEAR=? and schoolId=?";
            st = con.prepareStatement(query);
            st.setString(1, year);
            st.setString(2, schoolId);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    status = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return status;
    }
    
   
    
    /// Omkaram addded 20181112
    public ArrayList getPhaseList(String schoolId) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList phaseList = new ArrayList();
        String phaselist = "<option value='0'>Select Phase</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select distinct PHASE from RMSAMASTER where UDISE_CODE=? and PHASE is not NULL and PHASE!=? order by PHASE";
            pst = con.prepareStatement(query);
            pst.setString(1, schoolId);
            pst.setString(2, "");
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("phaseNumber", rs.getString(1));
                    String phaseName = "Phase-" + rs.getString(1);
                    map.put("phaseName", phaseName);
                    phaseList.add(map);
                    phaselist = phaselist + "<option value='" + rs.getString(1) + "'>" + phaseName + "</option>";
                    map = null;
                    phaseName = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return phaseList;
    }
}
