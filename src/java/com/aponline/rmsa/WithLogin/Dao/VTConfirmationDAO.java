/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.VCHMConfirmationForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 1250881
 */
public class VTConfirmationDAO {

    public ArrayList getVCHMConfirmationList(VCHMConfirmationForm myform) {

        String query = null;
        ArrayList vlist = new ArrayList();
//        ArrayList<VCHMConfirmationForm> vlist = new ArrayList<VCHMConfirmationForm>();
        PreparedStatement st = null, st1 = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (myform.getVcTrainerId() == null || myform.getVcTrainerId() == "") {
//                query = "select * from VC_APPTable where schcd ='" + myform.getSchoolId() + "' ";
                if (myform.getRoleName().equalsIgnoreCase("SrAsst")) {
                    query = " select * from VC_APPTable where VC_Status='Y' and  HM_Remarks is not null  ";
                } else if (myform.getRoleName().equalsIgnoreCase("Supdnt")) {
                    query = " select * from VC_APPTable where VC_Status='Y' and  SrAssistantRemarks is not null and   HM_Remarks is not null   ";
                } else if (myform.getRoleName().equalsIgnoreCase("asstDire")) {
                    query = " select * from VC_APPTable where VC_Status='Y' and  SrAssistantRemarks is not null and   HM_Remarks is not null and SuperindentRemarks is not null   ";
                } else if (myform.getRoleName().equalsIgnoreCase("Director")) {
                    query = " select * from VC_APPTable where VC_Status='Y' and  SrAssistantRemarks is not null and   HM_Remarks is not null and SuperindentRemarks is not null and AsstDirectorRemarks is not null ";
                }
            } else {
//                query = "select * from VC_APPTable where schcd ='" + myform.getSchoolId() + "' and VocationalTrainerId ='" + myform.getVcTrainerId() + "'";
                query = "select * from VC_APPTable where VocationalTrainerId ='" + myform.getVcTrainerId() + "'";
            }

//            VCHMConfirmationForm form = null;

            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schcd", rs.getString(1));
                    map.put("vctrainerId", rs.getString(2));
                    map.put("vctrainerName", rs.getString(3));
                    map.put("sectorId", rs.getString(4));
                    map.put("sectorName", rs.getString(5));
                    map.put("tradeId", rs.getString(6));
                    map.put("tradeName", rs.getString(7));
                    map.put("firstQAns", rs.getString(8));
                    map.put("secondQAns", rs.getString(9));
                    map.put("thirdQAns", rs.getString(10));
                    map.put("fourthQAns", rs.getString(11));
                    map.put("fifthQAns", rs.getString(12));
                    map.put("sixthQAns", rs.getString(13));
                    map.put("seventhQAns", rs.getString(14));
                    map.put("eightQAns", rs.getString(15));
                    map.put("ninethQAns", rs.getString(16));
                    map.put("tenthQAns", rs.getString(17));
                    map.put("remarks", rs.getString(18));
                    map.put("aadharno", rs.getString(19));
                    map.put("mobileno", rs.getString(20));
                    map.put("emailId", rs.getString(21));
                    map.put("dob", rs.getString(22));
                    map.put("year", rs.getString(23));
                    if (rs.getString(24) == null || rs.getString(24) == "") {
                        map.put("hmRemark", "0");
                    } else {
                        map.put("hmRemark", rs.getString(24));
                    }

                    //Invoice
                    if (rs.getString(27) == null || rs.getString(27) == "") {
                        map.put("vcOrgstatus", "0");
                    } else {
                        map.put("vcOrgstatus", rs.getString(27));
                    }
                    //srAsst
                    if (rs.getString(29) == null || rs.getString(29) == "") {
                        map.put("srAsstRemarks", "0");
                    } else {
                        map.put("srAsstRemarks", rs.getString(29));
                    }
                    //superd
                    if (rs.getString(31) == null || rs.getString(31) == "") {
                        map.put("suptRemarks", "0");
                    } else {
                        map.put("suptRemarks", rs.getString(31));
                    }
                    //asstdir
                    if (rs.getString(33) == null || rs.getString(33) == "") {
                        map.put("asstDirectorRemarks", "0");
                    } else {
                        map.put("asstDirectorRemarks", rs.getString(33));
                    }
                    //dir
                    if (rs.getString(35) == null || rs.getString(35) == "") {
                        map.put("directorRemarks", "0");
                    } else {
                        map.put("directorRemarks", rs.getString(35));
                    }
                    map.put("directorStatus", rs.getString(37));
                    vlist.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return vlist;
    }

    public static String dateTime() {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    public String remarksStatusUpdate(VCHMConfirmationForm myform) {
        Connection con = null;
        PreparedStatement pstmt = null;
        String hmStatus = null;
        int vcRemark;
        CommonConstants conistants = new CommonConstants();
        String query = null;
        try {

            String datetime = dateTime();
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (!myform.getRoleName().equalsIgnoreCase("Director")) {
                if (myform.getRoleName().equalsIgnoreCase("SrAsst")) {
                    query = "UPDATE VC_APPTable SET SrAssistantRemarks = ?, SrAssistUpdateDate = ? WHERE schcd = ? and VocationalTrainerId= ? ";
                } else if (myform.getRoleName().equalsIgnoreCase("Supdnt")) {
                    query = "UPDATE VC_APPTable SET SuperindentRemarks = ?, SuperindentUpdateDate = ? WHERE schcd = ? and VocationalTrainerId= ? ";
                } else if (myform.getRoleName().equalsIgnoreCase("AsstDire")) {
                    query = "UPDATE VC_APPTable SET AsstDirectorRemarks = ?, AsstDirectorUpdateDate = ? WHERE schcd = ? and VocationalTrainerId= ? ";
                }
                pstmt = con.prepareStatement(query);
                pstmt.setString(1, myform.getVcRemarksValue());
                pstmt.setString(2, datetime);
                pstmt.setString(3, myform.getSchoolId());
                pstmt.setString(4, myform.getvTrainerId());
                vcRemark = pstmt.executeUpdate();
                if (vcRemark > 0) {
                    hmStatus = myform.getSchoolId() + " Approved Successfully";
                }
            }

            if (myform.getRoleName().equalsIgnoreCase("Director")) {

                query = "UPDATE VC_APPTable SET DirectorRemarks = ?,DirectorStatus = ?, DirectorUpdateDate = ? WHERE schcd = ? and VocationalTrainerId= ? ";


                pstmt = con.prepareStatement(query);
                pstmt.setString(1, myform.getVcRemarksValue());
                pstmt.setString(2, myform.getStatusDirector());
                pstmt.setString(3, datetime);
                pstmt.setString(4, myform.getSchoolId());
                pstmt.setString(5, myform.getvTrainerId());
                vcRemark = pstmt.executeUpdate();
                if (vcRemark > 0) {
                    hmStatus = myform.getSchoolId() + " Approved Successfully";
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        return hmStatus;
    }
}
