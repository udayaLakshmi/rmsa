/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.axis2.databinding.types.soapencoding.Decimal;

/**
 *
 * @author 1250881
 */
public class RMSAComponentReportDAO {

    public ArrayList getDistList(String id, String year, String management) {
        ArrayList distList = new ArrayList();
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();

        BigDecimal civilNonRecTotal = new BigDecimal(0);
        BigDecimal majorNonRecTotal = new BigDecimal(0);
        BigDecimal minorNonRecTotal = new BigDecimal(0);
        BigDecimal toiletNonRecTotal = new BigDecimal(0);
        BigDecimal totNonRecTotal = new BigDecimal(0);

        BigDecimal expcivilNonRecTotal = new BigDecimal(0);
        BigDecimal expmajorNonRecTotal = new BigDecimal(0);
        BigDecimal expminorNonRecTotal = new BigDecimal(0);
        BigDecimal exptoiletNonRecTotal = new BigDecimal(0);
        BigDecimal exptotNonRecTotal = new BigDecimal(0);

        BigDecimal totcivilNonRecTotal = new BigDecimal(0);
        BigDecimal totmajorNonRecTotal = new BigDecimal(0);
        BigDecimal totminorNonRecTotal = new BigDecimal(0);
        BigDecimal tottoiletNonRecTotal = new BigDecimal(0);
        BigDecimal tottotNonRecTotal = new BigDecimal(0);

        BigDecimal waterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal booksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal minorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal sanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal needAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal provisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal totAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal expwaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal exptotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal twaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tNeedAnnualGrantsTotalBalance = new BigDecimal(0);
        BigDecimal tneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal ttotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal excurTriprecurringTotal = new BigDecimal(0);
        BigDecimal selfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal otherrecurringTotal = new BigDecimal(0);
        BigDecimal totrecurringTotal = new BigDecimal(0);

        BigDecimal excexcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal excselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal excotherrecurringTotal = new BigDecimal(0);
        BigDecimal exctotrecurringTotal = new BigDecimal(0);

        BigDecimal texcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal tselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal totherrecurringTotal = new BigDecimal(0);
        BigDecimal ttotrecurringTotal = new BigDecimal(0);

        BigDecimal relfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal rellabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal reltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal exfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal tfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal total_CivilWorks_OB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_OB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_OB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_OB = new BigDecimal(0);
        BigDecimal total_Toilets_OB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_OB = new BigDecimal(0);
        BigDecimal total_Interest_OB = new BigDecimal(0);
        BigDecimal total_total_total_OB = new BigDecimal(0);

        BigDecimal total_CivilWorks_EOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_EOB = new BigDecimal(0);
        BigDecimal total_Toilets_EOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_EOB = new BigDecimal(0);
        BigDecimal total_Interest_EOB = new BigDecimal(0);
        BigDecimal total_total_total_EOB = new BigDecimal(0);

        BigDecimal total_CivilWorks_UBOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Toilets_UBOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Interest_UBOB = new BigDecimal(0);
        BigDecimal total_total_total_UBOB = new BigDecimal(0);
        BigDecimal ubob_released_total = new BigDecimal(0);

        BigDecimal total_Earned_Interest = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest = new BigDecimal(0);
        BigDecimal total_Balance_Interest = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred = new BigDecimal(0);

        BigDecimal total_total_OB = new BigDecimal(0);
        BigDecimal total_totalRealse_Realses = new BigDecimal(0);
        BigDecimal total_Earned_Interest_total = new BigDecimal(0);
        BigDecimal totalRealse_total_total = new BigDecimal(0);

        BigDecimal Grand_Total_Expenditure_Incurred_on_Opening_balances = new BigDecimal(0);
        BigDecimal Grand_Total_Expenditure_Incurred_on_Releases_total = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest_total = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred_total = new BigDecimal(0);
        BigDecimal Grand_Total_Total_Expenditure_total = new BigDecimal(0);

        BigDecimal close_balnce_ub_ob_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_rb_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_ineterest_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_total_total = new BigDecimal(0);
        BigDecimal furniture_bal_sum = new BigDecimal(0);
        BigDecimal lab_equipment_sum = new BigDecimal(0);
        long totalSchoolsTotal = 0;


        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report](?,?)}");
//            cstmt.setString(1, id);
//            cstmt.setString(2, year);

            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report](?,?,?)}");
            cstmt.setString(1, id);
            cstmt.setString(2, year);
            cstmt.setString(3, management);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    BigDecimal civilReleased1 = null;
                    BigDecimal majorReleased1 = null;
                    BigDecimal minorReleased1 = null;
                    BigDecimal toiletsReleased1 = null;
                    BigDecimal civilExp1 = null;
                    BigDecimal majorExp1 = null;
                    BigDecimal minorExp1 = null;
                    BigDecimal toiletsExp1 = null;
                    BigDecimal waterReleased1 = null;
                    BigDecimal booksReleased1 = null;
                    BigDecimal minorReleasedAnnual1 = null;
                    BigDecimal sanitationReleased1 = null;
                    BigDecimal needReleased1 = null;
                    BigDecimal provisionalReleased1 = null;
                    BigDecimal waterExp1 = null;
                    BigDecimal booksExp1 = null;
                    BigDecimal minorExpAnnual1 = null;
                    BigDecimal sanitationExp1 = null;
                    BigDecimal needExp1 = null;
                    BigDecimal provisionalExp1 = null;
                    BigDecimal excurtionReleased1 = null;
                    BigDecimal selfdefReleased1 = null;
                    BigDecimal otherRecuGrantReleased1 = null;
                    BigDecimal excurtionExp1 = null;
                    BigDecimal selfdefExp1 = null;
                    BigDecimal otherRecuGrantExp1 = null;
                    BigDecimal furnitureReleased1 = null;
                    BigDecimal labEquipReleased1 = null;
                    BigDecimal furnitureExp1 = null;
                    BigDecimal labEquipExp1 = null;

                    map = new HashMap();
                    map.put("distcode", rs.getString(1));
                    map.put("distname", rs.getString(2));
                    if (rs.getString(3) == null) {
                        map.put("civilReleased", "0.00");
                    } else {
                        civilReleased1 = new BigDecimal(rs.getDouble(3));
                        map.put("civilReleased", civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(4) == null) {
                        map.put("majorReleased", "0.00");
                    } else {
                        majorReleased1 = new BigDecimal(rs.getDouble(4));
                        map.put("majorReleased", majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(5) == null) {
                        map.put("minorReleased", "0.00");
                    } else {
                        minorReleased1 = new BigDecimal(rs.getDouble(5));
                        map.put("minorReleased", minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(6) == null) {
                        map.put("toiletsReleased", "0.00");
                    } else {
                        toiletsReleased1 = new BigDecimal(rs.getDouble(6));
                        map.put("toiletsReleased", toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(7) == null) {
                        map.put("civilExp", "0.00");
                    } else {
                        civilExp1 = new BigDecimal(rs.getDouble(7));
                        map.put("civilExp", civilExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(8) == null) {
                        map.put("majorExp", "0.00");
                    } else {
                        majorExp1 = new BigDecimal(rs.getDouble(8));
                        map.put("majorExp", majorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(9) == null) {
                        map.put("minorExp", "0.00");
                    } else {
                        minorExp1 = new BigDecimal(rs.getDouble(9));
                        map.put("minorExp", minorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(10) == null) {
                        map.put("toiletsExp", "0.00");
                    } else {
                        toiletsExp1 = new BigDecimal(rs.getDouble(10));
                        map.put("toiletsExp", toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(11) == null) {
                        map.put("waterReleased", "0.00");
                    } else {
                        waterReleased1 = new BigDecimal(rs.getDouble(11));
                        map.put("waterReleased", waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(12) == null) {
                        map.put("booksReleased", "0.00");
                    } else {
                        booksReleased1 = new BigDecimal(rs.getDouble(12));
                        map.put("booksReleased", booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(13) == null) {
                        map.put("minorReleasedAnnual", "0.00");
                    } else {
                        minorReleasedAnnual1 = new BigDecimal(rs.getDouble(13));
                        map.put("minorReleasedAnnual", minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(14) == null) {
                        map.put("sanitationReleased", "0.00");
                    } else {
                        sanitationReleased1 = new BigDecimal(rs.getDouble(14));
                        map.put("sanitationReleased", sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(15) == null) {
                        map.put("needReleased", "0.00");
                    } else {
                        needReleased1 = new BigDecimal(rs.getDouble(15));
                        map.put("needReleased", needReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(16) == null) {
                        map.put("provisionalReleased", "0.00");
                    } else {
                        provisionalReleased1 = new BigDecimal(rs.getDouble(16));
                        map.put("provisionalReleased", provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(17) == null) {
                        map.put("waterExp", "0.00");
                    } else {
                        waterExp1 = new BigDecimal(rs.getDouble(17));
                        map.put("waterExp", waterExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(18) == null) {
                        map.put("booksExp", "0.00");
                    } else {
                        booksExp1 = new BigDecimal(rs.getDouble(18));
                        map.put("booksExp", booksExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(19) == null) {
                        map.put("minorExpAnnual", "0.00");
                    } else {
                        minorExpAnnual1 = new BigDecimal(rs.getDouble(19));
                        map.put("minorExpAnnual", minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(20) == null) {
                        map.put("sanitationExp", "0.00");
                    } else {
                        sanitationExp1 = new BigDecimal(rs.getDouble(20));
                        map.put("sanitationExp", sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(21) == null) {
                        map.put("needExp", "0.00");
                    } else {
                        needExp1 = new BigDecimal(rs.getDouble(21));
                        map.put("needExp", needExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(22) == null) {
                        map.put("provisionalExp", "0.00");
                    } else {
                        provisionalExp1 = new BigDecimal(rs.getDouble(22));
                        map.put("provisionalExp", provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(23) == null) {
                        map.put("excurtionReleased", "0.00");
                    } else {
                        excurtionReleased1 = new BigDecimal(rs.getDouble(23));
                        map.put("excurtionReleased", excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(24) == null) {
                        map.put("selfdefReleased", "0.00");
                    } else {
                        selfdefReleased1 = new BigDecimal(rs.getDouble(24));
                        map.put("selfdefReleased", selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(25) == null) {
                        map.put("otherRecuGrantReleased", "0.00");
                    } else {
                        otherRecuGrantReleased1 = new BigDecimal(rs.getDouble(25));
                        map.put("otherRecuGrantReleased", otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(26) == null) {
                        map.put("excurtionExp", "0.00");
                    } else {
                        excurtionExp1 = new BigDecimal(rs.getDouble(26));
                        map.put("excurtionExp", excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(27) == null) {
                        selfdefExp1 = new BigDecimal("0.00");
                        map.put("selfdefExp", "0.00");
                    } else {
                        selfdefExp1 = new BigDecimal(rs.getDouble(27));
                        map.put("selfdefExp", selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(28) == null) {
                        map.put("otherRecuGrantExp", "0.00");
                    } else {
                        otherRecuGrantExp1 = new BigDecimal(rs.getDouble(28));
                        map.put("otherRecuGrantExp", otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    if (rs.getString(29) == null) {
                        map.put("furnitureReleased", "0.00");
                    } else {
                        furnitureReleased1 = new BigDecimal(rs.getDouble(29));
                        map.put("furnitureReleased", furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(30) == null) {
                        map.put("labEquipReleased", "0.00");
                    } else {
                        labEquipReleased1 = new BigDecimal(rs.getDouble(30));
                        map.put("labEquipReleased", labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(31) == null) {
                        map.put("furnitureExp", "0.00");
                    } else {
                        furnitureExp1 = new BigDecimal(rs.getDouble(31));
                        map.put("furnitureExp", furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(32) == null) {
                        map.put("labEquipExp", "0.00");
                    } else {
                        labEquipExp1 = new BigDecimal(rs.getDouble(32));
                        map.put("labEquipExp", labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    
                    
                    BigDecimal acivirel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal aciviexp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal civibaaltot = acivirel.subtract(aciviexp);
                    map.put("civilBal", civibaaltot);
                    
                    
                    
                    BigDecimal majrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majexp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majtot = majrel.subtract(majexp);
                    map.put("majorBal", majtot);
                    BigDecimal minrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal minexp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mintot = minrel.subtract(minexp);
                    map.put("minorBal", mintot);
                    BigDecimal toirel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toiexp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toitot = toirel.subtract(toiexp);
                    map.put("toiletsBal", toitot);
                    //totals
                    BigDecimal tocivicnorecrel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecrel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectot = new BigDecimal(0);
                    tonorectot = tonorectot.add(tocivicnorecrel);
                    tonorectot = tonorectot.add(tomajnonrecrel);
                    tonorectot = tonorectot.add(tominnonrecrel);
                    tonorectot = tonorectot.add(tototinonrecrel);
                    map.put("totalNonRecurReleased", tonorectot);
                   
                    
                    BigDecimal tocivicnorecExp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecExp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecExp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecExp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectotExp = new BigDecimal(0);
                    tonorectotExp = tonorectotExp.add(tocivicnorecExp);
                    tonorectotExp = tonorectotExp.add(tomajnonrecExp);
                    tonorectotExp = tonorectotExp.add(tominnonrecExp);
                    tonorectotExp = tonorectotExp.add(tototinonrecExp);
                    map.put("totalNonRecurExp", tonorectotExp);
                    
                    
                    
                    BigDecimal totciv = civibaaltot;
                    BigDecimal totmaj = majtot;
                    BigDecimal totmin = mintot;
                    BigDecimal tottoil = toitot;
                    BigDecimal totmajexpbal = new BigDecimal(0);
                    totmajexpbal = totmajexpbal.add(totciv);
                    totmajexpbal = totmajexpbal.add(totmaj);
                    totmajexpbal = totmajexpbal.add(totmin);
                    totmajexpbal = totmajexpbal.add(tottoil);
                    map.put("totalNonRecurBal", totmajexpbal);
                    
                    
                    //endtotal
                    BigDecimal warel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal waexp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal watot = warel.subtract(waexp);
                    map.put("waterBal", watot);
                    BigDecimal borel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal boxp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal botot = borel.subtract(boxp);
                    map.put("booksBal", botot);
                    BigDecimal mianrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mianxp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal miantot = mianrel.subtract(mianxp);
                    map.put("minorBalAnnual", miantot);
                    BigDecimal sarel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal saexp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal satot = sarel.subtract(saexp);
                    map.put("sanitationBal", satot);
                    BigDecimal needrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needexp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needtot = needrel.subtract(needexp);
                    map.put("needBal", needtot);
                    BigDecimal pvrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvexp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvtot = pvrel.subtract(pvexp);
                    map.put("provisionalBal", pvtot);
                    //totals
                    BigDecimal towatcannualgrantrel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantrel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantrel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgranttot = new BigDecimal(0);
                    toannualgranttot = toannualgranttot.add(towatcannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tobookannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tominannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tosaniannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toneedannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toprovannualgrantrel);
                    map.put("totalGrantReleased", toannualgranttot);
                    BigDecimal towatcannualgrantExp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantExp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantExp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantExp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantExp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantExp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgrantExptot = new BigDecimal(0);
                    toannualgrantExptot = toannualgrantExptot.add(towatcannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tobookannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tominannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tosaniannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toneedannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toprovannualgrantExp);
                    map.put("totalGrantExp", toannualgrantExptot);
                    BigDecimal towatcannualgrantt = watot;
                    BigDecimal tobookannualgrantt = botot;
                    BigDecimal tominannualgrantt = miantot;
                    BigDecimal tosaniannualgrantt = satot;
                    BigDecimal toneedannualgrantt = needtot;
                    BigDecimal toprovannualgrantt = pvtot;
                    BigDecimal toannualgranttott = new BigDecimal(0);
                    toannualgranttott = toannualgranttott.add(towatcannualgrantt);
                    toannualgranttott = toannualgranttott.add(tobookannualgrantt);
                    toannualgranttott = toannualgranttott.add(tominannualgrantt);
                    toannualgranttott = toannualgranttott.add(tosaniannualgrantt);
                    toannualgranttott = toannualgranttott.add(toneedannualgrantt);
                    toannualgranttott = toannualgranttott.add(toprovannualgrantt);
                    map.put("totalGrantBal", toannualgranttott);
                    //endtotal
                    BigDecimal exrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal exexp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal extot = exrel.subtract(exexp);
                    map.put("excurtionBal", extot);
                    BigDecimal serel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal seexp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal setot = serel.subtract(seexp);
                    map.put("selfdefBal", setot);
                    BigDecimal otrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otexp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ottot = otrel.subtract(otexp);
                    map.put("otherRecuGrantBal", ottot);
                    //totals          
                    BigDecimal excurrecurrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurrel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutot = new BigDecimal(0);
                    recutot = recutot.add(excurrecurrel);
                    recutot = recutot.add(selfrecurrel);
                    recutot = recutot.add(otherrecurrel);
                    map.put("totalRecurReleased", recutot);
                    BigDecimal excurrecurExp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurExp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurExp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutotExp = new BigDecimal(0);
                    recutotExp = recutotExp.add(excurrecurExp);
                    recutotExp = recutotExp.add(selfrecurExp);
                    recutotExp = recutotExp.add(otherrecurExp);
                    map.put("totalRecurExp", recutotExp);
                    BigDecimal excurrecurt = extot;
                    BigDecimal selfrecurt = setot;
                    BigDecimal otherrecurt = ottot;
                    BigDecimal recutott = new BigDecimal(0);
                    recutott = recutott.add(excurrecurt);
                    recutott = recutott.add(selfrecurt);
                    recutott = recutott.add(otherrecurt);
                    map.put("totalRecurBal", recutott);
                    //endtotal
                    BigDecimal frel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal fexp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ftot = frel.subtract(fexp);
                    map.put("furnitureBal", ftot);
                    BigDecimal labrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labexp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labtot = labrel.subtract(labexp);
                    map.put("labEquipBal", labtot);
                    //totals    
                    BigDecimal furnfurlabrel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelreltot = furnfurlabrel.add(labfurlabrel);
                    map.put("totallabEquipReleased", furlabrelreltot);
                    BigDecimal furnfurlabExp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabExp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelExptot = furnfurlabExp.add(labfurlabExp);
                    map.put("totallabEquipExp", furlabrelExptot);
                    BigDecimal furnfurlabt = ftot;
                    BigDecimal labfurlabt = labtot;
                    BigDecimal furlabrelttot = furnfurlabt.add(labfurlabt);
                    map.put("totallabEquipBal", furlabrelttot);
                    furniture_bal_sum = furniture_bal_sum.add(furnfurlabt);
                    lab_equipment_sum = lab_equipment_sum.add(labfurlabt);
                    map.put("furniture_bal_sum", furniture_bal_sum);
                    map.put("lab_equipment_sum", lab_equipment_sum);
                    map.put("totalSchools", rs.getString(59));

                    //endtotal

                    /////////Final totals in tfoot
                    civilNonRecTotal = civilNonRecTotal.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    majorNonRecTotal = majorNonRecTotal.add(majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorNonRecTotal = minorNonRecTotal.add(minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    toiletNonRecTotal = toiletNonRecTotal.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totNonRecTotal = totNonRecTotal.add(tonorectot);

                    expcivilNonRecTotal = expcivilNonRecTotal.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    expmajorNonRecTotal = expmajorNonRecTotal.add(majorExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorNonRecTotal = expminorNonRecTotal.add(minorExp1.setScale(2, RoundingMode.HALF_UP));
                    exptoiletNonRecTotal = exptoiletNonRecTotal.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotNonRecTotal = exptotNonRecTotal.add(tonorectotExp);
                    totcivilNonRecTotal = totcivilNonRecTotal.add(civibaaltot);
                    totmajorNonRecTotal = totmajorNonRecTotal.add(majtot);
                    totminorNonRecTotal = totminorNonRecTotal.add(mintot);
                    tottoiletNonRecTotal = tottoiletNonRecTotal.add(toitot);
                    tottotNonRecTotal = tottotNonRecTotal.add(totmajexpbal);

                    waterAnnualGrantsTotal = waterAnnualGrantsTotal.add(waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    booksAnnualGrantsTotal = booksAnnualGrantsTotal.add(booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorAnnualGrantsTotal = minorAnnualGrantsTotal.add(minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    sanitationAnnualGrantsTotal = sanitationAnnualGrantsTotal.add(sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    needAnnualGrantsTotal = needAnnualGrantsTotal.add(needReleased1.setScale(2, RoundingMode.HALF_UP));
                    provisiAnnualGrantsTotal = provisiAnnualGrantsTotal.add(provisionalReleased1.setScale(2, RoundingMode.HALF_UP));

                    totAnnualGrantsTotal = totAnnualGrantsTotal.add(toannualgranttot);

                    expwaterAnnualGrantsTotal = expwaterAnnualGrantsTotal.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    expbooksAnnualGrantsTotal = expbooksAnnualGrantsTotal.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorAnnualGrantsTotal = expminorAnnualGrantsTotal.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    expsanitationAnnualGrantsTotal = expsanitationAnnualGrantsTotal.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    expneedAnnualGrantsTotal = expneedAnnualGrantsTotal.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    expprovisiAnnualGrantsTotal = expprovisiAnnualGrantsTotal.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotAnnualGrantsTotal = exptotAnnualGrantsTotal.add(toannualgrantExptot);
                    twaterAnnualGrantsTotal = twaterAnnualGrantsTotal.add(watot);
                    tbooksAnnualGrantsTotal = tbooksAnnualGrantsTotal.add(botot);
                    tminorAnnualGrantsTotal = tminorAnnualGrantsTotal.add(miantot);
                    tsanitationAnnualGrantsTotal = tsanitationAnnualGrantsTotal.add(satot);
                    tNeedAnnualGrantsTotalBalance = tNeedAnnualGrantsTotalBalance.add(needtot);
                    tneedAnnualGrantsTotal = tneedAnnualGrantsTotal.add(needexp);
                    tprovisiAnnualGrantsTotal = tprovisiAnnualGrantsTotal.add(pvtot);
                    ttotAnnualGrantsTotal = ttotAnnualGrantsTotal.add(toannualgranttott);

                    excurTriprecurringTotal = excurTriprecurringTotal.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    selfdefencerecurringTotal = selfdefencerecurringTotal.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    otherrecurringTotal = otherrecurringTotal.add(otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    totrecurringTotal = totrecurringTotal.add(recutot);

                    excexcurTriprecurringTotal = excexcurTriprecurringTotal.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    excselfdefencerecurringTotal = excselfdefencerecurringTotal.add(selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    excotherrecurringTotal = excotherrecurringTotal.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));

                    exctotrecurringTotal = exctotrecurringTotal.add(recutotExp);
                    texcurTriprecurringTotal = texcurTriprecurringTotal.add(extot);
                    tselfdefencerecurringTotal = tselfdefencerecurringTotal.add(setot);
                    totherrecurringTotal = totherrecurringTotal.add(ottot);
                    ttotrecurringTotal = ttotrecurringTotal.add(recutott);

                    relfurFurniturLabEquipmentTotal = relfurFurniturLabEquipmentTotal.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    rellabFurniturLabEquipmentTotal = rellabFurniturLabEquipmentTotal.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    reltotFurniturLabEquipmentTotal = reltotFurniturLabEquipmentTotal.add(furlabrelreltot);

                    exfurFurniturLabEquipmentTotal = exfurFurniturLabEquipmentTotal.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    exlabFurniturLabEquipmentTotal = exlabFurniturLabEquipmentTotal.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));

                    exltotFurniturLabEquipmentTotal = exltotFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tfurFurniturLabEquipmentTotal = tfurFurniturLabEquipmentTotal.add(furlabrelreltot);
                    tlabFurniturLabEquipmentTotal = tlabFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tltotFurniturLabEquipmentTotal = tltotFurniturLabEquipmentTotal.add(furlabrelttot);


                    totalSchoolsTotal = totalSchoolsTotal + rs.getLong(59);
                    map.put("totalSchoolsTotal", totalSchoolsTotal);

                    //annual grants

                    map.put("civilNonRecTotal", civilNonRecTotal);
                    map.put("majorNonRecTotal", majorNonRecTotal);
                    map.put("minorNonRecTotal", minorNonRecTotal);
                    map.put("toiletNonRecTotal", toiletNonRecTotal);
                    map.put("totNonRecTotal", totNonRecTotal);

                    map.put("expcivilNonRecTotal", expcivilNonRecTotal);
                    map.put("expmajorNonRecTotal", expmajorNonRecTotal);
                    map.put("expminorNonRecTotal", expminorNonRecTotal);
                    map.put("exptoiletNonRecTotal", exptoiletNonRecTotal);
                    map.put("exptotNonRecTotal", exptotNonRecTotal);

                    map.put("totcivilNonRecTotal", totcivilNonRecTotal);
                    map.put("totmajorNonRecTotal", totmajorNonRecTotal);
                    map.put("totminorNonRecTotal", totminorNonRecTotal);
                    map.put("tottoiletNonRecTotal", tottoiletNonRecTotal);
                    map.put("tottotNonRecTotal", tottotNonRecTotal);

                    map.put("waterAnnualGrantsTotal", waterAnnualGrantsTotal);
                    map.put("booksAnnualGrantsTotal", booksAnnualGrantsTotal);
                    map.put("minorAnnualGrantsTotal", minorAnnualGrantsTotal);
                    map.put("sanitationAnnualGrantsTotal", sanitationAnnualGrantsTotal);
                    map.put("needAnnualGrantsTotal", needAnnualGrantsTotal);
                    map.put("provisiAnnualGrantsTotal", provisiAnnualGrantsTotal);
                    map.put("totAnnualGrantsTotal", totAnnualGrantsTotal);

                    map.put("expwaterAnnualGrantsTotal", expwaterAnnualGrantsTotal);
                    map.put("expbooksAnnualGrantsTotal", expbooksAnnualGrantsTotal);
                    map.put("expminorAnnualGrantsTotal", expminorAnnualGrantsTotal);
                    map.put("expsanitationAnnualGrantsTotal", expsanitationAnnualGrantsTotal);
                    map.put("expneedAnnualGrantsTotal", expneedAnnualGrantsTotal);
                    map.put("expprovisiAnnualGrantsTotal", expprovisiAnnualGrantsTotal);
                    map.put("exptotAnnualGrantsTotal", exptotAnnualGrantsTotal);

                    map.put("twaterAnnualGrantsTotal", twaterAnnualGrantsTotal);
                    map.put("tbooksAnnualGrantsTotal", tbooksAnnualGrantsTotal);
                    map.put("tminorAnnualGrantsTotal", tminorAnnualGrantsTotal);
                    map.put("tsanitationAnnualGrantsTotal", tsanitationAnnualGrantsTotal);
                    map.put("tNeedAnnualGrantsTotalBalance", tNeedAnnualGrantsTotalBalance);
                    map.put("tneedAnnualGrantsTotal", tneedAnnualGrantsTotal);
                    map.put("tprovisiAnnualGrantsTotal", tprovisiAnnualGrantsTotal);
                    map.put("ttotAnnualGrantsTotal", ttotAnnualGrantsTotal);

                    map.put("excurTriprecurringTotal", excurTriprecurringTotal);
                    map.put("selfdefencerecurringTotal", selfdefencerecurringTotal);
                    map.put("otherrecurringTotal", otherrecurringTotal);
                    map.put("totrecurringTotal", totrecurringTotal);

                    map.put("excexcurTriprecurringTotal", excexcurTriprecurringTotal);
                    map.put("excselfdefencerecurringTotal", excselfdefencerecurringTotal);
                    map.put("excotherrecurringTotal", excotherrecurringTotal);
                    map.put("exctotrecurringTotal", exctotrecurringTotal);

                    map.put("texcurTriprecurringTotal", texcurTriprecurringTotal);
                    map.put("tselfdefencerecurringTotal", tselfdefencerecurringTotal);
                    map.put("totherrecurringTotal", totherrecurringTotal);
                    map.put("ttotrecurringTotal", ttotrecurringTotal);

                    map.put("relfurFurniturLabEquipmentTotal", relfurFurniturLabEquipmentTotal);
                    map.put("rellabFurniturLabEquipmentTotal", rellabFurniturLabEquipmentTotal);
                    map.put("reltotFurniturLabEquipmentTotal", reltotFurniturLabEquipmentTotal);

                    map.put("exfurFurniturLabEquipmentTotal", exfurFurniturLabEquipmentTotal);
                    map.put("exlabFurniturLabEquipmentTotal", exlabFurniturLabEquipmentTotal);
                    map.put("exltotFurniturLabEquipmentTotal", exltotFurniturLabEquipmentTotal);

                    map.put("tfurFurniturLabEquipmentTotal", tfurFurniturLabEquipmentTotal);
                    map.put("tlabFurniturLabEquipmentTotal", tlabFurniturLabEquipmentTotal);
                    map.put("tltotFurniturLabEquipmentTotal", tltotFurniturLabEquipmentTotal);

                    BigDecimal CivilWorks_OB = null;
                    BigDecimal MajorRepairs_OB = null;
                    BigDecimal MinorRepairs_OB = null;
                    BigDecimal AnnualOtherRecurringGrants_OB = null;
                    BigDecimal Toilets_OB = null;
                    BigDecimal SelfDefenseTrainingGrants_OB = null;
                    BigDecimal Interest_OB = null;

                    if (rs.getString(33) == null) {
                        map.put("CivilWorks_OB", "0.00");
                    } else {
                        CivilWorks_OB = new BigDecimal(rs.getDouble(33));
                        map.put("CivilWorks_OB", CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(34) == null) {
                        map.put("MajorRepairs_OB", "0.00");
                    } else {
                        MajorRepairs_OB = new BigDecimal(rs.getDouble(34));
                        map.put("MajorRepairs_OB", MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(35) == null) {
                        map.put("MinorRepairs_OB", "0.00");
                    } else {
                        MinorRepairs_OB = new BigDecimal(rs.getDouble(35));
                        map.put("MinorRepairs_OB", MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(36) == null) {
                        map.put("AnnualOtherRecurringGrants_OB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_OB = new BigDecimal(rs.getDouble(36));
                        map.put("AnnualOtherRecurringGrants_OB", AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(37) == null) {
                        map.put("Toilets_OB", "0.00");
                    } else {
                        Toilets_OB = new BigDecimal(rs.getDouble(37));
                        map.put("Toilets_OB", Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(38) == null) {
                        map.put("SelfDefenseTrainingGrants_OB", "0.00");
                    } else {
                        SelfDefenseTrainingGrants_OB = new BigDecimal(rs.getDouble(38));
                        map.put("SelfDefenseTrainingGrants_OB", SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(39) == null) {
                        map.put("Interest_OB", "0.00");
                    } else {
                        Interest_OB = new BigDecimal(rs.getDouble(39));
                        map.put("Interest_OB", Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    }

                    BigDecimal total_OB = new BigDecimal(0);
                    total_OB = total_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_OB", total_OB);

                    total_CivilWorks_OB = total_CivilWorks_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_OB", total_CivilWorks_OB);
                    total_MajorRepairs_OB = total_MajorRepairs_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_OB", total_MajorRepairs_OB);
                    total_MinorRepairs_OB = total_MinorRepairs_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_OB", total_MinorRepairs_OB);
                    total_AnnualOtherRecurringGrants_OB = total_AnnualOtherRecurringGrants_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_OB", total_AnnualOtherRecurringGrants_OB);
                    total_Toilets_OB = total_Toilets_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));;
                    map.put("total_Toilets_OB", total_Toilets_OB);
                    total_SelfDefenseTrainingGrants_OB = total_SelfDefenseTrainingGrants_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_OB", total_SelfDefenseTrainingGrants_OB);
                    total_Interest_OB = total_Interest_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_OB", total_Interest_OB);
                    total_total_total_OB = total_total_total_OB.add(total_OB);
                    map.put("total_total_total_OB", total_total_total_OB);
                    
                    BigDecimal CivilWorks_EOB = null;
                    BigDecimal MajorRepairs_EOB = null;
                    BigDecimal MinorRepairs_EOB = null;
                    BigDecimal AnnualOtherRecurringGrants_EOB = null;
                    BigDecimal Toilets_EOB = null;
                    BigDecimal SelfDefenseETrainingGrants_EOB = null;
                    BigDecimal Interest_EOB = null;

                    if (rs.getString(40) == null) {
                        map.put("CivilWorks_EOB", "0.00");
                    } else {
                        CivilWorks_EOB = new BigDecimal(rs.getDouble(40));
                        map.put("CivilWorks_EOB", CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(41) == null) {
                        map.put("MajorRepairs_EOB", "0.00");
                    } else {
                        MajorRepairs_EOB = new BigDecimal(rs.getDouble(41));
                        map.put("MajorRepairs_EOB", MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(42) == null) {
                        map.put("MinorRepairs_EOB", "0.00");
                    } else {
                        MinorRepairs_EOB = new BigDecimal(rs.getDouble(42));
                        map.put("MinorRepairs_EOB", MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(43) == null) {
                        map.put("AnnualOtherRecurringGrants_EOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_EOB = new BigDecimal(rs.getDouble(43));
                        map.put("AnnualOtherRecurringGrants_EOB", AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(44) == null) {
                        map.put("Toilets_EOB", "0.00");
                    } else {
                        Toilets_EOB = new BigDecimal(rs.getDouble(44));
                        map.put("Toilets_EOB", Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(45) == null) {
                        map.put("SelfDefenseETrainingGrants_EOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_EOB = new BigDecimal(rs.getDouble(45));
                        map.put("SelfDefenseETrainingGrants_EOB", SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(46) == null) {
                        map.put("Interest_EOB", "0.00");
                    } else {
                        Interest_EOB = new BigDecimal(rs.getDouble(46));
                        map.put("Interest_EOB", Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    
                    BigDecimal total_EOB = new BigDecimal(0);
                    total_EOB = total_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_EOB", total_EOB);
                    
                    
                    total_CivilWorks_EOB = total_CivilWorks_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_EOB", total_CivilWorks_EOB);
                    total_MajorRepairs_EOB = total_MajorRepairs_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_EOB", total_MajorRepairs_EOB);
                    total_MinorRepairs_EOB = total_MinorRepairs_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_EOB", total_MinorRepairs_EOB);
                    total_AnnualOtherRecurringGrants_EOB = total_AnnualOtherRecurringGrants_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_EOB", total_AnnualOtherRecurringGrants_EOB);
                    total_Toilets_EOB = total_Toilets_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_EOB", total_Toilets_EOB);
                    total_SelfDefenseTrainingGrants_EOB = total_SelfDefenseTrainingGrants_EOB .add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_EOB", total_SelfDefenseTrainingGrants_EOB);
                    total_Interest_EOB = total_Interest_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_EOB", total_Interest_EOB);
                    total_total_total_EOB = total_total_total_EOB.add(total_EOB);
                    map.put("total_total_total_EOB", total_total_total_EOB);
                    
                   
                    
                    BigDecimal CivilWorks_UBOB = null;
                    BigDecimal MajorRepairs_UBOB = null;
                    BigDecimal MinorRepairs_UBOB = null;
                    BigDecimal AnnualOtherRecurringGrants_UBOB = null;
                    BigDecimal Toilets_UBOB = null;
                    BigDecimal SelfDefenseETrainingGrants_UBOB = null;
                    BigDecimal Interest_UBOB = null;
                    BigDecimal Released = null;

                    if (rs.getString(47) == null) {
                        map.put("CivilWorks_UBOB", "0.00");
                    } else {
                        CivilWorks_UBOB = new BigDecimal(rs.getDouble(47));
                        map.put("CivilWorks_UBOB", CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(48) == null) {
                        map.put("MajorRepairs_UBOB", "0.00");
                    } else {
                        MajorRepairs_UBOB = new BigDecimal(rs.getDouble(48));
                        map.put("MajorRepairs_UBOB", MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(49) == null) {
                        map.put("MinorRepairs_UBOB", "0.00");
                    } else {
                        MinorRepairs_UBOB = new BigDecimal(rs.getDouble(49));
                        map.put("MinorRepairs_UBOB", MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(50) == null) {
                        map.put("AnnualOtherRecurringGrants_UBOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_UBOB = new BigDecimal(rs.getDouble(50));
                        map.put("AnnualOtherRecurringGrants_UBOB", AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(51) == null) {
                        map.put("Toilets_UBOB", "0.00");
                    } else {
                        Toilets_UBOB = new BigDecimal(rs.getDouble(51));
                        map.put("Toilets_UBOB", Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(52) == null) {
                        map.put("SelfDefenseETrainingGrants_UBOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_UBOB = new BigDecimal(rs.getDouble(52));
                        map.put("SelfDefenseETrainingGrants_UBOB", SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(53) == null) {
                        map.put("Interest_UBOB", "0.00");
                    } else {
                        Interest_UBOB = new BigDecimal(rs.getDouble(53));
                        map.put("Interest_UBOB", Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(54) == null) {
                        map.put("Released", "0.00");
                    } else {
                        Released = new BigDecimal(rs.getDouble(54));
                        map.put("Released", Released.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                   
                   
                    ubob_released_total = ubob_released_total.add(Released.setScale(2, RoundingMode.HALF_UP));
                    map.put("ubob_released_total", ubob_released_total);
                    
                    BigDecimal  total_UEOB = new BigDecimal(0);
                    total_UEOB = total_UEOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_UEOB", total_UEOB);
                    
                    total_CivilWorks_UBOB = total_CivilWorks_UBOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_UBOB", total_CivilWorks_UBOB);
                    total_MajorRepairs_UBOB = total_MajorRepairs_UBOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_UBOB", total_MajorRepairs_UBOB);
                    total_MinorRepairs_UBOB = total_MinorRepairs_UBOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_UBOB", total_MinorRepairs_UBOB);
                    total_AnnualOtherRecurringGrants_UBOB = total_AnnualOtherRecurringGrants_UBOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_UBOB", total_AnnualOtherRecurringGrants_UBOB);
                    total_Toilets_UBOB = total_Toilets_UBOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_UBOB", total_Toilets_UBOB);
                    total_SelfDefenseTrainingGrants_UBOB = total_SelfDefenseTrainingGrants_UBOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_UBOB", total_SelfDefenseTrainingGrants_UBOB);
                    total_Interest_UBOB = total_Interest_UBOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_UBOB", total_Interest_UBOB);
                    total_total_total_UBOB = total_total_total_UBOB.add(total_UEOB);
                    map.put("total_total_total_UBOB", total_total_total_UBOB);
                    
                    BigDecimal Earned_Interest=null;
                    BigDecimal Expenditure_Interest=null;
                    BigDecimal Balance_Interest=null;
                    BigDecimal RemmitanceAmount_ExpenditureIncurred=null;
                    
                    
                    if (rs.getString(55) == null) {
                        Earned_Interest = new BigDecimal("0.00");
                        map.put("Earned_Interest", "0.00");
                    } else {
                        Earned_Interest = new BigDecimal(rs.getDouble(55));
                        map.put("Earned_Interest", Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(56) == null) {
                        Expenditure_Interest = new BigDecimal("0.00");
                        map.put("Expenditure_Interest", "0.00");
                    } else {
                        Expenditure_Interest = new BigDecimal(rs.getDouble(56));
                        map.put("Expenditure_Interest", Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(57) == null) {
                        Balance_Interest = new BigDecimal("0.00");
                        map.put("Balance_Interest", "0.00");
                    } else {
                        Balance_Interest = new BigDecimal(rs.getDouble(57));
                        map.put("Balance_Interest", Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(58) == null) {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal("0.00");
                        map.put("RemmitanceAmount_ExpenditureIncurred", "0.00");
                    } else {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal(rs.getDouble(58));
                        map.put("RemmitanceAmount_ExpenditureIncurred", RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    total_Earned_Interest = total_Earned_Interest.add(Earned_Interest);
                    total_Earned_Interest = total_Earned_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Earned_Interest", total_Earned_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.add(Expenditure_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Expenditure_Interest", total_Expenditure_Interest);
                    total_Balance_Interest = total_Balance_Interest.add(Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Balance_Interest", total_Balance_Interest);
                    total_RemmitanceAmount_ExpenditureIncurred = total_RemmitanceAmount_ExpenditureIncurred.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_RemmitanceAmount_ExpenditureIncurred", total_RemmitanceAmount_ExpenditureIncurred);
                    
                    BigDecimal  totalRealse_Realses = new BigDecimal(0);
                    //totalRealse_Realses = totalRealse_Realses.add(Released.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_Realses", totalRealse_Realses);
                    
                    BigDecimal  totalRealse_total = new BigDecimal(0);
                    totalRealse_total = totalRealse_total.add(total_EOB);
                    totalRealse_total = totalRealse_total.add(Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_total", totalRealse_total);
                    
                    total_total_OB = total_total_OB.add(total_OB);
                    map.put("total_total_OB", total_total_OB);
                    total_totalRealse_Realses = total_totalRealse_Realses.add(totalRealse_Realses);
                    map.put("total_totalRealse_Realses", total_totalRealse_Realses);
                    total_Earned_Interest_total = total_Earned_Interest_total.add(Earned_Interest);
                    map.put("total_Earned_Interest_total",total_Earned_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_total_total = totalRealse_total_total.add(totalRealse_total);
                    map.put("totalRealse_total_total", totalRealse_total_total);
                    
                    BigDecimal  Grand_Total_Expenditure_Incurred_on_Releases = new BigDecimal(0);
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));

                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases", Grand_Total_Expenditure_Incurred_on_Releases);
                    
                    BigDecimal  Grand_Total_Total_Expenditure = new BigDecimal(0);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(total_EOB);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Total_Expenditure", Grand_Total_Total_Expenditure);
                    
                    BigDecimal close_balnce_ub_ob = total_OB.subtract(total_UEOB);
                    map.put("close_balnce_ub_ob", close_balnce_ub_ob);
                    BigDecimal close_balnce_ub_rb = totalRealse_Realses.subtract(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("close_balnce_ub_rb", close_balnce_ub_rb);
                    
                    BigDecimal close_balnce_ub_ineterest = (Earned_Interest.setScale(2, RoundingMode.HALF_UP)).subtract(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_ineterest", close_balnce_ub_ineterest);
                    
                    BigDecimal  close_balnce_ub_total = new BigDecimal(0);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_rb);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_ineterest);
                    close_balnce_ub_total = close_balnce_ub_total.subtract(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_total", close_balnce_ub_total);
                    
                    Grand_Total_Expenditure_Incurred_on_Opening_balances = Grand_Total_Expenditure_Incurred_on_Opening_balances.add(total_EOB);
                    map.put("Grand_Total_Expenditure_Incurred_on_Opening_balances", Grand_Total_Expenditure_Incurred_on_Opening_balances);
                    Grand_Total_Expenditure_Incurred_on_Releases_total = Grand_Total_Expenditure_Incurred_on_Releases_total.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases_total", Grand_Total_Expenditure_Incurred_on_Releases_total);
                    total_Expenditure_Interest_total = total_Expenditure_Interest_total.add(Expenditure_Interest);
                    map.put("total_Expenditure_Interest_total", total_Expenditure_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    total_RemmitanceAmount_ExpenditureIncurred_total = total_RemmitanceAmount_ExpenditureIncurred_total.add(total_RemmitanceAmount_ExpenditureIncurred);
                    map.put("total_RemmitanceAmount_ExpenditureIncurred_total", total_RemmitanceAmount_ExpenditureIncurred_total);
                    Grand_Total_Total_Expenditure_total = Grand_Total_Total_Expenditure_total.add(Grand_Total_Total_Expenditure);
                    map.put("Grand_Total_Total_Expenditure_total", Grand_Total_Total_Expenditure_total);

                    
                    close_balnce_ub_ob_total = close_balnce_ub_ob_total.add(close_balnce_ub_ob);
                    map.put("close_balnce_ub_ob_total", close_balnce_ub_ob_total);
                    close_balnce_ub_rb_total = close_balnce_ub_rb_total.add(close_balnce_ub_rb);
                    map.put("close_balnce_ub_rb_total", close_balnce_ub_rb_total);
                    close_balnce_ub_ineterest_total = close_balnce_ub_ineterest_total.add(close_balnce_ub_ineterest);
                    map.put("close_balnce_ub_ineterest_total", close_balnce_ub_ineterest_total);
                    close_balnce_ub_total_total = close_balnce_ub_total_total.add(close_balnce_ub_total);
                    map.put("close_balnce_ub_total_total", close_balnce_ub_total_total);
                    
                    distList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return distList;
    }
    
    public ArrayList getMandalList(String id, String year, String management) {
        ArrayList mandallist = new ArrayList();
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();

        BigDecimal civilNonRecTotal = new BigDecimal(0);
        BigDecimal majorNonRecTotal = new BigDecimal(0);
        BigDecimal minorNonRecTotal = new BigDecimal(0);
        BigDecimal toiletNonRecTotal = new BigDecimal(0);
        BigDecimal totNonRecTotal = new BigDecimal(0);

        BigDecimal expcivilNonRecTotal = new BigDecimal(0);
        BigDecimal expmajorNonRecTotal = new BigDecimal(0);
        BigDecimal expminorNonRecTotal = new BigDecimal(0);
        BigDecimal exptoiletNonRecTotal = new BigDecimal(0);
        BigDecimal exptotNonRecTotal = new BigDecimal(0);

        BigDecimal totcivilNonRecTotal = new BigDecimal(0);
        BigDecimal totmajorNonRecTotal = new BigDecimal(0);
        BigDecimal totminorNonRecTotal = new BigDecimal(0);
        BigDecimal tottoiletNonRecTotal = new BigDecimal(0);
        BigDecimal tottotNonRecTotal = new BigDecimal(0);

        BigDecimal waterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal booksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal minorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal sanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal needAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal provisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal totAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal expwaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal exptotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal twaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tNeedAnnualGrantsTotalBalance = new BigDecimal(0);
        BigDecimal tneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal ttotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal excurTriprecurringTotal = new BigDecimal(0);
        BigDecimal selfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal otherrecurringTotal = new BigDecimal(0);
        BigDecimal totrecurringTotal = new BigDecimal(0);

        BigDecimal excexcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal excselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal excotherrecurringTotal = new BigDecimal(0);
        BigDecimal exctotrecurringTotal = new BigDecimal(0);

        BigDecimal texcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal tselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal totherrecurringTotal = new BigDecimal(0);
        BigDecimal ttotrecurringTotal = new BigDecimal(0);

        BigDecimal relfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal rellabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal reltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal exfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal tfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal total_CivilWorks_OB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_OB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_OB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_OB = new BigDecimal(0);
        BigDecimal total_Toilets_OB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_OB = new BigDecimal(0);
        BigDecimal total_Interest_OB = new BigDecimal(0);
        BigDecimal total_total_total_OB = new BigDecimal(0);

        BigDecimal total_CivilWorks_EOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_EOB = new BigDecimal(0);
        BigDecimal total_Toilets_EOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_EOB = new BigDecimal(0);
        BigDecimal total_Interest_EOB = new BigDecimal(0);
        BigDecimal total_total_total_EOB = new BigDecimal(0);

        BigDecimal total_CivilWorks_UBOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Toilets_UBOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Interest_UBOB = new BigDecimal(0);
        BigDecimal total_total_total_UBOB = new BigDecimal(0);
        BigDecimal ubob_released_total = new BigDecimal(0);

        BigDecimal total_Earned_Interest = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest = new BigDecimal(0);
        BigDecimal total_Balance_Interest = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred = new BigDecimal(0);

        BigDecimal total_total_OB = new BigDecimal(0);
        BigDecimal total_totalRealse_Realses = new BigDecimal(0);
        BigDecimal total_Earned_Interest_total = new BigDecimal(0);
        BigDecimal totalRealse_total_total = new BigDecimal(0);

        BigDecimal Grand_Total_Expenditure_Incurred_on_Opening_balances = new BigDecimal(0);
        BigDecimal Grand_Total_Expenditure_Incurred_on_Releases_total = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest_total = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred_total = new BigDecimal(0);
        BigDecimal Grand_Total_Total_Expenditure_total = new BigDecimal(0);

        BigDecimal close_balnce_ub_ob_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_rb_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_ineterest_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_total_total = new BigDecimal(0);
        BigDecimal furniture_bal_sum = new BigDecimal(0);
        BigDecimal lab_equipment_sum = new BigDecimal(0);
        long totalSchoolsTotal = 0;


        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report](?,?)}");
//            cstmt.setString(1, id);
//            cstmt.setString(2, year);

            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report_mandal](?,?,?)}");
            cstmt.setString(1, id);
            cstmt.setString(2, year);
            cstmt.setString(3, management);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    BigDecimal civilReleased1 = null;
                    BigDecimal majorReleased1 = null;
                    BigDecimal minorReleased1 = null;
                    BigDecimal toiletsReleased1 = null;
                    BigDecimal civilExp1 = null;
                    BigDecimal majorExp1 = null;
                    BigDecimal minorExp1 = null;
                    BigDecimal toiletsExp1 = null;
                    BigDecimal waterReleased1 = null;
                    BigDecimal booksReleased1 = null;
                    BigDecimal minorReleasedAnnual1 = null;
                    BigDecimal sanitationReleased1 = null;
                    BigDecimal needReleased1 = null;
                    BigDecimal provisionalReleased1 = null;
                    BigDecimal waterExp1 = null;
                    BigDecimal booksExp1 = null;
                    BigDecimal minorExpAnnual1 = null;
                    BigDecimal sanitationExp1 = null;
                    BigDecimal needExp1 = null;
                    BigDecimal provisionalExp1 = null;
                    BigDecimal excurtionReleased1 = null;
                    BigDecimal selfdefReleased1 = null;
                    BigDecimal otherRecuGrantReleased1 = null;
                    BigDecimal excurtionExp1 = null;
                    BigDecimal selfdefExp1 = null;
                    BigDecimal otherRecuGrantExp1 = null;
                    BigDecimal furnitureReleased1 = null;
                    BigDecimal labEquipReleased1 = null;
                    BigDecimal furnitureExp1 = null;
                    BigDecimal labEquipExp1 = null;

                    map = new HashMap();
                    map.put("mandalcode", rs.getString(1));
                    map.put("mandalname", rs.getString(2));
                    if (rs.getString(3) == null) {
                        map.put("civilReleased", "0.00");
                    } else {
                        civilReleased1 = new BigDecimal(rs.getDouble(3));
                        map.put("civilReleased", civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(4) == null) {
                        map.put("majorReleased", "0.00");
                    } else {
                        majorReleased1 = new BigDecimal(rs.getDouble(4));
                        map.put("majorReleased", majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(5) == null) {
                        map.put("minorReleased", "0.00");
                    } else {
                        minorReleased1 = new BigDecimal(rs.getDouble(5));
                        map.put("minorReleased", minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(6) == null) {
                        map.put("toiletsReleased", "0.00");
                    } else {
                        toiletsReleased1 = new BigDecimal(rs.getDouble(6));
                        map.put("toiletsReleased", toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(7) == null) {
                        map.put("civilExp", "0.00");
                    } else {
                        civilExp1 = new BigDecimal(rs.getDouble(7));
                        map.put("civilExp", civilExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(8) == null) {
                        map.put("majorExp", "0.00");
                    } else {
                        majorExp1 = new BigDecimal(rs.getDouble(8));
                        map.put("majorExp", majorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(9) == null) {
                        map.put("minorExp", "0.00");
                    } else {
                        minorExp1 = new BigDecimal(rs.getDouble(9));
                        map.put("minorExp", minorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(10) == null) {
                        map.put("toiletsExp", "0.00");
                    } else {
                        toiletsExp1 = new BigDecimal(rs.getDouble(10));
                        map.put("toiletsExp", toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(11) == null) {
                        map.put("waterReleased", "0.00");
                    } else {
                        waterReleased1 = new BigDecimal(rs.getDouble(11));
                        map.put("waterReleased", waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(12) == null) {
                        map.put("booksReleased", "0.00");
                    } else {
                        booksReleased1 = new BigDecimal(rs.getDouble(12));
                        map.put("booksReleased", booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(13) == null) {
                        map.put("minorReleasedAnnual", "0.00");
                    } else {
                        minorReleasedAnnual1 = new BigDecimal(rs.getDouble(13));
                        map.put("minorReleasedAnnual", minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(14) == null) {
                        map.put("sanitationReleased", "0.00");
                    } else {
                        sanitationReleased1 = new BigDecimal(rs.getDouble(14));
                        map.put("sanitationReleased", sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(15) == null) {
                        map.put("needReleased", "0.00");
                    } else {
                        needReleased1 = new BigDecimal(rs.getDouble(15));
                        map.put("needReleased", needReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(16) == null) {
                        map.put("provisionalReleased", "0.00");
                    } else {
                        provisionalReleased1 = new BigDecimal(rs.getDouble(16));
                        map.put("provisionalReleased", provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(17) == null) {
                        map.put("waterExp", "0.00");
                    } else {
                        waterExp1 = new BigDecimal(rs.getDouble(17));
                        map.put("waterExp", waterExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(18) == null) {
                        map.put("booksExp", "0.00");
                    } else {
                        booksExp1 = new BigDecimal(rs.getDouble(18));
                        map.put("booksExp", booksExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(19) == null) {
                        map.put("minorExpAnnual", "0.00");
                    } else {
                        minorExpAnnual1 = new BigDecimal(rs.getDouble(19));
                        map.put("minorExpAnnual", minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(20) == null) {
                        map.put("sanitationExp", "0.00");
                    } else {
                        sanitationExp1 = new BigDecimal(rs.getDouble(20));
                        map.put("sanitationExp", sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(21) == null) {
                        map.put("needExp", "0.00");
                    } else {
                        needExp1 = new BigDecimal(rs.getDouble(21));
                        map.put("needExp", needExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(22) == null) {
                        map.put("provisionalExp", "0.00");
                    } else {
                        provisionalExp1 = new BigDecimal(rs.getDouble(22));
                        map.put("provisionalExp", provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(23) == null) {
                        map.put("excurtionReleased", "0.00");
                    } else {
                        excurtionReleased1 = new BigDecimal(rs.getDouble(23));
                        map.put("excurtionReleased", excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(24) == null) {
                        map.put("selfdefReleased", "0.00");
                    } else {
                        selfdefReleased1 = new BigDecimal(rs.getDouble(24));
                        map.put("selfdefReleased", selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(25) == null) {
                        map.put("otherRecuGrantReleased", "0.00");
                    } else {
                        otherRecuGrantReleased1 = new BigDecimal(rs.getDouble(25));
                        map.put("otherRecuGrantReleased", otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(26) == null) {
                        map.put("excurtionExp", "0.00");
                    } else {
                        excurtionExp1 = new BigDecimal(rs.getDouble(26));
                        map.put("excurtionExp", excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(27) == null) {
                        selfdefExp1 = new BigDecimal("0.00");
                        map.put("selfdefExp", "0.00");
                    } else {
                        selfdefExp1 = new BigDecimal(rs.getDouble(27));
                        map.put("selfdefExp", selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(28) == null) {
                        map.put("otherRecuGrantExp", "0.00");
                    } else {
                        otherRecuGrantExp1 = new BigDecimal(rs.getDouble(28));
                        map.put("otherRecuGrantExp", otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    if (rs.getString(29) == null) {
                        map.put("furnitureReleased", "0.00");
                    } else {
                        furnitureReleased1 = new BigDecimal(rs.getDouble(29));
                        map.put("furnitureReleased", furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(30) == null) {
                        map.put("labEquipReleased", "0.00");
                    } else {
                        labEquipReleased1 = new BigDecimal(rs.getDouble(30));
                        map.put("labEquipReleased", labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(31) == null) {
                        map.put("furnitureExp", "0.00");
                    } else {
                        furnitureExp1 = new BigDecimal(rs.getDouble(31));
                        map.put("furnitureExp", furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(32) == null) {
                        map.put("labEquipExp", "0.00");
                    } else {
                        labEquipExp1 = new BigDecimal(rs.getDouble(32));
                        map.put("labEquipExp", labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    
                    
                    BigDecimal acivirel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal aciviexp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal civibaaltot = acivirel.subtract(aciviexp);
                    map.put("civilBal", civibaaltot);
                    
                    
                    
                    BigDecimal majrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majexp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majtot = majrel.subtract(majexp);
                    map.put("majorBal", majtot);
                    BigDecimal minrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal minexp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mintot = minrel.subtract(minexp);
                    map.put("minorBal", mintot);
                    BigDecimal toirel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toiexp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toitot = toirel.subtract(toiexp);
                    map.put("toiletsBal", toitot);
                    //totals
                    BigDecimal tocivicnorecrel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecrel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectot = new BigDecimal(0);
                    tonorectot = tonorectot.add(tocivicnorecrel);
                    tonorectot = tonorectot.add(tomajnonrecrel);
                    tonorectot = tonorectot.add(tominnonrecrel);
                    tonorectot = tonorectot.add(tototinonrecrel);
                    map.put("totalNonRecurReleased", tonorectot);
                   
                    
                    BigDecimal tocivicnorecExp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecExp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecExp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecExp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectotExp = new BigDecimal(0);
                    tonorectotExp = tonorectotExp.add(tocivicnorecExp);
                    tonorectotExp = tonorectotExp.add(tomajnonrecExp);
                    tonorectotExp = tonorectotExp.add(tominnonrecExp);
                    tonorectotExp = tonorectotExp.add(tototinonrecExp);
                    map.put("totalNonRecurExp", tonorectotExp);
                    
                    
                    
                    BigDecimal totciv = civibaaltot;
                    BigDecimal totmaj = majtot;
                    BigDecimal totmin = mintot;
                    BigDecimal tottoil = toitot;
                    BigDecimal totmajexpbal = new BigDecimal(0);
                    totmajexpbal = totmajexpbal.add(totciv);
                    totmajexpbal = totmajexpbal.add(totmaj);
                    totmajexpbal = totmajexpbal.add(totmin);
                    totmajexpbal = totmajexpbal.add(tottoil);
                    map.put("totalNonRecurBal", totmajexpbal);
                    
                    
                    //endtotal
                    BigDecimal warel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal waexp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal watot = warel.subtract(waexp);
                    map.put("waterBal", watot);
                    BigDecimal borel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal boxp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal botot = borel.subtract(boxp);
                    map.put("booksBal", botot);
                    BigDecimal mianrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mianxp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal miantot = mianrel.subtract(mianxp);
                    map.put("minorBalAnnual", miantot);
                    BigDecimal sarel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal saexp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal satot = sarel.subtract(saexp);
                    map.put("sanitationBal", satot);
                    BigDecimal needrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needexp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needtot = needrel.subtract(needexp);
                    map.put("needBal", needtot);
                    BigDecimal pvrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvexp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvtot = pvrel.subtract(pvexp);
                    map.put("provisionalBal", pvtot);
                    //totals
                    BigDecimal towatcannualgrantrel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantrel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantrel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgranttot = new BigDecimal(0);
                    toannualgranttot = toannualgranttot.add(towatcannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tobookannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tominannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tosaniannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toneedannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toprovannualgrantrel);
                    map.put("totalGrantReleased", toannualgranttot);
                    BigDecimal towatcannualgrantExp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantExp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantExp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantExp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantExp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantExp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgrantExptot = new BigDecimal(0);
                    toannualgrantExptot = toannualgrantExptot.add(towatcannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tobookannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tominannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tosaniannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toneedannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toprovannualgrantExp);
                    map.put("totalGrantExp", toannualgrantExptot);
                    BigDecimal towatcannualgrantt = watot;
                    BigDecimal tobookannualgrantt = botot;
                    BigDecimal tominannualgrantt = miantot;
                    BigDecimal tosaniannualgrantt = satot;
                    BigDecimal toneedannualgrantt = needtot;
                    BigDecimal toprovannualgrantt = pvtot;
                    BigDecimal toannualgranttott = new BigDecimal(0);
                    toannualgranttott = toannualgranttott.add(towatcannualgrantt);
                    toannualgranttott = toannualgranttott.add(tobookannualgrantt);
                    toannualgranttott = toannualgranttott.add(tominannualgrantt);
                    toannualgranttott = toannualgranttott.add(tosaniannualgrantt);
                    toannualgranttott = toannualgranttott.add(toneedannualgrantt);
                    toannualgranttott = toannualgranttott.add(toprovannualgrantt);
                    map.put("totalGrantBal", toannualgranttott);
                    //endtotal
                    BigDecimal exrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal exexp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal extot = exrel.subtract(exexp);
                    map.put("excurtionBal", extot);
                    BigDecimal serel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal seexp = selfdefExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal setot = serel.subtract(seexp);
                    map.put("selfdefBal", setot);
                    BigDecimal otrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otexp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ottot = otrel.subtract(otexp);
                    map.put("otherRecuGrantBal", ottot);
                    //totals          
                    BigDecimal excurrecurrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurrel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutot = new BigDecimal(0);
                    recutot = recutot.add(excurrecurrel);
                    recutot = recutot.add(selfrecurrel);
                    recutot = recutot.add(otherrecurrel);
                    map.put("totalRecurReleased", recutot);
                    BigDecimal excurrecurExp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurExp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurExp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutotExp = new BigDecimal(0);
                    recutotExp = recutotExp.add(excurrecurExp);
                    recutotExp = recutotExp.add(selfrecurExp);
                    recutotExp = recutotExp.add(otherrecurExp);
                    map.put("totalRecurExp", recutotExp);
                    BigDecimal excurrecurt = extot;
                    BigDecimal selfrecurt = setot;
                    BigDecimal otherrecurt = ottot;
                    BigDecimal recutott = new BigDecimal(0);
                    recutott = recutott.add(excurrecurt);
                    recutott = recutott.add(selfrecurt);
                    recutott = recutott.add(otherrecurt);
                    map.put("totalRecurBal", recutott);
                    //endtotal
                    BigDecimal frel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal fexp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ftot = frel.subtract(fexp);
                    map.put("furnitureBal", ftot);
                    BigDecimal labrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labexp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labtot = labrel.subtract(labexp);
                    map.put("labEquipBal", labtot);
                    //totals    
                    BigDecimal furnfurlabrel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelreltot = furnfurlabrel.add(labfurlabrel);
                    map.put("totallabEquipReleased", furlabrelreltot);
                    BigDecimal furnfurlabExp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabExp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelExptot = furnfurlabExp.add(labfurlabExp);
                    map.put("totallabEquipExp", furlabrelExptot);
                    BigDecimal furnfurlabt = ftot;
                    BigDecimal labfurlabt = labtot;
                    BigDecimal furlabrelttot = furnfurlabt.add(labfurlabt);
                    map.put("totallabEquipBal", furlabrelttot);
                    furniture_bal_sum = furniture_bal_sum.add(furnfurlabt);
                    lab_equipment_sum = lab_equipment_sum.add(labfurlabt);
                    map.put("furniture_bal_sum", furniture_bal_sum);
                    map.put("lab_equipment_sum", lab_equipment_sum);
                    map.put("totalSchools", rs.getString(59));

                    //endtotal

                    /////////Final totals in tfoot
                    civilNonRecTotal = civilNonRecTotal.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    majorNonRecTotal = majorNonRecTotal.add(majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorNonRecTotal = minorNonRecTotal.add(minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    toiletNonRecTotal = toiletNonRecTotal.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totNonRecTotal = totNonRecTotal.add(tonorectot);

                    expcivilNonRecTotal = expcivilNonRecTotal.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    expmajorNonRecTotal = expmajorNonRecTotal.add(majorExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorNonRecTotal = expminorNonRecTotal.add(minorExp1.setScale(2, RoundingMode.HALF_UP));
                    exptoiletNonRecTotal = exptoiletNonRecTotal.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotNonRecTotal = exptotNonRecTotal.add(tonorectotExp);
                    totcivilNonRecTotal = totcivilNonRecTotal.add(civibaaltot);
                    totmajorNonRecTotal = totmajorNonRecTotal.add(majtot);
                    totminorNonRecTotal = totminorNonRecTotal.add(mintot);
                    tottoiletNonRecTotal = tottoiletNonRecTotal.add(toitot);
                    tottotNonRecTotal = tottotNonRecTotal.add(totmajexpbal);

                    waterAnnualGrantsTotal = waterAnnualGrantsTotal.add(waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    booksAnnualGrantsTotal = booksAnnualGrantsTotal.add(booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorAnnualGrantsTotal = minorAnnualGrantsTotal.add(minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    sanitationAnnualGrantsTotal = sanitationAnnualGrantsTotal.add(sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    needAnnualGrantsTotal = needAnnualGrantsTotal.add(needReleased1.setScale(2, RoundingMode.HALF_UP));
                    provisiAnnualGrantsTotal = provisiAnnualGrantsTotal.add(provisionalReleased1.setScale(2, RoundingMode.HALF_UP));

                    totAnnualGrantsTotal = totAnnualGrantsTotal.add(toannualgranttot);

                    expwaterAnnualGrantsTotal = expwaterAnnualGrantsTotal.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    expbooksAnnualGrantsTotal = expbooksAnnualGrantsTotal.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorAnnualGrantsTotal = expminorAnnualGrantsTotal.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    expsanitationAnnualGrantsTotal = expsanitationAnnualGrantsTotal.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    expneedAnnualGrantsTotal = expneedAnnualGrantsTotal.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    expprovisiAnnualGrantsTotal = expprovisiAnnualGrantsTotal.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotAnnualGrantsTotal = exptotAnnualGrantsTotal.add(toannualgrantExptot);
                    twaterAnnualGrantsTotal = twaterAnnualGrantsTotal.add(watot);
                    tbooksAnnualGrantsTotal = tbooksAnnualGrantsTotal.add(botot);
                    tminorAnnualGrantsTotal = tminorAnnualGrantsTotal.add(miantot);
                    tsanitationAnnualGrantsTotal = tsanitationAnnualGrantsTotal.add(satot);
                    tNeedAnnualGrantsTotalBalance = tNeedAnnualGrantsTotalBalance.add(needtot);
                    tneedAnnualGrantsTotal = tneedAnnualGrantsTotal.add(needexp);
                    tprovisiAnnualGrantsTotal = tprovisiAnnualGrantsTotal.add(pvtot);
                    ttotAnnualGrantsTotal = ttotAnnualGrantsTotal.add(toannualgranttott);

                    excurTriprecurringTotal = excurTriprecurringTotal.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    selfdefencerecurringTotal = selfdefencerecurringTotal.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    otherrecurringTotal = otherrecurringTotal.add(otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    totrecurringTotal = totrecurringTotal.add(recutot);

                    excexcurTriprecurringTotal = excexcurTriprecurringTotal.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    excselfdefencerecurringTotal = excselfdefencerecurringTotal.add(selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    excotherrecurringTotal = excotherrecurringTotal.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));

                    exctotrecurringTotal = exctotrecurringTotal.add(recutotExp);
                    texcurTriprecurringTotal = texcurTriprecurringTotal.add(extot);
                    tselfdefencerecurringTotal = tselfdefencerecurringTotal.add(setot);
                    totherrecurringTotal = totherrecurringTotal.add(ottot);
                    ttotrecurringTotal = ttotrecurringTotal.add(recutott);

                    relfurFurniturLabEquipmentTotal = relfurFurniturLabEquipmentTotal.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    rellabFurniturLabEquipmentTotal = rellabFurniturLabEquipmentTotal.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    reltotFurniturLabEquipmentTotal = reltotFurniturLabEquipmentTotal.add(furlabrelreltot);

                    exfurFurniturLabEquipmentTotal = exfurFurniturLabEquipmentTotal.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    exlabFurniturLabEquipmentTotal = exlabFurniturLabEquipmentTotal.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));

                    exltotFurniturLabEquipmentTotal = exltotFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tfurFurniturLabEquipmentTotal = tfurFurniturLabEquipmentTotal.add(furlabrelreltot);
                    tlabFurniturLabEquipmentTotal = tlabFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tltotFurniturLabEquipmentTotal = tltotFurniturLabEquipmentTotal.add(furlabrelttot);


                    totalSchoolsTotal = totalSchoolsTotal + rs.getLong(59);
                    map.put("totalSchoolsTotal", totalSchoolsTotal);

                    //annual grants

                    map.put("civilNonRecTotal", civilNonRecTotal);
                    map.put("majorNonRecTotal", majorNonRecTotal);
                    map.put("minorNonRecTotal", minorNonRecTotal);
                    map.put("toiletNonRecTotal", toiletNonRecTotal);
                    map.put("totNonRecTotal", totNonRecTotal);

                    map.put("expcivilNonRecTotal", expcivilNonRecTotal);
                    map.put("expmajorNonRecTotal", expmajorNonRecTotal);
                    map.put("expminorNonRecTotal", expminorNonRecTotal);
                    map.put("exptoiletNonRecTotal", exptoiletNonRecTotal);
                    map.put("exptotNonRecTotal", exptotNonRecTotal);

                    map.put("totcivilNonRecTotal", totcivilNonRecTotal);
                    map.put("totmajorNonRecTotal", totmajorNonRecTotal);
                    map.put("totminorNonRecTotal", totminorNonRecTotal);
                    map.put("tottoiletNonRecTotal", tottoiletNonRecTotal);
                    map.put("tottotNonRecTotal", tottotNonRecTotal);

                    map.put("waterAnnualGrantsTotal", waterAnnualGrantsTotal);
                    map.put("booksAnnualGrantsTotal", booksAnnualGrantsTotal);
                    map.put("minorAnnualGrantsTotal", minorAnnualGrantsTotal);
                    map.put("sanitationAnnualGrantsTotal", sanitationAnnualGrantsTotal);
                    map.put("needAnnualGrantsTotal", needAnnualGrantsTotal);
                    map.put("provisiAnnualGrantsTotal", provisiAnnualGrantsTotal);
                    map.put("totAnnualGrantsTotal", totAnnualGrantsTotal);

                    map.put("expwaterAnnualGrantsTotal", expwaterAnnualGrantsTotal);
                    map.put("expbooksAnnualGrantsTotal", expbooksAnnualGrantsTotal);
                    map.put("expminorAnnualGrantsTotal", expminorAnnualGrantsTotal);
                    map.put("expsanitationAnnualGrantsTotal", expsanitationAnnualGrantsTotal);
                    map.put("expneedAnnualGrantsTotal", expneedAnnualGrantsTotal);
                    map.put("expprovisiAnnualGrantsTotal", expprovisiAnnualGrantsTotal);
                    map.put("exptotAnnualGrantsTotal", exptotAnnualGrantsTotal);

                    map.put("twaterAnnualGrantsTotal", twaterAnnualGrantsTotal);
                    map.put("tbooksAnnualGrantsTotal", tbooksAnnualGrantsTotal);
                    map.put("tminorAnnualGrantsTotal", tminorAnnualGrantsTotal);
                    map.put("tsanitationAnnualGrantsTotal", tsanitationAnnualGrantsTotal);
                    map.put("tNeedAnnualGrantsTotalBalance", tNeedAnnualGrantsTotalBalance);
                    map.put("tneedAnnualGrantsTotal", tneedAnnualGrantsTotal);
                    map.put("tprovisiAnnualGrantsTotal", tprovisiAnnualGrantsTotal);
                    map.put("ttotAnnualGrantsTotal", ttotAnnualGrantsTotal);

                    map.put("excurTriprecurringTotal", excurTriprecurringTotal);
                    map.put("selfdefencerecurringTotal", selfdefencerecurringTotal);
                    map.put("otherrecurringTotal", otherrecurringTotal);
                    map.put("totrecurringTotal", totrecurringTotal);

                    map.put("excexcurTriprecurringTotal", excexcurTriprecurringTotal);
                    map.put("excselfdefencerecurringTotal", excselfdefencerecurringTotal);
                    map.put("excotherrecurringTotal", excotherrecurringTotal);
                    map.put("exctotrecurringTotal", exctotrecurringTotal);

                    map.put("texcurTriprecurringTotal", texcurTriprecurringTotal);
                    map.put("tselfdefencerecurringTotal", tselfdefencerecurringTotal);
                    map.put("totherrecurringTotal", totherrecurringTotal);
                    map.put("ttotrecurringTotal", ttotrecurringTotal);

                    map.put("relfurFurniturLabEquipmentTotal", relfurFurniturLabEquipmentTotal);
                    map.put("rellabFurniturLabEquipmentTotal", rellabFurniturLabEquipmentTotal);
                    map.put("reltotFurniturLabEquipmentTotal", reltotFurniturLabEquipmentTotal);

                    map.put("exfurFurniturLabEquipmentTotal", exfurFurniturLabEquipmentTotal);
                    map.put("exlabFurniturLabEquipmentTotal", exlabFurniturLabEquipmentTotal);
                    map.put("exltotFurniturLabEquipmentTotal", exltotFurniturLabEquipmentTotal);

                    map.put("tfurFurniturLabEquipmentTotal", tfurFurniturLabEquipmentTotal);
                    map.put("tlabFurniturLabEquipmentTotal", tlabFurniturLabEquipmentTotal);
                    map.put("tltotFurniturLabEquipmentTotal", tltotFurniturLabEquipmentTotal);

                    BigDecimal CivilWorks_OB = null;
                    BigDecimal MajorRepairs_OB = null;
                    BigDecimal MinorRepairs_OB = null;
                    BigDecimal AnnualOtherRecurringGrants_OB = null;
                    BigDecimal Toilets_OB = null;
                    BigDecimal SelfDefenseTrainingGrants_OB = null;
                    BigDecimal Interest_OB = null;

                    if (rs.getString(33) == null) {
                        map.put("CivilWorks_OB", "0.00");
                    } else {
                        CivilWorks_OB = new BigDecimal(rs.getDouble(33));
                        map.put("CivilWorks_OB", CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(34) == null) {
                        map.put("MajorRepairs_OB", "0.00");
                    } else {
                        MajorRepairs_OB = new BigDecimal(rs.getDouble(34));
                        map.put("MajorRepairs_OB", MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(35) == null) {
                        map.put("MinorRepairs_OB", "0.00");
                    } else {
                        MinorRepairs_OB = new BigDecimal(rs.getDouble(35));
                        map.put("MinorRepairs_OB", MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(36) == null) {
                        map.put("AnnualOtherRecurringGrants_OB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_OB = new BigDecimal(rs.getDouble(36));
                        map.put("AnnualOtherRecurringGrants_OB", AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(37) == null) {
                        map.put("Toilets_OB", "0.00");
                    } else {
                        Toilets_OB = new BigDecimal(rs.getDouble(37));
                        map.put("Toilets_OB", Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(38) == null) {
                        map.put("SelfDefenseTrainingGrants_OB", "0.00");
                    } else {
                        SelfDefenseTrainingGrants_OB = new BigDecimal(rs.getDouble(38));
                        map.put("SelfDefenseTrainingGrants_OB", SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(39) == null) {
                        map.put("Interest_OB", "0.00");
                    } else {
                        Interest_OB = new BigDecimal(rs.getDouble(39));
                        map.put("Interest_OB", Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    }

                    BigDecimal total_OB = new BigDecimal(0);
                    total_OB = total_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_OB", total_OB);

                    total_CivilWorks_OB = total_CivilWorks_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_OB", total_CivilWorks_OB);
                    total_MajorRepairs_OB = total_MajorRepairs_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_OB", total_MajorRepairs_OB);
                    total_MinorRepairs_OB = total_MinorRepairs_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_OB", total_MinorRepairs_OB);
                    total_AnnualOtherRecurringGrants_OB = total_AnnualOtherRecurringGrants_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_OB", total_AnnualOtherRecurringGrants_OB);
                    total_Toilets_OB = total_Toilets_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));;
                    map.put("total_Toilets_OB", total_Toilets_OB);
                    total_SelfDefenseTrainingGrants_OB = total_SelfDefenseTrainingGrants_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_OB", total_SelfDefenseTrainingGrants_OB);
                    total_Interest_OB = total_Interest_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_OB", total_Interest_OB);
                    total_total_total_OB = total_total_total_OB.add(total_OB);
                    map.put("total_total_total_OB", total_total_total_OB);
                    
                    BigDecimal CivilWorks_EOB = null;
                    BigDecimal MajorRepairs_EOB = null;
                    BigDecimal MinorRepairs_EOB = null;
                    BigDecimal AnnualOtherRecurringGrants_EOB = null;
                    BigDecimal Toilets_EOB = null;
                    BigDecimal SelfDefenseETrainingGrants_EOB = null;
                    BigDecimal Interest_EOB = null;

                    if (rs.getString(40) == null) {
                        map.put("CivilWorks_EOB", "0.00");
                    } else {
                        CivilWorks_EOB = new BigDecimal(rs.getDouble(40));
                        map.put("CivilWorks_EOB", CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(41) == null) {
                        map.put("MajorRepairs_EOB", "0.00");
                    } else {
                        MajorRepairs_EOB = new BigDecimal(rs.getDouble(41));
                        map.put("MajorRepairs_EOB", MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(42) == null) {
                        map.put("MinorRepairs_EOB", "0.00");
                    } else {
                        MinorRepairs_EOB = new BigDecimal(rs.getDouble(42));
                        map.put("MinorRepairs_EOB", MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(43) == null) {
                        map.put("AnnualOtherRecurringGrants_EOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_EOB = new BigDecimal(rs.getDouble(43));
                        map.put("AnnualOtherRecurringGrants_EOB", AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(44) == null) {
                        map.put("Toilets_EOB", "0.00");
                    } else {
                        Toilets_EOB = new BigDecimal(rs.getDouble(44));
                        map.put("Toilets_EOB", Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(45) == null) {
                        map.put("SelfDefenseETrainingGrants_EOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_EOB = new BigDecimal(rs.getDouble(45));
                        map.put("SelfDefenseETrainingGrants_EOB", SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(46) == null) {
                        map.put("Interest_EOB", "0.00");
                    } else {
                        Interest_EOB = new BigDecimal(rs.getDouble(46));
                        map.put("Interest_EOB", Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    
                    BigDecimal total_EOB = new BigDecimal(0);
                    total_EOB = total_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_EOB", total_EOB);
                    
                    
                    total_CivilWorks_EOB = total_CivilWorks_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_EOB", total_CivilWorks_EOB);
                    total_MajorRepairs_EOB = total_MajorRepairs_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_EOB", total_MajorRepairs_EOB);
                    total_MinorRepairs_EOB = total_MinorRepairs_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_EOB", total_MinorRepairs_EOB);
                    total_AnnualOtherRecurringGrants_EOB = total_AnnualOtherRecurringGrants_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_EOB", total_AnnualOtherRecurringGrants_EOB);
                    total_Toilets_EOB = total_Toilets_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_EOB", total_Toilets_EOB);
                    total_SelfDefenseTrainingGrants_EOB = total_SelfDefenseTrainingGrants_EOB .add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_EOB", total_SelfDefenseTrainingGrants_EOB);
                    total_Interest_EOB = total_Interest_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_EOB", total_Interest_EOB);
                    total_total_total_EOB = total_total_total_EOB.add(total_EOB);
                    map.put("total_total_total_EOB", total_total_total_EOB);
                    
                   
                    
                    BigDecimal CivilWorks_UBOB = null;
                    BigDecimal MajorRepairs_UBOB = null;
                    BigDecimal MinorRepairs_UBOB = null;
                    BigDecimal AnnualOtherRecurringGrants_UBOB = null;
                    BigDecimal Toilets_UBOB = null;
                    BigDecimal SelfDefenseETrainingGrants_UBOB = null;
                    BigDecimal Interest_UBOB = null;
                    BigDecimal Released = null;

                    if (rs.getString(47) == null) {
                        map.put("CivilWorks_UBOB", "0.00");
                    } else {
                        CivilWorks_UBOB = new BigDecimal(rs.getDouble(47));
                        map.put("CivilWorks_UBOB", CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(48) == null) {
                        map.put("MajorRepairs_UBOB", "0.00");
                    } else {
                        MajorRepairs_UBOB = new BigDecimal(rs.getDouble(48));
                        map.put("MajorRepairs_UBOB", MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(49) == null) {
                        map.put("MinorRepairs_UBOB", "0.00");
                    } else {
                        MinorRepairs_UBOB = new BigDecimal(rs.getDouble(49));
                        map.put("MinorRepairs_UBOB", MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(50) == null) {
                        map.put("AnnualOtherRecurringGrants_UBOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_UBOB = new BigDecimal(rs.getDouble(50));
                        map.put("AnnualOtherRecurringGrants_UBOB", AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(51) == null) {
                        map.put("Toilets_UBOB", "0.00");
                    } else {
                        Toilets_UBOB = new BigDecimal(rs.getDouble(51));
                        map.put("Toilets_UBOB", Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(52) == null) {
                        map.put("SelfDefenseETrainingGrants_UBOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_UBOB = new BigDecimal(rs.getDouble(52));
                        map.put("SelfDefenseETrainingGrants_UBOB", SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(53) == null) {
                        map.put("Interest_UBOB", "0.00");
                    } else {
                        Interest_UBOB = new BigDecimal(rs.getDouble(53));
                        map.put("Interest_UBOB", Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(54) == null) {
                        map.put("Released", "0.00");
                    } else {
                        Released = new BigDecimal(rs.getDouble(54));
                        map.put("Released", Released.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                   
                   
                    ubob_released_total = ubob_released_total.add(Released.setScale(2, RoundingMode.HALF_UP));
                    map.put("ubob_released_total", ubob_released_total);
                    
                    BigDecimal  total_UEOB = new BigDecimal(0);
                    total_UEOB = total_UEOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_UEOB", total_UEOB);
                    
                    total_CivilWorks_UBOB = total_CivilWorks_UBOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_UBOB", total_CivilWorks_UBOB);
                    total_MajorRepairs_UBOB = total_MajorRepairs_UBOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_UBOB", total_MajorRepairs_UBOB);
                    total_MinorRepairs_UBOB = total_MinorRepairs_UBOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_UBOB", total_MinorRepairs_UBOB);
                    total_AnnualOtherRecurringGrants_UBOB = total_AnnualOtherRecurringGrants_UBOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_UBOB", total_AnnualOtherRecurringGrants_UBOB);
                    total_Toilets_UBOB = total_Toilets_UBOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_UBOB", total_Toilets_UBOB);
                    total_SelfDefenseTrainingGrants_UBOB = total_SelfDefenseTrainingGrants_UBOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_UBOB", total_SelfDefenseTrainingGrants_UBOB);
                    total_Interest_UBOB = total_Interest_UBOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_UBOB", total_Interest_UBOB);
                    total_total_total_UBOB = total_total_total_UBOB.add(total_UEOB);
                    map.put("total_total_total_UBOB", total_total_total_UBOB);
                    
                    BigDecimal Earned_Interest=null;
                    BigDecimal Expenditure_Interest=null;
                    BigDecimal Balance_Interest=null;
                    BigDecimal RemmitanceAmount_ExpenditureIncurred=null;
                    
                    
                    if (rs.getString(55) == null) {
                        Earned_Interest = new BigDecimal("0.00");
                        map.put("Earned_Interest", "0.00");
                    } else {
                        Earned_Interest = new BigDecimal(rs.getDouble(55));
                        map.put("Earned_Interest", Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(56) == null) {
                        Expenditure_Interest = new BigDecimal("0.00");
                        map.put("Expenditure_Interest", "0.00");
                    } else {
                        Expenditure_Interest = new BigDecimal(rs.getDouble(56));
                        map.put("Expenditure_Interest", Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(57) == null) {
                        Balance_Interest = new BigDecimal("0.00");
                        map.put("Balance_Interest", "0.00");
                    } else {
                        Balance_Interest = new BigDecimal(rs.getDouble(57));
                        map.put("Balance_Interest", Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(58) == null) {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal("0.00");
                        map.put("RemmitanceAmount_ExpenditureIncurred", "0.00");
                    } else {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal(rs.getDouble(58));
                        map.put("RemmitanceAmount_ExpenditureIncurred", RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    total_Earned_Interest = total_Earned_Interest.add(Earned_Interest);
                    total_Earned_Interest = total_Earned_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Earned_Interest", total_Earned_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.add(Expenditure_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Expenditure_Interest", total_Expenditure_Interest);
                    total_Balance_Interest = total_Balance_Interest.add(Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Balance_Interest", total_Balance_Interest);
                    total_RemmitanceAmount_ExpenditureIncurred = total_RemmitanceAmount_ExpenditureIncurred.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_RemmitanceAmount_ExpenditureIncurred", total_RemmitanceAmount_ExpenditureIncurred);
                    
                    BigDecimal  totalRealse_Realses = new BigDecimal(0);
                    //totalRealse_Realses = totalRealse_Realses.add(Released.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_Realses", totalRealse_Realses);
                    
                    BigDecimal  totalRealse_total = new BigDecimal(0);
                    totalRealse_total = totalRealse_total.add(total_EOB);
                    totalRealse_total = totalRealse_total.add(Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_total", totalRealse_total);
                    
                    total_total_OB = total_total_OB.add(total_OB);
                    map.put("total_total_OB", total_total_OB);
                    total_totalRealse_Realses = total_totalRealse_Realses.add(totalRealse_Realses);
                    map.put("total_totalRealse_Realses", total_totalRealse_Realses);
                    total_Earned_Interest_total = total_Earned_Interest_total.add(Earned_Interest);
                    map.put("total_Earned_Interest_total",total_Earned_Interest_total.setScale(2, RoundingMode.HALF_UP) );
                    totalRealse_total_total = totalRealse_total_total.add(totalRealse_total);
                    map.put("totalRealse_total_total", totalRealse_total_total);
                    
                    BigDecimal  Grand_Total_Expenditure_Incurred_on_Releases = new BigDecimal(0);
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));

                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases", Grand_Total_Expenditure_Incurred_on_Releases);
                    
                    BigDecimal  Grand_Total_Total_Expenditure = new BigDecimal(0);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(total_EOB);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Total_Expenditure", Grand_Total_Total_Expenditure);
                    
                    BigDecimal close_balnce_ub_ob = total_OB.subtract(total_UEOB);
                    map.put("close_balnce_ub_ob", close_balnce_ub_ob);
                    BigDecimal close_balnce_ub_rb = totalRealse_Realses.subtract(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("close_balnce_ub_rb", close_balnce_ub_rb);
                    
                    BigDecimal close_balnce_ub_ineterest = (Earned_Interest.setScale(2, RoundingMode.HALF_UP)).subtract(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_ineterest", close_balnce_ub_ineterest);
                    
                    BigDecimal  close_balnce_ub_total = new BigDecimal(0);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_rb);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_ineterest);
                    close_balnce_ub_total = close_balnce_ub_total.subtract(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_total", close_balnce_ub_total);
                    
                    Grand_Total_Expenditure_Incurred_on_Opening_balances = Grand_Total_Expenditure_Incurred_on_Opening_balances.add(total_EOB);
                    map.put("Grand_Total_Expenditure_Incurred_on_Opening_balances", Grand_Total_Expenditure_Incurred_on_Opening_balances);
                    Grand_Total_Expenditure_Incurred_on_Releases_total = Grand_Total_Expenditure_Incurred_on_Releases_total.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases_total", Grand_Total_Expenditure_Incurred_on_Releases_total);
                    total_Expenditure_Interest_total = total_Expenditure_Interest_total.add(Expenditure_Interest);
                    map.put("total_Expenditure_Interest_total", total_Expenditure_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    total_RemmitanceAmount_ExpenditureIncurred_total = total_RemmitanceAmount_ExpenditureIncurred_total.add(total_RemmitanceAmount_ExpenditureIncurred);
                    map.put("total_RemmitanceAmount_ExpenditureIncurred_total", total_RemmitanceAmount_ExpenditureIncurred_total);
                    Grand_Total_Total_Expenditure_total = Grand_Total_Total_Expenditure_total.add(Grand_Total_Total_Expenditure);
                    map.put("Grand_Total_Total_Expenditure_total", Grand_Total_Total_Expenditure_total);

                    
                    close_balnce_ub_ob_total = close_balnce_ub_ob_total.add(close_balnce_ub_ob);
                    map.put("close_balnce_ub_ob_total", close_balnce_ub_ob_total);
                    close_balnce_ub_rb_total = close_balnce_ub_rb_total.add(close_balnce_ub_rb);
                    map.put("close_balnce_ub_rb_total", close_balnce_ub_rb_total);
                    close_balnce_ub_ineterest_total = close_balnce_ub_ineterest_total.add(close_balnce_ub_ineterest);
                    map.put("close_balnce_ub_ineterest_total", close_balnce_ub_ineterest_total);
                    close_balnce_ub_total_total = close_balnce_ub_total_total.add(close_balnce_ub_total);
                    map.put("close_balnce_ub_total_total", close_balnce_ub_total_total);
                    
                    mandallist.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandallist;
    }
    
    public ArrayList getSchoolList(String id, String year, String management) {
        ArrayList schoollist = new ArrayList();
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();

        BigDecimal civilNonRecTotal = new BigDecimal(0);
        BigDecimal majorNonRecTotal = new BigDecimal(0);
        BigDecimal minorNonRecTotal = new BigDecimal(0);
        BigDecimal toiletNonRecTotal = new BigDecimal(0);
        BigDecimal totNonRecTotal = new BigDecimal(0);

        BigDecimal expcivilNonRecTotal = new BigDecimal(0);
        BigDecimal expmajorNonRecTotal = new BigDecimal(0);
        BigDecimal expminorNonRecTotal = new BigDecimal(0);
        BigDecimal exptoiletNonRecTotal = new BigDecimal(0);
        BigDecimal exptotNonRecTotal = new BigDecimal(0);

        BigDecimal totcivilNonRecTotal = new BigDecimal(0);
        BigDecimal totmajorNonRecTotal = new BigDecimal(0);
        BigDecimal totminorNonRecTotal = new BigDecimal(0);
        BigDecimal tottoiletNonRecTotal = new BigDecimal(0);
        BigDecimal tottotNonRecTotal = new BigDecimal(0);

        BigDecimal waterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal booksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal minorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal sanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal needAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal provisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal totAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal expwaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal exptotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal twaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tNeedAnnualGrantsTotalBalance = new BigDecimal(0);
        BigDecimal tneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal ttotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal excurTriprecurringTotal = new BigDecimal(0);
        BigDecimal selfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal otherrecurringTotal = new BigDecimal(0);
        BigDecimal totrecurringTotal = new BigDecimal(0);

        BigDecimal excexcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal excselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal excotherrecurringTotal = new BigDecimal(0);
        BigDecimal exctotrecurringTotal = new BigDecimal(0);

        BigDecimal texcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal tselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal totherrecurringTotal = new BigDecimal(0);
        BigDecimal ttotrecurringTotal = new BigDecimal(0);

        BigDecimal relfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal rellabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal reltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal exfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal tfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal total_CivilWorks_OB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_OB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_OB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_OB = new BigDecimal(0);
        BigDecimal total_Toilets_OB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_OB = new BigDecimal(0);
        BigDecimal total_Interest_OB = new BigDecimal(0);
        BigDecimal total_total_total_OB = new BigDecimal(0);

        BigDecimal total_CivilWorks_EOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_EOB = new BigDecimal(0);
        BigDecimal total_Toilets_EOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_EOB = new BigDecimal(0);
        BigDecimal total_Interest_EOB = new BigDecimal(0);
        BigDecimal total_total_total_EOB = new BigDecimal(0);

        BigDecimal total_CivilWorks_UBOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Toilets_UBOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Interest_UBOB = new BigDecimal(0);
        BigDecimal total_total_total_UBOB = new BigDecimal(0);
        BigDecimal ubob_released_total = new BigDecimal(0);

        BigDecimal total_Earned_Interest = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest = new BigDecimal(0);
        BigDecimal total_Balance_Interest = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred = new BigDecimal(0);

        BigDecimal total_total_OB = new BigDecimal(0);
        BigDecimal total_totalRealse_Realses = new BigDecimal(0);
        BigDecimal total_Earned_Interest_total = new BigDecimal(0);
        BigDecimal totalRealse_total_total = new BigDecimal(0);

        BigDecimal Grand_Total_Expenditure_Incurred_on_Opening_balances = new BigDecimal(0);
        BigDecimal Grand_Total_Expenditure_Incurred_on_Releases_total = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest_total = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred_total = new BigDecimal(0);
        BigDecimal Grand_Total_Total_Expenditure_total = new BigDecimal(0);

        BigDecimal close_balnce_ub_ob_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_rb_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_ineterest_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_total_total = new BigDecimal(0);
        BigDecimal furniture_bal_sum = new BigDecimal(0);
        BigDecimal lab_equipment_sum = new BigDecimal(0);
        long totalSchoolsTotal = 0;


        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report](?,?)}");
//            cstmt.setString(1, id);
//            cstmt.setString(2, year);

            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report_school](?,?,?)}");
            cstmt.setString(1, id);
            cstmt.setString(2, year);
            cstmt.setString(3, management);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    BigDecimal civilReleased1 = null;
                    BigDecimal majorReleased1 = null;
                    BigDecimal minorReleased1 = null;
                    BigDecimal toiletsReleased1 = null;
                    BigDecimal civilExp1 = null;
                    BigDecimal majorExp1 = null;
                    BigDecimal minorExp1 = null;
                    BigDecimal toiletsExp1 = null;
                    BigDecimal waterReleased1 = null;
                    BigDecimal booksReleased1 = null;
                    BigDecimal minorReleasedAnnual1 = null;
                    BigDecimal sanitationReleased1 = null;
                    BigDecimal needReleased1 = null;
                    BigDecimal provisionalReleased1 = null;
                    BigDecimal waterExp1 = null;
                    BigDecimal booksExp1 = null;
                    BigDecimal minorExpAnnual1 = null;
                    BigDecimal sanitationExp1 = null;
                    BigDecimal needExp1 = null;
                    BigDecimal provisionalExp1 = null;
                    BigDecimal excurtionReleased1 = null;
                    BigDecimal selfdefReleased1 = null;
                    BigDecimal otherRecuGrantReleased1 = null;
                    BigDecimal excurtionExp1 = null;
                    BigDecimal selfdefExp1 = null;
                    BigDecimal otherRecuGrantExp1 = null;
                    BigDecimal furnitureReleased1 = null;
                    BigDecimal labEquipReleased1 = null;
                    BigDecimal furnitureExp1 = null;
                    BigDecimal labEquipExp1 = null;

                    map = new HashMap();
                    map.put("schoolCode", rs.getString(1));
                    map.put("schoolname", rs.getString(2));
                    if (rs.getString(3) == null) {
                        map.put("civilReleased", "0.00");
                    } else {
                        civilReleased1 = new BigDecimal(rs.getDouble(3));
                        map.put("civilReleased", civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(4) == null) {
                        map.put("majorReleased", "0.00");
                    } else {
                        majorReleased1 = new BigDecimal(rs.getDouble(4));
                        map.put("majorReleased", majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(5) == null) {
                        map.put("minorReleased", "0.00");
                    } else {
                        minorReleased1 = new BigDecimal(rs.getDouble(5));
                        map.put("minorReleased", minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(6) == null) {
                        map.put("toiletsReleased", "0.00");
                    } else {
                        toiletsReleased1 = new BigDecimal(rs.getDouble(6));
                        map.put("toiletsReleased", toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(7) == null) {
                        map.put("civilExp", "0.00");
                    } else {
                        civilExp1 = new BigDecimal(rs.getDouble(7));
                        map.put("civilExp", civilExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(8) == null) {
                        map.put("majorExp", "0.00");
                    } else {
                        majorExp1 = new BigDecimal(rs.getDouble(8));
                        map.put("majorExp", majorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(9) == null) {
                        map.put("minorExp", "0.00");
                    } else {
                        minorExp1 = new BigDecimal(rs.getDouble(9));
                        map.put("minorExp", minorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(10) == null) {
                        map.put("toiletsExp", "0.00");
                    } else {
                        toiletsExp1 = new BigDecimal(rs.getDouble(10));
                        map.put("toiletsExp", toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(11) == null) {
                        map.put("waterReleased", "0.00");
                    } else {
                        waterReleased1 = new BigDecimal(rs.getDouble(11));
                        map.put("waterReleased", waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(12) == null) {
                        map.put("booksReleased", "0.00");
                    } else {
                        booksReleased1 = new BigDecimal(rs.getDouble(12));
                        map.put("booksReleased", booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(13) == null) {
                        map.put("minorReleasedAnnual", "0.00");
                    } else {
                        minorReleasedAnnual1 = new BigDecimal(rs.getDouble(13));
                        map.put("minorReleasedAnnual", minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(14) == null) {
                        map.put("sanitationReleased", "0.00");
                    } else {
                        sanitationReleased1 = new BigDecimal(rs.getDouble(14));
                        map.put("sanitationReleased", sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(15) == null) {
                        map.put("needReleased", "0.00");
                    } else {
                        needReleased1 = new BigDecimal(rs.getDouble(15));
                        map.put("needReleased", needReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(16) == null) {
                        map.put("provisionalReleased", "0.00");
                    } else {
                        provisionalReleased1 = new BigDecimal(rs.getDouble(16));
                        map.put("provisionalReleased", provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(17) == null) {
                        map.put("waterExp", "0.00");
                    } else {
                        waterExp1 = new BigDecimal(rs.getDouble(17));
                        map.put("waterExp", waterExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(18) == null) {
                        map.put("booksExp", "0.00");
                    } else {
                        booksExp1 = new BigDecimal(rs.getDouble(18));
                        map.put("booksExp", booksExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(19) == null) {
                        map.put("minorExpAnnual", "0.00");
                    } else {
                        minorExpAnnual1 = new BigDecimal(rs.getDouble(19));
                        map.put("minorExpAnnual", minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(20) == null) {
                        map.put("sanitationExp", "0.00");
                    } else {
                        sanitationExp1 = new BigDecimal(rs.getDouble(20));
                        map.put("sanitationExp", sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(21) == null) {
                        map.put("needExp", "0.00");
                    } else {
                        needExp1 = new BigDecimal(rs.getDouble(21));
                        map.put("needExp", needExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(22) == null) {
                        map.put("provisionalExp", "0.00");
                    } else {
                        provisionalExp1 = new BigDecimal(rs.getDouble(22));
                        map.put("provisionalExp", provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(23) == null) {
                        map.put("excurtionReleased", "0.00");
                    } else {
                        excurtionReleased1 = new BigDecimal(rs.getDouble(23));
                        map.put("excurtionReleased", excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(24) == null) {
                        map.put("selfdefReleased", "0.00");
                    } else {
                        selfdefReleased1 = new BigDecimal(rs.getDouble(24));
                        map.put("selfdefReleased", selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(25) == null) {
                        map.put("otherRecuGrantReleased", "0.00");
                    } else {
                        otherRecuGrantReleased1 = new BigDecimal(rs.getDouble(25));
                        map.put("otherRecuGrantReleased", otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(26) == null) {
                        map.put("excurtionExp", "0.00");
                    } else {
                        excurtionExp1 = new BigDecimal(rs.getDouble(26));
                        map.put("excurtionExp", excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(27) == null) {
                        selfdefExp1 = new BigDecimal("0.00");
                        map.put("selfdefExp", "0.00");
                    } else {
                        selfdefExp1 = new BigDecimal(rs.getDouble(27));
                        map.put("selfdefExp", selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(28) == null) {
                        map.put("otherRecuGrantExp", "0.00");
                    } else {
                        otherRecuGrantExp1 = new BigDecimal(rs.getDouble(28));
                        map.put("otherRecuGrantExp", otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    if (rs.getString(29) == null) {
                        map.put("furnitureReleased", "0.00");
                    } else {
                        furnitureReleased1 = new BigDecimal(rs.getDouble(29));
                        map.put("furnitureReleased", furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(30) == null) {
                        map.put("labEquipReleased", "0.00");
                    } else {
                        labEquipReleased1 = new BigDecimal(rs.getDouble(30));
                        map.put("labEquipReleased", labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(31) == null) {
                        map.put("furnitureExp", "0.00");
                    } else {
                        furnitureExp1 = new BigDecimal(rs.getDouble(31));
                        map.put("furnitureExp", furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(32) == null) {
                        map.put("labEquipExp", "0.00");
                    } else {
                        labEquipExp1 = new BigDecimal(rs.getDouble(32));
                        map.put("labEquipExp", labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    
                    
                    BigDecimal acivirel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal aciviexp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal civibaaltot = acivirel.subtract(aciviexp);
                    map.put("civilBal", civibaaltot);
                    
                    BigDecimal majrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majexp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majtot = majrel.subtract(majexp);
                    map.put("majorBal", majtot);
                    BigDecimal minrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal minexp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mintot = minrel.subtract(minexp);
                    map.put("minorBal", mintot);
                    BigDecimal toirel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toiexp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toitot = toirel.subtract(toiexp);
                    map.put("toiletsBal", toitot);
                    //totals
                    BigDecimal tocivicnorecrel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecrel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectot = new BigDecimal(0);
                    tonorectot = tonorectot.add(tocivicnorecrel);
                    tonorectot = tonorectot.add(tomajnonrecrel);
                    tonorectot = tonorectot.add(tominnonrecrel);
                    tonorectot = tonorectot.add(tototinonrecrel);
                    map.put("totalNonRecurReleased", tonorectot);
                    
                    BigDecimal tocivicnorecExp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecExp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecExp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecExp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectotExp = new BigDecimal(0);
                    tonorectotExp = tonorectotExp.add(tocivicnorecExp);
                    tonorectotExp = tonorectotExp.add(tomajnonrecExp);
                    tonorectotExp = tonorectotExp.add(tominnonrecExp);
                    tonorectotExp = tonorectotExp.add(tototinonrecExp);
                    map.put("totalNonRecurExp", tonorectotExp);
                    
                    BigDecimal totciv = civibaaltot;
                    BigDecimal totmaj = majtot;
                    BigDecimal totmin = mintot;
                    BigDecimal tottoil = toitot;
                    BigDecimal totmajexpbal = new BigDecimal(0);
                    totmajexpbal = totmajexpbal.add(totciv);
                    totmajexpbal = totmajexpbal.add(totmaj);
                    totmajexpbal = totmajexpbal.add(totmin);
                    totmajexpbal = totmajexpbal.add(tottoil);
                    map.put("totalNonRecurBal", totmajexpbal);
                    
                    
                    //endtotal
                    BigDecimal warel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal waexp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal watot = warel.subtract(waexp);
                    map.put("waterBal", watot);
                    BigDecimal borel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal boxp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal botot = borel.subtract(boxp);
                    map.put("booksBal", botot);
                    BigDecimal mianrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mianxp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal miantot = mianrel.subtract(mianxp);
                    map.put("minorBalAnnual", miantot);
                    BigDecimal sarel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal saexp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal satot = sarel.subtract(saexp);
                    map.put("sanitationBal", satot);
                    BigDecimal needrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needexp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needtot = needrel.subtract(needexp);
                    map.put("needBal", needtot);
                    BigDecimal pvrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvexp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvtot = pvrel.subtract(pvexp);
                    map.put("provisionalBal", pvtot);
                    
                    //totals
                    BigDecimal towatcannualgrantrel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantrel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantrel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgranttot = new BigDecimal(0);
                    toannualgranttot = toannualgranttot.add(towatcannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tobookannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tominannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tosaniannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toneedannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toprovannualgrantrel);
                    map.put("totalGrantReleased", toannualgranttot);
                    BigDecimal towatcannualgrantExp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantExp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantExp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantExp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantExp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantExp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgrantExptot = new BigDecimal(0);
                    toannualgrantExptot = toannualgrantExptot.add(towatcannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tobookannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tominannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tosaniannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toneedannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toprovannualgrantExp);
                    map.put("totalGrantExp", toannualgrantExptot);
                    BigDecimal towatcannualgrantt = watot;
                    BigDecimal tobookannualgrantt = botot;
                    BigDecimal tominannualgrantt = miantot;
                    BigDecimal tosaniannualgrantt = satot;
                    BigDecimal toneedannualgrantt = needtot;
                    BigDecimal toprovannualgrantt = pvtot;
                    BigDecimal toannualgranttott = new BigDecimal(0);
                    toannualgranttott = toannualgranttott.add(towatcannualgrantt);
                    toannualgranttott = toannualgranttott.add(tobookannualgrantt);
                    toannualgranttott = toannualgranttott.add(tominannualgrantt);
                    toannualgranttott = toannualgranttott.add(tosaniannualgrantt);
                    toannualgranttott = toannualgranttott.add(toneedannualgrantt);
                    toannualgranttott = toannualgranttott.add(toprovannualgrantt);
                    map.put("totalGrantBal", toannualgranttott);
                    //endtotal
                    BigDecimal exrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal exexp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal extot = exrel.subtract(exexp);
                    map.put("excurtionBal", extot);
                    BigDecimal serel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal seexp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal setot = serel.subtract(seexp);
                    map.put("selfdefBal", setot);
                    BigDecimal otrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otexp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ottot = otrel.subtract(otexp);
                    map.put("otherRecuGrantBal", ottot);
                    //totals          
                    BigDecimal excurrecurrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurrel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutot = new BigDecimal(0);
                    recutot = recutot.add(excurrecurrel);
                    recutot = recutot.add(selfrecurrel);
                    recutot = recutot.add(otherrecurrel);
                    map.put("totalRecurReleased", recutot);
                    BigDecimal excurrecurExp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurExp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurExp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutotExp = new BigDecimal(0);
                    recutotExp = recutotExp.add(excurrecurExp);
                    recutotExp = recutotExp.add(selfrecurExp);
                    recutotExp = recutotExp.add(otherrecurExp);
                    map.put("totalRecurExp", recutotExp);
                    BigDecimal excurrecurt = extot;
                    BigDecimal selfrecurt = setot;
                    BigDecimal otherrecurt = ottot;
                    BigDecimal recutott = new BigDecimal(0);
                    recutott = recutott.add(excurrecurt);
                    recutott = recutott.add(selfrecurt);
                    recutott = recutott.add(otherrecurt);
                    map.put("totalRecurBal", recutott);
                    //endtotal
                    BigDecimal frel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal fexp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ftot = frel.subtract(fexp);
                    map.put("furnitureBal", ftot);
                    BigDecimal labrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labexp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labtot = labrel.subtract(labexp);
                    map.put("labEquipBal", labtot);
                    //totals    
                    BigDecimal furnfurlabrel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelreltot = furnfurlabrel.add(labfurlabrel);
                    map.put("totallabEquipReleased", furlabrelreltot);
                    BigDecimal furnfurlabExp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabExp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelExptot = furnfurlabExp.add(labfurlabExp);
                    map.put("totallabEquipExp", furlabrelExptot);
                    BigDecimal furnfurlabt = ftot;
                    BigDecimal labfurlabt = labtot;
                    BigDecimal furlabrelttot = furnfurlabt.add(labfurlabt);
                    map.put("totallabEquipBal", furlabrelttot);
                    furniture_bal_sum = furniture_bal_sum.add(furnfurlabt);
                    lab_equipment_sum = lab_equipment_sum.add(labfurlabt);
                    map.put("furniture_bal_sum", furniture_bal_sum);
                    map.put("lab_equipment_sum", lab_equipment_sum);
                    map.put("totalSchools", rs.getString(59));
                    //endtotal
         
                    /////////Final totals in tfoot
                    civilNonRecTotal = civilNonRecTotal.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    majorNonRecTotal = majorNonRecTotal.add(majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorNonRecTotal = minorNonRecTotal.add(minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    toiletNonRecTotal = toiletNonRecTotal.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totNonRecTotal = totNonRecTotal.add(tonorectot);

                    expcivilNonRecTotal = expcivilNonRecTotal.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    expmajorNonRecTotal = expmajorNonRecTotal.add(majorExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorNonRecTotal = expminorNonRecTotal.add(minorExp1.setScale(2, RoundingMode.HALF_UP));
                    exptoiletNonRecTotal = exptoiletNonRecTotal.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotNonRecTotal = exptotNonRecTotal.add(tonorectotExp);
                    totcivilNonRecTotal = totcivilNonRecTotal.add(civibaaltot);
                    totmajorNonRecTotal = totmajorNonRecTotal.add(majtot);
                    totminorNonRecTotal = totminorNonRecTotal.add(mintot);
                    tottoiletNonRecTotal = tottoiletNonRecTotal.add(toitot);
                    tottotNonRecTotal = tottotNonRecTotal.add(totmajexpbal);

                    waterAnnualGrantsTotal = waterAnnualGrantsTotal.add(waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    booksAnnualGrantsTotal = booksAnnualGrantsTotal.add(booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorAnnualGrantsTotal = minorAnnualGrantsTotal.add(minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    sanitationAnnualGrantsTotal = sanitationAnnualGrantsTotal.add(sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    needAnnualGrantsTotal = needAnnualGrantsTotal.add(needReleased1.setScale(2, RoundingMode.HALF_UP));
                    provisiAnnualGrantsTotal = provisiAnnualGrantsTotal.add(provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    totAnnualGrantsTotal = totAnnualGrantsTotal.add(toannualgranttot);

                    expwaterAnnualGrantsTotal = expwaterAnnualGrantsTotal.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    expbooksAnnualGrantsTotal = expbooksAnnualGrantsTotal.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorAnnualGrantsTotal = expminorAnnualGrantsTotal.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    expsanitationAnnualGrantsTotal = expsanitationAnnualGrantsTotal.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    expneedAnnualGrantsTotal = expneedAnnualGrantsTotal.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    expprovisiAnnualGrantsTotal = expprovisiAnnualGrantsTotal.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotAnnualGrantsTotal = exptotAnnualGrantsTotal.add(toannualgrantExptot);
                    twaterAnnualGrantsTotal = twaterAnnualGrantsTotal.add(watot);
                    tbooksAnnualGrantsTotal = tbooksAnnualGrantsTotal.add(botot);
                    tminorAnnualGrantsTotal = tminorAnnualGrantsTotal.add(miantot);
                    tsanitationAnnualGrantsTotal = tsanitationAnnualGrantsTotal.add(satot);
                    tNeedAnnualGrantsTotalBalance = tNeedAnnualGrantsTotalBalance.add(needtot);
                    tneedAnnualGrantsTotal = tneedAnnualGrantsTotal.add(needexp);
                    tprovisiAnnualGrantsTotal = tprovisiAnnualGrantsTotal.add(pvtot);
                    ttotAnnualGrantsTotal = ttotAnnualGrantsTotal.add(toannualgranttott);

                    excurTriprecurringTotal = excurTriprecurringTotal.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    selfdefencerecurringTotal = selfdefencerecurringTotal.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    otherrecurringTotal = otherrecurringTotal.add(otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    totrecurringTotal = totrecurringTotal.add(recutot);

                    excexcurTriprecurringTotal = excexcurTriprecurringTotal.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    excselfdefencerecurringTotal = excselfdefencerecurringTotal.add(selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    excotherrecurringTotal = excotherrecurringTotal.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));

                    exctotrecurringTotal = exctotrecurringTotal.add(recutotExp);
                    texcurTriprecurringTotal = texcurTriprecurringTotal.add(extot);
                    tselfdefencerecurringTotal = tselfdefencerecurringTotal.add(setot);
                    totherrecurringTotal = totherrecurringTotal.add(ottot);
                    ttotrecurringTotal = ttotrecurringTotal.add(recutott);

                    relfurFurniturLabEquipmentTotal = relfurFurniturLabEquipmentTotal.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    rellabFurniturLabEquipmentTotal = rellabFurniturLabEquipmentTotal.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    reltotFurniturLabEquipmentTotal = reltotFurniturLabEquipmentTotal.add(furlabrelreltot);

                    exfurFurniturLabEquipmentTotal = exfurFurniturLabEquipmentTotal.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    exlabFurniturLabEquipmentTotal = exlabFurniturLabEquipmentTotal.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));

                    exltotFurniturLabEquipmentTotal = exltotFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tfurFurniturLabEquipmentTotal = tfurFurniturLabEquipmentTotal.add(furlabrelreltot);
                    tlabFurniturLabEquipmentTotal = tlabFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tltotFurniturLabEquipmentTotal = tltotFurniturLabEquipmentTotal.add(furlabrelttot);


                   

             
                    //annual grants

                    map.put("civilNonRecTotal", civilNonRecTotal);
                    map.put("majorNonRecTotal", majorNonRecTotal);
                    map.put("minorNonRecTotal", minorNonRecTotal);
                    map.put("toiletNonRecTotal", toiletNonRecTotal);
                    map.put("totNonRecTotal", totNonRecTotal);

                    map.put("expcivilNonRecTotal", expcivilNonRecTotal);
                    map.put("expmajorNonRecTotal", expmajorNonRecTotal);
                    map.put("expminorNonRecTotal", expminorNonRecTotal);
                    map.put("exptoiletNonRecTotal", exptoiletNonRecTotal);
                    map.put("exptotNonRecTotal", exptotNonRecTotal);

                    map.put("totcivilNonRecTotal", totcivilNonRecTotal);
                    map.put("totmajorNonRecTotal", totmajorNonRecTotal);
                    map.put("totminorNonRecTotal", totminorNonRecTotal);
                    map.put("tottoiletNonRecTotal", tottoiletNonRecTotal);
                    map.put("tottotNonRecTotal", tottotNonRecTotal);

                    map.put("waterAnnualGrantsTotal", waterAnnualGrantsTotal);
                    map.put("booksAnnualGrantsTotal", booksAnnualGrantsTotal);
                    map.put("minorAnnualGrantsTotal", minorAnnualGrantsTotal);
                    map.put("sanitationAnnualGrantsTotal", sanitationAnnualGrantsTotal);
                    map.put("needAnnualGrantsTotal", needAnnualGrantsTotal);
                    map.put("provisiAnnualGrantsTotal", provisiAnnualGrantsTotal);
                    map.put("totAnnualGrantsTotal", totAnnualGrantsTotal);

                    map.put("expwaterAnnualGrantsTotal", expwaterAnnualGrantsTotal);
                    map.put("expbooksAnnualGrantsTotal", expbooksAnnualGrantsTotal);
                    map.put("expminorAnnualGrantsTotal", expminorAnnualGrantsTotal);
                    map.put("expsanitationAnnualGrantsTotal", expsanitationAnnualGrantsTotal);
                    map.put("expneedAnnualGrantsTotal", expneedAnnualGrantsTotal);
                    map.put("expprovisiAnnualGrantsTotal", expprovisiAnnualGrantsTotal);
                    map.put("exptotAnnualGrantsTotal", exptotAnnualGrantsTotal);

                    map.put("twaterAnnualGrantsTotal", twaterAnnualGrantsTotal);
                    map.put("tbooksAnnualGrantsTotal", tbooksAnnualGrantsTotal);
                    map.put("tminorAnnualGrantsTotal", tminorAnnualGrantsTotal);
                    map.put("tsanitationAnnualGrantsTotal", tsanitationAnnualGrantsTotal);
                    map.put("tNeedAnnualGrantsTotalBalance", tNeedAnnualGrantsTotalBalance);
                    map.put("tneedAnnualGrantsTotal", tneedAnnualGrantsTotal);
                    map.put("tprovisiAnnualGrantsTotal", tprovisiAnnualGrantsTotal);
                    map.put("ttotAnnualGrantsTotal", ttotAnnualGrantsTotal);

                    map.put("excurTriprecurringTotal", excurTriprecurringTotal);
                    map.put("selfdefencerecurringTotal", selfdefencerecurringTotal);
                    map.put("otherrecurringTotal", otherrecurringTotal);
                    map.put("totrecurringTotal", totrecurringTotal);

                    map.put("excexcurTriprecurringTotal", excexcurTriprecurringTotal);
                    map.put("excselfdefencerecurringTotal", excselfdefencerecurringTotal);
                    map.put("excotherrecurringTotal", excotherrecurringTotal);
                    map.put("exctotrecurringTotal", exctotrecurringTotal);

                    map.put("texcurTriprecurringTotal", texcurTriprecurringTotal);
                    map.put("tselfdefencerecurringTotal", tselfdefencerecurringTotal);
                    map.put("totherrecurringTotal", totherrecurringTotal);
                    map.put("ttotrecurringTotal", ttotrecurringTotal);

                    map.put("relfurFurniturLabEquipmentTotal", relfurFurniturLabEquipmentTotal);
                    map.put("rellabFurniturLabEquipmentTotal", rellabFurniturLabEquipmentTotal);
                    map.put("reltotFurniturLabEquipmentTotal", reltotFurniturLabEquipmentTotal);

                    map.put("exfurFurniturLabEquipmentTotal", exfurFurniturLabEquipmentTotal);
                    map.put("exlabFurniturLabEquipmentTotal", exlabFurniturLabEquipmentTotal);
                    map.put("exltotFurniturLabEquipmentTotal", exltotFurniturLabEquipmentTotal);

                    map.put("tfurFurniturLabEquipmentTotal", tfurFurniturLabEquipmentTotal);
                    map.put("tlabFurniturLabEquipmentTotal", tlabFurniturLabEquipmentTotal);
                    map.put("tltotFurniturLabEquipmentTotal", tltotFurniturLabEquipmentTotal);

                    BigDecimal CivilWorks_OB = null;
                    BigDecimal MajorRepairs_OB = null;
                    BigDecimal MinorRepairs_OB = null;
                    BigDecimal AnnualOtherRecurringGrants_OB = null;
                    BigDecimal Toilets_OB = null;
                    BigDecimal SelfDefenseTrainingGrants_OB = null;
                    BigDecimal Interest_OB = null;

                    if (rs.getString(33) == null) {
                        map.put("CivilWorks_OB", "0.00");
                    } else {
                        CivilWorks_OB = new BigDecimal(rs.getDouble(33));
                        map.put("CivilWorks_OB", CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(34) == null) {
                        map.put("MajorRepairs_OB", "0.00");
                    } else {
                        MajorRepairs_OB = new BigDecimal(rs.getDouble(34));
                        map.put("MajorRepairs_OB", MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(35) == null) {
                        map.put("MinorRepairs_OB", "0.00");
                    } else {
                        MinorRepairs_OB = new BigDecimal(rs.getDouble(35));
                        map.put("MinorRepairs_OB", MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(36) == null) {
                        map.put("AnnualOtherRecurringGrants_OB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_OB = new BigDecimal(rs.getDouble(36));
                        map.put("AnnualOtherRecurringGrants_OB", AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(37) == null) {
                        map.put("Toilets_OB", "0.00");
                    } else {
                        Toilets_OB = new BigDecimal(rs.getDouble(37));
                        map.put("Toilets_OB", Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(38) == null) {
                        map.put("SelfDefenseTrainingGrants_OB", "0.00");
                    } else {
                        SelfDefenseTrainingGrants_OB = new BigDecimal(rs.getDouble(38));
                        map.put("SelfDefenseTrainingGrants_OB", SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(39) == null) {
                        map.put("Interest_OB", "0.00");
                    } else {
                        Interest_OB = new BigDecimal(rs.getDouble(39));
                        map.put("Interest_OB", Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    BigDecimal total_OB = new BigDecimal(0);
                    total_OB = total_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_OB", total_OB);

                    total_CivilWorks_OB = total_CivilWorks_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_OB", total_CivilWorks_OB);
                    total_MajorRepairs_OB = total_MajorRepairs_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_OB", total_MajorRepairs_OB);
                    total_MinorRepairs_OB = total_MinorRepairs_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_OB", total_MinorRepairs_OB);
                    total_AnnualOtherRecurringGrants_OB = total_AnnualOtherRecurringGrants_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_OB", total_AnnualOtherRecurringGrants_OB);
                    total_Toilets_OB = total_Toilets_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));;
                    map.put("total_Toilets_OB", total_Toilets_OB);
                    total_SelfDefenseTrainingGrants_OB = total_SelfDefenseTrainingGrants_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_OB", total_SelfDefenseTrainingGrants_OB);
                    total_Interest_OB = total_Interest_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_OB", total_Interest_OB);
                    total_total_total_OB = total_total_total_OB.add(total_OB);
                    map.put("total_total_total_OB", total_total_total_OB);
                    
                    BigDecimal CivilWorks_EOB = null;
                    BigDecimal MajorRepairs_EOB = null;
                    BigDecimal MinorRepairs_EOB = null;
                    BigDecimal AnnualOtherRecurringGrants_EOB = null;
                    BigDecimal Toilets_EOB = null;
                    BigDecimal SelfDefenseETrainingGrants_EOB = null;
                    BigDecimal Interest_EOB = null;
                    if (rs.getString(40) == null) {
                        map.put("CivilWorks_EOB", "0.00");
                    } else {
                        CivilWorks_EOB = new BigDecimal(rs.getDouble(40));
                        map.put("CivilWorks_EOB", CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(41) == null) {
                        map.put("MajorRepairs_EOB", "0.00");
                    } else {
                        MajorRepairs_EOB = new BigDecimal(rs.getDouble(41));
                        map.put("MajorRepairs_EOB", MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(42) == null) {
                        map.put("MinorRepairs_EOB", "0.00");
                    } else {
                        MinorRepairs_EOB = new BigDecimal(rs.getDouble(42));
                        map.put("MinorRepairs_EOB", MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(43) == null) {
                        map.put("AnnualOtherRecurringGrants_EOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_EOB = new BigDecimal(rs.getDouble(43));
                        map.put("AnnualOtherRecurringGrants_EOB", AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(44) == null) {
                        map.put("Toilets_EOB", "0.00");
                    } else {
                        Toilets_EOB = new BigDecimal(rs.getDouble(44));
                        map.put("Toilets_EOB", Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(45) == null) {
                        map.put("SelfDefenseETrainingGrants_EOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_EOB = new BigDecimal(rs.getDouble(45));
                        map.put("SelfDefenseETrainingGrants_EOB", SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(46) == null) {
                        map.put("Interest_EOB", "0.00");
                    } else {
                        Interest_EOB = new BigDecimal(rs.getDouble(46));
                        map.put("Interest_EOB", Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    BigDecimal total_EOB = new BigDecimal(0);
                    total_EOB = total_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_EOB", total_EOB);
                    
                    total_CivilWorks_EOB = total_CivilWorks_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_EOB", total_CivilWorks_EOB);
                    total_MajorRepairs_EOB = total_MajorRepairs_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_EOB", total_MajorRepairs_EOB);
                    total_MinorRepairs_EOB = total_MinorRepairs_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_EOB", total_MinorRepairs_EOB);
                    total_AnnualOtherRecurringGrants_EOB = total_AnnualOtherRecurringGrants_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_EOB", total_AnnualOtherRecurringGrants_EOB);
                    total_Toilets_EOB = total_Toilets_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_EOB", total_Toilets_EOB);
                    total_SelfDefenseTrainingGrants_EOB = total_SelfDefenseTrainingGrants_EOB .add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_EOB", total_SelfDefenseTrainingGrants_EOB);
                    total_Interest_EOB = total_Interest_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_EOB", total_Interest_EOB);
                    total_total_total_EOB = total_total_total_EOB.add(total_EOB);
                    map.put("total_total_total_EOB", total_total_total_EOB);
                    
                   
                    
                    BigDecimal CivilWorks_UBOB = null;
                    BigDecimal MajorRepairs_UBOB = null;
                    BigDecimal MinorRepairs_UBOB = null;
                    BigDecimal AnnualOtherRecurringGrants_UBOB = null;
                    BigDecimal Toilets_UBOB = null;
                    BigDecimal SelfDefenseETrainingGrants_UBOB = null;
                    BigDecimal Interest_UBOB = null;
                    BigDecimal Released = null;

                    if (rs.getString(47) == null) {
                        map.put("CivilWorks_UBOB", "0.00");
                    } else {
                        CivilWorks_UBOB = new BigDecimal(rs.getDouble(47));
                        map.put("CivilWorks_UBOB", CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(48) == null) {
                        map.put("MajorRepairs_UBOB", "0.00");
                    } else {
                        MajorRepairs_UBOB = new BigDecimal(rs.getDouble(48));
                        map.put("MajorRepairs_UBOB", MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(49) == null) {
                        map.put("MinorRepairs_UBOB", "0.00");
                    } else {
                        MinorRepairs_UBOB = new BigDecimal(rs.getDouble(49));
                        map.put("MinorRepairs_UBOB", MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(50) == null) {
                        map.put("AnnualOtherRecurringGrants_UBOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_UBOB = new BigDecimal(rs.getDouble(50));
                        map.put("AnnualOtherRecurringGrants_UBOB", AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(51) == null) {
                        map.put("Toilets_UBOB", "0.00");
                    } else {
                        Toilets_UBOB = new BigDecimal(rs.getDouble(51));
                        map.put("Toilets_UBOB", Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(52) == null) {
                        map.put("SelfDefenseETrainingGrants_UBOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_UBOB = new BigDecimal(rs.getDouble(52));
                        map.put("SelfDefenseETrainingGrants_UBOB", SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(53) == null) {
                        map.put("Interest_UBOB", "0.00");
                    } else {
                        Interest_UBOB = new BigDecimal(rs.getDouble(53));
                        map.put("Interest_UBOB", Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(54) == null) {
                        map.put("Released", "0.00");
                    } else {
                        Released = new BigDecimal(rs.getDouble(54));
                        map.put("Released", Released.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    
                   
                    ubob_released_total = ubob_released_total.add(Released.setScale(2, RoundingMode.HALF_UP));
                    map.put("ubob_released_total", ubob_released_total);
                    
                    BigDecimal  total_UEOB = new BigDecimal(0);
                    total_UEOB = total_UEOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_UEOB", total_UEOB);
                    
                    total_CivilWorks_UBOB = total_CivilWorks_UBOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_UBOB", total_CivilWorks_UBOB);
                    total_MajorRepairs_UBOB = total_MajorRepairs_UBOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_UBOB", total_MajorRepairs_UBOB);
                    total_MinorRepairs_UBOB = total_MinorRepairs_UBOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_UBOB", total_MinorRepairs_UBOB);
                    total_AnnualOtherRecurringGrants_UBOB = total_AnnualOtherRecurringGrants_UBOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_UBOB", total_AnnualOtherRecurringGrants_UBOB);
                    total_Toilets_UBOB = total_Toilets_UBOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_UBOB", total_Toilets_UBOB);
                    total_SelfDefenseTrainingGrants_UBOB = total_SelfDefenseTrainingGrants_UBOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_UBOB", total_SelfDefenseTrainingGrants_UBOB);
                    total_Interest_UBOB = total_Interest_UBOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_UBOB", total_Interest_UBOB);
                    total_total_total_UBOB = total_total_total_UBOB.add(total_UEOB);
                    map.put("total_total_total_UBOB", total_total_total_UBOB);
                    
                    BigDecimal Earned_Interest=null;
                    BigDecimal Expenditure_Interest=null;
                    BigDecimal Balance_Interest=null;
                    BigDecimal RemmitanceAmount_ExpenditureIncurred=null;
                    
                    
                    if (rs.getString(55) == null) {
                        Earned_Interest = new BigDecimal("0.00");
                        map.put("Earned_Interest", "0.00");
                    } else {
                        Earned_Interest = new BigDecimal(rs.getDouble(55));
                        map.put("Earned_Interest", Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(56) == null) {
                        Expenditure_Interest = new BigDecimal("0.00");
                        map.put("Expenditure_Interest", "0.00");
                    } else {
                        Expenditure_Interest = new BigDecimal(rs.getDouble(56));
                        map.put("Expenditure_Interest", Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(57) == null) {
                        Balance_Interest = new BigDecimal("0.00");
                        map.put("Balance_Interest", "0.00");
                    } else {
                        Balance_Interest = new BigDecimal(rs.getDouble(57));
                        map.put("Balance_Interest", Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(58) == null) {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal("0.00");
                        map.put("RemmitanceAmount_ExpenditureIncurred", "0.00");
                    } else {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal(rs.getDouble(58));
                        map.put("RemmitanceAmount_ExpenditureIncurred", RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    map.put("distName", rs.getString(59));
                    map.put("MandalName", rs.getString(60));
                    map.put("bankAccNum", rs.getString(61));
                    map.put("ifsc", rs.getString(62));
                    map.put("bankName", rs.getString(63));
                    
                    total_Earned_Interest = total_Earned_Interest.add(Earned_Interest);
                    total_Earned_Interest = total_Earned_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Earned_Interest", total_Earned_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.add(Expenditure_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Expenditure_Interest", total_Expenditure_Interest);
                    total_Balance_Interest = total_Balance_Interest.add(Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Balance_Interest", total_Balance_Interest);
                    total_RemmitanceAmount_ExpenditureIncurred = total_RemmitanceAmount_ExpenditureIncurred.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_RemmitanceAmount_ExpenditureIncurred", total_RemmitanceAmount_ExpenditureIncurred);
                    
                    BigDecimal  totalRealse_Realses = new BigDecimal(0);
                    //totalRealse_Realses = totalRealse_Realses.add(Released.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_Realses", totalRealse_Realses);
                    
                    BigDecimal  totalRealse_total = new BigDecimal(0);
                    totalRealse_total = totalRealse_total.add(total_EOB);
                    totalRealse_total = totalRealse_total.add(Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_total", totalRealse_total);
                    
                    total_total_OB = total_total_OB.add(total_OB);
                    map.put("total_total_OB", total_total_OB);
                    total_totalRealse_Realses = total_totalRealse_Realses.add(totalRealse_Realses);
                    map.put("total_totalRealse_Realses", total_totalRealse_Realses);
                    total_Earned_Interest_total = total_Earned_Interest_total.add(Earned_Interest);
                    map.put("total_Earned_Interest_total", total_Earned_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_total_total = totalRealse_total_total.add(totalRealse_total);
                    map.put("totalRealse_total_total", totalRealse_total_total);
                    
                    BigDecimal  Grand_Total_Expenditure_Incurred_on_Releases = new BigDecimal(0);
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));

                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases", Grand_Total_Expenditure_Incurred_on_Releases);
                    
                    BigDecimal  Grand_Total_Total_Expenditure = new BigDecimal(0);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(total_EOB);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Total_Expenditure", Grand_Total_Total_Expenditure);
                    
                    BigDecimal close_balnce_ub_ob = total_OB.subtract(total_UEOB);
                    map.put("close_balnce_ub_ob", close_balnce_ub_ob);
                    BigDecimal close_balnce_ub_rb = totalRealse_Realses.subtract(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("close_balnce_ub_rb", close_balnce_ub_rb);
                    
                    BigDecimal close_balnce_ub_ineterest = (Earned_Interest.setScale(2, RoundingMode.HALF_UP)).subtract(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_ineterest", close_balnce_ub_ineterest);
                    
                    BigDecimal  close_balnce_ub_total = new BigDecimal(0);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_rb);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_ineterest);
                    close_balnce_ub_total = close_balnce_ub_total.subtract(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_total", close_balnce_ub_total);
                    
                    Grand_Total_Expenditure_Incurred_on_Opening_balances = Grand_Total_Expenditure_Incurred_on_Opening_balances.add(total_EOB);
                    map.put("Grand_Total_Expenditure_Incurred_on_Opening_balances", Grand_Total_Expenditure_Incurred_on_Opening_balances);
                    Grand_Total_Expenditure_Incurred_on_Releases_total = Grand_Total_Expenditure_Incurred_on_Releases_total.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases_total", Grand_Total_Expenditure_Incurred_on_Releases_total);
                    total_Expenditure_Interest_total = total_Expenditure_Interest_total.add(Expenditure_Interest);
                    map.put("total_Expenditure_Interest_total", total_Expenditure_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    total_RemmitanceAmount_ExpenditureIncurred_total = total_RemmitanceAmount_ExpenditureIncurred_total.add(total_RemmitanceAmount_ExpenditureIncurred);
                    map.put("total_RemmitanceAmount_ExpenditureIncurred_total", total_RemmitanceAmount_ExpenditureIncurred_total);
                    Grand_Total_Total_Expenditure_total = Grand_Total_Total_Expenditure_total.add(Grand_Total_Total_Expenditure);
                    map.put("Grand_Total_Total_Expenditure_total", Grand_Total_Total_Expenditure_total);

                    
                    close_balnce_ub_ob_total = close_balnce_ub_ob_total.add(close_balnce_ub_ob);
                    map.put("close_balnce_ub_ob_total", close_balnce_ub_ob_total);
                    close_balnce_ub_rb_total = close_balnce_ub_rb_total.add(close_balnce_ub_rb);
                    map.put("close_balnce_ub_rb_total", close_balnce_ub_rb_total);
                    close_balnce_ub_ineterest_total = close_balnce_ub_ineterest_total.add(close_balnce_ub_ineterest);
                    map.put("close_balnce_ub_ineterest_total", close_balnce_ub_ineterest_total);
                    close_balnce_ub_total_total = close_balnce_ub_total_total.add(close_balnce_ub_total);
                    map.put("close_balnce_ub_total_total", close_balnce_ub_total_total);
                    
                    schoollist.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoollist;
    }
    
      public ArrayList getAllSchoolList(String id, String year, String management) {
        ArrayList schoollist = new ArrayList();
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();

        BigDecimal civilNonRecTotal = new BigDecimal(0);
        BigDecimal majorNonRecTotal = new BigDecimal(0);
        BigDecimal minorNonRecTotal = new BigDecimal(0);
        BigDecimal toiletNonRecTotal = new BigDecimal(0);
        BigDecimal totNonRecTotal = new BigDecimal(0);

        BigDecimal expcivilNonRecTotal = new BigDecimal(0);
        BigDecimal expmajorNonRecTotal = new BigDecimal(0);
        BigDecimal expminorNonRecTotal = new BigDecimal(0);
        BigDecimal exptoiletNonRecTotal = new BigDecimal(0);
        BigDecimal exptotNonRecTotal = new BigDecimal(0);

        BigDecimal totcivilNonRecTotal = new BigDecimal(0);
        BigDecimal totmajorNonRecTotal = new BigDecimal(0);
        BigDecimal totminorNonRecTotal = new BigDecimal(0);
        BigDecimal tottoiletNonRecTotal = new BigDecimal(0);
        BigDecimal tottotNonRecTotal = new BigDecimal(0);

        BigDecimal waterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal booksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal minorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal sanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal needAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal provisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal totAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal expwaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal expprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal exptotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal twaterAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tbooksAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tminorAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tsanitationAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tNeedAnnualGrantsTotalBalance = new BigDecimal(0);
        BigDecimal tneedAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal tprovisiAnnualGrantsTotal = new BigDecimal(0);
        BigDecimal ttotAnnualGrantsTotal = new BigDecimal(0);

        BigDecimal excurTriprecurringTotal = new BigDecimal(0);
        BigDecimal selfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal otherrecurringTotal = new BigDecimal(0);
        BigDecimal totrecurringTotal = new BigDecimal(0);

        BigDecimal excexcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal excselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal excotherrecurringTotal = new BigDecimal(0);
        BigDecimal exctotrecurringTotal = new BigDecimal(0);

        BigDecimal texcurTriprecurringTotal = new BigDecimal(0);
        BigDecimal tselfdefencerecurringTotal = new BigDecimal(0);
        BigDecimal totherrecurringTotal = new BigDecimal(0);
        BigDecimal ttotrecurringTotal = new BigDecimal(0);

        BigDecimal relfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal rellabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal reltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal exfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal exltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal tfurFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tlabFurniturLabEquipmentTotal = new BigDecimal(0);
        BigDecimal tltotFurniturLabEquipmentTotal = new BigDecimal(0);

        BigDecimal total_CivilWorks_OB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_OB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_OB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_OB = new BigDecimal(0);
        BigDecimal total_Toilets_OB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_OB = new BigDecimal(0);
        BigDecimal total_Interest_OB = new BigDecimal(0);
        BigDecimal total_total_total_OB = new BigDecimal(0);

        BigDecimal total_CivilWorks_EOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_EOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_EOB = new BigDecimal(0);
        BigDecimal total_Toilets_EOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_EOB = new BigDecimal(0);
        BigDecimal total_Interest_EOB = new BigDecimal(0);
        BigDecimal total_total_total_EOB = new BigDecimal(0);

        BigDecimal total_CivilWorks_UBOB = new BigDecimal(0);
        BigDecimal total_MajorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_MinorRepairs_UBOB = new BigDecimal(0);
        BigDecimal total_AnnualOtherRecurringGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Toilets_UBOB = new BigDecimal(0);
        BigDecimal total_SelfDefenseTrainingGrants_UBOB = new BigDecimal(0);
        BigDecimal total_Interest_UBOB = new BigDecimal(0);
        BigDecimal total_total_total_UBOB = new BigDecimal(0);
        BigDecimal ubob_released_total = new BigDecimal(0);

        BigDecimal total_Earned_Interest = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest = new BigDecimal(0);
        BigDecimal total_Balance_Interest = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred = new BigDecimal(0);

        BigDecimal total_total_OB = new BigDecimal(0);
        BigDecimal total_totalRealse_Realses = new BigDecimal(0);
        BigDecimal total_Earned_Interest_total = new BigDecimal(0);
        BigDecimal totalRealse_total_total = new BigDecimal(0);

        BigDecimal Grand_Total_Expenditure_Incurred_on_Opening_balances = new BigDecimal(0);
        BigDecimal Grand_Total_Expenditure_Incurred_on_Releases_total = new BigDecimal(0);
        BigDecimal total_Expenditure_Interest_total = new BigDecimal(0);
        BigDecimal total_RemmitanceAmount_ExpenditureIncurred_total = new BigDecimal(0);
        BigDecimal Grand_Total_Total_Expenditure_total = new BigDecimal(0);

        BigDecimal close_balnce_ub_ob_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_rb_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_ineterest_total = new BigDecimal(0);
        BigDecimal close_balnce_ub_total_total = new BigDecimal(0);
        BigDecimal furniture_bal_sum = new BigDecimal(0);
        BigDecimal lab_equipment_sum = new BigDecimal(0);
        long totalSchoolsTotal = 0;


        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report](?,?)}");
//            cstmt.setString(1, id);
//            cstmt.setString(2, year);

            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_report_Allschool](?,?,?)}");
            cstmt.setString(1, id);
            cstmt.setString(2, year);
            cstmt.setString(3, management);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    BigDecimal civilReleased1 = null;
                    BigDecimal majorReleased1 = null;
                    BigDecimal minorReleased1 = null;
                    BigDecimal toiletsReleased1 = null;
                    BigDecimal civilExp1 = null;
                    BigDecimal majorExp1 = null;
                    BigDecimal minorExp1 = null;
                    BigDecimal toiletsExp1 = null;
                    BigDecimal waterReleased1 = null;
                    BigDecimal booksReleased1 = null;
                    BigDecimal minorReleasedAnnual1 = null;
                    BigDecimal sanitationReleased1 = null;
                    BigDecimal needReleased1 = null;
                    BigDecimal provisionalReleased1 = null;
                    BigDecimal waterExp1 = null;
                    BigDecimal booksExp1 = null;
                    BigDecimal minorExpAnnual1 = null;
                    BigDecimal sanitationExp1 = null;
                    BigDecimal needExp1 = null;
                    BigDecimal provisionalExp1 = null;
                    BigDecimal excurtionReleased1 = null;
                    BigDecimal selfdefReleased1 = null;
                    BigDecimal otherRecuGrantReleased1 = null;
                    BigDecimal excurtionExp1 = null;
                    BigDecimal selfdefExp1 = null;
                    BigDecimal otherRecuGrantExp1 = null;
                    BigDecimal furnitureReleased1 = null;
                    BigDecimal labEquipReleased1 = null;
                    BigDecimal furnitureExp1 = null;
                    BigDecimal labEquipExp1 = null;

                    map = new HashMap();
                    map.put("schoolCode", rs.getString(1));
                    map.put("schoolname", rs.getString(2));
                    if (rs.getString(3) == null) {
                        map.put("civilReleased", "0.00");
                    } else {
                        civilReleased1 = new BigDecimal(rs.getDouble(3));
                        map.put("civilReleased", civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(4) == null) {
                        map.put("majorReleased", "0.00");
                    } else {
                        majorReleased1 = new BigDecimal(rs.getDouble(4));
                        map.put("majorReleased", majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(5) == null) {
                        map.put("minorReleased", "0.00");
                    } else {
                        minorReleased1 = new BigDecimal(rs.getDouble(5));
                        map.put("minorReleased", minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(6) == null) {
                        map.put("toiletsReleased", "0.00");
                    } else {
                        toiletsReleased1 = new BigDecimal(rs.getDouble(6));
                        map.put("toiletsReleased", toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(7) == null) {
                        map.put("civilExp", "0.00");
                    } else {
                        civilExp1 = new BigDecimal(rs.getDouble(7));
                        map.put("civilExp", civilExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(8) == null) {
                        map.put("majorExp", "0.00");
                    } else {
                        majorExp1 = new BigDecimal(rs.getDouble(8));
                        map.put("majorExp", majorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(9) == null) {
                        map.put("minorExp", "0.00");
                    } else {
                        minorExp1 = new BigDecimal(rs.getDouble(9));
                        map.put("minorExp", minorExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(10) == null) {
                        map.put("toiletsExp", "0.00");
                    } else {
                        toiletsExp1 = new BigDecimal(rs.getDouble(10));
                        map.put("toiletsExp", toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(11) == null) {
                        map.put("waterReleased", "0.00");
                    } else {
                        waterReleased1 = new BigDecimal(rs.getDouble(11));
                        map.put("waterReleased", waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(12) == null) {
                        map.put("booksReleased", "0.00");
                    } else {
                        booksReleased1 = new BigDecimal(rs.getDouble(12));
                        map.put("booksReleased", booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(13) == null) {
                        map.put("minorReleasedAnnual", "0.00");
                    } else {
                        minorReleasedAnnual1 = new BigDecimal(rs.getDouble(13));
                        map.put("minorReleasedAnnual", minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(14) == null) {
                        map.put("sanitationReleased", "0.00");
                    } else {
                        sanitationReleased1 = new BigDecimal(rs.getDouble(14));
                        map.put("sanitationReleased", sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(15) == null) {
                        map.put("needReleased", "0.00");
                    } else {
                        needReleased1 = new BigDecimal(rs.getDouble(15));
                        map.put("needReleased", needReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(16) == null) {
                        map.put("provisionalReleased", "0.00");
                    } else {
                        provisionalReleased1 = new BigDecimal(rs.getDouble(16));
                        map.put("provisionalReleased", provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(17) == null) {
                        map.put("waterExp", "0.00");
                    } else {
                        waterExp1 = new BigDecimal(rs.getDouble(17));
                        map.put("waterExp", waterExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(18) == null) {
                        map.put("booksExp", "0.00");
                    } else {
                        booksExp1 = new BigDecimal(rs.getDouble(18));
                        map.put("booksExp", booksExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(19) == null) {
                        map.put("minorExpAnnual", "0.00");
                    } else {
                        minorExpAnnual1 = new BigDecimal(rs.getDouble(19));
                        map.put("minorExpAnnual", minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(20) == null) {
                        map.put("sanitationExp", "0.00");
                    } else {
                        sanitationExp1 = new BigDecimal(rs.getDouble(20));
                        map.put("sanitationExp", sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(21) == null) {
                        map.put("needExp", "0.00");
                    } else {
                        needExp1 = new BigDecimal(rs.getDouble(21));
                        map.put("needExp", needExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(22) == null) {
                        map.put("provisionalExp", "0.00");
                    } else {
                        provisionalExp1 = new BigDecimal(rs.getDouble(22));
                        map.put("provisionalExp", provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(23) == null) {
                        map.put("excurtionReleased", "0.00");
                    } else {
                        excurtionReleased1 = new BigDecimal(rs.getDouble(23));
                        map.put("excurtionReleased", excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(24) == null) {
                        map.put("selfdefReleased", "0.00");
                    } else {
                        selfdefReleased1 = new BigDecimal(rs.getDouble(24));
                        map.put("selfdefReleased", selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(25) == null) {
                        map.put("otherRecuGrantReleased", "0.00");
                    } else {
                        otherRecuGrantReleased1 = new BigDecimal(rs.getDouble(25));
                        map.put("otherRecuGrantReleased", otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(26) == null) {
                        map.put("excurtionExp", "0.00");
                    } else {
                        excurtionExp1 = new BigDecimal(rs.getDouble(26));
                        map.put("excurtionExp", excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(27) == null) {
                        selfdefExp1 = new BigDecimal("0.00");
                        map.put("selfdefExp", "0.00");
                    } else {
                        selfdefExp1 = new BigDecimal(rs.getDouble(27));
                        map.put("selfdefExp", selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(28) == null) {
                        map.put("otherRecuGrantExp", "0.00");
                    } else {
                        otherRecuGrantExp1 = new BigDecimal(rs.getDouble(28));
                        map.put("otherRecuGrantExp", otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    if (rs.getString(29) == null) {
                        map.put("furnitureReleased", "0.00");
                    } else {
                        furnitureReleased1 = new BigDecimal(rs.getDouble(29));
                        map.put("furnitureReleased", furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(30) == null) {
                        map.put("labEquipReleased", "0.00");
                    } else {
                        labEquipReleased1 = new BigDecimal(rs.getDouble(30));
                        map.put("labEquipReleased", labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(31) == null) {
                        map.put("furnitureExp", "0.00");
                    } else {
                        furnitureExp1 = new BigDecimal(rs.getDouble(31));
                        map.put("furnitureExp", furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(32) == null) {
                        map.put("labEquipExp", "0.00");
                    } else {
                        labEquipExp1 = new BigDecimal(rs.getDouble(32));
                        map.put("labEquipExp", labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    }

                    
                    
                    BigDecimal acivirel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal aciviexp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal civibaaltot = acivirel.subtract(aciviexp);
                    map.put("civilBal", civibaaltot);
                    
                    BigDecimal majrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majexp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal majtot = majrel.subtract(majexp);
                    map.put("majorBal", majtot);
                    BigDecimal minrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal minexp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mintot = minrel.subtract(minexp);
                    map.put("minorBal", mintot);
                    BigDecimal toirel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toiexp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toitot = toirel.subtract(toiexp);
                    map.put("toiletsBal", toitot);
                    //totals
                    BigDecimal tocivicnorecrel = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecrel = majorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecrel = minorReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecrel = toiletsReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectot = new BigDecimal(0);
                    tonorectot = tonorectot.add(tocivicnorecrel);
                    tonorectot = tonorectot.add(tomajnonrecrel);
                    tonorectot = tonorectot.add(tominnonrecrel);
                    tonorectot = tonorectot.add(tototinonrecrel);
                    map.put("totalNonRecurReleased", tonorectot);
                    
                    BigDecimal tocivicnorecExp = civilExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tomajnonrecExp = majorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominnonrecExp = minorExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tototinonrecExp = toiletsExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tonorectotExp = new BigDecimal(0);
                    tonorectotExp = tonorectotExp.add(tocivicnorecExp);
                    tonorectotExp = tonorectotExp.add(tomajnonrecExp);
                    tonorectotExp = tonorectotExp.add(tominnonrecExp);
                    tonorectotExp = tonorectotExp.add(tototinonrecExp);
                    map.put("totalNonRecurExp", tonorectotExp);
                    
                    BigDecimal totciv = civibaaltot;
                    BigDecimal totmaj = majtot;
                    BigDecimal totmin = mintot;
                    BigDecimal tottoil = toitot;
                    BigDecimal totmajexpbal = new BigDecimal(0);
                    totmajexpbal = totmajexpbal.add(totciv);
                    totmajexpbal = totmajexpbal.add(totmaj);
                    totmajexpbal = totmajexpbal.add(totmin);
                    totmajexpbal = totmajexpbal.add(tottoil);
                    map.put("totalNonRecurBal", totmajexpbal);
                    
                    
                    //endtotal
                    BigDecimal warel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal waexp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal watot = warel.subtract(waexp);
                    map.put("waterBal", watot);
                    BigDecimal borel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal boxp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal botot = borel.subtract(boxp);
                    map.put("booksBal", botot);
                    BigDecimal mianrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal mianxp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal miantot = mianrel.subtract(mianxp);
                    map.put("minorBalAnnual", miantot);
                    BigDecimal sarel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal saexp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal satot = sarel.subtract(saexp);
                    map.put("sanitationBal", satot);
                    BigDecimal needrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needexp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal needtot = needrel.subtract(needexp);
                    map.put("needBal", needtot);
                    BigDecimal pvrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvexp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal pvtot = pvrel.subtract(pvexp);
                    map.put("provisionalBal", pvtot);
                    
                    //totals
                    BigDecimal towatcannualgrantrel = waterReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantrel = booksReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantrel = minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantrel = sanitationReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantrel = needReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantrel = provisionalReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgranttot = new BigDecimal(0);
                    toannualgranttot = toannualgranttot.add(towatcannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tobookannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tominannualgrantrel);
                    toannualgranttot = toannualgranttot.add(tosaniannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toneedannualgrantrel);
                    toannualgranttot = toannualgranttot.add(toprovannualgrantrel);
                    map.put("totalGrantReleased", toannualgranttot);
                    BigDecimal towatcannualgrantExp = waterExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tobookannualgrantExp = booksExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tominannualgrantExp = minorExpAnnual1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal tosaniannualgrantExp = sanitationExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toneedannualgrantExp = needExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toprovannualgrantExp = provisionalExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal toannualgrantExptot = new BigDecimal(0);
                    toannualgrantExptot = toannualgrantExptot.add(towatcannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tobookannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tominannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(tosaniannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toneedannualgrantExp);
                    toannualgrantExptot = toannualgrantExptot.add(toprovannualgrantExp);
                    map.put("totalGrantExp", toannualgrantExptot);
                    BigDecimal towatcannualgrantt = watot;
                    BigDecimal tobookannualgrantt = botot;
                    BigDecimal tominannualgrantt = miantot;
                    BigDecimal tosaniannualgrantt = satot;
                    BigDecimal toneedannualgrantt = needtot;
                    BigDecimal toprovannualgrantt = pvtot;
                    BigDecimal toannualgranttott = new BigDecimal(0);
                    toannualgranttott = toannualgranttott.add(towatcannualgrantt);
                    toannualgranttott = toannualgranttott.add(tobookannualgrantt);
                    toannualgranttott = toannualgranttott.add(tominannualgrantt);
                    toannualgranttott = toannualgranttott.add(tosaniannualgrantt);
                    toannualgranttott = toannualgranttott.add(toneedannualgrantt);
                    toannualgranttott = toannualgranttott.add(toprovannualgrantt);
                    map.put("totalGrantBal", toannualgranttott);
                    //endtotal
                    BigDecimal exrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal exexp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal extot = exrel.subtract(exexp);
                    map.put("excurtionBal", extot);
                    BigDecimal serel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal seexp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal setot = serel.subtract(seexp);
                    map.put("selfdefBal", setot);
                    BigDecimal otrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otexp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ottot = otrel.subtract(otexp);
                    map.put("otherRecuGrantBal", ottot);
                    //totals          
                    BigDecimal excurrecurrel = excurtionReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurrel = selfdefReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurrel = otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutot = new BigDecimal(0);
                    recutot = recutot.add(excurrecurrel);
                    recutot = recutot.add(selfrecurrel);
                    recutot = recutot.add(otherrecurrel);
                    map.put("totalRecurReleased", recutot);
                    BigDecimal excurrecurExp = excurtionExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal selfrecurExp = civilReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal otherrecurExp = otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal recutotExp = new BigDecimal(0);
                    recutotExp = recutotExp.add(excurrecurExp);
                    recutotExp = recutotExp.add(selfrecurExp);
                    recutotExp = recutotExp.add(otherrecurExp);
                    map.put("totalRecurExp", recutotExp);
                    BigDecimal excurrecurt = extot;
                    BigDecimal selfrecurt = setot;
                    BigDecimal otherrecurt = ottot;
                    BigDecimal recutott = new BigDecimal(0);
                    recutott = recutott.add(excurrecurt);
                    recutott = recutott.add(selfrecurt);
                    recutott = recutott.add(otherrecurt);
                    map.put("totalRecurBal", recutott);
                    //endtotal
                    BigDecimal frel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal fexp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal ftot = frel.subtract(fexp);
                    map.put("furnitureBal", ftot);
                    BigDecimal labrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labexp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labtot = labrel.subtract(labexp);
                    map.put("labEquipBal", labtot);
                    //totals    
                    BigDecimal furnfurlabrel = furnitureReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabrel = labEquipReleased1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelreltot = furnfurlabrel.add(labfurlabrel);
                    map.put("totallabEquipReleased", furlabrelreltot);
                    BigDecimal furnfurlabExp = furnitureExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal labfurlabExp = labEquipExp1.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal furlabrelExptot = furnfurlabExp.add(labfurlabExp);
                    map.put("totallabEquipExp", furlabrelExptot);
                    BigDecimal furnfurlabt = ftot;
                    BigDecimal labfurlabt = labtot;
                    BigDecimal furlabrelttot = furnfurlabt.add(labfurlabt);
                    map.put("totallabEquipBal", furlabrelttot);
                    furniture_bal_sum = furniture_bal_sum.add(furnfurlabt);
                    lab_equipment_sum = lab_equipment_sum.add(labfurlabt);
                    map.put("furniture_bal_sum", furniture_bal_sum);
                    map.put("lab_equipment_sum", lab_equipment_sum);
                    map.put("totalSchools", rs.getString(59));
                    //endtotal
         
                    /////////Final totals in tfoot
                    civilNonRecTotal = civilNonRecTotal.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    majorNonRecTotal = majorNonRecTotal.add(majorReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorNonRecTotal = minorNonRecTotal.add(minorReleased1.setScale(2, RoundingMode.HALF_UP));
                    toiletNonRecTotal = toiletNonRecTotal.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totNonRecTotal = totNonRecTotal.add(tonorectot);

                    expcivilNonRecTotal = expcivilNonRecTotal.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    expmajorNonRecTotal = expmajorNonRecTotal.add(majorExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorNonRecTotal = expminorNonRecTotal.add(minorExp1.setScale(2, RoundingMode.HALF_UP));
                    exptoiletNonRecTotal = exptoiletNonRecTotal.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotNonRecTotal = exptotNonRecTotal.add(tonorectotExp);
                    totcivilNonRecTotal = totcivilNonRecTotal.add(civibaaltot);
                    totmajorNonRecTotal = totmajorNonRecTotal.add(majtot);
                    totminorNonRecTotal = totminorNonRecTotal.add(mintot);
                    tottoiletNonRecTotal = tottoiletNonRecTotal.add(toitot);
                    tottotNonRecTotal = tottotNonRecTotal.add(totmajexpbal);

                    waterAnnualGrantsTotal = waterAnnualGrantsTotal.add(waterReleased1.setScale(2, RoundingMode.HALF_UP));
                    booksAnnualGrantsTotal = booksAnnualGrantsTotal.add(booksReleased1.setScale(2, RoundingMode.HALF_UP));
                    minorAnnualGrantsTotal = minorAnnualGrantsTotal.add(minorReleasedAnnual1.setScale(2, RoundingMode.HALF_UP));
                    sanitationAnnualGrantsTotal = sanitationAnnualGrantsTotal.add(sanitationReleased1.setScale(2, RoundingMode.HALF_UP));
                    needAnnualGrantsTotal = needAnnualGrantsTotal.add(needReleased1.setScale(2, RoundingMode.HALF_UP));
                    provisiAnnualGrantsTotal = provisiAnnualGrantsTotal.add(provisionalReleased1.setScale(2, RoundingMode.HALF_UP));
                    totAnnualGrantsTotal = totAnnualGrantsTotal.add(toannualgranttot);

                    expwaterAnnualGrantsTotal = expwaterAnnualGrantsTotal.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    expbooksAnnualGrantsTotal = expbooksAnnualGrantsTotal.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    expminorAnnualGrantsTotal = expminorAnnualGrantsTotal.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    expsanitationAnnualGrantsTotal = expsanitationAnnualGrantsTotal.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    expneedAnnualGrantsTotal = expneedAnnualGrantsTotal.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    expprovisiAnnualGrantsTotal = expprovisiAnnualGrantsTotal.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));

                    exptotAnnualGrantsTotal = exptotAnnualGrantsTotal.add(toannualgrantExptot);
                    twaterAnnualGrantsTotal = twaterAnnualGrantsTotal.add(watot);
                    tbooksAnnualGrantsTotal = tbooksAnnualGrantsTotal.add(botot);
                    tminorAnnualGrantsTotal = tminorAnnualGrantsTotal.add(miantot);
                    tsanitationAnnualGrantsTotal = tsanitationAnnualGrantsTotal.add(satot);
                    tNeedAnnualGrantsTotalBalance = tNeedAnnualGrantsTotalBalance.add(needtot);
                    tneedAnnualGrantsTotal = tneedAnnualGrantsTotal.add(needexp);
                    tprovisiAnnualGrantsTotal = tprovisiAnnualGrantsTotal.add(pvtot);
                    ttotAnnualGrantsTotal = ttotAnnualGrantsTotal.add(toannualgranttott);

                    excurTriprecurringTotal = excurTriprecurringTotal.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    selfdefencerecurringTotal = selfdefencerecurringTotal.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    otherrecurringTotal = otherrecurringTotal.add(otherRecuGrantReleased1.setScale(2, RoundingMode.HALF_UP));
                    totrecurringTotal = totrecurringTotal.add(recutot);

                    excexcurTriprecurringTotal = excexcurTriprecurringTotal.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    excselfdefencerecurringTotal = excselfdefencerecurringTotal.add(selfdefExp1.setScale(2, RoundingMode.HALF_UP));
                    excotherrecurringTotal = excotherrecurringTotal.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));

                    exctotrecurringTotal = exctotrecurringTotal.add(recutotExp);
                    texcurTriprecurringTotal = texcurTriprecurringTotal.add(extot);
                    tselfdefencerecurringTotal = tselfdefencerecurringTotal.add(setot);
                    totherrecurringTotal = totherrecurringTotal.add(ottot);
                    ttotrecurringTotal = ttotrecurringTotal.add(recutott);

                    relfurFurniturLabEquipmentTotal = relfurFurniturLabEquipmentTotal.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    rellabFurniturLabEquipmentTotal = rellabFurniturLabEquipmentTotal.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    reltotFurniturLabEquipmentTotal = reltotFurniturLabEquipmentTotal.add(furlabrelreltot);

                    exfurFurniturLabEquipmentTotal = exfurFurniturLabEquipmentTotal.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));
                    exlabFurniturLabEquipmentTotal = exlabFurniturLabEquipmentTotal.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));

                    exltotFurniturLabEquipmentTotal = exltotFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tfurFurniturLabEquipmentTotal = tfurFurniturLabEquipmentTotal.add(furlabrelreltot);
                    tlabFurniturLabEquipmentTotal = tlabFurniturLabEquipmentTotal.add(furlabrelExptot);
                    tltotFurniturLabEquipmentTotal = tltotFurniturLabEquipmentTotal.add(furlabrelttot);


                   

             
                    //annual grants

                    map.put("civilNonRecTotal", civilNonRecTotal);
                    map.put("majorNonRecTotal", majorNonRecTotal);
                    map.put("minorNonRecTotal", minorNonRecTotal);
                    map.put("toiletNonRecTotal", toiletNonRecTotal);
                    map.put("totNonRecTotal", totNonRecTotal);

                    map.put("expcivilNonRecTotal", expcivilNonRecTotal);
                    map.put("expmajorNonRecTotal", expmajorNonRecTotal);
                    map.put("expminorNonRecTotal", expminorNonRecTotal);
                    map.put("exptoiletNonRecTotal", exptoiletNonRecTotal);
                    map.put("exptotNonRecTotal", exptotNonRecTotal);

                    map.put("totcivilNonRecTotal", totcivilNonRecTotal);
                    map.put("totmajorNonRecTotal", totmajorNonRecTotal);
                    map.put("totminorNonRecTotal", totminorNonRecTotal);
                    map.put("tottoiletNonRecTotal", tottoiletNonRecTotal);
                    map.put("tottotNonRecTotal", tottotNonRecTotal);

                    map.put("waterAnnualGrantsTotal", waterAnnualGrantsTotal);
                    map.put("booksAnnualGrantsTotal", booksAnnualGrantsTotal);
                    map.put("minorAnnualGrantsTotal", minorAnnualGrantsTotal);
                    map.put("sanitationAnnualGrantsTotal", sanitationAnnualGrantsTotal);
                    map.put("needAnnualGrantsTotal", needAnnualGrantsTotal);
                    map.put("provisiAnnualGrantsTotal", provisiAnnualGrantsTotal);
                    map.put("totAnnualGrantsTotal", totAnnualGrantsTotal);

                    map.put("expwaterAnnualGrantsTotal", expwaterAnnualGrantsTotal);
                    map.put("expbooksAnnualGrantsTotal", expbooksAnnualGrantsTotal);
                    map.put("expminorAnnualGrantsTotal", expminorAnnualGrantsTotal);
                    map.put("expsanitationAnnualGrantsTotal", expsanitationAnnualGrantsTotal);
                    map.put("expneedAnnualGrantsTotal", expneedAnnualGrantsTotal);
                    map.put("expprovisiAnnualGrantsTotal", expprovisiAnnualGrantsTotal);
                    map.put("exptotAnnualGrantsTotal", exptotAnnualGrantsTotal);

                    map.put("twaterAnnualGrantsTotal", twaterAnnualGrantsTotal);
                    map.put("tbooksAnnualGrantsTotal", tbooksAnnualGrantsTotal);
                    map.put("tminorAnnualGrantsTotal", tminorAnnualGrantsTotal);
                    map.put("tsanitationAnnualGrantsTotal", tsanitationAnnualGrantsTotal);
                    map.put("tNeedAnnualGrantsTotalBalance", tNeedAnnualGrantsTotalBalance);
                    map.put("tneedAnnualGrantsTotal", tneedAnnualGrantsTotal);
                    map.put("tprovisiAnnualGrantsTotal", tprovisiAnnualGrantsTotal);
                    map.put("ttotAnnualGrantsTotal", ttotAnnualGrantsTotal);

                    map.put("excurTriprecurringTotal", excurTriprecurringTotal);
                    map.put("selfdefencerecurringTotal", selfdefencerecurringTotal);
                    map.put("otherrecurringTotal", otherrecurringTotal);
                    map.put("totrecurringTotal", totrecurringTotal);

                    map.put("excexcurTriprecurringTotal", excexcurTriprecurringTotal);
                    map.put("excselfdefencerecurringTotal", excselfdefencerecurringTotal);
                    map.put("excotherrecurringTotal", excotherrecurringTotal);
                    map.put("exctotrecurringTotal", exctotrecurringTotal);

                    map.put("texcurTriprecurringTotal", texcurTriprecurringTotal);
                    map.put("tselfdefencerecurringTotal", tselfdefencerecurringTotal);
                    map.put("totherrecurringTotal", totherrecurringTotal);
                    map.put("ttotrecurringTotal", ttotrecurringTotal);

                    map.put("relfurFurniturLabEquipmentTotal", relfurFurniturLabEquipmentTotal);
                    map.put("rellabFurniturLabEquipmentTotal", rellabFurniturLabEquipmentTotal);
                    map.put("reltotFurniturLabEquipmentTotal", reltotFurniturLabEquipmentTotal);

                    map.put("exfurFurniturLabEquipmentTotal", exfurFurniturLabEquipmentTotal);
                    map.put("exlabFurniturLabEquipmentTotal", exlabFurniturLabEquipmentTotal);
                    map.put("exltotFurniturLabEquipmentTotal", exltotFurniturLabEquipmentTotal);

                    map.put("tfurFurniturLabEquipmentTotal", tfurFurniturLabEquipmentTotal);
                    map.put("tlabFurniturLabEquipmentTotal", tlabFurniturLabEquipmentTotal);
                    map.put("tltotFurniturLabEquipmentTotal", tltotFurniturLabEquipmentTotal);

                    BigDecimal CivilWorks_OB = null;
                    BigDecimal MajorRepairs_OB = null;
                    BigDecimal MinorRepairs_OB = null;
                    BigDecimal AnnualOtherRecurringGrants_OB = null;
                    BigDecimal Toilets_OB = null;
                    BigDecimal SelfDefenseTrainingGrants_OB = null;
                    BigDecimal Interest_OB = null;

                    if (rs.getString(33) == null) {
                        map.put("CivilWorks_OB", "0.00");
                    } else {
                        CivilWorks_OB = new BigDecimal(rs.getDouble(33));
                        map.put("CivilWorks_OB", CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(34) == null) {
                        map.put("MajorRepairs_OB", "0.00");
                    } else {
                        MajorRepairs_OB = new BigDecimal(rs.getDouble(34));
                        map.put("MajorRepairs_OB", MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(35) == null) {
                        map.put("MinorRepairs_OB", "0.00");
                    } else {
                        MinorRepairs_OB = new BigDecimal(rs.getDouble(35));
                        map.put("MinorRepairs_OB", MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(36) == null) {
                        map.put("AnnualOtherRecurringGrants_OB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_OB = new BigDecimal(rs.getDouble(36));
                        map.put("AnnualOtherRecurringGrants_OB", AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(37) == null) {
                        map.put("Toilets_OB", "0.00");
                    } else {
                        Toilets_OB = new BigDecimal(rs.getDouble(37));
                        map.put("Toilets_OB", Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(38) == null) {
                        map.put("SelfDefenseTrainingGrants_OB", "0.00");
                    } else {
                        SelfDefenseTrainingGrants_OB = new BigDecimal(rs.getDouble(38));
                        map.put("SelfDefenseTrainingGrants_OB", SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(39) == null) {
                        map.put("Interest_OB", "0.00");
                    } else {
                        Interest_OB = new BigDecimal(rs.getDouble(39));
                        map.put("Interest_OB", Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    }
                    BigDecimal total_OB = new BigDecimal(0);
                    total_OB = total_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    total_OB = total_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_OB", total_OB);

                    total_CivilWorks_OB = total_CivilWorks_OB.add(CivilWorks_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_OB", total_CivilWorks_OB);
                    total_MajorRepairs_OB = total_MajorRepairs_OB.add(MajorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_OB", total_MajorRepairs_OB);
                    total_MinorRepairs_OB = total_MinorRepairs_OB.add(MinorRepairs_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_OB", total_MinorRepairs_OB);
                    total_AnnualOtherRecurringGrants_OB = total_AnnualOtherRecurringGrants_OB.add(AnnualOtherRecurringGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_OB", total_AnnualOtherRecurringGrants_OB);
                    total_Toilets_OB = total_Toilets_OB.add(Toilets_OB.setScale(2, RoundingMode.HALF_UP));;
                    map.put("total_Toilets_OB", total_Toilets_OB);
                    total_SelfDefenseTrainingGrants_OB = total_SelfDefenseTrainingGrants_OB.add(SelfDefenseTrainingGrants_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_OB", total_SelfDefenseTrainingGrants_OB);
                    total_Interest_OB = total_Interest_OB.add(Interest_OB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_OB", total_Interest_OB);
                    total_total_total_OB = total_total_total_OB.add(total_OB);
                    map.put("total_total_total_OB", total_total_total_OB);
                    
                    BigDecimal CivilWorks_EOB = null;
                    BigDecimal MajorRepairs_EOB = null;
                    BigDecimal MinorRepairs_EOB = null;
                    BigDecimal AnnualOtherRecurringGrants_EOB = null;
                    BigDecimal Toilets_EOB = null;
                    BigDecimal SelfDefenseETrainingGrants_EOB = null;
                    BigDecimal Interest_EOB = null;
                    if (rs.getString(40) == null) {
                        map.put("CivilWorks_EOB", "0.00");
                    } else {
                        CivilWorks_EOB = new BigDecimal(rs.getDouble(40));
                        map.put("CivilWorks_EOB", CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(41) == null) {
                        map.put("MajorRepairs_EOB", "0.00");
                    } else {
                        MajorRepairs_EOB = new BigDecimal(rs.getDouble(41));
                        map.put("MajorRepairs_EOB", MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(42) == null) {
                        map.put("MinorRepairs_EOB", "0.00");
                    } else {
                        MinorRepairs_EOB = new BigDecimal(rs.getDouble(42));
                        map.put("MinorRepairs_EOB", MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(43) == null) {
                        map.put("AnnualOtherRecurringGrants_EOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_EOB = new BigDecimal(rs.getDouble(43));
                        map.put("AnnualOtherRecurringGrants_EOB", AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(44) == null) {
                        map.put("Toilets_EOB", "0.00");
                    } else {
                        Toilets_EOB = new BigDecimal(rs.getDouble(44));
                        map.put("Toilets_EOB", Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(45) == null) {
                        map.put("SelfDefenseETrainingGrants_EOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_EOB = new BigDecimal(rs.getDouble(45));
                        map.put("SelfDefenseETrainingGrants_EOB", SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(46) == null) {
                        map.put("Interest_EOB", "0.00");
                    } else {
                        Interest_EOB = new BigDecimal(rs.getDouble(46));
                        map.put("Interest_EOB", Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    BigDecimal total_EOB = new BigDecimal(0);
                    total_EOB = total_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    total_EOB = total_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_EOB", total_EOB);
                    
                    total_CivilWorks_EOB = total_CivilWorks_EOB.add(CivilWorks_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_EOB", total_CivilWorks_EOB);
                    total_MajorRepairs_EOB = total_MajorRepairs_EOB.add(MajorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_EOB", total_MajorRepairs_EOB);
                    total_MinorRepairs_EOB = total_MinorRepairs_EOB.add(MinorRepairs_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_EOB", total_MinorRepairs_EOB);
                    total_AnnualOtherRecurringGrants_EOB = total_AnnualOtherRecurringGrants_EOB.add(AnnualOtherRecurringGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_EOB", total_AnnualOtherRecurringGrants_EOB);
                    total_Toilets_EOB = total_Toilets_EOB.add(Toilets_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_EOB", total_Toilets_EOB);
                    total_SelfDefenseTrainingGrants_EOB = total_SelfDefenseTrainingGrants_EOB .add(SelfDefenseETrainingGrants_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_EOB", total_SelfDefenseTrainingGrants_EOB);
                    total_Interest_EOB = total_Interest_EOB.add(Interest_EOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_EOB", total_Interest_EOB);
                    total_total_total_EOB = total_total_total_EOB.add(total_EOB);
                    map.put("total_total_total_EOB", total_total_total_EOB);
                    
                   
                    
                    BigDecimal CivilWorks_UBOB = null;
                    BigDecimal MajorRepairs_UBOB = null;
                    BigDecimal MinorRepairs_UBOB = null;
                    BigDecimal AnnualOtherRecurringGrants_UBOB = null;
                    BigDecimal Toilets_UBOB = null;
                    BigDecimal SelfDefenseETrainingGrants_UBOB = null;
                    BigDecimal Interest_UBOB = null;
                    BigDecimal Released = null;

                    if (rs.getString(47) == null) {
                        map.put("CivilWorks_UBOB", "0.00");
                    } else {
                        CivilWorks_UBOB = new BigDecimal(rs.getDouble(47));
                        map.put("CivilWorks_UBOB", CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(48) == null) {
                        map.put("MajorRepairs_UBOB", "0.00");
                    } else {
                        MajorRepairs_UBOB = new BigDecimal(rs.getDouble(48));
                        map.put("MajorRepairs_UBOB", MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(49) == null) {
                        map.put("MinorRepairs_UBOB", "0.00");
                    } else {
                        MinorRepairs_UBOB = new BigDecimal(rs.getDouble(49));
                        map.put("MinorRepairs_UBOB", MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(50) == null) {
                        map.put("AnnualOtherRecurringGrants_UBOB", "0.00");
                    } else {
                        AnnualOtherRecurringGrants_UBOB = new BigDecimal(rs.getDouble(50));
                        map.put("AnnualOtherRecurringGrants_UBOB", AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(51) == null) {
                        map.put("Toilets_UBOB", "0.00");
                    } else {
                        Toilets_UBOB = new BigDecimal(rs.getDouble(51));
                        map.put("Toilets_UBOB", Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(52) == null) {
                        map.put("SelfDefenseETrainingGrants_UBOB", "0.00");
                    } else {
                        SelfDefenseETrainingGrants_UBOB = new BigDecimal(rs.getDouble(52));
                        map.put("SelfDefenseETrainingGrants_UBOB", SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(53) == null) {
                        map.put("Interest_UBOB", "0.00");
                    } else {
                        Interest_UBOB = new BigDecimal(rs.getDouble(53));
                        map.put("Interest_UBOB", Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(54) == null) {
                        map.put("Released", "0.00");
                    } else {
                        Released = new BigDecimal(rs.getDouble(54));
                        map.put("Released", Released.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    
                   
                    ubob_released_total = ubob_released_total.add(Released.setScale(2, RoundingMode.HALF_UP));
                    map.put("ubob_released_total", ubob_released_total);
                    
                    BigDecimal  total_UEOB = new BigDecimal(0);
                    total_UEOB = total_UEOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    total_UEOB = total_UEOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_UEOB", total_UEOB);
                    
                    total_CivilWorks_UBOB = total_CivilWorks_UBOB.add(CivilWorks_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_CivilWorks_UBOB", total_CivilWorks_UBOB);
                    total_MajorRepairs_UBOB = total_MajorRepairs_UBOB.add(MajorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MajorRepairs_UBOB", total_MajorRepairs_UBOB);
                    total_MinorRepairs_UBOB = total_MinorRepairs_UBOB.add(MinorRepairs_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_MinorRepairs_UBOB", total_MinorRepairs_UBOB);
                    total_AnnualOtherRecurringGrants_UBOB = total_AnnualOtherRecurringGrants_UBOB.add(AnnualOtherRecurringGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_AnnualOtherRecurringGrants_UBOB", total_AnnualOtherRecurringGrants_UBOB);
                    total_Toilets_UBOB = total_Toilets_UBOB.add(Toilets_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Toilets_UBOB", total_Toilets_UBOB);
                    total_SelfDefenseTrainingGrants_UBOB = total_SelfDefenseTrainingGrants_UBOB.add(SelfDefenseETrainingGrants_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_SelfDefenseTrainingGrants_UBOB", total_SelfDefenseTrainingGrants_UBOB);
                    total_Interest_UBOB = total_Interest_UBOB.add(Interest_UBOB.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Interest_UBOB", total_Interest_UBOB);
                    total_total_total_UBOB = total_total_total_UBOB.add(total_UEOB);
                    map.put("total_total_total_UBOB", total_total_total_UBOB);
                    
                    BigDecimal Earned_Interest=null;
                    BigDecimal Expenditure_Interest=null;
                    BigDecimal Balance_Interest=null;
                    BigDecimal RemmitanceAmount_ExpenditureIncurred=null;
                    
                    
                    if (rs.getString(55) == null) {
                        map.put("Earned_Interest", "0.00");
                    } else {
                        Earned_Interest = new BigDecimal(rs.getDouble(55));
                        map.put("Earned_Interest", Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(56) == null) {
                        map.put("Expenditure_Interest", "0.00");
                    } else {
                        Expenditure_Interest = new BigDecimal(rs.getDouble(56));
                        map.put("Expenditure_Interest", Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(57) == null) {
                        map.put("Balance_Interest", "0.00");
                    } else {
                        Balance_Interest = new BigDecimal(rs.getDouble(57));
                        map.put("Balance_Interest", Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    }
                    if (rs.getString(58) == null) {
                        map.put("RemmitanceAmount_ExpenditureIncurred", "0.00");
                    } else {
                        RemmitanceAmount_ExpenditureIncurred = new BigDecimal(rs.getDouble(58));
                        map.put("RemmitanceAmount_ExpenditureIncurred", RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    }
                    
                    map.put("distName", rs.getString(59));
                    map.put("MandalName", rs.getString(60));
                    map.put("bankAccNum", rs.getString(61));
                    map.put("ifsc", rs.getString(62));
                    map.put("bankName", rs.getString(63));
                    
                    total_Earned_Interest = total_Earned_Interest.add(Earned_Interest);
                    total_Earned_Interest = total_Earned_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Earned_Interest", total_Earned_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.add(Expenditure_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest.setScale(2, RoundingMode.HALF_UP);
                    map.put("total_Expenditure_Interest", total_Expenditure_Interest);
                    total_Balance_Interest = total_Balance_Interest.add(Balance_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_Balance_Interest", total_Balance_Interest);
                    total_RemmitanceAmount_ExpenditureIncurred = total_RemmitanceAmount_ExpenditureIncurred.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("total_RemmitanceAmount_ExpenditureIncurred", total_RemmitanceAmount_ExpenditureIncurred);
                    
                    BigDecimal  totalRealse_Realses = new BigDecimal(0);
                    //totalRealse_Realses = totalRealse_Realses.add(Released.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(furnitureReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(labEquipReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(civilReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(toiletsReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(selfdefReleased1.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_Realses = totalRealse_Realses.add(excurtionReleased1.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_Realses", totalRealse_Realses);
                    
                    BigDecimal  totalRealse_total = new BigDecimal(0);
                    totalRealse_total = totalRealse_total.add(total_EOB);
                    totalRealse_total = totalRealse_total.add(Earned_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("totalRealse_total", totalRealse_total);
                    
                    total_total_OB = total_total_OB.add(total_OB);
                    map.put("total_total_OB", total_total_OB);
                    total_totalRealse_Realses = total_totalRealse_Realses.add(totalRealse_Realses);
                    map.put("total_totalRealse_Realses", total_totalRealse_Realses);
                    total_Earned_Interest_total = total_Earned_Interest_total.add(Earned_Interest);
                    map.put("total_Earned_Interest_total",total_Earned_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    totalRealse_total_total = totalRealse_total_total.add(totalRealse_total);
                    map.put("totalRealse_total_total", totalRealse_total_total);
                    
                    BigDecimal  Grand_Total_Expenditure_Incurred_on_Releases = new BigDecimal(0);
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(waterExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(booksExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(minorExpAnnual1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(sanitationExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(needExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(provisionalExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(furnitureExp1.setScale(2, RoundingMode.HALF_UP));

                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(labEquipExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(civilExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(toiletsExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(excurtionExp1.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Expenditure_Incurred_on_Releases = Grand_Total_Expenditure_Incurred_on_Releases.add(otherRecuGrantExp1.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases", Grand_Total_Expenditure_Incurred_on_Releases);
                    
                    BigDecimal  Grand_Total_Total_Expenditure = new BigDecimal(0);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(total_EOB);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    Grand_Total_Total_Expenditure = Grand_Total_Total_Expenditure.add(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("Grand_Total_Total_Expenditure", Grand_Total_Total_Expenditure);
                    
                    BigDecimal close_balnce_ub_ob = total_OB.subtract(total_UEOB);
                    map.put("close_balnce_ub_ob", close_balnce_ub_ob);
                    BigDecimal close_balnce_ub_rb = totalRealse_Realses.subtract(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("close_balnce_ub_rb", close_balnce_ub_rb);
                    
                    BigDecimal close_balnce_ub_ineterest = (Earned_Interest.setScale(2, RoundingMode.HALF_UP)).subtract(Expenditure_Interest.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_ineterest", close_balnce_ub_ineterest);
                    
                    BigDecimal  close_balnce_ub_total = new BigDecimal(0);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_rb);
                    close_balnce_ub_total = close_balnce_ub_total.add(close_balnce_ub_ineterest);
                    close_balnce_ub_total = close_balnce_ub_total.subtract(RemmitanceAmount_ExpenditureIncurred.setScale(2, RoundingMode.HALF_UP));
                    map.put("close_balnce_ub_total", close_balnce_ub_total);
                    
                    Grand_Total_Expenditure_Incurred_on_Opening_balances = Grand_Total_Expenditure_Incurred_on_Opening_balances.add(total_EOB);
                    map.put("Grand_Total_Expenditure_Incurred_on_Opening_balances", Grand_Total_Expenditure_Incurred_on_Opening_balances);
                    Grand_Total_Expenditure_Incurred_on_Releases_total = Grand_Total_Expenditure_Incurred_on_Releases_total.add(Grand_Total_Expenditure_Incurred_on_Releases);
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases_total", Grand_Total_Expenditure_Incurred_on_Releases_total);
                    total_Expenditure_Interest_total = total_Expenditure_Interest_total.add(Expenditure_Interest);
                    map.put("total_Expenditure_Interest_total", total_Expenditure_Interest_total.setScale(2, RoundingMode.HALF_UP));
                    total_RemmitanceAmount_ExpenditureIncurred_total = total_RemmitanceAmount_ExpenditureIncurred_total.add(total_RemmitanceAmount_ExpenditureIncurred);
                    map.put("total_RemmitanceAmount_ExpenditureIncurred_total", total_RemmitanceAmount_ExpenditureIncurred_total);
                    Grand_Total_Total_Expenditure_total = Grand_Total_Total_Expenditure_total.add(Grand_Total_Total_Expenditure);
                    map.put("Grand_Total_Total_Expenditure_total", Grand_Total_Total_Expenditure_total);

                    
                    close_balnce_ub_ob_total = close_balnce_ub_ob_total.add(close_balnce_ub_ob);
                    map.put("close_balnce_ub_ob_total", close_balnce_ub_ob_total);
                    close_balnce_ub_rb_total = close_balnce_ub_rb_total.add(close_balnce_ub_rb);
                    map.put("close_balnce_ub_rb_total", close_balnce_ub_rb_total);
                    close_balnce_ub_ineterest_total = close_balnce_ub_ineterest_total.add(close_balnce_ub_ineterest);
                    map.put("close_balnce_ub_ineterest_total", close_balnce_ub_ineterest_total);
                    close_balnce_ub_total_total = close_balnce_ub_total_total.add(close_balnce_ub_total);
                    map.put("close_balnce_ub_total_total", close_balnce_ub_total_total);
                    
                    schoollist.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoollist;
    }
   
}
