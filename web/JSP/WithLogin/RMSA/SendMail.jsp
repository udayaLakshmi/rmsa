<%--
    Document   : NFBS
    Created on : Jul 18, 2013, 11:56:17 AM
    Author     : 747577
--%>

<%@page import="javax.sound.midi.SysexMessage"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html:html>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        int i = 0;
    %>
    <script>
        function hodcheckAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].checkall1[0].checked === true)
                {
                    document.getElementById("sendButton").style.display = "";
                    elems[i].checked = true;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function hodremoveAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].checkall1[1].checked === true)
                {
                    document.getElementById("sendButton").style.display = "none";
                    elems[i].checked = false;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }

        function rdocheckeAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].rdocheckall[0].checked === true)
                {
                    document.getElementById("sendButton").style.display = "";
                    elems[i].checked = true;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function rdoremoveAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].rdocheckall[1].checked === true)
                {
                    document.getElementById("sendButton").style.display = "none";
                    elems[i].checked = false;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }

        function mlcheckedAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].mlcheck[0].checked === true)
                {
                    document.getElementById("sendButton").style.display = "";
                    elems[i].checked = true;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function mlremoveAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].mlcheck[1].checked === true)
                {
                    document.getElementById("sendButton").style.display = "none";
                    elems[i].checked = false;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function vlcheckedAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].vlcheck[0].checked === true)
                {
                    document.getElementById("sendButton").style.display = "";
                    elems[i].checked = true;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function vlremoveAll() {
            var elems = document.getElementsByName("email");
            for (var i = 0; i < elems.length; i++)
            {
                if (document.forms[0].vlcheck[1].checked === true)
                {
                    document.getElementById("sendButton").style.display = "none";
                    elems[i].checked = false;
                }
                else {
                    elems[i].checked = false;
                }
            }
        }
        function textCounter(field, cntfield, maxlimit) {
            if (field.value.length > maxlimit) {  // if too long...trim it!
                field.value = field.value.substring(0, maxlimit);
                // otherwise, update 'characters left' counter
            }
            else {
                cntfield.value = maxlimit - field.value.length;
            }
        }


        function validate() {
            var subject = $.trim($("#subject").val());
            var body = $.trim($("#body").val());
            if (subject === "") {
                alert('Subject field cannot be empty');
                $("#subject").focus();
                return false;
            } else if (body === "") {
                alert('Body field cannot be empty');
                $("#body").focus();
                return false;
            }
            if ($('#offtype').val() === '00') {
                alert('Please select Officer Type');
                $('#offtype').focus();
                return false;
            }
            var k = false;
            $('.distoff').each(function() {


                if (this.checked) {

                    k = true;
                }


            });
            if (!k) {
                alert("Atleast one contact must be selected");
                return false();
            }
            getDetails();
        }
        function getDetails() {
            var d = document.forms[0];
            var msg = "Are you sure do you want send EMail";
            var accept = confirm(msg);
            if (accept)
            {
                document.forms[0].elements['mode'].value = "sendMail";
                //d.mode.value='sendSMS';
                d.submit();
                document.getElementById('hide').style.display = 'none';
                document.getElementById('loding').style.display = '';
            }
        }
        function getDepartmentContactDetailsMail(value) {
            document.forms[0].elements['mode'].value = "getDepartmentContactDetailsMail";
            if (value === "hod") {
                // document.getElementById("hod1").style.display="";
            }
            document.forms[0].submit();

        }
        function getOfficersList(value) {
            if (value === "hod") {
                // document.getElementById("hod1").style.display="";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "none";
                document.forms[0].elements['mode'].value = "getSelectedListmails";
                document.forms[0].submit();

            } else if (value === "rdo") {
                document.getElementById("rdo1").style.display = "";
                //  document.getElementById("hod1").style.display="none";

                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "none";
                document.forms[0].elements['mode'].value = "getSelectedListmails";
                document.forms[0].submit();
            } else if (value === "ml") {
                // document.getElementById("hod1").style.display="none";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "block";
                document.getElementById("vl1").style.display = "none";
                document.forms[0].elements['mode'].value = "getSelectedListmails";
                document.forms[0].submit();
            } else if (value === "vl") {
                // document.getElementById("hod1").style.display="none";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "block";
                document.forms[0].elements['mode'].value = "getSelectedListmails";
                document.forms[0].submit();
            }
        }
        function getTahasildar() {

            document.forms[0].elements['mode'].value = "getTahasildarmails";
            document.forms[0].submit();
        }
        function getMpdo() {

            document.forms[0].elements['mode'].value = "getMpdomails";
            document.forms[0].submit();
        }
        function loadHod() {
            var value = document.forms[0].elements['offtype'].value;

            if (value === "hod") {

                document.getElementById("hod1").style.display = "";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "none";

            } else if (value === "rdo") {

                document.getElementById("rdo1").style.display = "";
                document.getElementById("hod1").style.display = "none";

                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "none";

            } else if (value === "ml") {
                document.getElementById("hod1").style.display = "none";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "";
                document.getElementById("vl1").style.display = "none";

            } else if (value === "vl") {
                document.getElementById("hod1").style.display = "none";
                document.getElementById("rdo1").style.display = "none";
                document.getElementById("ml1").style.display = "none";
                document.getElementById("vl1").style.display = "";

            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            $('#search').keyup(function() {
                searchTable($(this).val());
            });
        });
        $(document).ready(function() {
            $('#search1').keyup(function() {
                searchTable1($(this).val());
            });
        });
        $(document).ready(function() {
            $('#search2').keyup(function() {
                searchTable2($(this).val());
            });
        });
        $(document).ready(function() {
            $('#search3').keyup(function() {
                searchTable3($(this).val());
            });
        });
        $(document).ready(function() {
            $('#search4').keyup(function() {
                searchTable4($(this).val());
            });
        });
        $(document).ready(function() {
            $('#search5').keyup(function() {
                searchTable5($(this).val());
            });
        });
        function searchTable(inputVal) {
            var table = $('#data');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found === true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function searchTable1(inputVal) {
            var table = $('#data1');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found == true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function searchTable2(inputVal) {
            var table = $('#data2');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found === true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function searchTable3(inputVal) {
            var table = $('#data3');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found == true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function searchTable4(inputVal) {
            var table = $('#data4');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found == true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function searchTable5(inputVal) {
            var table = $('#data5');
            table.find('tr').each(function(index, row) {
                var allCells = $(row).find('td');
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, 'i');
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    if (found == true)
                        $(row).show();
                    else
                        $(row).hide();
                }
            });
        }
        function validateButton() {
            var k = false;
            $('.distoff').each(function() {
                if (this.checked) {
                    document.getElementById("sendButton").style.display = "";
                    k = true;
                }
                if (k === false) {
                    document.getElementById("sendButton").style.display = "none";
                }
            });

        }
    </script>
    <script type="text/javascript">
        var reg, username = "";
        $(document).ready(function() {
            if ($('#usr1').val() !== null)
                username = $('#usr1').val();
         <%--       reg = "\n\<br/><br/><br/><br/><br/>Thanks & Regards<br/>\n" + username + "\n\ --%>
            
          reg = "\n\<br/><br/><br/><br/><br/>Thanks & Regards<br/>\n" +""+"\n\
 RMSA.<br/><br/>\n\
<center><font color='red'><b>Note :</b><i> Its an Auto Generated email.Please do not reply to this email.</i></font></center>";
        });

    </script>
    <head>
        <%--    <script type="text/javascript" src="<%=basePath%>js/ckeditor.js"></script>
            <script src="<%=basePath%>js/sample.js" type="text/javascript"></script>
            <link href="<%=basePath%>css/sample.css" rel="stylesheet" type="text/css" /> --%>

        <script type="text/javascript" src="<%=basePath%>ckeditor/ckeditor.js"></script>
        <script src="<%=basePath%>ckeditor/sample.js" type="text/javascript"></script>
        <link href="<%=basePath%>ckeditor/sample.css" rel="stylesheet" type="text/css" />


        <style>
            .cke_skin_office2003 .cke_top {
                border-top: solid 1px #fafaf5;
                border-left: solid 1px #fafaf5;
                border-right: solid 1px #696969;
                border-bottom: solid 2px #696969;
            }
            html .cke_skin_office2003 {
                visibility: inherit;
            }
            table.altrowstable th {
                background-color: #0E94C5;
                text-align: center;
                border-bottom: 1px #083254 solid;
                padding-left: 3px;
                padding-right: 3px;
                border-left: 1px #083254 solid;
                vertical-align: middle;
                font-size: 11px;
                font-family: verdana;
                height: 35px;
                font-weight: bold;
                color: #fff;
                padding: 5px;
            }
            table.altrowstable td {
                text-align: left;
                border-bottom: 1px #083254 solid;
                border-left: 1px #083254 solid;
                border-right:1px #083254 solid;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                height: 30px;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable {
                border-right: 1px #083254 solid;
                border-top: 1px #083254 solid;
            }
            .cke_skin_office2003 .cke_top {
                border-top: solid 1px #fafaf5;
                border-left: solid 1px #fafaf5;
                border-right: solid 1px #696969;
                border-bottom: solid 2px #696969;
            }

        </style>
    </head>

    <body >
        <html:form action="/sendMail"  enctype="multipart/form-data">
            <html:hidden property="mode"/>
            <html:hidden property="officerPost"/>



            <logic:present name="msg" scope="request">
                <script language="javascript" type="text/javascript">
        alert("${msg}");
                </script>
            </logic:present>
            <logic:present name="nodata">
                <center><font color="red"><bean:write name="nodata"/></font></center>
                    </logic:present>
                    <%if (session.getAttribute("name") != null) {%>
            <input type="hidden" value="<%=session.getAttribute("name").toString()%>" class="table"/>
            <%}%>
    <section class="testimonial_sec clear_fix">
          <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="innerbodyrow">
            <table width="100%" border="0" cellspacing="0" cellpadding="5" id="Table7">
                <div class="col-xs-12"></div>
                <tr>
                    <td height="25" align="left" valign="middle" class="grn-hd-txt"><h3>Email</h3></td>
                </tr>
                </div>
                <tr>
                    <td>
                        <table align="center" cellpadding="5" cellspacing="0" border="0" width="100%" id="altrowstable" class="altrowstable">
                            <tr class="tr1">
                                <th width="10%" class="formbg1" style="text-align: left" align="left">
                                    <strong>Subject<font color="red">*</font>&nbsp;</strong>
                                </th>
                                <td width="28%" class="formbg2" style="text-align: left" align="left" colspan="5">
                                    <html:text property="subject" styleId="subject" style="height:25px;width=250px" size="100" onkeypress="return space(event,this)" maxlength="200"/>

                                </td>
                            </tr>
                            <tr class="tr1">
                                <th width="10%" class="formbg1" style="text-align: left" align="left">
                                    <strong>Body<font color="red"></font>&nbsp;</strong>
                                </th>
                                <td width="28%" class="formbg2" align="left" colspan="5">
                                    <html:textarea property="body" rows="5" cols="50" styleId="body" />

                                </td>
                            </tr>
                            <tr class="tr1">
                                <th width="10%" class="formbg1"  style="text-align: left" align="left"><b>UploadFile</b></th>
                                <td colspan="5" align="left" style="text-align: left">
                                    <html:file property="uploadFile1"  style="width=250px"   />
                                </td>
                            </tr>
                            <tr class="tr1">
                                <th width="10%" class="formbg1" style="text-align: left" align="left"><b>UploadFile</b></th>
                                <td colspan="5" align="left" style="text-align: left">
                                    <html:file property="uploadFile2"  style="width=250px"  />
                                </td>
                            </tr>
                            <tr class="tr1">
                                <th width="10%" class="formbg1" style="text-align: left" align="left"><b>UploadFile</b></th>
                                <td colspan="5" align="left" style="text-align: left">
                                    <html:file property="uploadFile3"  style="width=250px"  />
                                </td>

                            </tr>
                            <script type="text/javascript">
                                var text;
                                $(document).ready(function() {
                                    text = document.forms[0].elements['body'].value;
                                    CKEDITOR.replace('body',
                                            {
                                                fullPage: true
                                            });
                                    if (CKEDITOR.instances['body'].getData().length == 0) {
                                        CKEDITOR.instances['body'].setData(reg);
                                    }
                                    else {
                                        CKEDITOR.instances['body'].setData(text);

                                    }
                                });

                            </script>
                            <tr id="officerTp" >
                                <th width="20%"  class="formbg1" style="text-align: left">

                                    <strong>  Officers Type <font color="red" >*</font>&nbsp;</strong>
                                </th>
                                <td width="80%" class="gridbg3" style="text-align: left" >
                                    <html:select property="officerType" styleId="offtype" onchange="getOfficersList(this.value);" style="width=250px">
                                        <html:option value="00">-- Select --</html:option>
                                        <html:option value="rdo">School's List</html:option>

                                        <%--     <html:option value="hod">HOD Officers</html:option>
                                        <html:option value="rdo">RJD Officers</html:option>
                                        <html:option value="rdo">DEO Officers</html:option>
                                        <html:option value="ml">MEO Officers</html:option>
                                        <html:option value="vl">Principal Officers</html:option> --%>
                                    </html:select>
                                </td>


                            </tr>
                            <!--                            <tr style="display: none" id="hod1">
                                                            <td width="20%" class="formbg1" style="text-align: left"> <strong> Department : </strong> </td>
                                                            <td class="gridbg3" width="80%"><html:select property="department" styleId="hod"  onchange="getDepartmentContactDetailsMail(this.id);" >
                                <html:option value="00">--ALL--</html:option>
                                <html:optionsCollection property="departmentList" label="departmentName" value="departmentId"/>
                            </html:select>
                        </td>
                    </tr>-->
                            <tr  id="hod">
                                <logic:notEmpty name="deptlist" >

                                    <td width="20%" class="formbg1" style="text-align: left">
                                        <strong>HOD's<font color="red">*</font>&nbsp;</strong>
                                    </td>
                                    <td width="80%" class="gridbg3" style="text-align: left">
                                        Select All <input type="radio" name="checkall1" onclick="hodcheckAll();" style="border: none;width: 40px">
                                        Deselect All <input type="radio" name="checkall1" onclick="hodremoveAll();" style="border: none;width: 40px">
                                        <br/> <br/>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="gridtb1" id="data">
                                            <tr>
                                                <th class="grn-hd-txt" colspan="2" style="text-align: left">District Officers</th>
                                                <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search" title="Search" style="width:auto;color:#000;" />
                                                </th>
                                            </tr>
                                            <%i = 2;%>
                                            <tr class="tr1" id="one" >
                                                <logic:iterate name="deptlist" id="deff">
                                                    <% if (i > 1) {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${deff.id}" onclick="validateButton();"/>${deff.name}(${deff.department})
                                                    </td>

                                                    <%  i--;
                                                    } else {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${deff.id}" onclick="validateButton();"/>${deff.name}(${deff.department})
                                                    </td>
                                                </tr>
                                                <tr class="tr1" id="one">
                                                    <% i = 2;
                                                        }%>
                                                </logic:iterate>
                                            </tr>
                                        </table>
                                    </td>

                                </logic:notEmpty>
                            </tr>
                            <tr id="rdo1" style="display: none">

                                <logic:notEmpty name="rdoList">
                                    <td width="20%" class="formbg1" align="left">
                                        <strong>School's<font color="red">*</font>&nbsp;</strong>
                                    </td>
                                    <td width="80%" class="gridbg3" align="left">
                                        Select All <input type="radio" name="rdocheckall" onclick="rdocheckeAll();" style="border: none;width: 40px">
                                        Deselect All <input type="radio" name="rdocheckall" onclick="rdoremoveAll();" style="border: none;width: 40px"><br/>
                                        <br/> <br/>
                                        <logic:present name="rdoList">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="gridtb1" id="data1">
                                                <tr>
                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">School Teacher's</th>
                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search1" title="Search" style="width:auto;color:#000;" />
                                                    </th>
                                                </tr>
                                                <%i = 2;%>
                                                <tr class="tr1" >
                                                    <logic:iterate name="rdoList" id="rdos">
                                                        <% if (i > 1) {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" styleClass="distoff" value="${rdos.id}" onclick="validateButton();"/>${rdos.name}
                                                        </td>

                                                        <%  i--;
                                                        } else {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" styleClass="distoff" value="${rdos.id}" onclick="validateButton();"/>${rdos.name}
                                                        </td>
                                                    </tr>
                                                    <tr class="tr1" id="one">
                                                        <% i = 2;
                                                            }%>
                                                    </logic:iterate>
                                                </tr>
                                            </table>
                                        </logic:present>
                                    </td>
                                </logic:notEmpty>
                            </tr>
                            <%if (request.getAttribute("mpdoList") != null) {%>
                            <tr id="ml1" style="display: block">
                                <%} else {%>
                            <tr id="ml1" style="display: none">
                                <%}%>
                                <td width="20%" class="formbg1" align="left">
                                    <strong>Mandal Level Officers<font color="red">*</font>&nbsp;</strong>
                                </td>
                                <td width="80%" class="gridbg3" align="left">
                                    <!--                                    Tahsildar<html:radio property="radioButtonProperty" value="Tahasildar" onclick="getTahasildar();" style="border: none;width:45px"/>
                                                                        MPDO<html:radio property="radioButtonProperty" value="MPDO" onclick="getMpdo();" style="border: none;width:110px"/><br/>-->

                                    Select All <input type="radio" name="mlcheck" onclick="mlcheckedAll();" style="border: none;width: 40px">
                                    Deselect All <input type="radio" name="mlcheck" onclick="mlremoveAll();" style="border: none;width: 40px"><br/><br/>
                                    <logic:present name="list">

                                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="gridtb1" ID="data2">
                                            <tr>
                                                <th class="grn-hd-txt" colspan="2" style="text-align: left">Tahsildars</th>
                                                <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search2" title="Search" style="width:auto;color:#000;" />
                                                </th>
                                            </tr>

                                            <%i = 2;%>
                                            <tr class="tr1" id="one">

                                                <logic:iterate name="list" id="tash">


                                                    <% if (i > 1) {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${tash.id}" onclick="validateButton();"/>&nbsp;${tash.name}&nbsp(${tash.mandal})
                                                    </td>

                                                    <%  i--;
                                                    } else {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${tash.id}" onclick="validateButton();"/>&nbsp;${tash.name}&nbsp;(${tash.mandal})
                                                    </td>
                                                </tr>
                                                <tr class="tr1" id="one">
                                                    <% i = 2;
                                                        }%>
                                                </logic:iterate>
                                            </tr>
                                        </table>
                                        <hr style="color: red">
                                    </logic:present>

                                    <logic:present name="mpdoList">

                                        <br/> <br/>
                                        <table cellpadding="5" cellspacing="0" border="0" width="100%"  class="gridtb1" ID="data3">
                                            <tr>
                                                <th class="grn-hd-txt" colspan="2" style="text-align: left">MPDOs</th>
                                                <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search3" title="Search" style="width:auto;color:#000;" />
                                                </th>
                                            </tr>

                                            <br/>
                                            <%i = 2;%>
                                            <tr class="tr1" id="one">
                                                <logic:iterate name="mpdoList" id="mpd">
                                                    <% if (i > 1) {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${mpd.id}" onclick="validateButton();"/>&nbsp;${mpd.name}&nbsp;(${mpd.mandal})
                                                    </td>

                                                    <%  i--;
                                                    } else {%>
                                                    <td class="gridbg0" colspan="2">
                                                        <html:checkbox property="email" styleClass="distoff" value="${mpd.id}" onclick="validateButton();"/>&nbsp;${mpd.name}&nbsp;(${mpd.mandal})
                                                    </td>
                                                </tr>
                                                <tr class="tr1" id="one">
                                                    <% i = 2;
                                                        }%>
                                                </logic:iterate>
                                            </tr>
                                        </table>
                                    </logic:present>
                                </td>
                            </tr>
                            <tr id="vl1" style="display: none">
                                <logic:notEmpty name="vlList">

                                    <td width="20%" class="formbg1" align="left">
                                        <strong>Village Level Officers<font color="red">*</font>&nbsp;</strong>
                                    </td>
                                    <td width="80%" class="gridbg3" align="left" >
                                        <logic:present name="vlList">
                                            Select All <input type="radio" name="vlcheck" onclick="vlcheckedAll();" style="border: none;width: 40px">
                                            Deselect All <input type="radio" name="vlcheck" onclick="vlremoveAll();" style="border: none;width: 40px"><br/>
                                            <br/> <br/>
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" id="data4">
                                                <tr>
                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">Village Revenue Officer</th>
                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search4" title="Search" style="width:auto;color:#000;" />
                                                    </th>
                                                </tr>
                                                <%i = 2;%>
                                                <tr class="tr1" id="one">
                                                    <logic:iterate name="vlList" id="vr">
                                                        <% if (i > 1) {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" styleClass="distoff" value="${vr.id}" onclick="validateButton();"/>&nbsp;${vr.name}&nbsp;(${vr.mandal}/${vr.village})
                                                        </td>

                                                        <%  i--;
                                                        } else {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" styleClass="distoff" value="${vr.id}" onclick="validateButton();"/>&nbsp;${vr.name}&nbsp;(${vr.mandal}/${vr.village})
                                                        </td>
                                                    </tr>
                                                    <tr class="tr1" id="one">
                                                        <% i = 2;
                                                            }%>

                                                    </logic:iterate>
                                                </tr>
                                            </table>
                                            <hr style="color: red">
                                        </logic:present>

                                        <logic:present name="panchayatSecretaryList">

                                            <br/> <br/>
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%"  class="gridtb1" ID="data5">
                                                <tr>
                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">Panchayat Secretary Officers</th>
                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search5" title="Search" style="width:auto;color:#000;" />
                                                    </th>
                                                </tr>

                                                <br/>
                                                <%i = 2;%>
                                                <tr class="tr1" id="one">
                                                    <logic:iterate name="panchayatSecretaryList" id="panchayat">
                                                        <% if (i > 1) {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" value="${panchayat.id}" onclick="validateButton();"/>&nbsp;${panchayat.name}&nbsp;(${panchayat.mandal}/${panchayat.village})
                                                        </td>

                                                        <%  i--;
                                                        } else {%>
                                                        <td class="gridbg0" colspan="2">
                                                            <html:checkbox property="email" styleClass="distoff" value="${panchayat.id}" onclick="validateButton();"/>&nbsp;${panchayat.name}&nbsp;(${panchayat.mandal}/${panchayat.village})
                                                        </td>
                                                    </tr>
                                                    <tr class="tr1" id="one">
                                                        <% i = 2;
                                                            }%>
                                                    </logic:iterate>
                                                </tr>
                                            </table>
                                        </logic:present>
                                    </td>

                                </logic:notEmpty>
                            </tr>

                            <tr >
                                <td colspan="6" align="center" valign="middle" class="gridbg3" style="text-align:center;display: none" id="sendButton">
                                    <html:button value="Send" property="Send" onclick="validate();" />
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table> <br><br>
                   </div></div></div></div>          </section>   
            <br/>
            <logic:present name="DEPTHODDETAILS">
                <center>
                    <font color="red" size="2">

                        <bean:write name="DEPTHODDETAILS"/>

                    </font>
                </center>
            </logic:present>
            <logic:present name="tottalFileSize">
                <center>
                    <font color="red" size="2">

                        <bean:write name="tottalFileSize"/>

                    </font>
                </center>
            </logic:present><br/>
            <div id="valid_msg"></div>
        </html:form>
        <script>
                                loadHod();
        </script>

    </body>
</html:html>