<%  String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    response.addHeader("X-FRAME-OPTIONS", "DENY");
    response.addHeader("X-Xss-Protection", "1; mode=block");
    response.addHeader("X-Content-Type-Options", "nosniff");
    response.addHeader("Content-Security-Policy", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
    response.addHeader("Referrer-Policy", "no-referrer");
    response.addHeader("Strict-Transport-Security", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
    response.addHeader("Feature-Policy", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
%>
<%-- 
    Document   : index.jsp
    Created on : Jan 4, 2018, 11:32:38 AM
    Author     : 1250892
--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld"  prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- For Resposive Device -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: RMSA ::</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="js/jquery-2.1.4.js"></script> 
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Stroke Gap Icon -->
        <!-- owl-carousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <!-- Custom Css -->
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
        <script src="js/md5.js" type="text/javascript"></script>

        <script>

            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 46) {
                    return true;
                } else if (charCode === 13) {
                    myFunction();
                } else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allow Numbers Only");
                    return false;
                }
                return true;
            }

            $(document).ready(function() {
                $('input, :input').attr('autocomplete', 'off');
                $("#loginClick").click(function() {
                    var orgFinal = "";
                    var dupFinal = "";
                    for (var i = 1; i <= 5; i++) {
                        var a = Math.ceil(Math.random() * 9);
                        orgFinal = orgFinal + a;
                        dupFinal = dupFinal + a + ' ';
                    }
                    $("#userName").val("");
                    $("#passWord").val("");
                    $("#captchaCode1").val("");
                    $("#captchaCode").val("");
                    $("#captchaCode2").val("");
                    $("#captchaCode2").val(orgFinal);
                    $("#captchaCode").val(dupFinal);
                });

                $("#subForm").click(function() {
                    myFunction();
                });

            });

            function myFunction() {
                var userName = $("#userName").val();
                var password = $("#passWord").val();
                var captchaCode = $("#captchaCode1").val();
                var trimCaptchaCode = $("#captchaCode2").val();
                if (userName === "" || userName === undefined) {
                    $("#userName").focus();
                    alert("Enter UserName");
                } else if (password === "" || password === undefined) {
                    $("#passWord").focus();
                    alert("Enter Password");
                } else if (captchaCode === "" || captchaCode === undefined) {
                    $("#captchaCode1").focus();
                    alert("Enter Capture Code");
                } else if (trimCaptchaCode !== captchaCode) {
                    $("#captchaCode1").focus();
                    alert("Enter Valid Capture Code");
                } else {
                    document.forms[0].mode.value = "checkDetails";
                    document.forms[0].userName.value = $("#userName").val();
                    document.forms[0].password.value = calcMD5($("#passWord").val());
                    document.forms[0].submit();
                }
            }



        </script>
        <SCRIPT TYPE="TEXT/JAVASCRIPT" SRC="https://gistlangserver.in/Content/WSLToolbar/Scripts/Snippets/js/cdac-gist-translation-snippet.min.js"></SCRIPT>
        <script type='text/javascript'>
            var isCtrl = false;
            document.onkeyup = function(e) {
                if (e.which === 17)
                    isCtrl = false;
                if (e.which === 16)
                    isCtrl = false;
            };
            document.onkeydown = function(e) {
                alert(e.which);
                if (e.which === 17)
                    isCtrl = true;
                if (e.which === 85 && isCtrl === true) {
                    //run code for CTRL+U -- ie, whatever!
//        alert('CTRL + U stuff');
                    return false;
                }
                //I
                if (e.which === 73 && isCtrl === true) {
                    return false;
                }//J
                if (e.which === 74 && isCtrl === true) {
                    return false;
                }
                //F12
                if ((e.which === 123) === true) {
                    return false;
                }
                if ((e.which === 116) === true) {
                    return false;
                }
            }
        </script> 
        <style tye="text/css">
            .top-bar {
                background: #124779;
            }
            .fonts-icon {
                margin-top: 0px;
            }
            .fonts-icon a {
                border-radius: 5px;
                color: #fff;
                padding: 4px 8px;
                font-size: 12px;
            }
            .footer_link a {
                color: #fff;
                text-decoration: none;
                margin-left: 10px;
            }
        </style> 
    </head>
    <body class="home" oncontextmenu="return false;">
        <div style="background: #f6bb19; overflow: hidden; height: 30px;">

            <div class="col-xs-12">
                <ul style="padding:0px;">

                    <div class="fonts-icon" style=" padding: 5px;">
                        <a href="#" id="btn-decrease">A-</a>
                        <a href="#" id="btn-orig">A</a>
                        <a href="#" id="btn-increase">A+</a>
                        <a href="#" class="color" style="color:#fff; background:#000">A</a>
                        <a href="#" class="nocolor" style="color:#000; background:#fff">A</a>
                        <a href="#navbar-new">Skip to Header</a>

                    </div>
                    <p style="text-align: right;" id ="CDACGISTWebLocalisationDIV"></p>
                    <select id="cdacL10nLanguagesComboBox" class="NALOC" style="padding:0 0 0 !important; margin-top: -40px;float:right; width:inherit !important; height:inherit !important; min-height: 25px !important; min-width: 26px !important; border-radius: 2px !important; font-family:Segoe UI, Tahoma, Geneva, Verdana, sans-serif !important;" onchange="fetchOnlyComboBoxSelectedLang()">
                        <option class="langMenu NALOC" value="Translate" code="" selected="true" style="display:none;"> Select Language </option>
                        <option class="langMenu NALOC" value="English" code="eng"> English </option>
                        <option class="langMenu NALOC" value="Telugu" code="tel">Telugu</option>
                    </select>
                </ul>
            </div>  
        </div>
        <html:form action="/Welcome" method="post" >
            <html:hidden property="mode"/>  
            <html:hidden property="userName"/>  
            <html:hidden property="password"/>  
            <!-- =======Header ======= -->
            <header>
                <div class="container-fluid top_header"> 
                    <!-- end container --> 
                </div>
                <!-- end top_header -->
                <div class="bottom_header top-bar-gradient">
                    <div class="container clear_fix">
                        <div class="row hidden-xs">
                            <div class="">
                                <div class="col-xs-6 col-sm-2 logo"> <a href="index.html"> <img src="img/RMSA-Logo.png" style="width:90px" class="pull-left" alt=""> </a> </div>
                                <div class="col-xs-12 col-sm-8 text-center">
                                    <h2 class="logo-txt">Rashtriya Madhyamik Shiksha Abhiyan</h2>
                                    <h2 class="logo-sm">( Department of School Education , Government of Andhra Pradesh )</h2>
                                </div>
                                <div class="col-xs-6 col-sm-2 logo"> <a href="index.html"> <img src="img/Andhra-Pradesh-AP-Govt-Logo.png" style="width:80px" class="pull-right" alt=""> </a> </div>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="">
                                <div class="col-xs-6 logo"> <a href="index.html"> <img src="img/RMSA-Logo.png" style="width:90px" class="pull-left" alt=""> </a> </div>
                                <div class="col-xs-6 logo"> <a href="index.html"> <img src="img/Andhra-Pradesh-AP-Govt-Logo.png" style="width:80px" class="pull-right" alt=""> </a> </div>
                                <div class="col-xs-12 text-center">
                                    <h2 class="logo-txt">Rashtriya Madhyamik Shiksha Abhiyan</h2>
                                    <h2 class="logo-sm">( Department of School Education , Government of Andhra Pradesh )</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end container --> 
                </div>
                <!-- end bottom_header --> 
            </header>
            <!-- end header --> 
            <!-- ======= /Header ======= --> 

            <!-- ======= mainmenu-area section ======= -->
            <section class="mainmenu-area stricky">
                <div class="container">
                    <nav class="clearfix"> 
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header clearfix">
                            <button type="button" class="navbar-toggle collapsed"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-th fa-2x"></span> </button>
                        </div>
                        <div class="nav_main_list custom-scroll-bar pull-left" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav" id="navbar-new">
                                <li><a href="<%=basePath%>">Home</a></li>

                                <li class="arrow_down"><a href="#">About Us</a>
                                    <div class="sub-menu">
                                        <ul>
                                            <li id="hover"><a>RMSA</a>

                                                <ul id="box" style="display: none; margin-left:200px; width:330px; margin-top:-45px;    position: absolute;">
                                                    <li><a href="#">STRENGTHENING OF EXISTING SCHOOLS</a></li>
                                                    <li><a href="#">ANNUAL GRANDS</a></li>
                                                    <li><a href="#">COMMUNICATION MOBILIZATION</a></li>
                                                    <li><a href="#">TEACHERS TRAINING</a></li>
                                                    <!--                                                    <li><a href="#" class="bt btleft">Highlight it</a> </li>
                                                                                                        <li><a href="#" class="bt btright">Reset</a> </li>-->
                                                </ul>

                                            </li>
                                            <li><a href="#">IEDSS</a></li>
                                            <li><a href="#">VOCATIONAL EDUCATION</a></li>
                                            <li><a href="#">ICT</a></li>
                                            <li><a href="#">GIRLS HOSTELS</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li><a href="#">PIB</a></li>
                                <li class="arrow_down"><a href="#">CIRCULARS & GO'S</a>
                                    <div class="sub-menu">
                                        <ul>
                                            <li><a href="#">GOV OF AP</a></li>
                                            <li><a href="#">GOV OF INDIA</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="arrow_down"><a href="#">REPORTS</a>
                                    <div class="sub-menu">
                                        <ul>
                                            <li><a href="#">FACTS & FIGURES</a></li>
                                            <li><a href="#">ANNUAL & AUDIT REPORT</a></li>
                                            <li><a href="#">PUBLICATIONS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="./rtsView.do">RTI ACT</a></li>
                                <li><a href="#">CONTACT US</a></li>
                            </ul>
                        </div>
                        <!--                        <div class="find-advisor pull-right">
                                                    <a href="#" class="advisor " data-toggle="modal"
                                                       data-target="#myModal" id="loginClick">Login</a>
                                                </div>-->
                    </nav>
                    <!-- End Nav --> 
                </div>
                <!-- End Container --> 
            </section>

            <section class="rev_slider_wrapper me-fin-banner">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    </ol>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
            </section><br><br><br><br><br><br>
            <div style="min-height: 400px;">  
                <tiles:insert attribute="content" name="content"/>
            </div>                   
            <br><br><br><br>

            <!-- End consultation --> 
            <!-- ============ /free consultation ================ --> 

            <!-- ============= Footer ================ -->
            <footer>
                <div class="top_footer container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 part2">
                                <h5>Important Links</h5>
                                <ul class="p0">
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Circulars</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Case Study</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Notifications</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Research</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;FAQ's</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 part2">
                                <h5>Other Links</h5>
                                <ul class="p0">
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Terms and Conditions</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Privacy Policy</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Help</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Site Map</a></li>
                                    <li><a href=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Copyright Policy</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 part2">
                                <h5>Contact Us</h5>
                                <ul class="p0">
                                    <li><a>Rashtriya Madhyamik Shiksha Abhiyan,</a></li>
                                    <li> <a> Shri. R.P. Sisodia, </a></li>
                                    <li> <a> Secretariat Building, </a></li>
                                    <li> <a> Hyderabad-500004</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 part2 ">
                                <h5>Feedback Form</h5>
                                <br>
                                <form action="">
                                    <input class="form-control form-controlnew" type="text" name="name" placeholder="Your Name">
                                    <input class="form-control form-controlnew" type="email" name="email" placeholder="Your Email">
                                    <textarea class="form-control form-controlnew" name="message" placeholder="Message"></textarea>
                                    <button type="submit" class="submit new-submit">submit now <i class="fa fa-arrow-circle-right"></i></button>
                                </form>
                            </div>
                        </div>
                        <!-- End row -->
                    </div>
                </div>
                <!-- End top_footer -->
                <div class="bottom_footer container-fluid">
                    <div class="container">
                        <p class="float_left">Copyright &copy; RMSA. All rights reserved. </p>
                        <p class="float_right">Designed &amp; Developed by <img src="img/APO.png" class="apo"></p>
                    </div>
                </div>
                <!-- End bottom_footer -->
            </footer>
            <!-- ============= /Footer =============== --> 

            <!-- Js File --> 
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Login</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-xs-12 col-sm-12">
                                <label class="formlabel">UserName </label> <input type="text" class="form-control textbox" placeholder="UserName" name="userName" id="userName" />
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <label class="formlabel">Password</label> <input type="password" class="form-control textbox" placeholder="Password" name="passWord" id="passWord" />
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <label class="formlabel">Enter Captcha </label> 
                                <input name="captchaCode2" id="captchaCode2" style="color: #f00;font-weight: bold;font-size: 15px;border-width: 0px;" type="hidden"/>
                                <input name="captchaCode" id="captchaCode" style="color: #f00;font-weight: bold;font-size: 15px;border-width: 0px;background-color: white;" readonly disabled/>
                                <input type="text" class="form-control textbox" placeholder="Capture Code" name="captchaCode1" id="captchaCode1" onkeypress='return onlyNumbers(event);'/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" id="subForm" >Submit</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- j Query --> 

            <script type="text/javascript" src="js/validate.js"></script> 
            <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
            <script type="text/javascript" src="js/jquery.bxslider.min.js"></script> 
            <script src="js/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools --> 
            <script src="js/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider --> 
            <script type="text/javascript" src="js/revolution.extension.actions.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.carousel.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.kenburn.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.layeranimation.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.migration.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.navigation.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.parallax.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.slideanims.min.js"></script> 
            <script type="text/javascript" src="js/revolution.extension.video.min.js"></script> 

            <!-- Bootstrap JS --> 
            <script type="text/javascript" src="js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="js/jquery.appear.js"></script> 
            <script type="text/javascript" src="js/jquery.countTo.js"></script> 
            <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script> 

            <!-- owl-carousel --> 
            <script type="text/javascript" src="js/owl.carousel.js"></script> 
            <script src="js/owl-custom.js"></script> 
            <!-- Custom & Vendor js --> 
            <script type="text/javascript" src="js/custom.js"></script>
            <script type="text/javascript">
            var $affectedElements = $("*");
            $affectedElements.each(function() {
                var $this = $(this);
                $this.data("orig-size", $this.css("font-size"));
            });

            $("#btn-increase").click(function() {
                $("p, a, li, td, th, span, label").css("font-size", "15px");
                $("h1, h2, h3, h4, h5, h6").css("font-size", "17px");
            })

            $("#btn-decrease").click(function() {
                $("p, a, li, td, th, span, label").css("font-size", "12px");
                $("h1, h2, h3, h4, h5, h6").css("font-size", "15px");
            });

            $("#btn-orig").click(function() {
                $affectedElements.each(function() {
                    var $this = $(this);
                    $this.css("font-size", $this.data("orig-size"));
                });
            });

            function changeFontSize(direction) {
                $affectedElements.each(function() {
                    var $this = $(this);
                    $this.css("font-size", parseInt($this.css("font-size")) + direction);
                });
            }

            $(".color").click(function() {
                $("body").css({"-moz-filter": "grayscale(100%)", "-webkit-filter": "grayscale(100%)", "filter": "gray", "filter": "grayscale(100%)"});
                $("body").css("background", "#000");
            });


            $(".nocolor").click(function() {
                $("body").css({"-moz-filter": "grayscale(0%)", "-webkit-filter": "grayscale(0%)", "filter": "white", "filter": "grayscale(0%)"});
                $("body").css("background", "url(../images/bg-ts.jpg)");
            });


            </script>
            <script type="text/javascript">
                $("#hover").hover(function() {
                    $("#box").slideDown();

                }, function() {
                    $("#box").slideUp();
                });


            </script>
        </html:form>
    </body>
</html>
